import { ShareGoalForm } from "../forms/share-goal-form.js";

export class TreatmentGoalsArticle {
  constructor(parameters) {
    var t = this;

    // Setup form
    t.shareGoalForm = new ShareGoalForm(parameters);
  }
}
