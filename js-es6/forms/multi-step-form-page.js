﻿import { Form } from "../components/form.js";

export class MultiStepFormPage extends Form {
    constructor(parameters, container, callbacks) {
        super(parameters, container);

        const t = this;
        
        t.container = container;
        t.callbacks = callbacks == null ? {} : callbacks;

        // Setup form submit
        container.addEventListener("submit", function (event) {
            event.preventDefault();

            // Call the derived class's submit handler
            t.handleSubmit(event);
        });
    }

    handleSubmit() {
        throw `"handleSubmit" not implemented`;
    }
}
