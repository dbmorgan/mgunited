import { State } from "../components/state.js";
import { MultiStepForm } from "./multi-step-form.js";
import { ShareStoryFormPage } from "./multi-step-form-pages/share-story-form-page.js";
import { SignUpFormPage } from "./multi-step-form-pages/sign-up-form-page.js";
import { SurveyFormPage } from "./multi-step-form-pages/survey-form-page.js";

export class RealStoriesForm extends MultiStepForm {
	constructor(parameters) {
        super(
            parameters,
            document.getElementById("form-container"));

        // Stop if this script was placed on a page without a form
        if (this.container == null)
            return;

        const t = this,

            navNextCallback = function (event) {
                t.navigate(t.currentPageIndex + 1, false, event);
            };

        // Loop through the child elements of our container and create form pages
        for (let i = 0; i < t.container.childNodes.length; i++) {
            let element = t.container.childNodes[i];
            if (element.nodeType != 1)
                continue;

            // Determine what type of form page this is based on the element's id
            switch (element.id) {
            	case ShareStoryFormPage.domId:
            		{
                        let page = new ShareStoryFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: function (event, result) {
                                	let pagesToSkip = 0;

                                	// Skip the sign up form if the user has already
                                	// signed up
                                	if (State.signUpCompleted === true) {
                                		pagesToSkip++;

	                                	// Skip the survey form if the user has already
	                                	// completed it
	                                	if (State.surveyCompleted === true)
                                			pagesToSkip++;
                                	}

                                	while (pagesToSkip > 0) {
                                        t.pages.splice(t.currentPageIndex + 1, 1);
                                        pagesToSkip--;
                                	}

                                    navNextCallback(event);
                                }
                            });

                        t.pages.push(page);
            		}
            		break;

                case SignUpFormPage.domId:
                    {
                        let page = new SignUpFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: function (event, result) {
                                	// Skip the survey form if the user has already
                                	// completed it
                                    if (State.surveyCompleted === true)
                                        t.pages.splice(t.currentPageIndex + 1, 1);

                                    navNextCallback(event);
                                }
                            });

                        t.pages.push(page);
                    }
                    break;

                case SurveyFormPage.domId:
                    {
                        let page = new SurveyFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: navNextCallback
                            });

                        t.pages.push(page);
                    }
                    break;

                default:
                    // Add a generic object that allows us to navigate to this element
                    t.pages.push({
                        container: element
                    });
                    break;
            }
        }

         //// Testing
         //setTimeout(
         //    function () {
         //        t.navigate(1, true);
         //    },
         //    0);
    }
}
