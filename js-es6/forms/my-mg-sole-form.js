import { State } from "../components/state.js";
import { MultiStepForm } from "./multi-step-form.js";
import { SignUpFormPage } from "./multi-step-form-pages/sign-up-form-page.js";
import { SurveyFormPage } from "./multi-step-form-pages/survey-form-page.js";

export class MyMGSoleForm extends MultiStepForm {
    constructor(parameters) {
        super(
            parameters,
            document.getElementById("form-container"));

        // Stop if this script was placed on a page without a form
        if (this.container == null)
            return;

        const t = this,

            navNextCallback = function (event) {
                t.navigate(t.currentPageIndex + 1, false, event);
            };

        // Loop through the child elements of our container and create form pages
        for (let i = 0; i < t.container.childNodes.length; i++) {
            let element = t.container.childNodes[i];
            if (element.nodeType != 1)
                continue;

            // Determine what type of form page this is based on the element's id
            switch (element.id) {
                case SignUpFormPage.domId:
                    {
                        let page = new SignUpFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: function (event, result) {
                                	// Skip the survey form if the user has already
                                	// completed it
                                    if (State.surveyCompleted === true)
                                        t.pages.splice(t.currentPageIndex + 1, 1);

                                    navNextCallback(event);
                                }
                            },

                            // This sign up form uses a different API method than the default
                            "MyMGSoleSignUp");

                        t.pages.push(page);
                    }
                    break;

                case SurveyFormPage.domId:
                    {
                        let page = new SurveyFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: navNextCallback
                            });

                        t.pages.push(page);
                    }
                    break;

                default:
                    // Add a generic object that allows us to navigate to this element
                    t.pages.push({
                        container: element
                    });
                    break;
            }
        }

        // Auto-fill form if the user has already signed up previously
        if (State.firstName) {
			let element = document.querySelector("input#sign-up-first-name");
			if (element)
				element.value = State.firstName;
		}

		if (State.lastName) {
			let element = document.querySelector("input#sign-up-last-name");
			if (element)
				element.value = State.lastName;
		}

		if (State.email) {
			let element = document.querySelector("input#sign-up-email");
			if (element)
				element.value = State.email;
		}

        //// Testing
        //setTimeout(
        //    function () {
        //        t.navigate(2, true);
        //    },
        //    0);
    }
}
