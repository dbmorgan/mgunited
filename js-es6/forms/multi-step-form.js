﻿import { Utilities } from "../utilities/utilities.js";
import { WindowMetrics } from "../utilities/window-metrics.js";

const
    settings = {
        navigationDuration: 0.5,
        scrollPadding: 32
    };

export class MultiStepForm {
    constructor(parameters, container) {
        const t = this;

        t.container = container;
        t.pages = [];
        t.currentPageIndex = 0;
    }

    navigate(toFormIndex, immediate = false, event = null, resizeCallback = null) {
        const t = this;

        // Stop if the index is invalid or we're already on the specified page
        if (toFormIndex < 0)
            return;
        if (toFormIndex >= t.pages.length)
            return;
        if (toFormIndex == t.currentPageIndex)
            return;
        
        let fromForm = t.pages[t.currentPageIndex],
            toForm = t.pages[toFormIndex],
            containerBounds = t.container.getBoundingClientRect(),
            header = document.querySelector("header.sticky"),
            headerHeight = header ? header.clientHeight : 0,
            scrollTargetPosition = headerHeight;

        // On desktop, leave a little whitespace above the form
        if (!WindowMetrics.isMobile())
            scrollTargetPosition += settings.scrollPadding;

        if (immediate) {
            // Transition immediately
            if (containerBounds.top < scrollTargetPosition)
                window.scrollTo(0, containerBounds.top + window.pageYOffset - scrollTargetPosition);

            Utilities.resetStyles(t.container);

            fromForm.container.style.display = "none";
            toForm.container.style.display = "block";

            if (resizeCallback)
                resizeCallback();
        } else {
            // Animate transition
            const callback = function () {
                if (t.timeline)
                    t.timeline.progress(1);

                t.container.style.height = `${t.container.clientHeight}px`;

                fromForm.container.style.position = "absolute";

                toForm.container.style.position = "absolute";
                toForm.container.style.display = "block"
                toForm.container.style.opacity = 0;

                t.timeline = new TimelineMax();

                t.timeline.to(
                    t.container,
                    settings.navigationDuration,
                    {
                        ease: Power1.easeInOut,
                        height: toForm.container.clientHeight,
                        onUpdate: resizeCallback
                    },
                    0);

                t.timeline.fromTo(
                    fromForm.container,
                    settings.navigationDuration,
                    {
                        opacity: 1
                    },
                    {
                        ease: Power1.easeInOut,
                        opacity: 0
                    },
                    0);

                t.timeline.fromTo(
                    toForm.container,
                    settings.navigationDuration,
                    {
                        opacity: 0
                    },
                    {
                        ease: Power1.easeInOut,
                        opacity: 1
                    },
                    0);

                t.timeline.add(function () {
                    Utilities.resetStyles(t.container);

                    fromForm.container.style.display = "none";
                    toForm.container.style.position = "";
                });
            };

            // Scroll if necessary
            if (containerBounds.top < scrollTargetPosition) {
                Utilities.scrollWindowToPosition(
                    containerBounds.top + window.pageYOffset - scrollTargetPosition,
                    callback);
            } else {
                callback();
            }
        }

        // Update state
        t.currentPageIndex = toFormIndex;
    }
}
