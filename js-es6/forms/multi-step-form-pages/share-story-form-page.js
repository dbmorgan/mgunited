import { State } from "../../components/state.js";
import { SignUpApi } from "../../utilities/signup-api.js";
import { MultiStepFormPage } from "../multi-step-form-page.js";

const domId = "form-step-share-story";
let awaitingApiResponse = false;

export class ShareStoryFormPage extends MultiStepFormPage {
    constructor(parameters, container, callbacks) {
        super(parameters, container, callbacks);

        const t = this;

        // Get references to DOM elements
        t.inputs = {
            story: document.getElementById("share-story"),
            optin: [
                document.getElementById("share-story-optin-yes"),
                document.getElementById("share-story-optin-no")
            ]
        };

        // Setup textarea limits
        t.setupTextareaLimit(t.inputs.story, 5000);

        // Setup input validators
        t.setupValidator(t.inputs.story);
        t.setupValidator(t.inputs.optin);
    }

    static get domId() {
        return domId;
    }

    handleErrors(errors) {
        const t = this;
        let inputs = [],
            messages = [];

        for (let i = 0; i < errors.length; i++) {
            let errorCode = errors[i];
            switch (errorCode) {
                case SignUpApi.signUpErrorCodes.storyRequired:
                    inputs.push(t.inputs.story);
                    messages.push("Please tell us your story.");
                    break;

                case SignUpApi.signUpErrorCodes.storyOptInRequired:
                    inputs.push(t.inputs.optin);
                    messages.push("Please select &#x201c;Yes&#x201d; or &#x201c;No.&#x201d;");
                    break;

                default:
                    SignUpApi.showUnknownError();
                    break;
            }
        }

        if (inputs.length > 0)
            t.showFieldErrors(inputs, messages);
    }

    handleSubmit(event) {
        const t = this;

        // Stop if we're already waiting for an API response
        if (awaitingApiResponse)
            return;

        awaitingApiResponse = true;

        // Call the API
        SignUpApi.callApi(
            "CreateStory",
            {
                story: t.inputs.story.value,
                sharingOptIn: t.getValueForRadioGroup(t.inputs.optin)
            },
            function (result) {
                // Clear our waiting flag
                awaitingApiResponse = false;

                if (result.errors) {
                    // Clear any existing errors
                    //t.clearFieldErrors();

                    // Show the errors
                    t.handleErrors(result.errors);
                    return;
                }

                if (result.code == SignUpApi.basicResponseCodes.ok) {
                    // Perform callback, if provided
                    if (t.callbacks.onSubmit)
                        t.callbacks.onSubmit.call(t, event, result);
                } else {
                    SignUpApi.showUnknownError(result);
                }
            });
    }
}
