import { State } from "../../components/state.js";
import { SignUpApi } from "../../utilities/signup-api.js";
import { MultiStepFormPage } from "../multi-step-form-page.js";

const domId = "form-step-stay-involved";
let awaitingApiResponse = false;

export class StayInvolvedFormPage extends MultiStepFormPage {
    constructor(parameters, container, callbacks) {
        super(parameters, container, callbacks);

        const t = this;

        // Get references to DOM elements
        t.inputs = {
            stayInvolvedOptions: t.createArrayForNodeList(document.querySelectorAll(`input[name="sign-up-stay-involved"]`)),
            story: document.getElementById("share-story")
        };

        // Setup textarea limits
        t.setupTextareaLimit(t.inputs.story, 5000);

        // Setup input validators
        t.setupValidator(t.inputs.story);

        // EDIT: this isn't working on some Android devices; we'll just rely on the form submit
        // event to present validation for the checkboxes
        //let checkboxValidationTimeout = null;
        //const stayInvolvedOptionsValidateCallback = function (event) {
        //    if (checkboxValidationTimeout) {
        //        clearTimeout(checkboxValidationTimeout);
        //        checkboxValidationTimeout = null;
        //    }

        //    checkboxValidationTimeout = setTimeout(
        //        function () {
        //            SignUpApi.callApi(
        //                "ValidateStayInvolvedOptions",
        //                {
        //                    stayInvolvedOptions: t.getValuesForCheckboxGroup(t.inputs.stayInvolvedOptions),
        //                    story: t.inputs.story.value
        //                },
        //                function (result) {
        //                    console.log(result);
        //                    if (result.errors) {
        //                        // Show the error
        //                        t.handleErrors(result.errors);
        //                    } else {
        //                        // Clear the error
        //                        t.clearFieldError(t.inputs.stayInvolvedOptions[0]);
        //                    }
        //                });
        //        },
        //        200);
        //};

        const stayInvolvedOptionsValidateCallback = function (event) {
            let selections = t.getValuesForCheckboxGroup(t.inputs.stayInvolvedOptions);
            if (selections != null)
                t.clearFieldError(t.inputs.stayInvolvedOptions[0]);
        };

        for (let i = 0; i < t.inputs.stayInvolvedOptions.length; i++)
            t.inputs.stayInvolvedOptions[i].addEventListener("change", stayInvolvedOptionsValidateCallback);

        // Set up share story checkbox
        let timeline = null;
        const shareCheckbox = t.inputs.stayInvolvedOptions[t.inputs.stayInvolvedOptions.length - 1],
            storyContainer = document.getElementById("form-step-stay-involved-story"),

            shareCheckboxChangeCallback = function () {
                // Stop any ongoing animation
                if (timeline)
                    timeline.progress(1);

                if (shareCheckbox.checked) {
                    // Show the story inputs
                    storyContainer.style.display = "block";

                    let height = storyContainer.clientHeight;
                    storyContainer.style.height = "0px";

                    timeline = new TimelineMax();
                    timeline.to(
                        storyContainer,
                        1,
                        {
                            height: height,
                            ease: Power2.easeOut,
                            onComplete: function () {
                                timeline = null;
                            }
                        });

                    timeline.add(function () {
                        timeline = null;
                        storyContainer.style.height = "";
                    });
                } else {
                    // Hide the story inputs
                    let height = storyContainer.clientHeight;

                    timeline = new TimelineMax();
                    timeline.to(
                        storyContainer,
                        1,
                        {
                            height: 0,
                            ease: Power2.easeOut,
                            onComplete: function () {
                                timeline = null;
                            }
                        });

                    timeline.add(function () {
                        timeline = null;
                        storyContainer.removeAttribute("style");
                    });
                }
            };

        shareCheckbox.addEventListener("change", shareCheckboxChangeCallback);
        shareCheckboxChangeCallback();

        window.addEventListener("resize", function () {
            // Stop any ongoing animation
            if (timeline)
                timeline.progress(1);
        });
    }

    static get domId() {
        return domId;
    }

    handleErrors(errors) {
        const t = this;
        let inputs = [],
            messages = [];

        for (let i = 0; i < errors.length; i++) {
            let errorCode = errors[i];
            switch (errorCode) {
                case SignUpApi.signUpErrorCodes.stayInvolvedOptionRequired:
                    inputs.push(t.inputs.stayInvolvedOptions);
                    messages.push("Please select at least one option.");
                    break;

                case SignUpApi.signUpErrorCodes.storyRequired:
                    inputs.push(t.inputs.story);
                    messages.push("Please tell us your story.");
                    break;

                default:
                    SignUpApi.showUnknownError();
                    break;
            }
        }

        if (inputs.length > 0)
            t.showFieldErrors(inputs, messages);
    }

    handleSubmit(event) {
        const t = this;

        // Stop if we're already waiting for an API response
        if (awaitingApiResponse)
            return;

        awaitingApiResponse = true;

        // Call the API
        SignUpApi.callApi(
            "CreateStayInvolvedOptions",
            {
                stayInvolvedOptionIds: t.getValuesForCheckboxGroup(t.inputs.stayInvolvedOptions),
                story: t.inputs.story.value
            },
            function (result) {
                // Clear our waiting flag
                awaitingApiResponse = false;

                if (result.errors) {
                    // Clear any existing errors
                    //t.clearFieldErrors();

                    // Show the errors
                    t.handleErrors(result.errors);
                    return;
                }

                if (result.code == SignUpApi.basicResponseCodes.ok) {
                    // Perform callback, if provided
                    if (t.callbacks.onSubmit)
                        t.callbacks.onSubmit.call(t, event, result);
                } else {
                    SignUpApi.showUnknownError(result);
                }
            });
    }
}
