import { State } from "../../components/state.js";
import { SignUpApi } from "../../utilities/signup-api.js";
import { MultiStepFormPage } from "../multi-step-form-page.js";

const domId = "form-step-sign-up";
let awaitingApiResponse = false;

export class SignUpFormPage extends MultiStepFormPage {
    constructor(parameters, container, callbacks, signUpApiMethod = "SignUp") {
        super(parameters, container, callbacks);

        const t = this;

        // Sign up API method - the documentary RSVP & My MG Sole sign up pages
        // will provide different values for this
        t.signUpApiMethod = signUpApiMethod;

        // Get references to DOM elements
        t.inputs = {
            firstName: document.getElementById("sign-up-first-name"),
            lastName: document.getElementById("sign-up-last-name"),
            email: document.getElementById("sign-up-email"),
            optin: document.getElementById("sign-up-optin")
        };

        // Setup input validators
        t.setupValidator(t.inputs.firstName);
        t.setupValidator(t.inputs.lastName);
        t.setupValidator(t.inputs.email);
        t.setupValidator(t.inputs.optin);
    }

    static get domId() {
        return domId;
    }

    handleErrors(errors) {
        const t = this;
        let inputs = [],
            messages = [];

        for (let i = 0; i < errors.length; i++) {
            let errorCode = errors[i];
            switch (errorCode) {
                case SignUpApi.signUpErrorCodes.firstNameRequired:
                    inputs.push(t.inputs.firstName);
                    messages.push("Please provide your first name.");
                    break;

                case SignUpApi.signUpErrorCodes.firstNameFormat:
                    inputs.push(t.inputs.firstName);
                    messages.push("Please provide a valid first name.");
                    break;

                case SignUpApi.signUpErrorCodes.lastNameRequired:
                    inputs.push(t.inputs.lastName);
                    messages.push("Please provide your last name.");
                    break;

                case SignUpApi.signUpErrorCodes.lastNameFormat:
                    inputs.push(t.inputs.lastName);
                    messages.push("Please provide a valid last name.");
                    break;

                case SignUpApi.signUpErrorCodes.emailRequired:
                    inputs.push(t.inputs.email);
                    messages.push("Please provide your email address.");
                    break;

                case SignUpApi.signUpErrorCodes.emailFormat:
                    inputs.push(t.inputs.email);
                    messages.push("Please provide a valid email address.");
                    break;

                case SignUpApi.signUpErrorCodes.duplicateEmail:
                    inputs.push(t.inputs.email);
                    messages.push("Email is already in use");
                    break;

                case SignUpApi.signUpErrorCodes.signUpOptInRequired:
                    inputs.push(t.inputs.optin);
                    messages.push("Please check box to agree.");
                    break;

                default:
                    SignUpApi.showUnknownError();
                    break;
            }
        }

        if (inputs.length > 0)
            t.showFieldErrors(inputs, messages);
    }

    handleSubmit(event) {
        const t = this;

        // Stop if we're already waiting for an API response
        if (awaitingApiResponse)
            return;

        awaitingApiResponse = true;

        // Call the API - note that this supports different API actions / endpoints.
        // this was added to support the alternate version of the SignUp method on
        // the documentary page for recording RSVPs
        SignUpApi.callApiForAction(
            t.apiAction,
            t.signUpApiMethod,
            {
                firstName: t.inputs.firstName.value,
                lastName: t.inputs.lastName.value,
                email: t.inputs.email.value,
                optin: t.inputs.optin.checked
            },
            function (result) {
                // Clear our waiting flag
                awaitingApiResponse = false;

                if (result.errors) {
                    // Clear any existing errors
                    //t.clearFieldErrors();

                    // Show the errors
                    t.handleErrors(result.errors);
                    return;
                }

                if (result.code == SignUpApi.basicResponseCodes.ok) {
                    // GA tracking
                    let gtmEventCallback = function () {
                        // Store information returned by the API in session
                        State.signUpId = result.signUpId;
                        State.guid = result.guid;
                        State.firstName = result.firstName;
                        State.lastName = result.lastName;
                        State.email = result.email;
                        State.signUpCompleted = true;
                        State.surveyCompleted = result.surveyCompleted;

                        // Update any personalization elements on the page
                        State.updateAllPersonalizedContent();

                        // Perform callback, if provided
                        if (t.callbacks.onSubmit)
                            t.callbacks.onSubmit.call(t, event, result);
                    };

                    if (typeof ga === 'function') {
                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                            event: "data-completion",
                            "data-completion": "completed",
                            "signUpID": result.signUpId,
                            "eventCallback": function () {
                                // We don't do anything here because the user doesn't leave
                                // the page when the GTM call finishes
                            }
                        });

                        // Note that if we used this also for the "eventCallback", this would
                        // still fire and cause a second navigation step - to the thank you
                        // screen, in most cases
                        gtmEventCallback();
                    } else {
                        // GTM doesn't seem to be present
                        gtmEventCallback();
                    }
                } else {
                    SignUpApi.showUnknownError(result);
                }
            });
    }
}
