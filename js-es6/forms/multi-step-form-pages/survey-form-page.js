import { State } from "../../components/state.js";
import { SignUpApi } from "../../utilities/signup-api.js";
import { MultiStepFormPage } from "../multi-step-form-page.js";

const domId = "form-step-survey";
let awaitingApiResponse = false;

export class SurveyFormPage extends MultiStepFormPage {
    constructor(parameters, container, callbacks) {
        super(parameters, container, callbacks);

        const t = this;

        // Get references to DOM elements
        t.inputs = {
            profileType: t.createArrayForNodeList(document.querySelectorAll(`input[name="sign-up-type"]`)),
            gender: t.createArrayForNodeList(document.querySelectorAll(`input[name="sign-up-gender"]`)),
            otherGender: document.getElementById("sign-up-gender-other"),
            topicsOfInterest: t.createArrayForNodeList(document.querySelectorAll(`input[name="sign-up-topics-of-interest"]`))
            //otherTopics: document.getElementById("sign-up-other-topics")
        };

        // Setup textarea limits
        //t.setupTextareaLimit(t.inputs.otherTopics, 5000);

        // Setup other gender enable/disable
        const updateOtherGenderAvailability = function () {
            t.inputs.otherGender.disabled = !t.inputs.gender[2].checked;
        };

        for (let i = 0; i < t.inputs.gender.length; i++)
            t.inputs.gender[i].addEventListener("change", updateOtherGenderAvailability);

        updateOtherGenderAvailability();

        // Setup input validators
        t.setupValidator(t.inputs.profileType);

        // Gender & topics of interest have unique validation in that the
        // validation rules span combinations of inputs
        const genderValidateCallback = function (event) {
            SignUpApi.callApiForAction(
                t.apiAction,
                "ValidateGender",
                {
                    gender: t.getValueForRadioGroup(t.inputs.gender),
                    otherGender: t.inputs.otherGender.value
                },
                function (result) {
                    if (result.errors) {
                        // Show the error
                        t.handleErrors(result.errors);
                    } else {
                        // Clear the error
                        t.clearFieldError(t.inputs.otherGender);
                    }
                });
        };

        for (let i = 0; i < t.inputs.gender.length; i++)
            t.inputs.gender[i].addEventListener("blur", genderValidateCallback);

        t.inputs.otherGender.addEventListener("blur", genderValidateCallback);

        const topicsOfInterestValidateCallback = function (event) {
            SignUpApi.callApiForAction(
                t.apiAction,
                "ValidateTopicsOfInterest",
                {
                    topicsOfInterest: t.getValuesForCheckboxGroup(t.inputs.topicsOfInterest)
                    //otherTopicOfInterest: t.inputs.otherTopics.value
                },
                function (result) {
                    if (result.errors) {
                        // Show the error
                        t.handleErrors(result.errors);
                    } else {
                        // Clear the error
                        //t.clearFieldError(t.inputs.otherTopics);
                    }
                });
        };

        for (let i = 0; i < t.inputs.topicsOfInterest.length; i++)
            t.inputs.topicsOfInterest[i].addEventListener("blur", topicsOfInterestValidateCallback);

        //t.inputs.otherTopics.addEventListener("blur", topicsOfInterestValidateCallback);
    }

    static get domId() {
        return domId;
    }

    handleErrors(errors) {
        const t = this;
        let inputs = [],
            messages = [];

        for (let i = 0; i < errors.length; i++) {
            let errorCode = errors[i];
            switch (errorCode) {
                case SignUpApi.signUpErrorCodes.invalidProfileTypeId:
                    inputs.push(t.inputs.profileType);
                    messages.push("Select an option.");
                    break;

                // EDIT: Gender is optional according to the copy doc
                //case SignUpApi.signUpErrorCodes.genderRequired:
                //    inputs.push(t.inputs.otherGender);
                //    messages.push("Please enter your gender.");
                //    break;

                // EDIT: "Other topics of interest" was removed as part of the October 2020
                // updates; the topic of interest checkboxes are not required
                // case SignUpApi.signUpErrorCodes.topicsOfInterestRequired:
                //     inputs.push(t.inputs.otherTopics);
                //     messages.push("Please choose at least one topic of interest.");
                //     break;

                default:
                    SignUpApi.showUnknownError();
                    break;
            }
        }

        if (inputs.length > 0)
            t.showFieldErrors(inputs, messages);
    }

    handleSubmit(event) {
        const t = this;

        // Stop if we're already waiting for an API response
        if (awaitingApiResponse)
            return;

        awaitingApiResponse = true;

        // Call the API
        SignUpApi.callApiForAction(
            t.apiAction,
            "CompleteSurvey",
            {
                profileType: t.getValueForRadioGroup(t.inputs.profileType),
                gender: t.getValueForRadioGroup(t.inputs.gender),
                otherGender: t.inputs.otherGender.value,
                topicsOfInterest: t.getValuesForCheckboxGroup(t.inputs.topicsOfInterest)
                //otherTopicsOfInterest: t.inputs.otherTopics.value
            },
            function (result) {
                // Clear our waiting flag
                awaitingApiResponse = false;

                if (result.errors) {
                    // Clear any existing errors
                    t.clearFieldErrors();

                    // Show the errors
                    t.handleErrors(result.errors);
                    return;
                }

                if (result.code == SignUpApi.basicResponseCodes.ok) {
                    // Update state to reflect that the survey has been completed
                    State.surveyCompleted = true;

                    // Perform callback, if provided
                    if (t.callbacks.onSubmit)
                        t.callbacks.onSubmit.call(t, event, result);
                } else {
                    SignUpApi.showUnknownError(result);
                }
            });
    }
}
