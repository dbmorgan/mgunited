import { State } from "../components/state.js";
import { MultiStepForm } from "./multi-step-form.js";
import { SignUpFormPage } from "./multi-step-form-pages/sign-up-form-page.js";
import { SurveyFormPage } from "./multi-step-form-pages/survey-form-page.js";

// This is the form on the sign up page; it always displays the sign up form
// but skips the survey form if it's already been completed
export class JoinNowForm extends MultiStepForm {
    constructor(parameters) {
        super(
            parameters,
            document.getElementById("form-container"));

        // Stop if this script was placed on a page without a form
        if (this.container == null)
            return;

        const t = this,

            navNextCallback = function (event) {
                t.navigate(t.currentPageIndex + 1, false, event);
            };

        // Loop through the child elements of our container and create form pages
        for (let i = 0; i < t.container.childNodes.length; i++) {
            let element = t.container.childNodes[i];
            if (element.nodeType != 1)
                continue;

            // Determine what type of form page this is based on the element's id
            switch (element.id) {
                case SignUpFormPage.domId:
                    {
                        let page = new SignUpFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: function (event, result) {
                                    // Check if the API returned a flag indicating that the user
                                    // has already completed the survey - this may be the case if
                                    // they just re-registered using the same email address. If
                                    // so, and the next step in the form flow is the survey page
                                    // due to us not knowing this when the page was initialized,
                                    // skip over that form to whatever follows it (assumed to be
                                    // the thank you page)
                                    if (State.surveyCompleted === true)
                                        t.pages.splice(t.currentPageIndex + 1, 1);

                                    navNextCallback(event);
                                }
                            });

                        t.pages.push(page);
                    }
                    break;

                case SurveyFormPage.domId:
                    {
                        let page = new SurveyFormPage(
                            parameters,
                            element,
                            {
                                onSubmit: navNextCallback
                            });

                        t.pages.push(page);
                    }
                    break;

                default:
                    // Add a generic object that allows us to navigate to this element
                    t.pages.push({
                        container: element
                    });
                    break;
            }
        }

        //// Testing
        //setTimeout(
        //    function () {
        //        t.navigate(2, true);
        //    },
        //    0);
    }
}
