import { Modal } from "./modal.js";

let
  template = `<section class="cl-modal-template"><h2>Attention</h2><p>You are now leaving MG-United.com.</p><div class="buttons"><a role="button" class="capsule primary" target="_blank">OK</a><a role="button" class="capsule tertiary" target="_blank">CANCEL</a></div></section>`;

export class LeavingSiteModal {
    constructor() {
    }

    static show(url, immediate, event) {
        let t = this;

        Modal.show(
            template,
            "leaving-site-modal",
            Modal.presentationTypes.scaleUp,
            Modal.presentationTypes.scaleDown,
            immediate === true,
            {
                preShow: function () {
                    let modal = this,
                        //mdl = document.querySelector('.cl-modal-template'),
                        buttons = document.querySelectorAll('.cl-modal-template .buttons > .capsule');

                    // OK button
                    buttons[0].addEventListener("click", function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        // Navigate & close the modal
                        window.open(url);

                        modal.close(
                            true,
                            Modal.presentationTypes.fade,
                            event);
                    });

                    // Cancel button
                    buttons[1].addEventListener("click", function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        // Close the modal
                        modal.close(
                            false,
                            Modal.presentationTypes.scaleDown,
                            event);
                    });
                }
            },
            event);
    }
}
