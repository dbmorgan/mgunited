import { SignUpApi } from "../utilities/signup-api.js";

const key = "mg-united-state";

// Try to load state from session storage
let state = localStorage.getItem(key),
	stateIsNew;

if (state) {
	// A preexisting state object was found
	stateIsNew = false;

	// Convert from base64
	state = atob(state);

	// Parse JSON string
	state = JSON.parse(state);

	// Because a state has been previously established, check to see if there
	// is an active session
	let session = sessionStorage.getItem(key);
	if (session == null) {
		// Attempt to establish a session via the API using the stored
		// user GUID
		SignUpApi.callApi(
			"ReturnVisit",
			{
				guid: state.guid
			},
			function (result) {
				if (result.code != SignUpApi.basicResponseCodes.ok)
					return;

				// A session has been established
				sessionStorage.setItem(key, "true");
			});
	}
} else {
	// If nothing was found in storage, start with an empty state object
	stateIsNew = true;
	state = {};
}

const saveState = function () {
	// Flag that the state has been stored
	stateIsNew = false;

	// Convert state object to a JSON string and base64 encode it
	let encodedState = JSON.stringify(state);
	encodedState = btoa(encodedState);

	// Store the encoded state object
	localStorage.setItem(key, encodedState);

	// Check if a session needs to be established
	let session = sessionStorage.getItem(key);
	if (session == null) {
		// A session has been established
		sessionStorage.setItem(key, "true");
	}
}

export class State {
	// guid
	static get guid() {
		return state.guid;
	}

	static set guid(value) {
		state.guid = value;
		saveState();
	}

	// firstName
	static get firstName() {
		return state.firstName;
	}

	static set firstName(value) {
		state.firstName = value;
		saveState();
	}

	// lastName
	static get lastName() {
		return state.lastName;
	}

	static set lastName(value) {
		state.lastName = value;
		saveState();
	}

	// email
	static get email() {
		return state.email;
	}

	static set email(value) {
		state.email = value;
		saveState();
	}

	// signUpCompleted
	static get signUpCompleted() {
		return state.signUpCompleted;
	}

	static set signUpCompleted(value) {
		state.signUpCompleted = value;
		saveState();
	}

	// surveyCompleted
	static get surveyCompleted() {
		return state.surveyCompleted;
	}

	static set surveyCompleted(value) {
		state.surveyCompleted = value;
		saveState();
	}

	// Regular methods
	static updateAllPersonalizedContent() {
		if (stateIsNew)
			return;

		// If there are any elements on the page with the class "sign-up-first-name"
		// "sign-up-last-name", or "sign-up-email", set their content to the stored values
		if (State.firstName) {
			let elements = document.querySelectorAll(".sign-up-first-name");
			for (let i = 0; i < elements.length; i++)
				elements[i].innerText = State.firstName;
		}

		if (State.lastName) {
			let elements = document.querySelectorAll(".sign-up-last-name");
			for (let i = 0; i < elements.length; i++)
				elements[i].innerText = State.lastName;
		}

		if (State.email) {
			let elements = document.querySelectorAll(".sign-up-email");
			for (let i = 0; i < elements.length; i++)
				elements[i].innerText = State.email;
		}
	}
}

// Set any personalized content on the page using the stored values
State.updateAllPersonalizedContent();
