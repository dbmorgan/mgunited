﻿import { Utilities } from "../utilities/utilities.js";
import { SignUpApi } from "../utilities/signup-api.js";

var
    settings = {
        // Animation for showing & hiding validation errors
        validationErrorShowHideDuration: 0.25,

        // Class names
        validationErrorClassName: "validation-error",
        validationMessageClassName: "error-message",

        // Extra space to figure into calculations when keeping fields in view
        scrollPadding: 16
    },

    findContainingElementForFormElement = function (element) {
        // Check if this element contains a child for displaying error messages
        if (element.querySelectorAll(".error-message").length > 0)
            return element;

        // Stop if we're at the root of the DOM
        if (element.parentNode == null)
            return null;

        // Recurse up the DOM tree
        return findContainingElementForFormElement(element.parentNode);
    },

    makeSureElementIsInView = function (element, callback) {
        let elementBounds = element.getBoundingClientRect(),
            header = document.querySelector("header.sticky"),
            headerHeight = header ? header.clientHeight : 0,
            scrollTargetPosition = settings.scrollPadding + headerHeight;

        // Check top of viewport, incorporating header height
        if (elementBounds.top < scrollTargetPosition) {
            Utilities.scrollWindowToPosition(
                window.pageYOffset + elementBounds.top - scrollTargetPosition,
                callback);

            return;
        }

        // Check bottom of viewport
        let viewportOverlap = elementBounds.top + elementBounds.height + settings.scrollPadding - window.innerHeight;
        if (viewportOverlap > 0) {
            Utilities.scrollWindowToPosition(
                window.pageYOffset + viewportOverlap,
                callback);

            return;
        }

        // Perform callback if specified & we didn't decide to scroll
        if (callback)
            callback();
    },

    presentValidationError = function (elementContainer) {
        // Don't do anything if we're already presenting an error
        if (elementContainer._presentingValidationError === true)
            return;

        // Stop any ongoing animation
        let timeline = elementContainer._timeline;
        if (timeline)
            timeline.progress(1);

        // Do nothing if it's already visible
        if (elementContainer.classList.contains(settings.validationErrorClassName))
            return;

        // Animate it appearing
        let errorMessage = elementContainer.querySelector(".error-message");
        if (errorMessage == null)
            return;

        elementContainer._presentingValidationError = true;

        // Add error class to display the element
        elementContainer.classList.add(settings.validationErrorClassName);

        let height = errorMessage.clientHeight;

        timeline = new TimelineMax();
        timeline.fromTo(
            errorMessage,
            settings.validationErrorShowHideDuration,
            {
                height: 0
            },
            {
                ease: Power1.easeOut,
                height: height
            });

        timeline.add(function () {
            elementContainer._timeline = null;
            elementContainer._presentingValidationError = false;
            Utilities.resetStyles(errorMessage);
        });

        elementContainer._timeline = timeline;
    },

    hideValidationError = function (elementContainer) {
        // Don't do anything if we're already presenting an error
        if (elementContainer._hidingValidationError === true)
            return;

        // Stop any ongoing animation
        let timeline = elementContainer._timeline;
        if (timeline)
            timeline.progress(1);

        // Do nothing if it's already hidden
        if (!elementContainer.classList.contains(settings.validationErrorClassName))
            return;

        // Animate it disappearing
        let errorMessage = elementContainer.querySelector(".error-message");
        if (errorMessage == null)
            return;

        elementContainer._hidingValidationError = true;

        let height = errorMessage.clientHeight;

        timeline = new TimelineMax();
        timeline.fromTo(
            errorMessage,
            settings.validationErrorShowHideDuration,
            {
                height: height
            },
            {
                ease: Power1.easeOut,
                height: 0
            });

        timeline.add(function () {
            elementContainer._timeline = null;
            elementContainer._hidingValidationError = false;
            Utilities.resetStyles(errorMessage);

            // Remove error class to hide the element
            elementContainer.classList.remove(settings.validationErrorClassName);
        });

        elementContainer._timeline = timeline;
    };

export class Form {
    constructor(parameters, container) {
        const t = this;

        t.form = container;

        // If parameters.apiAction is undefined, the default "signUpApi" action / endpoint will be used
        t.apiAction = parameters.apiAction;
    }

    createArrayForNodeList(nodeList) {
        let results = [];
        for (let i = 0; i < nodeList.length; i++)
            results.push(nodeList[i]);

        return results;
    }

    getValueForRadioGroup(radioGroupArray) {
        for (let i = 0; i < radioGroupArray.length; i++) {
            let input = radioGroupArray[i];
            if (input.checked)
                return input.value;
        }

        return null;
    }

    getValuesForCheckboxGroup(checkboxGroupArray) {
        let values = [];

        for (let i = 0; i < checkboxGroupArray.length; i++) {
            let input = checkboxGroupArray[i];
            if (input.checked)
                values.push(input.value);
        }

        return values.length > 0 ? values : null;
    }

    setupTextareaLimit(input, limit) {
        let callback = function () {
            if (input.value.length >= limit) {
                event.preventDefault();

                if (input.value.length > limit)
                    input.value = input.value.substr(0, limit);
            }
        };

        input.addEventListener("keypress", callback);
        input.addEventListener("change", callback);
    }

    setupValidator(input) {
        const t = this,

            performValidation = function (input, data) {
                SignUpApi.callApiForAction(
                    t.apiAction,
                    "Validate",
                    data,
                    function (result) {
                        if (result.errors) {
                            // Show the error
                            t.handleErrors(result.errors);
                        } else {
                            // Clear the error
                            t.clearFieldError(input);
                        }
                    });
            };

        // Setup API call on blur events
        if (Array.isArray(input)) {
            // This is an array of grouped radio buttons or checkboxes
            let inputs = input,
                isCheckbox = inputs[0].type.toLowerCase() == "checkbox";

            for (let i = 0; i < inputs.length; i++) {
                input = inputs[i];

                input.addEventListener("blur", function () {
                    let data = {
                        id: input.name,
                        value: null
                    };

                    if (isCheckbox)
                        data.value = t.getValuesForCheckboxGroup(inputs);
                    else
                        data.value = t.getValueForRadioGroup(inputs);

                    performValidation(input, data);
                });
            }
        } else {
            let isCheckbox = input.type.toLowerCase() == "checkbox";

            // This is a single input and not part of a group
            input.addEventListener("blur", function () {
                let data = {
                    id: input.id,
                    value: null
                };

                if (isCheckbox)
                    data.value = input.checked;
                else
                    data.value = input.value;

                performValidation(input, data);
            });
        }
    }

    clearFieldErrors() {
        var t = this;

        // Find all input containers on the page the have the "validation-error" class applied to them
        var elementContainers = t.form.querySelectorAll(`.${settings.validationErrorClassName}`);
        for (var i = 0; i < elementContainers.length; i++)
            hideValidationError(elementContainers[i]);
    }

    clearFieldError(input) {
        var elementContainer = findContainingElementForFormElement(input);

        if (elementContainer)
            hideValidationError(elementContainer);
    }

    showFieldError(input, message, scrollToField = true) {
        if (Array.isArray(input))
            input = input[0];

        let elementContainer = findContainingElementForFormElement(input);
        if (elementContainer == null)
            return null;

        let errorMessage = elementContainer.querySelector(`.${settings.validationMessageClassName}`);
        if (errorMessage)
            errorMessage.innerHTML = message;

        presentValidationError(elementContainer);

        // Scroll to error if necessary
        if (scrollToField)
            makeSureElementIsInView(elementContainer);

        // showFieldErrors() makes use of this
        return elementContainer;
    }

    // This shows multiple errors and scrolls to the topmost one
    showFieldErrors(inputs, messages, scrollToField = true) {
        const t = this;
        let topmostInput = null,
            topmostBounds = null;

        for (let i = 0; i < inputs.length; i++) {
            let input = inputs[i],
                elementContainer = t.showFieldError(input, messages[i], false);

            if (elementContainer) {
                if (i == 0) {
                    topmostInput = elementContainer;
                    topmostBounds = elementContainer.getBoundingClientRect();
                } else {
                    let testBounds = elementContainer.getBoundingClientRect();
                    if (testBounds.top < topmostBounds.top) {
                        topmostInput = elementContainer;
                        topmostBounds = testBounds;
                    }
                }
            }
        }

        // Scroll to error if necessary
        if (topmostInput) {
            if (scrollToField)
                makeSureElementIsInView(topmostInput);
        }
    }
}
