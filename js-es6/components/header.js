import { Utilities } from "../utilities/utilities.js";
import { WindowMetrics } from "../utilities/window-metrics.js";

let
    // Defines the sticky behavior of the header
    stickyModes = {
        // Header never becomes sticky
        off: 0,

        // Header is always sticky
        alwaysVisible: 1,

        // Header is sticky but appears and disappears
        // as the user scrolls the page upward and downward
        revealOnScroll: 2
    },

    // Defines menu behavior on mobile
    mobileMenuModes = {
        // Menus are not collapsable & always open
        alwaysOpen: 0,

        // Menus may be opened and closed, and the menu corresponding
        // to the current page will initially be presented in an open state
        collapsable: 1
    },

    // Defines menu behavior on desktop
    desktopMenuModes = {
        // Don't do anything with the menus via javascript -
        // it's assumed any transitions will be handled by CSS
        cssOnly: 0,

        // Animate menus opening vertically
        dropdown: 1
    },

    settings = {
        slowMoScale: 4,

        // Opening and closing the mobile menu via the hamburger
        openDuration: 0.5,
        closeDuration: 0.375,

        // Opening and closing submenus, both mobile (click) & desktop (hover)
        submenuOpenDuration: 0.375,
        submenuCloseDuration: 0.375,

        // Sticky show/hide duration, for the "revealOnScroll" mode
        stickyDuration: 0.375,

        // "revealOnScroll" -related settings
        scrollVelocity: {
            minimumScrollDistanceToTriggerSticky: 500,
            smoothing: 0.9,
            triggerSpeed: 1.2 // pixels per millisecond
        },

        // Class names
        selectedClassName: "selected",
        openClassName: "open"
    };

// Menus within the header navigation
class Menu {
    constructor(header, container, mobileMenuMode, desktopMenuMode) {
        let t = this;

        t.isOpen = false;
        t.header = header;
        t.mobileMenuMode = mobileMenuMode;
        t.desktopMenuMode = desktopMenuMode;

        t.container = container;

        // Check if this menu has a submenu
        t.submenuContainer = t.container.querySelector("ul");
        if (t.submenuContainer) {
            // This menu item contains a submenu
            let label = t.container.querySelector("span"),
                submenuItems = t.submenuContainer.querySelectorAll("li");

            // Setup mobile menu mode
            switch (mobileMenuMode) {
                case mobileMenuModes.collapsable:
                    // Setup open & close functionality. Note - don't use touch events
                    // here so that menus won't toggle if users touch them while attempting
                    // to scroll through the menu
                    label.addEventListener("click", function (event) {
                        let slowMo = event.shiftKey === true;

                        if (t.isOpen)
                            t.close(false, slowMo);
                        else
                            t.open(false, slowMo);
                    });
                    break;

                // mobileMenuModes.alwaysOpen
                default:
                    // Just open the submenu & don't add an open/close click event
                    t.submenuContainer.classList.add(settings.openClassName);
                    t.isOpen = true;
                    break;
            }
            
            // Setup desktop menu mode
            switch (desktopMenuMode) {
                case desktopMenuModes.dropdown:
                    // The menu slides open and closed vertically
                    {
                        let timeline = null;

                        t.container.addEventListener("mouseenter", function (event) {
                            // Stop if we're in mobile mode
                            if (WindowMetrics.isMobile())
                                return;

                            let slowMo = event.shiftKey === true;

                            // Stop any ongoing animation
                            if (timeline)
                                timeline.progress(1);

                            // Set initial conditions
                            Utilities.resetStyles(t.submenuContainer);

                            t.submenuContainer.style.display = "block";
                            t.submenuContainer.style.height = "auto";
                            let height = t.submenuContainer.clientHeight;

                            // Setup animation
                            let duration = settings.submenuOpenDuration;
                            if (slowMo === true)
                                duration *= settings.slowMoScale;

                            timeline = new TimelineMax();
                            timeline.fromTo(
                                t.submenuContainer,
                                duration,
                                {
                                    height: 0
                                },
                                {
                                    ease: Power2.easeOut,
                                    height: height,
                                    onComplete: () => {
                                        timeline = null;
                                        Utilities.resetStyles(t.submenuContainer);
                                    }
                                });
                        });

                        t.container.addEventListener("mouseleave", function (event) {
                            // Stop if we're in mobile mode
                            if (WindowMetrics.isMobile())
                                return;

                            let slowMo = event.shiftKey === true;

                            // Stop any ongoing animation
                            if (timeline)
                                timeline.progress(1);

                            // Set initial conditions
                            Utilities.resetStyles(t.submenuContainer);

                            t.submenuContainer.style.display = "block";
                            t.submenuContainer.style.height = "auto";

                            // Setup animation
                            let duration = settings.submenuCloseDuration;
                            if (slowMo === true)
                                duration *= settings.slowMoScale;

                            timeline = new TimelineMax();
                            timeline.fromTo(
                                t.submenuContainer,
                                duration,
                                {
                                    opacity: 1
                                },
                                {
                                    ease: Power2.easeOut,
                                    opacity: 0,
                                    onComplete: () => {
                                        timeline = null;
                                        Utilities.resetStyles(t.submenuContainer);
                                    }
                                });
                        });
                    }
                    break;

                // desktopMenuModes.cssOnly
                default:
                    // Nothing needed here
                    break;
            }

            // Set active state
            for (let i = 0; i < submenuItems.length; i++) {
                let submenuItem = submenuItems[i],
                    link = submenuItem.querySelector("a");

                if (link == null)
                    continue;
                
                // Check if this submenu item's href matches the current page
                let href = link.getAttribute("href");
                if (document.location.pathname == href) {
                    // Open the submenu
                    t.submenuContainer.classList.add(settings.openClassName);
                    t.isOpen = true;

                    // Set the "selected" class to the submenu and submenu item
                    submenuItem.classList.add(settings.selectedClassName);
                    t.container.classList.add(settings.selectedClassName);
                    break;
                }
            }
        } else {
            // This menu item is just a single link - set active state
            let link = t.container.querySelector("a");
            if (link) {
                let href = link.getAttribute("href");

                if (document.location.pathname == href)
                    t.container.classList.add(settings.selectedClassName);
            }
        }
    }

    open(immediate, slowMo = false) {
        let t = this;

        // Stop if we're already open
        if (t.isOpen)
            return;

        // Stop if we don't have a submenu
        if (t.submenuContainer == undefined)
            return;

        // Stop if we're not in mobile mode
        if (!WindowMetrics.isMobile())
            return;

        t.isOpen = true;

        // Stop any ongoing animation
        if (t.header.openCloseTimeline)
            t.header.openCloseTimeline.progress(1);

        // Set initial conditions
        Utilities.resetStyles(t.submenuContainer);
        t.submenuContainer.classList.add(settings.openClassName);
        
        if (immediate) {
            updateScrollability.call(t.header, true);
        } else {
            // Setup animation
            let duration = settings.submenuOpenDuration;
            if (slowMo === true)
                duration *= settings.slowMoScale;

            // Determine the height of the open menu
            let height = t.submenuContainer.clientHeight;

            t.header.openCloseTimeline = new TimelineMax();
            t.header.openCloseTimeline.fromTo(
                t.submenuContainer,
                duration,
                {
                    height: 0
                },
                {
                    ease: Power2.easeOut,
                    height: height,
                    onUpdate: function () {
                        updateScrollability.call(t.header, true);
                    }
                });

            t.header.openCloseTimeline.add(function () {
                t.header.openCloseTimeline = null;

                Utilities.resetStyles(t.submenuContainer);
            });
        }
    }

    close(immediate, slowMo = false) {
        let t = this;

        // Stop if we're already closed
        if (!t.isOpen)
            return;

        // Stop if we don't have a submenu
        if (t.submenuContainer == undefined)
            return;

        // Stop if we're not in mobile mode
        if (!WindowMetrics.isMobile())
            return;

        t.isOpen = false;

        // Stop any ongoing animation
        if (t.header.openCloseTimeline)
            t.header.openCloseTimeline.progress(1);

        // Set initial conditions
        Utilities.resetStyles(t.submenuContainer);
        t.submenuContainer.classList.add(settings.openClassName);
        
        if (immediate) {
            t.submenuContainer.classList.remove(settings.openClassName);
            updateScrollability.call(t.header, true);
        } else {
            // Setup animation
            let duration = settings.submenuCloseDuration;
            if (slowMo === true)
                duration *= settings.slowMoScale;

            // Determine the height of the open menu
            let height = t.submenuContainer.clientHeight;

            t.header.openCloseTimeline = new TimelineMax();
            t.header.openCloseTimeline.fromTo(
                t.submenuContainer,
                duration,
                {
                    height: height
                },
                {
                    ease: Power2.easeOut,
                    height: 0,
                    onUpdate: function () {
                        updateScrollability.call(t.header, true);
                    }
                });

            t.header.openCloseTimeline.add(function () {
                t.header.openCloseTimeline = null;

                Utilities.resetStyles(t.submenuContainer);
                t.submenuContainer.classList.remove(settings.openClassName);
            });
        }
    }
}

// The main Header class
export class Header {
    constructor(stickyMode, mobileMenuMode, desktopMenuMode) {
        let t = this;

        // Settings
        t.stickyMode = stickyMode;
        t.mobileMenuMode = mobileMenuMode;
        t.desktopMenuMode = desktopMenuMode;

        // Mobile open/close state
        t.isOpen = false;
        t.openCloseTimeline = null;

        // Sticky state
        t.isSticky = false;
        t.suppressSticky = false;
        t.stickyTimeline = null;

        // "revealOnScroll" -specific stuff
        if (desktopMenuMode == desktopMenuModes.revealOnScroll) {
            t.scrollVelocity = 0;
            t.lastScrollPosition = window.pageYOffset;
            t.lastScrollTime = (new Date()).getTime();
        }

        // Setup DOM
        t.container = document.querySelector("header");
        //t.links = document.getElementById("mg-header-links");
        t.content = document.getElementById("navigation-content");
        t.navigation = document.getElementById("navigation");
        t.navigationContent = t.navigation.querySelector("div");
        t.hamburger = document.getElementById("hamburger");
        t.backdrop = document.getElementById("backdrop");
        
        // Create backdrop
        t.backdrop = Utilities.createElementFromTemplate(`<div id="backdrop"></div>`);
        t.container.parentNode.insertBefore(t.backdrop, t.container);

        // Create menus
        t.menus = [];
        let menuItems = t.container.querySelectorAll("#navigation > div > ul > li");
        for (let i = 0; i < menuItems.length; i++) {
            let menu = new Menu(
                t,
                menuItems[i],
                mobileMenuMode,
                desktopMenuMode);
            t.menus.push(menu);
        }
        
        // Setup resize callback
        t.stickyScrollPosition;
        let resizeCallback = function () {
            // Recalculate the header height - the "sticky" class mucks with our
            // ability to measure the header's unsticky height, so remove it for
            // a moment if necessary
            if (t.isSticky)
                t.container.classList.remove("sticky");

            // Recalculate the scroll position at which the
            // header becomes sticky
            t.normalHeight = t.container.clientHeight;
            t.stickyHeight = calculateStickyHeight.call(t);
            t.stickyScrollPosition = t.container.clientHeight - t.stickyHeight;

            if (t.isSticky)
                t.container.classList.add("sticky");
            
            // Update the sticky body padding, if necessary
            updateStickyBodyPadding.call(t);

            // If we're in mobile mode, update scrollability
            if (WindowMetrics.isMobile())
                updateScrollability.call(t, true);
        };

        window.addEventListener("resize", resizeCallback);
        resizeCallback();

        // Register for breakpoint change notifications
        WindowMetrics.addBeakpointChangeCallback(function (mobileDesktopTransition) {
            if (!mobileDesktopTransition)
                return;

            // Close header on mobile-desktop transitions
            t.close(true);
                
            // ...also make sure it's not sticky, if we're using "revealOnScroll" mode
            if (t.stickyMode == stickyModes.revealOnScroll) {
                if (t.isSticky)
                    t.toggleSticky(true);
            }
        });

        // Setup scroll callback, but only if screenshot mode isn't enabled
        if (!Utilities.screenshotMode) {
            window.addEventListener("scroll", function () {
                // Skip this if we're in mobile mode and the header is open
                if (WindowMetrics.isMobile()) {
                    if (t.isOpen)
                        return;
                }

                // Update stickiness
                updateStickiness.call(t);
            });
        }

        // Setup ISI anchor link
        let isiLink = t.container.querySelector(".header-isi-link");
        if (isiLink) {
            isiLink.addEventListener("click", function (event) {
                event.preventDefault();
                event.stopPropagation();

                if (WindowMetrics.isMobile()) {
                    if (t.isOpen)
                        t.close(false);
                }

                let
                    psp = document.getElementById("psp"),
                    footer = document.querySelector("footer"),
                    position = psp.offsetTop + t.container.offsetHeight - 100,
                    contentHeight = footer.offsetTop + footer.offsetHeight;

                if (position + window.innerHeight > contentHeight)
                    position = contentHeight - window.innerHeight;

                Utilities.scrollWindowToPosition(
                    position,
                    null);
            });
        }

        // Setup hamburger click
        let
            hamburgerClickCallback = function (event) {
                let slowMo = event.shiftKey === true;

                if (t.isOpen)
                    t.close(false, slowMo);
                else
                    t.open(false, slowMo);
            },

            backdropClickCallback = function (event) {
                // Debounce this so that it doesn't trigger repeatedly by accident
                if (t.openCloseTimeline)
                    return;

                hamburgerClickCallback(event);
            };

        t.hamburger.addEventListener("click", hamburgerClickCallback);
        t.hamburger.addEventListener("touchstart", function (event) {
            event.preventDefault();
            event.stopPropagation();

            t.hamburger.removeEventListener("touchstart", hamburgerClickCallback);
            hamburgerClickCallback(event);
        });

        t.backdrop.addEventListener("click", backdropClickCallback);
        t.backdrop.addEventListener("touchstart", function (event) {
            event.preventDefault();
            event.stopPropagation();

            t.backdrop.removeEventListener("touchstart", backdropClickCallback);
            backdropClickCallback(event);
        });
    }

    static get stickyModes() {
        return stickyModes;
    }

    static get mobileMenuModes() {
        return mobileMenuModes;
    }

    static get desktopMenuModes() {
        return desktopMenuModes;
    }

    static initialize(stickyMode, mobileMenuMode, desktopMenuMode) {
        // Default initialization; there should only be one of these at a time
        new Header(stickyMode, mobileMenuMode, desktopMenuMode);
    }

    enableTriggeringStickyMode() {
        t.suppressSticky = false;
    }

    disableTriggeringStickyMode() {
        t.suppressSticky = true;
    }

    toggleSticky(immediate) {
        let t = this;

        // Don't do anything if we're open
        if (t.isOpen)
            return;

        // Don't do anything if we're suppressing stickiness
        if (t.suppressSticky)
            return;

        // Stop any ongoing animation
        if (t.stickyTimeline)
            t.stickyTimeline.progress(1);

        // Reset our scroll velocity
        t.scrollVelocity = 0;
        
        // Update our sticky flag
        t.isSticky = !t.isSticky;

        if (immediate) {
            // Transition immediately
            if (t.isSticky)
                t.container.classList.add("sticky");
            else
                t.container.classList.remove("sticky");

            updateStickyBodyPadding.call(t);
        } else {
            // Animate transition
            t.stickyTimeline = new TimelineMax();

            if (t.isSticky) {
                // Make the header sticky
                t.container.classList.add("sticky");
                updateStickyBodyPadding.call(t);

                t.stickyTimeline.fromTo(
                    t.container,
                    settings.stickyDuration,
                    {
                        yPercent: -100
                    },
                    {
                        ease: Power3.easeOut,
                        yPercent: 0,
                        onComplete: function () {
                            t.stickyTimeline = null;

                            Utilities.resetStyles(t.container);
                        }
                    });
            } else {
                // Make the header unsticky
                t.stickyTimeline.fromTo(
                    t.container,
                    settings.stickyDuration,
                    {
                        yPercent: 0
                    },
                    {
                        ease: Power3.easeOut,
                        yPercent: -100,
                        onComplete: function () {
                            t.stickyTimeline = null;

                            t.container.classList.remove("sticky");
                            Utilities.resetStyles(t.container);

                            updateStickyBodyPadding.call(t);
                        }
                    });
            }
        }
    }

    open(immediate, slowMo = false) {
        let t = this;

        // Stop if we're already open
        if (t.isOpen)
            return;

        t.isOpen = true;

        // Stop any ongoing animation
        if (t.openCloseTimeline)
            t.openCloseTimeline.progress(1);

        // Set initial state
        t.container.classList.add(settings.openClassName);
        t.navigation.style.display = "block";
        t.backdrop.style.display = "block";

        let
            bounds = t.navigation.getBoundingClientRect(),
            openHeight = t.navigationContent.clientHeight;
        if (openHeight + bounds.top > window.innerHeight)
            openHeight = window.innerHeight - bounds.top;
        
        // Fix content position so that it's not scrollable
        Utilities.lockScrolling(t.navigation);

        if (immediate === true) {
            // Transition immediately
            t.openCloseTimeline = null;

            t.navigation.style.height = Utilities.numberToPx(openHeight);
            t.backdrop.style.opacity = 1;

            if (WindowMetrics.isMobile()) {
                // Update scrollability
                updateScrollability.call(t, false);
            }
        } else {
            // Setup animation
            let duration = settings.openDuration;
            if (slowMo === true)
                duration *= settings.slowMoScale;

            t.openCloseTimeline = new TimelineMax();

            t.openCloseTimeline.fromTo(
                t.navigation,
                duration,
                {
                    height: 0
                },
                {
                    ease: Power3.easeOut,
                    height: openHeight,
                    onUpdate: function () {
                        if (WindowMetrics.isMobile()) {
                            // Update scrollability
                            updateScrollability.call(t, false);
                        }
                    }
                },
                0);

            t.openCloseTimeline.fromTo(
                t.backdrop,
                duration,
                {
                    opacity: 0
                },
                {
                    ease: Power3.easeOut,
                    opacity: 1
                },
                0);

            t.openCloseTimeline.add(function () {
                t.openCloseTimeline = null;
            });
        }
    }

    close(immediate, slowMo = false) {
        let t = this;

        // Stop if we're already closed
        if (!t.isOpen) {
            Utilities.resetStyles(t.navigation);
            Utilities.resetStyles(t.backdrop);
            return;
        }

        t.isOpen = false;

        // Stop any ongoing animation
        if (t.openCloseTimeline)
            t.openCloseTimeline.progress(1);

        // Set initial state
        t.container.classList.remove(settings.openClassName);
        t.navigation.style.display = "block";
        t.backdrop.style.display = "block";

        let openHeight = t.navigationContent.clientHeight;

        // Undo the underlying body content's fixed positioning
        Utilities.unlockScrolling(t.navigation);

        if (immediate === true) {
            // Transition immediately
            t.openCloseTimeline = null;

            Utilities.resetStyles(t.navigation);
            Utilities.resetStyles(t.backdrop);
        } else {
            // Setup animation
            let duration = settings.openDuration;
            if (slowMo === true)
                duration *= settings.slowMoScale;

            t.openCloseTimeline = new TimelineMax();

            t.openCloseTimeline.fromTo(
                t.navigation,
                duration,
                {
                    height: openHeight
                },
                {
                    ease: Power3.easeOut,
                    height: 0
                },
                0);

            t.openCloseTimeline.fromTo(
                t.backdrop,
                duration,
                {
                    opacity: 1
                },
                {
                    ease: Power3.easeOut,
                    opacity: 0
                },
                0);

            t.openCloseTimeline.add(function () {
                t.openCloseTimeline = null;

                Utilities.resetStyles(t.navigation);
                Utilities.resetStyles(t.backdrop);
            });
        }
    }
}

// Private methods
let
    // This function determines the height of the sticky
    // header, which in turn determines the scroll position
    // at which the header becomes sticky. This should be
    // modified when necessary to accomodate differing
    // site designs
    calculateStickyHeight = function () {
        let t = this;

        // In all but tablet mode, the height of the sticky header
        // matches its content region
        if (WindowMetrics.currentBreakpoint == WindowMetrics.breakpoints.medium) {
            return t.content.clientHeight - (6 * 8); // 6 rem additional top padding at this breakpoint
        } else {
            return t.content.clientHeight;
        }
    },

    // Manages the sticky state of the header; used when
    // the window is resized or scrolls
    updateStickiness = function () {
        let t = this,
            scrollPosition = window.pageYOffset;

        // Check what sticky mode we're configured to use
        switch (t.stickyMode) {
            case stickyModes.alwaysVisible:
                // Simply toggle the "sticky" class on and off and set the top padding
                // on the <body> element
                if (scrollPosition > t.stickyScrollPosition) {
                    // We're scrolled beyond the point where the header
                    // should be sticky
                    if (!t.isSticky)
                        t.toggleSticky(true);
                } else {
                    // We're scrolled above the point where the header
                    // should be sticky
                    if (t.isSticky)
                        t.toggleSticky(true);
                }
                break;

            case stickyModes.revealOnScroll:
                // Don't do anything if we're scrolling due to a Utilities call
                if (Utilities.currentlyScrolling)
                    break;

                // Determine the current scroll velocity
                let scrollTime = (new Date()).getTime(),
                    timeDifference = scrollTime - t.lastScrollTime;

                // Filter out large gaps in time and smooth out big changes in velocity
                if (timeDifference > 500)
                    t.scrollVelocity = 0;
                else
                    t.scrollVelocity = (t.lastScrollPosition - scrollPosition) / timeDifference;

                // Update the previousle sampled values to what we just measured
                t.lastScrollPosition = scrollPosition;
                t.lastScrollTime = scrollTime;

                // Check if we're at the top of the page, supressing the sticky toggle if the user hasn't
                // scrolled at least a third of a screen height down the page
                if (scrollPosition > settings.scrollVelocity.minimumScrollDistanceToTriggerSticky && scrollPosition > t.stickyScrollPosition) {
                    // We're not at the top of the page - check if we've surpassed our scroll speed threshold
                    // scrolling either upward or downward
                    if (t.scrollVelocity >= settings.scrollVelocity.triggerSpeed) {
                        // We've surpassed the threshold scrolling upward - make the header sticky if it isn't
                        if (!t.isSticky)
                            t.toggleSticky(false);
                    } else if (t.scrollVelocity <= -settings.scrollVelocity.triggerSpeed) {
                        // We've surpassed the threshold scrolling downward - make the header unsticky if it isn't
                        if (t.isSticky)
                            t.toggleSticky(false);
                    }
                } else if (scrollPosition <= t.stickyScrollPosition) {
                    // We're at the top of the page - make sure the header is unsticky
                    if (t.isSticky)
                        t.toggleSticky(true);
                }
                break;

            // Do nothing if we're not using one of the sticky modes
            default:
                return;
        }
    },

    updateStickyBodyPadding = function () {
        let t = this;

        // Update the body padding to match the normal, unsticky height of the header
        // to accomodate it changing to fixed positioning; otherwise, remove the padding
        if (t.isSticky)
            document.body.style.paddingTop = Utilities.numberToPx(t.normalHeight);
        else
            document.body.style.paddingTop = "";
    },

    // Manages whether the mobile header's content has scrolling
    // enabled; used when the menu opens, when its submenus open or close,
    // and when the window is resized
    updateScrollability = function (updateHeight) {
        let t = this;

        // We only need to do this if the mobile menu is open
        if (!t.isOpen)
            return;

        // Determine the amount of space between the top of the menu's
        // content and the bottom of the viewport
        let bounds = t.navigation.getBoundingClientRect(),
            availableHeight = window.innerHeight - bounds.top;

        // Check if the available space is smaller than the menu's content
        if (availableHeight < t.navigationContent.clientHeight) {
            // There isn't enough room for the menu - enable scrolling
            t.navigation.style.overflowY = "scroll";

            // Update the height of the open menu (i.e., when the window resizes)
            if (updateHeight)
                t.navigation.style.height = Utilities.numberToPx(availableHeight);
        } else {
            // There's enough room to display the full menu - disable scrolling
            t.navigation.style.overflowY = "";

            // Update the height of the open menu (i.e., when the window resizes)
            if (updateHeight)
                t.navigation.style.height = Utilities.numberToPx(t.navigationContent.clientHeight);
        }
    };
