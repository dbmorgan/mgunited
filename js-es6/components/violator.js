import { Utilities } from "../utilities/utilities.js";
import { WindowMetrics } from "../utilities/window-metrics.js";
import { State } from "./state.js";

export class Violator {
  constructor() {
    let t = this;

    t.violator = document.getElementById( "violator" );

    // Stop here if the violator isn't present on the page
    if (t.violator === null)
      return;

    t.body = t.container = document.querySelector( "body" );
    t.toggleButton = document.getElementById( "violator-toggle" );
    t.violatorBody = document.getElementById( "violator-body" );
    t.violatorBttn = document.querySelector('#violator .capsule')

    // if violator exists
    if (t.violator !== null) {

      t.toggleButton.addEventListener( 'click', function() {
        if (t.violatorBody.classList.contains( 'open' )) {
          t.violatorBody.classList.remove( 'open' );
          t.violator.classList.remove( 'open' );
          t.body.classList.remove( 'violator-open' );
        } else {
          t.violatorBody.classList.add( 'open' );
          t.violator.classList.add( 'open' );
          t.body.classList.add( 'violator-open' );
        }
      } )
    }

    var body = document.body,
    html = document.documentElement;

    // TODO: confirm this works on all devices
    var height = Math.max( body.scrollHeight, body.offsetHeight,
      html.clientHeight, html.scrollHeight, html.offsetHeight );

    var third = Math.floor( height / 3 );

    var last_known_scroll_position;
    if (State.signUpCompleted !== true) {
      last_known_scroll_position = window.scrollY || document.documentElement.scrollTop;
      if (last_known_scroll_position > third) {
        if (! t.violator.classList.contains( 'shown' )) {
          TweenLite.fromTo( '#violator', 1, { y: 300, opacity: 0 }, { y: 0, opacity: 1, ease: 'Power2.ease-out' } );
          t.violator.classList.add( 'shown' );
          t.body.classList.add( 'violator-shown' );
          t.body.classList.add( "violator-open" );
        }
      }
    } else {
      t.violator.classList.remove('shown');
      t.body.classList.remove('violator-shown');
    }

    // hide violator when these are on page
    t.navHeight = document.querySelector('#navigation').getBoundingClientRect().height;
    t.formModule = document.querySelector('.form-module');
    t.signupViolator= document.querySelector('.violator');

    if (t.violatorBttn !== null) {
      t.violatorBttn.addEventListener('click', function(e) {
       e.preventDefault;
       if (t.formModule !== null){
         Utilities.scrollWindowToPosition(t.formModule.getBoundingClientRect().top + window.pageYOffset - t.navHeight);
       }
       else {
        window.location ='/sign-up';
      }
    });
    }

    if (t.signupViolator !== null )
      var signUpVTop = t.signupViolator.offsetTop - t.signupViolator.getBoundingClientRect().height;


    // if violator exists
    if (t.violator !== null) {

      window.addEventListener( 'scroll', function() {
        if (State.signUpCompleted !== true) {
          if (signUpVTop !== null) {
            var formModuleTop = t.formModule.offsetTop - t.formModule.getBoundingClientRect().height;
            if (last_known_scroll_position > signUpVTop || last_known_scroll_position > formModuleTop) {
              TweenLite.to( '#violator', 0.8, { opacity: 0, ease: 'Power2.ease-out' } );
              TweenLite.to( '#violator', 0.8, { display: "none", ease: 'Power2.ease-out' } );
              t.body.classList.remove( 'violator-shown' );
            } else {
              TweenLite.to( '#violator', 0.8, { opacity: 1, ease: 'Power2.ease-out' } );
              TweenLite.to( '#violator', 0.8, { display: "block", ease: 'Power2.ease-out' } );
            }
          }

          last_known_scroll_position = window.scrollY || document.documentElement.scrollTop;
          if (last_known_scroll_position > third) {
            if (! t.violator.classList.contains( 'shown' )) {
              TweenLite.fromTo( '#violator', 1, { y: 300, opacity: 0 }, { y: 0, opacity: 1, ease: 'Power2.ease-out' } );
              TweenLite.fromTo( '#violator', 1, { y: 300, display: "none" }, {
                y: 0,
                display: "block",
                ease: 'Power2.ease-out'
              } );
              t.violator.classList.add( 'shown' );
              t.body.classList.add( 'violator-shown' );
              t.body.classList.add( "violator-open" );
            }
          }
        } else {
          t.violator.classList.remove('shown');
          t.body.classList.remove("violator-shown");
        }

      });
    }

  }
}

