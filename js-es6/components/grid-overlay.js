import { Utilities } from "../utilities/utilities.js";

let gridOverlay = null,
	gridOverlayVisible = false;

export class GridOverlay {
	static initialize(parameters) {
		if (gridOverlay)
			return;

        gridOverlay = Utilities.createElementFromTemplate(`<div id="grid-overlay">
	<div class="small-only">
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	</div>

	<div class="large-only">
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	    <div></div>
	</div>
</div>`);

        document.body.appendChild(gridOverlay);

        window.addEventListener("keypress", function (event) {
            if (event.key == "`") {
                if (gridOverlayVisible)
                    Utilities.resetStyles(gridOverlay);
                else
                    gridOverlay.style.display = "block";

                gridOverlayVisible = !gridOverlayVisible;
            }
        });
	}
}