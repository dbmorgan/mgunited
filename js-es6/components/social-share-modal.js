import { Modal } from "./modal.js";


var myCurrPage = window.location.href;
var res = encodeURI(myCurrPage);
var myPageDesc = document.title;

// Not every page has a .social-share element on it
var myPageImg = document.querySelector('.social-share');
if (myPageImg)
    myPageImg = myPageImg.getAttribute('data-image');

let
    template = `<section class="cl-modal-template">
    <div><h2>Share Article</h2>
        <div class="share-buttons">
            <div class="social-button">
            <div id="fb-root"></div>
            <!-- Your share button code -->
            <div class="fb-share-button" 
            data-href="` + myCurrPage + `" 
            data-layout="button_count">Facebook share
            </div>
            <p>Facebook</p>
        </div>
        <div class="social-button">
                <!-- Load Twitter button -->
                <a class="twitter-share-button" href="https://twitter.com/intent/tweet?&text=` + res + `">Tweet</a>
                <p>Twitter</p>
        </div>
        <div class="social-button">
            <!-- Load Pinterest button -->
            <div class="social pinterest">
                <a href="#" data-pin-custom="true" data-service="pinterest" target="_blank">Pin it</a></div>
               <p>Pinterest</p>
        </div>
        </div>
        <div id="copy-url-form">
        <input type="text" id="copy-url" value="` + myCurrPage + `">
        <input type="submit" id="copy-url-bttn" value="COPY">
        </div>
    </section>
    <div class="alert-copied">COPIED</DIV>
`;

let myAlert = document.querySelector('.alert-copied');

export class SocialShareModal {
    constructor() {
    }

    static show(immediate, event) {
        let t = this;

        // Don't do anything if we couldn't initialize as expected
        if (myPageImg == null)
            return;

        Modal.show(
            template,
            "social-share-modal",
            Modal.presentationTypes.scaleUp,
            Modal.presentationTypes.scaleDown,
            immediate === true,
            {
                preShow: function () {
                    //load fb button pls
                    document.querySelector('.fb-share-button').addEventListener('click',function(){
                        FB.ui({
                            method: 'share',
                            href: myCurrPage
                        }, function(response){});
                    });
                    document.querySelector('.pinterest').addEventListener('click',function(){
                        window.open('https://www.pinterest.com/pin/create/link/?url=' + myCurrPage +'&description=' + myPageDesc +'&media='+ myPageImg);
                    });
                },
                postShow: function(){
                    // alert div animation for successful copying 
                    var alertTL = new TimelineLite({paused:true});
                    alertTL
                        .fromTo('.alert-copied', 0.2, {autoAlpha:0,display:'none'},{autoAlpha:1, display:'flex' })
                        .to('.alert-copied', 0.2, {autoAlpha:0,  display:'none' }, 1.4);

                    // attach event to input box for auto-hilite pls
                    var copyURLbox = document.querySelector('#copy-url');
                    copyURLbox.addEventListener('click',function(){
                        this.setSelectionRange(0, this.value.length);
                    })

                    // attach event Listener for copy button pls
                    var copyURLBtn = document.querySelector('#copy-url-bttn');
                    copyURLBtn.addEventListener('click', function(event) {
                        
                        var copyURLInput = document.querySelector('#copy-url');  
                        var range = document.createRange();  
                        range.selectNode(copyURLInput);
                        window.getSelection().addRange(range);

                        try {  
                            // Now that we've selected the text, well, select it again  
                            document.querySelector('#copy-url').select();
                            document.execCommand('copy');
                            alertTL.play(0);
                        } catch(err) {  
                            //console.log('Oops, unable to copy');  
                        }  

                        // Remove the selections - NOTE: Should use
                        // removeRange(range) when it is supported  
                        window.getSelection().removeAllRanges();  
                    });
                }

            },
            event);
    }
}
