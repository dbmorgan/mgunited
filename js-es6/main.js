import { Header } from "./components/header.js";
import { LeavingSiteModal } from "./components/leaving-site-modal.js";
import { SocialShareModal } from "./components/social-share-modal.js";
import { GridOverlay } from "./components/grid-overlay.js";
import { Violator } from './components/violator.js';

import { HomePage } from "./pages/home.js";
import { AboutPage } from "./pages/about.js";
import { LivingWithMGPage } from "./pages/living-with-mg.js";
import { RealStoriesPage } from "./pages/real-stories.js";
import { DocumentaryPage } from "./pages/documentary.js";
import { SignUpPage } from "./pages/sign-up.js";
import { SitemapPage } from "./pages/sitemap.js";
import { MyMGSoleGallery } from "./pages/mymgsole-gallery.js";

import { TreatmentGoalsArticle } from "./articles/treatment-goals.js";
import { Utilities } from "./utilities/utilities.js";

export class MGUnited {
    constructor(parameters) {
        var t = this;

        // Setup our header
        t.header = new Header(
            Header.stickyModes.revealOnScroll,
            Header.mobileMenuModes.alwaysOpen,
            Header.desktopMenuModes.cssOnly);

        // Setup the violator
        t.violator = new Violator();

        // Setup external site links
        let targetBlankLinks = document.querySelectorAll(`a[target="_blank"]`);
        for (let i = 0; i < targetBlankLinks.length; i++) {
            // Skip this link if it has the class "no-interstitial" applied to it
            let targetBlankLink = targetBlankLinks[i];
            if (targetBlankLink.classList.contains("no-interstitial"))
                continue;

            // Show the leaving-site interstitial before navigating
            targetBlankLink.addEventListener("click", function (event) {
                event.preventDefault();
                event.stopPropagation();

                LeavingSiteModal.show(
                    targetBlankLink.href,
                    false,
                    event);
            });
        }

        // References
        var refs = document.getElementById('references');
        if (typeof (refs) != 'undefined' && refs != null) {
            var refLoc = refs.getBoundingClientRect(),
                refBttn = refs.querySelector('p'),
                refList = refs.querySelector("ol"),
                refOpen = false,
                refTimeline = null;

            refBttn.addEventListener('click', function (e) {
                // Stop any ongoing animation
                if (refTimeline)
                    refTimeline.progress(1);

                // Set initial conditions
                refList.removeAttribute('style');
                refList.style.display = 'block';
                let height = refList.clientHeight;

                if (refOpen) {
                    refs.classList.remove('active');

                    refTimeline = new TimelineMax();
                    refTimeline.fromTo(
                        refList,
                        0.8,
                        {
                            height: height
                        },
                        {
                            height: '0',
                            opacity: 0,
                            ease: Power2.easeOut,
                            onComplete: function () {
                                refList.removeAttribute('style');
                                refTimeline = null;
                            }
                        });

                    refOpen = false;
                }
                else {
                    refs.classList.add('active');

                    refTimeline = new TimelineMax();
                    refTimeline.fromTo(
                        refList,
                        0.8,
                        {
                            height: 0
                        },
                        {
                            height: height,
                            opacity: 1,
                            ease: Power2.easeOut,
                            onComplete: function () {
                                refList.style.height = "";
                                refTimeline = null;
                            }
                        });

                    refOpen = true;

                    //TweenLite.set('#references ol', {height: 'auto', opacity:1, display:'block'});
                    //TweenLite.from('#references ol', 0.8,{ height: 0, opacity:0, display:'none', ease: 'power2.easeOut'});
                    //Utilities.scrollWindowToPosition(refLoc.bottom);
                    Utilities.scrollElementToPosition(refs, refLoc.top);
                }
                //refs.classList.toggle('active');
                e.preventDefault();
            });
        }

        // Jumplinks
        var jump = document.getElementById('jumplinks');
        if (typeof (jump) != 'undefined' && jump != null) {
            const jumpLinks = document.querySelectorAll('#jumplinks a');

            for (const jumpLink of jumpLinks) {
                jumpLink.addEventListener('click', function (e) {
                    var getLoc = jumpLink.getAttribute('href'),
                        loc = document.querySelector(getLoc).getBoundingClientRect();
                    Utilities.scrollWindowToPosition(loc.top);
                })
            }
        }

        // Touch event button highlighting
        var touchEventButtons = document.querySelectorAll(".capsule, input[type='submit'], button, a[role='button']");
        for (let i = 0; i < touchEventButtons.length; i++) {
            let touchEventButton = touchEventButtons[i];

            touchEventButton.addEventListener("touchstart", function () {
                // This class remains on the button & inhibits the CSS :hover styles - they make the
                // button look "stuck" on touch devices if users touch & scroll, etc.
                touchEventButton.classList.add("touch");

                // Thic class actually applies the highlight styling
                touchEventButton.classList.add("touch-active");
            });

            touchEventButton.addEventListener("touchend", function () {
                touchEventButton.classList.remove("touch-active");
            });
        }

        // Accordion of terms
        const terms = document.querySelectorAll('.accordion dl dt'),
            defs = document.querySelectorAll('.accordion dl dd');
        for (let i = 0; i < terms.length; i++) {
            const term = terms[i];

            term.addEventListener('click', function (e) {

                // close this one
                if (term.classList.contains('open') === true) {
                    term.classList.remove('open');
                    TweenLite.set(term.nextElementSibling, { height: '0', opacity: 0, display: 'none' });
                }
                // open this one
                else {
                    // close others
                    terms.forEach(node => {
                        node.classList.remove('open');
                    });
                    TweenLite.set(defs, { clearProps: 'all' });
                    setTimeout(function () {
                        // open this one
                        term.classList.add('open');
                        TweenLite.set(term.nextElementSibling, { height: 'auto', opacity: 1, display: 'block' });
                        TweenLite.from(term.nextElementSibling, 0.2, { height: 0, opacity: 0, display: 'none', ease: 'power2.easeOut' });
                    }, 200);
                }
                e.preventDefault;
            });
        }

        var socialShareLinks = document.querySelectorAll('.social-share');

        for (let i = 0; i < socialShareLinks.length; i++) {
            if (typeof (socialShareLinks[i]) != 'undefined' && socialShareLinks[i] != null) {
                socialShareLinks[i].addEventListener('click', function (event) {

                    SocialShareModal.show(
                        false,
                        event);
                    event.preventDefault();
                    event.stopPropagation();
                });
            }
        }

        // Grid overlay
        if (parameters.enableGridOverlay)
            GridOverlay.initialize(parameters);

        // Perform page-specific setup
        switch (document.body.id) {
            case "home":
                new HomePage(parameters);
                break;

            case "about":
                new AboutPage(parameters);
                break;

            case "living-with-mg":
                new LivingWithMGPage(parameters);
                break;

            case "real-stories":
                new RealStoriesPage(parameters);
                break;
            case "kathy-and-diane":
                new RealStoriesPage(parameters);
                break;
            case "been-living-with-mg-for-some-time":
                new RealStoriesPage(parameters);
                break;
            case "victor-and-iris-yipp":
                new RealStoriesPage(parameters);
                break;
            case "leah-gaitan-diaz":
                new RealStoriesPage(parameters);
                break;
            case "the-invisible-burden":
                new RealStoriesPage(parameters);
                break;
            case "morgan-greene":
                new RealStoriesPage(parameters);
                break;
            case "chris-givens":
                new RealStoriesPage(parameters);
                break;
            case "kait-masters-myasthenia-gravis-story":
                new RealStoriesPage(parameters);
                break;
            case "a-mystery-to-me":
                new DocumentaryPage(parameters);
                break;
            case "glenn-phillips":
                new DocumentaryPage(parameters);
                break;
            case "vanetta-drummer-fenton":
                new DocumentaryPage(parameters);
                break;
            case "teresa-hill-putnam":
                new DocumentaryPage(parameters);
                break;

            case "sign-up":
                new SignUpPage(parameters);
                break;

            case "sitemap":
                new SitemapPage(parameters);
                break;

            case "styleguide":
                new StyleGuidePage(parameters);
                break;

            case "treatment-goals":
                new TreatmentGoalsArticle(parameters);
                break;

            case "mymgsole":
                new MyMGSoleGallery(parameters);
                break;

            case "my-mg-sole-myasthenia-gravis-art-project":
                new MyMGSoleGallery(parameters);
                break;

            default:
                break;
        }
    }
}

window.MGUnited = MGUnited;
export default MGUnited;
