﻿let
    settings = {
        scrolling: {
            pixelsPerSecond: 500,
            minDuration: 0.25,
            maxDuration: 1.0
        }
    },

    browserIsIe = false,
    browserIsEdge = false,

    scrollTween = null;

// Detect if the browser is IE or Edge; found this here:
// https://stackoverflow.com/questions/31757852/how-can-i-detect-internet-explorer-ie-and-microsoft-edge-using-javascript
if (/MSIE 10/i.test(navigator.userAgent))
    browserIsIe = true;

if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent))
    browserIsIe = true;

if (/Edge\/\d./i.test(navigator.userAgent))
    browserIsEdge = true;

// Perform some IE fixes
if (browserIsIe && !browserIsEdge) {
    // matches
    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector;
    }

    // remove
    Element.prototype.remove = function () {
        if (this.parentElement !== null)
            this.parentElement.removeChild(this);
    }

    NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
        for (let i = this.length - 1; i >= 0; i--) {
            let node = this[i];

            if (node !== null && node.parentElement !== null)
                node.parentElement.removeChild(node);
        }
    }
}

// Screenshot mode is enabled by including "?screenshot-mode" in the URL
let screenshotMode = /[\?\&]screenshot-mode/.exec(document.location.search) !== null;

/**********************************************************
 *
 *  Code supporting the scroll locking methods - this was adapted from
 *  "body-scroll-lock" found here:
 *  https://github.com/willmcpo/body-scroll-lock
 *
 *********************************************************/

// Older browsers don't support event options, feature detect it.
let hasPassiveEvents = false;
if (typeof window !== 'undefined') {
    const passiveTestOptions = {
        get passive() {
            hasPassiveEvents = true;
            return undefined;
        },
    };
    window.addEventListener('testPassive', null, passiveTestOptions);
    window.removeEventListener('testPassive', null, passiveTestOptions);
}

const isIosDevice =
    typeof window !== 'undefined' &&
    window.navigator &&
    window.navigator.platform &&
    /iP(ad|hone|od)/.test(window.navigator.platform);

let locks = [];
let documentListenerAdded = false;
let initialClientY = -1;
let previousBodyOverflowSetting;
let previousBodyPaddingRight;

// returns true if `el` should be allowed to receive touchmove events.
const allowTouchMove = (el) =>
    locks.some(lock => {
        if (lock.options.allowTouchMove && lock.options.allowTouchMove(el)) {
            return true;
        }

        return false;
    });

const preventDefault = (rawEvent) => {
    const e = rawEvent || window.event;

    // For the case whereby consumers adds a touchmove event listener to document.
    // Recall that we do document.addEventListener('touchmove', preventDefault, { passive: false })
    // in disableBodyScroll - so if we provide this opportunity to allowTouchMove, then
    // the touchmove event on document will break.
    if (allowTouchMove(e.target)) {
        return true;
    }

    // Do not prevent if the event has more than one touch (usually meaning this is a multi touch gesture like pinch to zoom).
    if (e.touches.length > 1) return true;

    if (e.preventDefault) e.preventDefault();

    return false;
};

const setOverflowHidden = (options) => {
    // Setting overflow on body/documentElement synchronously in Desktop Safari slows down
    // the responsiveness for some reason. Setting within a setTimeout fixes this.
    setTimeout(() => {
        // If previousBodyPaddingRight is already set, don't set it again.
        if (previousBodyPaddingRight === undefined) {
            const reserveScrollBarGap = !!options && options.reserveScrollBarGap === true;
            const scrollBarGap = window.innerWidth - document.documentElement.clientWidth;

            if (reserveScrollBarGap && scrollBarGap > 0) {
                previousBodyPaddingRight = document.body.style.paddingRight;
                document.body.style.paddingRight = `${scrollBarGap}px`;
            }
        }

        // If previousBodyOverflowSetting is already set, don't set it again.
        if (previousBodyOverflowSetting === undefined) {
            previousBodyOverflowSetting = document.body.style.overflow;
            document.body.style.overflow = 'hidden';
        }
    });
};

const restoreOverflowSetting = () => {
    // Setting overflow on body/documentElement synchronously in Desktop Safari slows down
    // the responsiveness for some reason. Setting within a setTimeout fixes this.
    setTimeout(() => {
        if (previousBodyPaddingRight !== undefined) {
            document.body.style.paddingRight = previousBodyPaddingRight;

            // Restore previousBodyPaddingRight to undefined so setOverflowHidden knows it
            // can be set again.
            previousBodyPaddingRight = undefined;
        }

        if (previousBodyOverflowSetting !== undefined) {
            document.body.style.overflow = previousBodyOverflowSetting;

            // Restore previousBodyOverflowSetting to undefined
            // so setOverflowHidden knows it can be set again.
            previousBodyOverflowSetting = undefined;
        }
    });
};

// https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
const isTargetElementTotallyScrolled = (targetElement) =>
    targetElement ? targetElement.scrollHeight - targetElement.scrollTop <= targetElement.clientHeight : false;

const handleScroll = (event, targetElement) => {
    const clientY = event.targetTouches[0].clientY - initialClientY;

    if (allowTouchMove(event.target)) {
        return false;
    }

    if (targetElement && targetElement.scrollTop === 0 && clientY > 0) {
        // element is at the top of its scroll.
        return preventDefault(event);
    }

    if (isTargetElementTotallyScrolled(targetElement) && clientY < 0) {
        // element is at the top of its scroll.
        return preventDefault(event);
    }

    event.stopPropagation();
    return true;
};

/**********************************************************
 *
 *  Utilities class
 *
 *********************************************************/

export class Utilities {
    // IE & Edge flags
    static get browserIsIe() {
        return browserIsIe;
    }

    static get browserIsEdge() {
        return browserIsEdge;
    }

    static get screenshotMode() {
        return screenshotMode;
    }

    // Preloads images - used mainly for CSS rules that are
    // triggered by events (:hover), as that can cause a brief
    // flicker while the browser loads the event-related asset
    static preloadImage(url) {
        var image = new Image();
        image.src = url;
    }

    static numberToPx(value) {
        return value.toString() + "px";
    }

    static getChildElement(element, type) {
        type = type.toLowerCase();

        for (var i = 0; i < element.childNodes.length; i++) {
            var childNode = element.childNodes[i];
            if (childNode.nodeType != 1)
                continue;

            if (childNode.tagName.toLowerCase() == type)
                return childNode;
        }

        return null;
    }

    static getChildElements(element, type) {
        var results = [];

        type = type.toLowerCase();

        for (var i = 0; i < element.childNodes.length; i++) {
            var childNode = element.childNodes[i];
            if (childNode.nodeType != 1)
                continue;

            if (childNode.tagName.toLowerCase() == type)
                results.push(childNode);
        }

        return results;
    }

    static getClosestElement(element, selector) {
        if (element.closest)
            return element.closest(selector);

        var node = element.parentNode;
        while (node && node.tagName !== undefined) {
            if (node.matches(selector))
                return node;

            node = node.parentNode;
        }

        return null;
    }

    static resetStyles(element) {
        element.removeAttribute("style");
    }

    static createElementFromTemplate(template) {
        var div = document.createElement("div");
        div.innerHTML = template;

        return div.firstChild;
    }

    static formatDateFromJson(value) {
        var
            date = new Date(value),
            year = date.getFullYear().toString().substring(2, 4),
            hour = date.getHours(),
            minutes = date.getMinutes().toString(),
            amPm;

        if (hour >= 12) {
            amPm = "pm";

            if (hour > 12)
                hour -= 12;
        } else {
            amPm = "am";
        }

        if (minutes.length == 1)
            minutes = "0" + minutes;

        return `${date.getMonth() + 1}/${date.getDate()}/${year} ${hour}:${minutes}${amPm}`;
    }

    static addZipCodeInputValidation(element) {
        element.addEventListener("input", function (e) {
            e.target.value = e.target.value.replace(/\D/g, "");
        });
    }

    static addNpiCodeInputValidation(element) {
        element.addEventListener("input", function (e) {
            e.target.value = e.target.value.replace(/\D/g, "");
        });
    }

    static addTelephoneInputValidation(element) {
        element.addEventListener("input", function (e) {
            var x = e.target.value.replace(/\D/g, "").match(/(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,1})(\d{0,9})/);
            e.target.value = (!x[1] ? "" : "(" + x[1]) + (!x[2] ? "" : x[2]) + (x[4] ? x[3] + ") " : x[3]) + x[4] + x[5] + x[6] + (x[7] ? "-" + x[7] + x[8] + x[9] + x[10] : "") + (x[11] ? " " + x[11] : "");
        });
    }

    static get currentlyScrolling() {
        return scrollTween !== null;
    }

    static scrollWindowToPosition(position, callback) {
        // Stop any ongoing animation
        if (scrollTween)
            scrollTween.progress(1);

        var
            currentScrollTop = window.pageYOffset,
            duration = Math.abs(position - currentScrollTop) / settings.scrolling.pixelsPerSecond;

        // Transition immediately if the distance is small
        if (duration < settings.scrolling.minDuration) {
            window.scrollTo(0, position);

            if (callback)
                callback();
            return;
        }

        // Cap the duration
        if (duration > settings.scrolling.maxDuration)
            duration = settings.scrolling.maxDuration;

        // Perform the transition
        var progressObject = {
            position: 0
        };

        scrollTween = TweenLite.fromTo(
            progressObject,
            duration,
            {
                position: currentScrollTop
            },
            {
                ease: Power2.easeOut,
                position: position,
                onUpdate: function () {
                    window.scrollTo(0, progressObject.position);
                },
                onComplete: function () {
                    scrollTween = null;

                    if (callback)
                        callback();
                }
            });
    }

    static scrollElementToPosition(element, position, callback) {
        // Stop any ongoing animation
        if (scrollTween)
            scrollTween.progress(1);

        var
            currentScrollTop = element.scrollTop,
            duration = Math.abs(position - currentScrollTop) / settings.scrolling.pixelsPerSecond;

        // Transition immediately if the distance is small
        if (duration < settings.scrolling.minDuration) {
            element.scrollTop = position;

            if (callback)
                callback();
            return;
        }

        // Cap the duration
        if (duration > settings.scrolling.maxDuration)
            duration = settings.scrolling.maxDuration;

        // Perform the transition
        var progressObject = {
            position: 0
        };

        scrollTween = TweenLite.fromTo(
            progressObject,
            duration,
            {
                position: currentScrollTop
            },
            {
                ease: Power2.easeOut,
                position: position,
                onUpdate: function () {
                    element.scrollTop = progressObject.position;
                },
                onComplete: function () {
                    scrollTween = null;

                    if (callback)
                        callback();
                }
            });
    }

    /**********************************************************
     *
     * Scroll locking methods
     *
     *********************************************************/

    static lockScrolling(targetElement, options) {
        if (isIosDevice) {
            // targetElement must be provided, and disableBodyScroll must not have been
            // called on this targetElement before.
            if (!targetElement) {
                // eslint-disable-next-line no-console
                console.error(
                    'lockScrolling: targetElement required'
                );
                return;
            }

            if (targetElement && !locks.some(lock => lock.targetElement === targetElement)) {
                const lock = {
                    targetElement,
                    options: options || {},
                };

                locks = [...locks, lock];

                targetElement.ontouchstart = (event) => {
                    if (event.targetTouches.length === 1) {
                        // detect single touch.
                        initialClientY = event.targetTouches[0].clientY;
                    }
                };

                targetElement.ontouchmove = (event) => {
                    if (event.targetTouches.length === 1) {
                        // detect single touch.
                        handleScroll(event, targetElement);
                    }
                };

                if (!documentListenerAdded) {
                    document.addEventListener('touchmove', preventDefault, hasPassiveEvents ? { passive: false } : undefined);
                    documentListenerAdded = true;
                }
            }
        } else {
            setOverflowHidden(options);
            const lock = {
                targetElement,
                options: options || {},
            };

            locks = [...locks, lock];
        }
    }

    static unlockScrolling() {
        if (isIosDevice) {
            // Clear all locks ontouchstart/ontouchmove handlers, and the references.
            locks.forEach((lock) => {
                lock.targetElement.ontouchstart = null;
                lock.targetElement.ontouchmove = null;
            });

            if (documentListenerAdded) {
                document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? { passive: false } : undefined);
                documentListenerAdded = false;
            }

            locks = [];

            // Reset initial clientY.
            initialClientY = -1;
        } else {
            restoreOverflowSetting();
            locks = [];
        }
    }

    static unlockScrollingForTargetElement(targetElement) {
        if (isIosDevice) {
            if (!targetElement) {
                // eslint-disable-next-line no-console
                console.error(
                    'unlockScrollingForTargetElement: targetElement required'
                );
                return;
            }

            targetElement.ontouchstart = null;
            targetElement.ontouchmove = null;

            locks = locks.filter(lock => lock.targetElement !== targetElement);

            if (documentListenerAdded && locks.length === 0) {
                document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? { passive: false } : undefined);

                documentListenerAdded = false;
            }
        } else {
            locks = locks.filter(lock => lock.targetElement !== targetElement);
            if (!locks.length) {
                restoreOverflowSetting();
            }
        }
    }

    static serializeObject(obj) {
      const serialized = [];
      Object.keys(obj).forEach((item) => {
        serialized.push(encodeURIComponent(item)+"="+encodeURIComponent(obj[item]));
      });
      return serialized.join('&');
    }
}
