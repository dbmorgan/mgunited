const
    settings = {
        // API URL
        apiUrl: myAjax.ajaxurl,

        // Minimum duration for API calls, in milliseconds;
        // increase this if you want to make sure the progress
        // indicators appear for a beat before the call completes
        apiMinDelay: 0,

        // Redirect paths
        paths: {
            error: "/error"
        },

        performErrorRedirect: false
    },

    basicResponseCodes = {
        ok: 1,
        notAuthenticated: 2,
        notAuthorized: 3,
        validationError: 4,
        systemError: 5
    },

    signUpErrorCodes = {
        // General
        signUpRequired: 1,

        // Sign up
        firstNameRequired: 2,
        firstNameFormat: 3,
        lastNameRequired: 4,
        lastNameFormat: 5,
        emailRequired: 6,
        emailFormat: 7,
        duplicateEmail: 8,
        signUpOptInRequired: 9,

        // Goal
        goalRequired: 10,
        goalOptInRequired: 11,

        // Story
        storyRequired: 12,
        storyOptInRequired: 13,

        // Survey
        invalidProfileTypeId: 14,
        invalidGenderId: 15,
        genderRequired: 16,
        invalidTopicOfInterestId: 17,
        topicsOfInterestRequired: 18,

        // Stay involved
        invalidStayInvolvedOptionId: 19,
        stayInvolvedOptionRequired: 20
    };

export class SignUpApi {
	static callApiForAction(action = "signupApi", method, data, callback) {
        var
            apiCallStart = (new Date()).getTime(),
            request = new XMLHttpRequest();
        
        request.open("POST", settings.apiUrl, true);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                // Success
                var response = JSON.parse(request.response);

                // Perform callback, if specified
                if (typeof (callback) == "function") {
                    var apiCallEnd = (new Date()).getTime();
                    var apiCallDuration = apiCallEnd - apiCallStart;

                    // Check if the api call duration was smaller than
                    // our minimum wait time
                    if (apiCallDuration < settings.apiMinDelay) {
                        // Call took less than our minimum wait time;
                        // perform callback after the appropriate delay
                        setTimeout(
                            function () {
                                callback(response);
                            },
                            settings.apiMinDelay - apiCallDuration);
                    } else {
                        // Call took equal or more than our minimum wait
                        // time; perform callback immediately
                        callback(response);
                    }
                }
            } else {
                // Error
                if (settings.performErrorRedirect)
                    window.location.href = settings.paths.error;
                else
                    throw "";
            }
        };

        request.onerror = function () {
            // Error
            if (settings.performErrorRedirect)
                window.location.href = settings.paths.error;
            else
                throw "";
        };

        // Assemble our POST
        var postData = [];
        postData.push(`action=${action}`);
        postData.push("method=" + encodeURIComponent(method));
        postData.push("nonce=" + encodeURIComponent(myAjax.signup_nonce));

        if (data)
            postData.push("data=" + encodeURIComponent(JSON.stringify(data)));

        postData = postData.join("&");
        request.send(postData);
    }

	static callApi(method, data, callback) {
        // Call the default API action
        SignUpApi.callApiForAction(undefined, method, data, callback);
    }

    static get basicResponseCodes() {
        return basicResponseCodes;
    }

    static get signUpErrorCodes() {
        return signUpErrorCodes;
    }

    static showUnknownError(response) {
        if (response)
            console.log(response);

        // Error
        if (settings.performErrorRedirect)
            window.location.href = settings.paths.error;
        else
            throw "";
    }
}
