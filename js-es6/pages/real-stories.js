﻿import { RealStoriesForm } from "../forms/real-stories-form.js";

export class RealStoriesPage {
    constructor(parameters) {
        var t = this;
        
        // Setup form
        t.realStoriesForm = new RealStoriesForm(parameters);
    }
}
