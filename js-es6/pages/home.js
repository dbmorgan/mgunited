﻿import { JoinNowForm  } from "../forms/join-now-form.js";
import { Utilities } from "../utilities/utilities.js";

export class HomePage {
  constructor(parameters) {
    var t = this;

    // Setup form
    t.joinNowForm = new JoinNowForm(parameters);

    var heroPromo = document.querySelector('#home-documentary-promo');
    var heroVideo = document.querySelector('#hero-video');
    heroPromo.addEventListener('click', function() {
      if (!heroVideo.playing) {
        heroVideo.play();
      }
    });

    heroPromo.addEventListener('touchstart', function() {
      if (!heroVideo.playing) {
        heroVideo.play();
      }
    });

    var jumplinks = document.querySelectorAll('#home-jump-links a');
    for (var i = 0; i < jumplinks.length; i++) {
      let jumplink = jumplinks[i];
      jumplink.addEventListener('click', function (e) {
        e.preventDefault();
        var id = this.getAttribute('href'),
            loc = id.replace(/#/, ''),
            pos = document.getElementById(loc).offsetTop;

        Utilities.scrollWindowToPosition(pos);
      });

    }

  }
}
