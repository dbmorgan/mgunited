import { Utilities } from "../utilities/utilities.js";
import { MyMGSoleForm } from "../forms/my-mg-sole-form.js";

export class MyMGSoleGallery {
	constructor(parameters) {
		const t = this;

        // Setup form
        t.myMGSoleForm = new MyMGSoleForm(parameters);

		// Setup "get started" button
		var getStartedLink = document.querySelector('.steps-wrap .capsule');
		getStartedLink.addEventListener("click", function (e) {
			e.preventDefault();

			var id = this.getAttribute("data-id"),
				pos = document.getElementById(id).offsetTop;
			
			Utilities.scrollWindowToPosition(pos);
		});

	}
}