﻿import { JoinNowForm } from "../forms/join-now-form.js";
import { Utilities } from "../utilities/utilities.js";
import { WindowMetrics } from "../utilities/window-metrics.js";

export class SignUpPage {
	constructor(parameters) {
		var t = this;

        // Setup form
        t.joinNowForm = new JoinNowForm(parameters);

        var jumplink = document.querySelector('#hero .capsule'),
            header = document.querySelector('header'),
            spacer = 120;
                 
        	jumplink.addEventListener('click', function (e) {
        		e.preventDefault();

                if (header.classList.contains('sticky')) { 
                    spacer = 50; 
                }

        		var id = this.getAttribute('href'),
        		loc = id.replace(/#/, ''),
        		pos = document.getElementById(loc).offsetTop + spacer;

        		Utilities.scrollWindowToPosition(pos);
        	});

   }
}
