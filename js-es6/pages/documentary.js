﻿import { Modal } from "../components/modal.js";
import { DocumentaryForm } from "../forms/documentary-form.js";
import { Utilities } from "../utilities/utilities.js";

var myCurrPage = window.location.href;

export class DocumentaryPage {
	constructor(parameters) {
		var t = this;

		// Setup form
		t.documentaryForm = new DocumentaryForm(parameters);

		// "Keep me updated" & "RSVP" scroll
		if (t.documentaryForm.container) {
			let keepMeUpdatedButton = document.querySelector('#intro a.underline'),
				rsvpBttn = document.querySelector('#rsvp-modal'),
				header = document.querySelector('header'),
				spacer,

				callback = function (e) {
					e.preventDefault();

					let bounds = t.documentaryForm.container.getBoundingClientRect();
					
					// make room for sticky menu 
					if (header.classList.contains('sticky')) { 
		                    spacer = header.getBoundingClientRect().height;
		               }
		               else {
		               	spacer = 0
		               }

					Utilities.scrollWindowToPosition(bounds.top + window.pageYOffset - spacer);
				};

			if (keepMeUpdatedButton)
				keepMeUpdatedButton.addEventListener('click', callback)

			if (rsvpBttn)
				rsvpBttn.addEventListener('click', callback)
		}

		// Video modal
		let heroVideo = document.querySelector('#hero-video'),
			docuBttn = document.querySelector('#trailer-button'),
			template = `<section class="cl-modal-template">
        <iframe src="https://player.vimeo.com/video/458975377" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </section>`;

    var hero = document.querySelector('#hero');
    hero.addEventListener('click', function() {
      if (!heroVideo.playing) {
        heroVideo.play();
      }
    });

    hero.addEventListener('touchstart', function() {
      if (!heroVideo.playing) {
        heroVideo.play();
      }
    });

		if (docuBttn) {
			docuBttn.addEventListener('click', function (event) {
				// video modal
				Modal.show(
					template,
					"documentary-video-modal",
					Modal.presentationTypes.scaleUp,
					Modal.presentationTypes.scaleDown,
					true,
					{
						preShow: function () {
							// pause background video in hero
							// timeout to get past ios fix above
							setTimeout(function(){
								heroVideo.pause();
							},400)
						},

						postClose: function () {
							// play background video in hero again
							heroVideo.play();
						}
					},
					event);
			});
		}

	}
}
