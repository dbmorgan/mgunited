import { MultiStepForm } from "../forms/multi-step-form.js";

export class AboutPage {
    constructor(parameters) {
        var t = this;
        
        // Setup forms
        t.multiStepForm = new MultiStepForm(parameters);
    }
}
