﻿import { MultiStepForm } from "../forms/multi-step-form.js";

export class LivingWithMGPage {
    constructor(parameters) {
        var t = this;

        // Setup quote carousel
        // var slider = tns({
        //     container: '.slider',
        //     items: 1,
        //     slideBy: 'page',
        //     autoplay: true,
        //     autoplayTimeout: 8000,
        //     autoplayHoverPause: true,
        //     controls: false,
        //     navContainer: '.slider-dots',
        //     autoplayButtonOutput: false,
        //     animateIn: 'tns-fadeIn',
        //     onInit: fadeSliderIn()
        // });
        // fade in carousel
        // function fadeSliderIn() {
        //     TweenLite.to('.slider', 0.8, { opacity: 1, ease: 'power2.easeInOut' });
        // }
        // Setup forms
        t.multiStepForm = new MultiStepForm(parameters);
    }
}
