-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: sftp96505.wpengine.com:13306
-- Generation Time: Jul 27, 2020 at 09:20 PM
-- Server version: 5.6.41-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wp_mguniteddev`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://mguniteddev.wpengine.com', 'yes'),
(2, 'home', 'https://mguniteddev.wpengine.com', 'yes'),
(3, 'blogname', 'MG United', 'yes'),
(4, 'blogdescription', 'Your SUPER-powered WP Engine Site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'techmgr@closerlook.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%category%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:1;s:27:\"redirection/redirection.php\";i:2;s:41:\"wordpress-importer/wordpress-importer.php\";i:3;s:24:\"wordpress-seo/wp-seo.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'https://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'mgunited', 'yes'),
(41, 'stylesheet', 'mgunited', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:27:\"redirection/redirection.php\";a:2:{i:0;s:17:\"Redirection_Admin\";i:1;s:16:\"plugin_uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '10', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '46', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1603657729', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:15:{i:1595885332;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1595893049;a:1:{s:49:\"WPEngineSecurityAuditor_Scans_fingerprint_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1595894026;a:1:{s:46:\"WPEngineSecurityAuditor_Scans_fingerprint_core\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1595896294;a:1:{s:49:\"WPEngineSecurityAuditor_Scans_fingerprint_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1595908310;a:1:{s:48:\"WPEngineSecurityAuditor_Scans_fingerprint_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1595915327;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1595918897;a:1:{s:46:\"WPEngineSecurityAuditor_Scans_fingerprint_core\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1595924932;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1595924960;a:1:{s:39:\"WPEngineSecurityAuditor_Scans_scheduler\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1595946812;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1595946813;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1595968132;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1596054532;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1596174524;a:1:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_wpe_powered_by_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(117, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1588170822;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(118, 'wpe_notices', 'a:1:{s:4:\"read\";s:0:\"\";}', 'yes'),
(119, 'wpe_notices_ttl', '1595622035', 'yes'),
(120, 'can_compress_scripts', '0', 'no'),
(121, 'current_theme', '', 'yes'),
(122, 'theme_mods_mgunited', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(123, 'theme_switched', '', 'yes'),
(124, 'recently_activated', 'a:1:{s:39:\"custom-permalinks/custom-permalinks.php\";i:1594668404;}', 'yes'),
(126, 'WPLANG', '', 'yes'),
(127, 'new_admin_email', 'techmgr@closerlook.com', 'yes'),
(140, 'wpseo', 'a:26:{s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:0;s:25:\"ignore_indexation_warning\";b:0;s:29:\"indexation_warning_hide_until\";b:0;s:18:\"indexation_started\";b:0;s:7:\"version\";s:4:\"14.1\";s:16:\"previous_version\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:4:\"blog\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:7:\"staging\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1589521724;s:13:\"myyoast-oauth\";b:0;}', 'yes'),
(141, 'wpseo_titles', 'a:73:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-pipe\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:1;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:1;s:12:\"disable-date\";b:1;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:2:\"»\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";s:1:\"0\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";s:1:\"0\";s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:26:\"taxonomy-category-ptparent\";s:1:\"0\";s:26:\"taxonomy-post_tag-ptparent\";s:1:\"0\";s:29:\"taxonomy-post_format-ptparent\";s:1:\"0\";}', 'yes'),
(142, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(143, 'wpseo_flush_rewrite', '1', 'yes'),
(144, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:4:\"14.1\";}', 'yes'),
(145, 'wpseo_ryte', 'a:2:{s:6:\"status\";i:1;s:10:\"last_fetch\";i:1595616730;}', 'yes'),
(169, 'custom_permalinks_plugin_version', '1.5.1', 'yes'),
(187, 'rewrite_rules', 'a:98:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=10&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:31:\".+?/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\".+?/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\".+?/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\".+?/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\".+?/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\".+?/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"(.+?)/([^/]+)/embed/?$\";s:63:\"index.php?category_name=$matches[1]&name=$matches[2]&embed=true\";s:26:\"(.+?)/([^/]+)/trackback/?$\";s:57:\"index.php?category_name=$matches[1]&name=$matches[2]&tb=1\";s:46:\"(.+?)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:41:\"(.+?)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:34:\"(.+?)/([^/]+)/page/?([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&paged=$matches[3]\";s:41:\"(.+?)/([^/]+)/comment-page-([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&cpage=$matches[3]\";s:30:\"(.+?)/([^/]+)(?:/([0-9]+))?/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&page=$matches[3]\";s:20:\".+?/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:30:\".+?/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:50:\".+?/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\".+?/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\".+?/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:26:\".+?/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:14:\"(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:26:\"(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:33:\"(.+?)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&cpage=$matches[2]\";s:8:\"(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";}', 'yes'),
(240, 'category_children', 'a:0:{}', 'yes'),
(247, 'redirection_options', 'a:24:{s:7:\"support\";b:0;s:5:\"token\";s:32:\"50c6068e410f4a9368a2f3cff1889506\";s:12:\"monitor_post\";i:1;s:13:\"monitor_types\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:19:\"associated_redirect\";s:0:\"\";s:11:\"auto_target\";s:0:\"\";s:15:\"expire_redirect\";i:-1;s:10:\"expire_404\";i:-1;s:7:\"modules\";a:0:{}s:10:\"newsletter\";b:0;s:14:\"redirect_cache\";i:1;s:10:\"ip_logging\";i:0;s:13:\"last_group_id\";i:1;s:8:\"rest_api\";i:0;s:5:\"https\";b:0;s:7:\"headers\";a:0:{}s:8:\"database\";s:3:\"4.1\";s:8:\"relocate\";s:0:\"\";s:16:\"preferred_domain\";s:0:\"\";s:7:\"aliases\";a:0:{}s:10:\"flag_query\";s:5:\"exact\";s:9:\"flag_case\";b:0;s:13:\"flag_trailing\";b:0;s:10:\"flag_regex\";b:0;}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 3, '_wp_page_template', 'default'),
(10, 17, '_menu_item_type', 'post_type'),
(11, 17, '_menu_item_menu_item_parent', '0'),
(12, 17, '_menu_item_object_id', '12'),
(13, 17, '_menu_item_object', 'page'),
(14, 17, '_menu_item_target', ''),
(15, 17, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(16, 17, '_menu_item_xfn', ''),
(17, 17, '_menu_item_url', ''),
(18, 18, '_menu_item_type', 'post_type'),
(19, 18, '_menu_item_menu_item_parent', '0'),
(20, 18, '_menu_item_object_id', '14'),
(21, 18, '_menu_item_object', 'page'),
(22, 18, '_menu_item_target', ''),
(23, 18, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(24, 18, '_menu_item_xfn', ''),
(25, 18, '_menu_item_url', ''),
(26, 19, '_menu_item_type', 'post_type'),
(27, 19, '_menu_item_menu_item_parent', '0'),
(28, 19, '_menu_item_object_id', '11'),
(29, 19, '_menu_item_object', 'page'),
(30, 19, '_menu_item_target', ''),
(31, 19, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(32, 19, '_menu_item_xfn', ''),
(33, 19, '_menu_item_url', ''),
(34, 20, '_menu_item_type', 'post_type'),
(35, 20, '_menu_item_menu_item_parent', '0'),
(36, 20, '_menu_item_object_id', '16'),
(37, 20, '_menu_item_object', 'page'),
(38, 20, '_menu_item_target', ''),
(39, 20, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(40, 20, '_menu_item_xfn', ''),
(41, 20, '_menu_item_url', ''),
(52, 11, '_edit_lock', '1594132797:4'),
(53, 9, '_edit_lock', '1590357991:4'),
(54, 24, '_edit_lock', '1588877292:3'),
(55, 14, '_edit_lock', '1594132796:4'),
(57, 27, '_edit_lock', '1594773948:4'),
(58, 27, '_edit_last', '4'),
(59, 27, '_yoast_wpseo_primary_category', '3'),
(62, 28, '_edit_lock', '1594344962:4'),
(63, 28, '_edit_last', '4'),
(64, 28, '_yoast_wpseo_primary_category', '3'),
(67, 29, '_edit_lock', '1594341257:4'),
(68, 29, '_edit_last', '4'),
(69, 29, '_yoast_wpseo_primary_category', '4'),
(72, 30, '_edit_lock', '1594761684:4'),
(73, 30, '_edit_last', '4'),
(74, 30, '_yoast_wpseo_primary_category', '3'),
(77, 31, '_edit_lock', '1594341905:4'),
(78, 31, '_edit_last', '4'),
(79, 31, '_yoast_wpseo_primary_category', '5'),
(82, 10, '_edit_lock', '1590549481:4'),
(92, 14, '_edit_last', '4'),
(93, 9, '_edit_last', '4'),
(94, 34, '_wp_attached_file', '2020/05/Square-Image-MG-Sole-scaled.jpg'),
(95, 34, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:2560;s:4:\"file\";s:39:\"2020/05/Square-Image-MG-Sole-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"Square-Image-MG-Sole-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"Square-Image-MG-Sole-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"Square-Image-MG-Sole-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"Square-Image-MG-Sole-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:34:\"Square-Image-MG-Sole-1536x1536.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:34:\"Square-Image-MG-Sole-2048x2048.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:24:\"Square-Image-MG-Sole.jpg\";}'),
(98, 31, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(99, 31, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(100, 31, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg'),
(101, 31, '_yoast_wpseo_opengraph-image-id', '34'),
(102, 31, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(103, 31, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(104, 31, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg'),
(105, 31, '_yoast_wpseo_twitter-image-id', '34'),
(106, 35, '_wp_attached_file', '2020/05/Square-Image-Crisis-scaled.jpg'),
(107, 35, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:2560;s:4:\"file\";s:38:\"2020/05/Square-Image-Crisis-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"Square-Image-Crisis-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"Square-Image-Crisis-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Square-Image-Crisis-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"Square-Image-Crisis-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:33:\"Square-Image-Crisis-1536x1536.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:33:\"Square-Image-Crisis-2048x2048.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:23:\"Square-Image-Crisis.jpg\";}'),
(110, 30, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(111, 30, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(112, 30, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg'),
(113, 30, '_yoast_wpseo_opengraph-image-id', '35'),
(114, 30, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(115, 30, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(116, 30, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg'),
(117, 30, '_yoast_wpseo_twitter-image-id', '35'),
(120, 36, '_wp_attached_file', '2020/05/Square-Image-Emotional-scaled.jpg'),
(121, 36, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:2560;s:4:\"file\";s:41:\"2020/05/Square-Image-Emotional-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"Square-Image-Emotional-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"Square-Image-Emotional-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"Square-Image-Emotional-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"Square-Image-Emotional-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:36:\"Square-Image-Emotional-1536x1536.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:36:\"Square-Image-Emotional-2048x2048.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:26:\"Square-Image-Emotional.jpg\";}'),
(124, 29, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(125, 29, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(126, 29, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg'),
(127, 29, '_yoast_wpseo_opengraph-image-id', '36'),
(128, 29, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(129, 29, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(130, 29, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg'),
(131, 29, '_yoast_wpseo_twitter-image-id', '36'),
(132, 37, '_wp_attached_file', '2020/05/Square-Image-scaled.jpg'),
(133, 37, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:2560;s:4:\"file\";s:31:\"2020/05/Square-Image-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"Square-Image-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"Square-Image-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"Square-Image-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"Square-Image-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:26:\"Square-Image-1536x1536.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:26:\"Square-Image-2048x2048.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:16:\"Square-Image.jpg\";}'),
(136, 28, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(137, 28, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(138, 28, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg'),
(139, 28, '_yoast_wpseo_opengraph-image-id', '37'),
(140, 28, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(141, 28, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(142, 28, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg'),
(143, 28, '_yoast_wpseo_twitter-image-id', '37'),
(144, 38, '_wp_attached_file', '2020/05/Square-Image-Goals-scaled.jpg'),
(145, 38, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:2560;s:4:\"file\";s:37:\"2020/05/Square-Image-Goals-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"Square-Image-Goals-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"Square-Image-Goals-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"Square-Image-Goals-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"Square-Image-Goals-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:32:\"Square-Image-Goals-1536x1536.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:32:\"Square-Image-Goals-2048x2048.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:22:\"Square-Image-Goals.jpg\";}'),
(148, 27, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(149, 27, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(150, 27, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg'),
(151, 27, '_yoast_wpseo_opengraph-image-id', '38'),
(152, 27, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(153, 27, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(154, 27, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg'),
(155, 27, '_yoast_wpseo_twitter-image-id', '38'),
(166, 10, '_edit_last', '4'),
(167, 10, '_yoast_wpseo_metadesc', 'Fierce allies in the daily battle for a better life with myasthenia gravis.'),
(168, 10, '_yoast_wpseo_content_score', '60'),
(169, 14, '_yoast_wpseo_metadesc', 'Understand what to expect from your body after a myasthenia gravis diagnosis. MG definitions explained.'),
(170, 11, '_edit_last', '4'),
(171, 11, '_yoast_wpseo_metadesc', 'We’re here to provide personalized resources to help you confront your MG questions and concerns.'),
(172, 11, '_yoast_wpseo_content_score', '30'),
(173, 16, '_edit_lock', '1590075030:4'),
(174, 16, '_edit_last', '4'),
(175, 16, '_yoast_wpseo_metadesc', 'MG United members share their stories'),
(176, 12, '_edit_lock', '1590075228:4'),
(177, 12, '_edit_last', '4'),
(178, 12, '_yoast_wpseo_metadesc', 'Sign up to track the production of the first documentary about myasthenia gravis.'),
(179, 9, '_yoast_wpseo_metadesc', 'Sign up to join MG United.'),
(205, 45, '_wp_attached_file', '2020/05/mg-united-icon.png'),
(206, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:26:\"2020/05/mg-united-icon.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"mg-united-icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"mg-united-icon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(207, 46, '_wp_attached_file', '2020/05/cropped-mg-united-icon.png'),
(208, 46, '_wp_attachment_context', 'site-icon'),
(209, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:34:\"2020/05/cropped-mg-united-icon.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"cropped-mg-united-icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"cropped-mg-united-icon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:34:\"cropped-mg-united-icon-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:34:\"cropped-mg-united-icon-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:34:\"cropped-mg-united-icon-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:32:\"cropped-mg-united-icon-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 31, '_thumbnail_id', '34'),
(220, 31, '_yoast_wpseo_content_score', '30'),
(222, 30, '_thumbnail_id', '35'),
(225, 30, '_yoast_wpseo_content_score', '30'),
(227, 29, '_thumbnail_id', '36'),
(230, 29, '_yoast_wpseo_content_score', '30'),
(232, 28, '_thumbnail_id', '37'),
(235, 28, '_yoast_wpseo_content_score', '30'),
(245, 31, '_yoast_wpseo_metadesc', 'My MG Sole fights coronavirus blues with kicky art.'),
(249, 30, '_yoast_wpseo_metadesc', 'Crisis 411: What you need to know about myasthenic crisis.'),
(255, 29, '_yoast_wpseo_metadesc', 'How to have good emotional health and MG at the same time.'),
(259, 28, '_yoast_wpseo_metadesc', 'What is myasthenia gravis, and why does it happen?'),
(263, 27, '_yoast_wpseo_metadesc', 'The Importance of Setting Your Personal MG Treatment Goals'),
(264, 27, '_thumbnail_id', '38'),
(267, 27, '_yoast_wpseo_content_score', '30'),
(272, 58, '_edit_lock', '1594772634:4'),
(274, 58, '_edit_last', '4'),
(279, 59, '_edit_lock', '1594762084:4'),
(281, 59, '_edit_last', '4'),
(283, 60, '_edit_lock', '1594762582:4'),
(285, 60, '_edit_last', '4'),
(287, 63, '_edit_lock', '1594764146:4'),
(289, 63, '_edit_last', '4'),
(291, 64, '_edit_lock', '1594763463:4'),
(293, 64, '_edit_last', '4'),
(297, 66, '_edit_lock', '1594341370:4'),
(298, 66, '_edit_last', '4'),
(299, 67, '_edit_lock', '1594762244:4'),
(301, 67, '_edit_last', '4'),
(303, 68, '_edit_lock', '1594762458:4'),
(305, 68, '_edit_last', '4'),
(307, 69, '_edit_lock', '1594762163:4'),
(309, 69, '_edit_last', '4'),
(311, 70, '_edit_lock', '1594763025:4'),
(313, 70, '_edit_last', '4'),
(316, 28, '_wp_old_slug', 'about-mg'),
(325, 68, '_wp_old_slug', 'living-with-mg-for-sometime'),
(328, 67, '_wp_old_slug', 'how-to-cope'),
(331, 70, '_wp_old_slug', 'real-stories-victor'),
(334, 68, '_wp_old_slug', 'living-with-mg-for-some-time'),
(337, 69, '_wp_old_slug', '14-ways-to-outsmart-summer'),
(340, 64, '_wp_old_slug', 'leah-profile'),
(343, 63, '_wp_old_slug', 'kathy-profile'),
(346, 60, '_wp_old_slug', 'mg-friendly-smoothies'),
(349, 59, '_wp_old_slug', 'the-tools-neurologists-use'),
(354, 58, '_yoast_wpseo_primary_category', '10'),
(359, 59, '_yoast_wpseo_primary_category', '3'),
(362, 60, '_yoast_wpseo_primary_category', '8'),
(365, 63, '_yoast_wpseo_primary_category', '9'),
(368, 64, '_yoast_wpseo_primary_category', '9'),
(371, 70, '_yoast_wpseo_primary_category', '9'),
(374, 67, '_yoast_wpseo_primary_category', '3'),
(377, 68, '_yoast_wpseo_primary_category', '3'),
(380, 69, '_yoast_wpseo_primary_category', '10'),
(384, 28, '_wp_old_slug', 'about-mg-article'),
(390, 77, '_wp_attached_file', '2020/06/illuminate-recap-featured-image.jpg'),
(391, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:865;s:6:\"height\";i:486;s:4:\"file\";s:43:\"2020/06/illuminate-recap-featured-image.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:43:\"illuminate-recap-featured-image-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:43:\"illuminate-recap-featured-image-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:43:\"illuminate-recap-featured-image-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(393, 58, '_thumbnail_id', '77'),
(395, 58, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(396, 58, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(397, 58, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg'),
(398, 58, '_yoast_wpseo_opengraph-image-id', '77'),
(399, 58, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(400, 58, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(401, 58, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg'),
(402, 58, '_yoast_wpseo_twitter-image-id', '77'),
(403, 58, '_yoast_wpseo_content_score', '30'),
(404, 78, '_wp_attached_file', '2020/06/doctor-tools-about-mg.png'),
(405, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:466;s:6:\"height\";i:327;s:4:\"file\";s:33:\"2020/06/doctor-tools-about-mg.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"doctor-tools-about-mg-300x211.png\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"doctor-tools-about-mg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(407, 59, '_thumbnail_id', '78'),
(409, 59, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(410, 59, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(411, 59, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png'),
(412, 59, '_yoast_wpseo_opengraph-image-id', '78'),
(413, 59, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(414, 59, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(415, 59, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png'),
(416, 59, '_yoast_wpseo_twitter-image-id', '78'),
(417, 59, '_yoast_wpseo_content_score', '30'),
(418, 79, '_wp_attached_file', '2020/07/outsmart-summer.jpg'),
(419, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:375;s:6:\"height\";i:375;s:4:\"file\";s:27:\"2020/07/outsmart-summer.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"outsmart-summer-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"outsmart-summer-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(421, 69, '_thumbnail_id', '79'),
(423, 69, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(424, 69, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(425, 69, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg'),
(426, 69, '_yoast_wpseo_opengraph-image-id', '79'),
(427, 69, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(428, 69, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(429, 69, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg'),
(430, 69, '_yoast_wpseo_twitter-image-id', '79'),
(431, 69, '_yoast_wpseo_content_score', '30'),
(432, 80, '_wp_attached_file', '2020/07/newly-diagnosed.jpg'),
(433, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:416;s:6:\"height\";i:367;s:4:\"file\";s:27:\"2020/07/newly-diagnosed.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"newly-diagnosed-300x265.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:265;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"newly-diagnosed-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(435, 67, '_thumbnail_id', '80'),
(437, 67, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(438, 67, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(439, 67, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg'),
(440, 67, '_yoast_wpseo_opengraph-image-id', '80'),
(441, 67, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(442, 67, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(443, 67, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg'),
(444, 67, '_yoast_wpseo_twitter-image-id', '80'),
(445, 67, '_yoast_wpseo_content_score', '30'),
(446, 81, '_wp_attached_file', '2020/07/sometimes-mg.jpg'),
(447, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:330;s:4:\"file\";s:24:\"2020/07/sometimes-mg.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"sometimes-mg-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"sometimes-mg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(449, 68, '_thumbnail_id', '81'),
(451, 68, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(452, 68, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(453, 68, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg'),
(454, 68, '_yoast_wpseo_opengraph-image-id', '81'),
(455, 68, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(456, 68, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(457, 68, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg'),
(458, 68, '_yoast_wpseo_twitter-image-id', '81'),
(459, 68, '_yoast_wpseo_content_score', '30'),
(461, 82, '_wp_attached_file', '2020/06/friendly-smoothies.jpg'),
(462, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:638;s:6:\"height\";i:330;s:4:\"file\";s:30:\"2020/06/friendly-smoothies.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"friendly-smoothies-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"friendly-smoothies-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(464, 60, '_thumbnail_id', '82'),
(466, 60, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(467, 60, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(468, 60, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg'),
(469, 60, '_yoast_wpseo_opengraph-image-id', '82'),
(470, 60, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(471, 60, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(472, 60, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg'),
(473, 60, '_yoast_wpseo_twitter-image-id', '82'),
(474, 60, '_yoast_wpseo_content_score', '30'),
(475, 83, '_wp_attached_file', '2020/07/victor-profile.jpg'),
(476, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:375;s:6:\"height\";i:416;s:4:\"file\";s:26:\"2020/07/victor-profile.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"victor-profile-270x300.jpg\";s:5:\"width\";i:270;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"victor-profile-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(478, 70, '_thumbnail_id', '83'),
(480, 70, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(481, 70, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(482, 70, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg'),
(483, 70, '_yoast_wpseo_opengraph-image-id', '83'),
(484, 70, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(485, 70, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(486, 70, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg'),
(487, 70, '_yoast_wpseo_twitter-image-id', '83'),
(488, 70, '_yoast_wpseo_content_score', '30'),
(490, 84, '_wp_attached_file', '2020/06/leah-profile-real-story.jpg'),
(491, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:464;s:6:\"height\";i:360;s:4:\"file\";s:35:\"2020/06/leah-profile-real-story.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"leah-profile-real-story-300x233.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:233;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"leah-profile-real-story-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(493, 64, '_thumbnail_id', '84'),
(495, 64, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(496, 64, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(497, 64, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg'),
(498, 64, '_yoast_wpseo_opengraph-image-id', '84'),
(499, 64, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(500, 64, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(501, 64, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg'),
(502, 64, '_yoast_wpseo_twitter-image-id', '84'),
(503, 64, '_yoast_wpseo_content_score', '30'),
(504, 85, '_wp_attached_file', '2020/06/kathy-profile.jpg'),
(505, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:375;s:6:\"height\";i:416;s:4:\"file\";s:25:\"2020/06/kathy-profile.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"kathy-profile-270x300.jpg\";s:5:\"width\";i:270;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"kathy-profile-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(507, 63, '_thumbnail_id', '85'),
(509, 63, '_yoast_wpseo_opengraph-title', 'Check out this article from MG United!'),
(510, 63, '_yoast_wpseo_opengraph-description', 'MG United provides clear information for those with myasthenia gravis.'),
(511, 63, '_yoast_wpseo_opengraph-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg'),
(512, 63, '_yoast_wpseo_opengraph-image-id', '85'),
(513, 63, '_yoast_wpseo_twitter-title', 'Check out this article from MG United!'),
(514, 63, '_yoast_wpseo_twitter-description', 'MG United provides clear information for those with myasthenia gravis.'),
(515, 63, '_yoast_wpseo_twitter-image', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg'),
(516, 63, '_yoast_wpseo_twitter-image-id', '85'),
(517, 63, '_yoast_wpseo_content_score', '30'),
(523, 27, '_encloseme', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3, 1, '2020-04-28 20:28:50', '2020-04-28 20:28:50', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: https://mguniteddev.wpengine.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-04-28 20:28:50', '2020-04-28 20:28:50', '', 0, 'https://mguniteddev.wpengine.com/?page_id=3', 0, 'page', '', 0),
(9, 3, '2020-04-27 06:43:39', '2020-04-27 06:43:39', '', 'Sign up', '', 'publish', 'closed', 'closed', '', 'sign-up', '', '', '2020-05-24 22:06:30', '2020-05-24 22:06:30', '', 0, 'https://mgunited.test/?page_id=5', 0, 'page', '', 0),
(10, 3, '2020-04-27 06:45:05', '2020-04-27 06:45:05', '<!-- wp:paragraph -->\n<p>test content</p>\n<!-- /wp:paragraph -->', 'United for You', '', 'publish', 'closed', 'closed', '', 'united-for-you', '', '', '2020-05-21 15:15:10', '2020-05-21 15:15:10', '', 0, 'https://mgunited.test/?page_id=7', 0, 'page', '', 0),
(11, 3, '2020-04-27 06:45:28', '2020-04-27 06:45:28', '<!-- wp:paragraph -->\n<p>about</p>\n<!-- /wp:paragraph -->', 'Our Mission', '', 'publish', 'closed', 'closed', '', 'mission', '', '', '2020-07-07 14:40:26', '2020-07-07 14:40:26', '', 0, 'https://mgunited.test/?page_id=9', 0, 'page', '', 0),
(12, 3, '2020-04-27 06:46:57', '2020-04-27 06:46:57', '', 'Documentary', '', 'publish', 'closed', 'closed', '', 'documentary', '', '', '2020-05-21 15:33:47', '2020-05-21 15:33:47', '', 0, 'https://mgunited.test/?page_id=11', 0, 'page', '', 0),
(14, 3, '2020-04-27 06:47:37', '2020-04-27 06:47:37', '', 'Living with MG', '', 'publish', 'closed', 'closed', '', 'about-mg', '', '', '2020-07-07 14:38:54', '2020-07-07 14:38:54', '', 0, 'https://mgunited.test/?page_id=14', 0, 'page', '', 0),
(16, 3, '2020-04-27 06:48:29', '2020-04-27 06:48:29', '', 'Real Stories', '', 'publish', 'closed', 'closed', '', 'real-stories', '', '', '2020-05-21 15:32:35', '2020-05-21 15:32:35', '', 0, 'https://mgunited.test/?page_id=16', 0, 'page', '', 0),
(17, 2, '2020-04-29 14:35:07', '2020-04-29 14:35:07', ' ', '', '', 'publish', 'closed', 'closed', '', '17', '', '', '2020-04-29 14:35:07', '2020-04-29 14:35:07', '', 0, 'https://mguniteddev.wpengine.com/?p=17', 4, 'nav_menu_item', '', 0),
(18, 2, '2020-04-29 14:35:07', '2020-04-29 14:35:07', ' ', '', '', 'publish', 'closed', 'closed', '', '18', '', '', '2020-04-29 14:35:07', '2020-04-29 14:35:07', '', 0, 'https://mguniteddev.wpengine.com/?p=18', 1, 'nav_menu_item', '', 0),
(19, 2, '2020-04-29 14:35:07', '2020-04-29 14:35:07', '', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2020-04-29 14:35:07', '2020-04-29 14:35:07', '', 0, 'https://mguniteddev.wpengine.com/?p=19', 2, 'nav_menu_item', '', 0),
(20, 2, '2020-04-29 14:35:08', '2020-04-29 14:35:08', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2020-04-29 14:35:08', '2020-04-29 14:35:08', '', 0, 'https://mguniteddev.wpengine.com/?p=20', 3, 'nav_menu_item', '', 0),
(24, 3, '2020-05-07 18:27:16', '2020-05-07 18:27:16', '', 'Technical Difficulties', '', 'publish', 'closed', 'closed', '', 'error', '', '', '2020-05-07 18:27:29', '2020-05-07 18:27:29', '', 0, 'https://mguniteddev.wpengine.com/?page_id=24', 0, 'page', '', 0),
(27, 4, '2020-05-15 18:33:20', '2020-05-15 18:33:20', '', 'Treatment Goals', '', 'publish', 'open', 'open', '', 'treatment-goals', '', '', '2020-07-15 00:36:52', '2020-07-15 00:36:52', '', 0, 'https://mguniteddev.wpengine.com/?p=27', 0, 'post', '', 0),
(28, 4, '2020-05-15 18:33:59', '2020-05-15 18:33:59', '', 'About MG', '', 'publish', 'open', 'open', '', 'what-is-mg', '', '', '2020-07-10 00:33:17', '2020-07-10 00:33:17', '', 0, 'https://mguniteddev.wpengine.com/?p=28', 0, 'post', '', 0),
(29, 4, '2020-05-15 18:35:41', '2020-05-15 18:35:41', '', 'Emotional Health', '', 'publish', 'open', 'open', '', 'emotional-health', '', '', '2020-05-24 22:03:14', '2020-05-24 22:03:14', '', 0, 'https://mguniteddev.wpengine.com/?p=29', 0, 'post', '', 0),
(30, 4, '2020-05-15 18:36:36', '2020-05-15 18:36:36', '', 'Crisis 411', '', 'publish', 'open', 'open', '', 'crisis-planning', '', '', '2020-07-10 00:34:02', '2020-07-10 00:34:02', '', 0, 'https://mguniteddev.wpengine.com/?p=30', 0, 'post', '', 0),
(31, 4, '2020-05-15 18:37:53', '2020-05-15 18:37:53', '', 'My MG Sole', '', 'publish', 'open', 'open', '', 'my-mg-sole', '', '', '2020-05-24 22:01:42', '2020-05-24 22:01:42', '', 0, 'https://mguniteddev.wpengine.com/?p=31', 0, 'post', '', 0),
(34, 4, '2020-05-18 18:36:53', '2020-05-18 18:36:53', '', 'My MG Sole', '', 'inherit', 'open', 'closed', '', 'square-image-mg-sole', '', '', '2020-05-18 18:37:12', '2020-05-18 18:37:12', '', 31, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 4, '2020-05-18 18:40:24', '2020-05-18 18:40:24', '', 'Crisis Planning', '', 'inherit', 'open', 'closed', '', 'square-image-crisis', '', '', '2020-05-18 18:40:36', '2020-05-18 18:40:36', '', 30, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 4, '2020-05-18 18:45:23', '2020-05-18 18:45:23', '', 'Emotional Health', '', 'inherit', 'open', 'closed', '', 'square-image-emotional', '', '', '2020-05-18 18:45:37', '2020-05-18 18:45:37', '', 29, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional.jpg', 0, 'attachment', 'image/jpeg', 0),
(37, 4, '2020-05-18 18:48:00', '2020-05-18 18:48:00', '', 'About MG', '', 'inherit', 'open', 'closed', '', 'square-image', '', '', '2020-05-18 18:48:14', '2020-05-18 18:48:14', '', 28, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image.jpg', 0, 'attachment', 'image/jpeg', 0),
(38, 4, '2020-05-18 18:52:44', '2020-05-18 18:52:44', '', 'Treatment Goals', '', 'inherit', 'open', 'closed', '', 'square-image-goals', '', '', '2020-05-18 18:54:21', '2020-05-18 18:54:21', '', 27, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 4, '2020-05-21 16:30:35', '2020-05-21 16:30:35', '', 'mg-united-icon', '', 'inherit', 'open', 'closed', '', 'mg-united-icon', '', '', '2020-05-21 16:30:35', '2020-05-21 16:30:35', '', 0, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/mg-united-icon.png', 0, 'attachment', 'image/png', 0),
(46, 4, '2020-05-21 16:30:40', '2020-05-21 16:30:40', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/cropped-mg-united-icon.png', 'cropped-mg-united-icon.png', '', 'inherit', 'open', 'closed', '', 'cropped-mg-united-icon-png', '', '', '2020-05-21 16:30:40', '2020-05-21 16:30:40', '', 0, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/cropped-mg-united-icon.png', 0, 'attachment', 'image/png', 0),
(58, 4, '2020-06-23 14:16:22', '2020-06-23 14:16:22', '', 'MG Illuminate Recap', '', 'publish', 'open', 'open', '', 'mg-illuminate-recap', '', '', '2020-07-14 21:25:23', '2020-07-14 21:25:23', '', 0, 'https://mguniteddev.wpengine.com/?p=58', 0, 'post', '', 0),
(59, 4, '2020-06-26 14:12:27', '2020-06-26 14:12:27', '', 'Doctor Tools You Can Use', '', 'publish', 'open', 'open', '', 'tools-neuros-use', '', '', '2020-07-14 21:28:03', '2020-07-14 21:28:03', '', 0, 'https://mguniteddev.wpengine.com/?p=59', 0, 'post', '', 0),
(60, 4, '2020-06-26 14:13:05', '2020-06-26 14:13:05', '', '5 MG-Friendly Smoothies', '', 'publish', 'open', 'open', '', '5-mg-friendly-smoothies', '', '', '2020-07-14 21:36:21', '2020-07-14 21:36:21', '', 0, 'https://mguniteddev.wpengine.com/?p=60', 0, 'post', '', 0),
(63, 4, '2020-06-30 15:14:31', '2020-06-30 15:14:31', '', 'Patient Profile - Kathy & Diane', '', 'publish', 'open', 'open', '', 'kathy-and-diane', '', '', '2020-07-14 21:54:15', '2020-07-14 21:54:15', '', 0, 'https://mguniteddev.wpengine.com/?p=63', 0, 'post', '', 0),
(64, 4, '2020-06-30 15:14:44', '2020-06-30 15:14:44', '', 'Patient Profile - Leah', '', 'publish', 'open', 'open', '', 'leah-gaitan-diaz', '', '', '2020-07-14 21:51:02', '2020-07-14 21:51:02', '', 0, 'https://mguniteddev.wpengine.com/?p=64', 0, 'post', '', 0),
(66, 4, '2020-07-07 14:23:13', '2020-07-07 14:23:13', '', 'Life with MG', '', 'publish', 'closed', 'closed', '', 'life-with-mg', '', '', '2020-07-07 14:23:14', '2020-07-07 14:23:14', '', 0, 'https://mguniteddev.wpengine.com/?page_id=66', 0, 'page', '', 0),
(67, 4, '2020-07-07 14:24:01', '2020-07-07 14:24:01', '', 'Newly Diagnosed', '', 'publish', 'open', 'open', '', 'newly-diagnosed', '', '', '2020-07-14 21:30:43', '2020-07-14 21:30:43', '', 0, 'https://mguniteddev.wpengine.com/?p=67', 0, 'post', '', 0),
(68, 4, '2020-07-07 14:24:26', '2020-07-07 14:24:26', '', 'Living with MG for Some Time', '', 'publish', 'open', 'open', '', 'been-living-with-mg-for-some-time', '', '', '2020-07-14 21:34:18', '2020-07-14 21:34:18', '', 0, 'https://mguniteddev.wpengine.com/?p=68', 0, 'post', '', 0),
(69, 4, '2020-07-07 14:24:55', '2020-07-07 14:24:55', '', 'Beating the Summer Heat', '', 'publish', 'open', 'open', '', 'summer-heat-tips', '', '', '2020-07-14 21:29:23', '2020-07-14 21:29:23', '', 0, 'https://mguniteddev.wpengine.com/?p=69', 0, 'post', '', 0),
(70, 4, '2020-07-07 14:26:11', '2020-07-07 14:26:11', '', 'Patient Profile - Victor & Iris', '', 'publish', 'open', 'open', '', 'victor-and-iris-yipp', '', '', '2020-07-14 21:43:44', '2020-07-14 21:43:44', '', 0, 'https://mguniteddev.wpengine.com/?p=70', 0, 'post', '', 0),
(71, 4, '2020-07-07 14:39:11', '2020-07-07 14:39:11', '', 'Living with MG', '', 'inherit', 'closed', 'closed', '', '14-autosave-v1', '', '', '2020-07-07 14:39:11', '2020-07-07 14:39:11', '', 14, 'https://mguniteddev.wpengine.com/14-autosave-v1/', 0, 'revision', '', 0),
(72, 4, '2020-07-07 14:40:28', '2020-07-07 14:40:28', '<!-- wp:paragraph -->\n<p>about</p>\n<!-- /wp:paragraph -->', 'Our Mission', '', 'inherit', 'closed', 'closed', '', '11-autosave-v1', '', '', '2020-07-07 14:40:28', '2020-07-07 14:40:28', '', 11, 'https://mguniteddev.wpengine.com/11-autosave-v1/', 0, 'revision', '', 0),
(76, 4, '2020-07-10 00:33:45', '2020-07-10 00:33:45', '', 'About MG', '', 'inherit', 'closed', 'closed', '', '28-autosave-v1', '', '', '2020-07-10 00:33:45', '2020-07-10 00:33:45', '', 28, 'https://mguniteddev.wpengine.com/uncategorized/28-autosave-v1/', 0, 'revision', '', 0),
(77, 4, '2020-07-14 21:24:53', '2020-07-14 21:24:53', '', 'illuminate-recap-featured-image', '', 'inherit', 'open', 'closed', '', 'illuminate-recap-featured-image', '', '', '2020-07-14 21:24:53', '2020-07-14 21:24:53', '', 58, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 4, '2020-07-14 21:27:40', '2020-07-14 21:27:40', '', 'doctor-tools-about-mg', '', 'inherit', 'open', 'closed', '', 'doctor-tools-about-mg', '', '', '2020-07-14 21:27:40', '2020-07-14 21:27:40', '', 59, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png', 0, 'attachment', 'image/png', 0),
(79, 4, '2020-07-14 21:29:08', '2020-07-14 21:29:08', '', 'outsmart-summer', '', 'inherit', 'open', 'closed', '', 'outsmart-summer', '', '', '2020-07-14 21:29:08', '2020-07-14 21:29:08', '', 69, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 4, '2020-07-14 21:30:17', '2020-07-14 21:30:17', '', 'newly-diagnosed', '', 'inherit', 'open', 'closed', '', 'newly-diagnosed-2', '', '', '2020-07-14 21:30:17', '2020-07-14 21:30:17', '', 67, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 4, '2020-07-14 21:33:18', '2020-07-14 21:33:18', '', 'sometimes-mg', '', 'inherit', 'open', 'closed', '', 'sometimes-mg', '', '', '2020-07-14 21:33:18', '2020-07-14 21:33:18', '', 68, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 4, '2020-07-14 21:35:07', '2020-07-14 21:35:07', '', 'friendly-smoothies', '', 'inherit', 'open', 'closed', '', 'friendly-smoothies', '', '', '2020-07-14 21:35:07', '2020-07-14 21:35:07', '', 60, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg', 0, 'attachment', 'image/jpeg', 0),
(83, 4, '2020-07-14 21:38:33', '2020-07-14 21:38:33', '', 'victor-profile', '', 'inherit', 'open', 'closed', '', 'victor-profile', '', '', '2020-07-14 21:38:33', '2020-07-14 21:38:33', '', 70, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 4, '2020-07-14 21:50:47', '2020-07-14 21:50:47', '', 'leah-profile-real-story', '', 'inherit', 'open', 'closed', '', 'leah-profile-real-story', '', '', '2020-07-14 21:50:47', '2020-07-14 21:50:47', '', 64, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg', 0, 'attachment', 'image/jpeg', 0),
(85, 4, '2020-07-14 21:53:58', '2020-07-14 21:53:58', '', 'kathy-profile', '', 'inherit', 'open', 'closed', '', 'kathy-profile', '', '', '2020-07-14 21:53:58', '2020-07-14 21:53:58', '', 63, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 4, '2020-07-24 19:20:35', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-07-24 19:20:35', '0000-00-00 00:00:00', '', 0, 'https://mguniteddev.wpengine.com/?p=86', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_redirection_404`
--

CREATE TABLE `wp_redirection_404` (
  `id` int(11) UNSIGNED NOT NULL,
  `created` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `agent` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referrer` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_redirection_groups`
--

CREATE TABLE `wp_redirection_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tracking` int(11) NOT NULL DEFAULT '1',
  `module_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'enabled',
  `position` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_redirection_groups`
--

INSERT INTO `wp_redirection_groups` (`id`, `name`, `tracking`, `module_id`, `status`, `position`) VALUES
(1, 'Redirections', 1, 1, 'enabled', 0),
(2, 'Modified Posts', 1, 1, 'enabled', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_redirection_items`
--

CREATE TABLE `wp_redirection_items` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `match_url` varchar(2000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `match_data` text COLLATE utf8mb4_unicode_520_ci,
  `regex` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `position` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `last_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_access` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'enabled',
  `action_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action_code` int(11) UNSIGNED NOT NULL,
  `action_data` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `match_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_redirection_items`
--

INSERT INTO `wp_redirection_items` (`id`, `url`, `match_url`, `match_data`, `regex`, `position`, `last_count`, `last_access`, `group_id`, `status`, `action_type`, `action_code`, `action_data`, `match_type`, `title`) VALUES
(12, '^/about/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 0, 7, '2020-07-14 19:44:16', 1, 'enabled', 'url', 301, '/mission', 'url', ''),
(13, '^/living-with-mg/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 1, 6, '2020-07-14 20:49:09', 1, 'enabled', 'url', 301, '/about-mg', 'url', ''),
(14, '^/community-project/June-2020/my-mg-sole/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 2, 1, '2020-07-14 20:48:09', 1, 'enabled', 'url', 301, '/community-project/my-mg-sole/', 'url', ''),
(15, '^/disease-and-treatment/June-2020/about-mg/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 3, 2, '2020-07-14 20:47:39', 1, 'enabled', 'url', 301, '/disease-and-treatment/what-is-mg/', 'url', ''),
(16, '^/disease-and-treatment/June-2020/treatment-goals/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 4, 1, '2020-07-14 20:45:09', 1, 'enabled', 'url', 301, '/disease-and-treatment/treatment-goals', 'url', ''),
(17, '^/emotional-support/June-2020/emotional-health/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 5, 1, '2020-07-14 20:45:43', 1, 'enabled', 'url', 301, '/emotional-support/emotional-health', 'url', ''),
(18, '^/disease-and-treatment/June-2020/crisis-planning/?$', 'regex', '{\"source\":{\"flag_regex\":true}}', 1, 6, 1, '2020-07-14 20:46:35', 1, 'enabled', 'url', 301, '/disease-and-treatment/crisis-planning', 'url', '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_redirection_logs`
--

CREATE TABLE `wp_redirection_logs` (
  `id` int(11) UNSIGNED NOT NULL,
  `created` datetime NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `sent_to` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `agent` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `referrer` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `redirection_id` int(11) UNSIGNED DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `module_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Menu 1', 'menu-1', 0),
(3, 'Disease &amp; Treatment', 'disease-and-treatment', 0),
(4, 'Emotional Support', 'emotional-support', 0),
(5, 'Community Project', 'community-project', 0),
(8, 'Eating &amp; MG', 'eating-and-mg', 0),
(9, 'Real Stories', 'real-stories', 0),
(10, 'Life with MG', 'life-with-mg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(17, 2, 0),
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(27, 3, 0),
(28, 3, 0),
(29, 4, 0),
(30, 3, 0),
(31, 5, 0),
(58, 10, 0),
(59, 3, 0),
(60, 8, 0),
(63, 9, 0),
(64, 9, 0),
(67, 3, 0),
(68, 3, 0),
(69, 10, 0),
(70, 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'category', '', 0, 6),
(4, 4, 'category', '', 0, 1),
(5, 5, 'category', '', 0, 1),
(8, 8, 'category', '', 0, 1),
(9, 9, 'category', '', 0, 3),
(10, 10, 'category', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'wpengine'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', 'This is the \"wpengine\" admin user that our staff uses to gain access to your admin area to provide support and troubleshooting. It can only be accessed by a button in our secure log that auto generates a password and dumps that password after the staff member has logged in. We have taken extreme measures to ensure that our own user is not going to be misused to harm any of our clients sites.'),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 2, 'nickname', 'mgunitedstage'),
(17, 2, 'first_name', ''),
(18, 2, 'last_name', ''),
(19, 2, 'description', ''),
(20, 2, 'rich_editing', 'true'),
(21, 2, 'syntax_highlighting', 'true'),
(22, 2, 'comment_shortcuts', 'false'),
(23, 2, 'admin_color', 'fresh'),
(24, 2, 'use_ssl', '0'),
(25, 2, 'show_admin_bar_front', 'true'),
(26, 2, 'locale', ''),
(27, 2, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(28, 2, 'wp_user_level', '10'),
(29, 2, 'default_password_nag', ''),
(30, 2, 'session_tokens', 'a:4:{s:64:\"47f4d0fd659578318ec6c378a9327f173d7f55ed4dd62e42843ca64b331e52a7\";a:4:{s:10:\"expiration\";i:1588964839;s:2:\"ip\";s:12:\"98.253.65.22\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588792039;}s:64:\"49327692e138f6474a52e5f6d0f82f14be5ee8f731e00feb7f2b20c5e9f18822\";a:4:{s:10:\"expiration\";i:1588971452;s:2:\"ip\";s:12:\"98.253.65.22\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588798652;}s:64:\"9bc7ff962e5a2c767a3ce1438871c2b5ba68f3bd5ba20323a35455e99a8a3e87\";a:4:{s:10:\"expiration\";i:1588972422;s:2:\"ip\";s:12:\"98.253.65.22\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588799622;}s:64:\"2be0ff1e8e1090d6ec09cf9f87d6f17d90812bae419bb7728a4aefc44b8e4dfc\";a:4:{s:10:\"expiration\";i:1588973398;s:2:\"ip\";s:12:\"98.253.65.22\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588800598;}}'),
(31, 2, 'wp_dashboard_quick_press_last_post_id', '21'),
(32, 2, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"98.253.65.0\";}'),
(33, 3, 'nickname', 'wenglish'),
(34, 3, 'first_name', ''),
(35, 3, 'last_name', ''),
(36, 3, 'description', ''),
(37, 3, 'rich_editing', 'true'),
(38, 3, 'syntax_highlighting', 'true'),
(39, 3, 'comment_shortcuts', 'false'),
(40, 3, 'admin_color', 'fresh'),
(41, 3, 'use_ssl', '0'),
(42, 3, 'show_admin_bar_front', 'true'),
(43, 3, 'locale', ''),
(44, 3, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(45, 3, 'wp_user_level', '10'),
(46, 3, 'dismissed_wp_pointers', ''),
(47, 4, 'nickname', 'rcasanova'),
(48, 4, 'first_name', 'Ricky'),
(49, 4, 'last_name', 'Casanova'),
(50, 4, 'description', ''),
(51, 4, 'rich_editing', 'true'),
(52, 4, 'syntax_highlighting', 'true'),
(53, 4, 'comment_shortcuts', 'false'),
(54, 4, 'admin_color', 'fresh'),
(55, 4, 'use_ssl', '0'),
(56, 4, 'show_admin_bar_front', 'true'),
(57, 4, 'locale', ''),
(58, 4, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(59, 4, 'wp_user_level', '10'),
(60, 4, 'dismissed_wp_pointers', ''),
(63, 4, 'wp_dashboard_quick_press_last_post_id', '86'),
(64, 4, 'community-events-location', 'a:1:{s:2:\"ip\";s:12:\"69.245.207.0\";}'),
(65, 3, 'session_tokens', 'a:1:{s:64:\"2f90da4c2f84e8fb5efe53bf0787aa6e9dc17e95e1ed939dfcd52f2bc8f7da80\";a:4:{s:10:\"expiration\";i:1590863800;s:2:\"ip\";s:12:\"98.253.65.22\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36\";s:5:\"login\";i:1590691000;}}'),
(66, 3, 'wp_dashboard_quick_press_last_post_id', '55'),
(67, 3, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"98.253.65.0\";}'),
(68, 4, 'wp_yoast_notifications', 'a:1:{i:0;a:2:{s:7:\"message\";s:322:\"The configuration wizard helps you to easily configure your site to have the optimal SEO settings.<br/>We have detected that you have not finished this wizard yet, so we recommend you to <a href=\"https://mguniteddev.wpengine.com/wp-admin/?page=wpseo_configurator\">start the configuration wizard to configure Yoast SEO</a>.\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:31:\"wpseo-dismiss-onboarding-notice\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"4\";s:10:\"user_login\";s:9:\"rcasanova\";s:9:\"user_pass\";s:34:\"$P$BfUubvAG0ajPko6CGaWgxr80qSYMDW/\";s:13:\"user_nicename\";s:9:\"rcasanova\";s:10:\"user_email\";s:24:\"rcasanova@closerlook.com\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2020-05-06 19:10:50\";s:19:\"user_activation_key\";s:45:\"1588792251:$P$B.b4MiJk4xnU.EOzE.XUnwcChpKYG8/\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:14:\"Ricky Casanova\";}s:2:\"ID\";i:4;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:63:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.8;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(69, 4, 'wp_user-settings', 'libraryContent=browse&mfold=o'),
(70, 4, 'wp_user-settings-time', '1594132806'),
(71, 4, 'closedpostboxes_post', 'a:0:{}'),
(72, 4, 'metaboxhidden_post', 'a:0:{}'),
(74, 5, 'nickname', 'tkneedy'),
(75, 5, 'first_name', 'Todd'),
(76, 5, 'last_name', 'Kneedy'),
(77, 5, 'description', ''),
(78, 5, 'rich_editing', 'true'),
(79, 5, 'syntax_highlighting', 'true'),
(80, 5, 'comment_shortcuts', 'false'),
(81, 5, 'admin_color', 'fresh'),
(82, 5, 'use_ssl', '0'),
(83, 5, 'show_admin_bar_front', 'true'),
(84, 5, 'locale', ''),
(85, 5, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(86, 5, 'wp_user_level', '10'),
(87, 5, '_yoast_wpseo_profile_updated', '1590179420'),
(88, 5, 'dismissed_wp_pointers', ''),
(89, 5, 'facebook', ''),
(90, 5, 'instagram', ''),
(91, 5, 'linkedin', ''),
(92, 5, 'myspace', ''),
(93, 5, 'pinterest', ''),
(94, 5, 'soundcloud', ''),
(95, 5, 'tumblr', ''),
(96, 5, 'twitter', ''),
(97, 5, 'youtube', ''),
(98, 5, 'wikipedia', ''),
(100, 5, 'wp_dashboard_quick_press_last_post_id', '49'),
(101, 5, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"24.148.82.0\";}'),
(103, 6, 'nickname', 'zortega'),
(104, 6, 'first_name', 'Zulema'),
(105, 6, 'last_name', 'Ortega'),
(106, 6, 'description', ''),
(107, 6, 'rich_editing', 'true'),
(108, 6, 'syntax_highlighting', 'true'),
(109, 6, 'comment_shortcuts', 'false'),
(110, 6, 'admin_color', 'coffee'),
(111, 6, 'use_ssl', '0'),
(112, 6, 'show_admin_bar_front', 'true'),
(113, 6, 'locale', ''),
(114, 6, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(115, 6, 'wp_user_level', '10'),
(116, 6, '_yoast_wpseo_profile_updated', '1590346739'),
(117, 6, 'dismissed_wp_pointers', ''),
(118, 6, 'facebook', ''),
(119, 6, 'instagram', ''),
(120, 6, 'linkedin', ''),
(121, 6, 'myspace', ''),
(122, 6, 'pinterest', ''),
(123, 6, 'soundcloud', ''),
(124, 6, 'tumblr', ''),
(125, 6, 'twitter', ''),
(126, 6, 'youtube', ''),
(127, 6, 'wikipedia', ''),
(128, 6, 'wpseo_title', ''),
(129, 6, 'wpseo_metadesc', ''),
(130, 6, 'wpseo_noindex_author', ''),
(131, 6, 'wpseo_content_analysis_disable', ''),
(132, 6, 'wpseo_keyword_analysis_disable', ''),
(134, 6, 'wp_dashboard_quick_press_last_post_id', '65'),
(135, 6, 'community-events-location', 'a:1:{s:2:\"ip\";s:12:\"70.238.249.0\";}'),
(142, 4, 'session_tokens', 'a:1:{s:64:\"1eaa70886c3cf80ece18447aa33bc40abaad760a09736932784ff376d296f336\";a:4:{s:10:\"expiration\";i:1595791234;s:2:\"ip\";s:13:\"69.245.207.25\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36\";s:5:\"login\";i:1595618434;}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'wpengine', '$P$Bx6m.lX5M4b/GVQaXph4.h0FYrOp.R.', 'wpengine', 'bitbucket@wpengine.com', 'https://wpengine.com', '2020-04-28 20:28:50', '', 0, 'wpengine'),
(2, 'mgunitedstage', '$P$BTo4Ml1Z/Ys/Gikvn9frE5pJ22XQEz1', 'mgunitedstage', 'techmgr@closerlook.com', 'https://mguniteddev.wpengine.com', '2020-04-28 20:29:19', '', 0, 'mgunitedstage'),
(3, 'wenglish', '$P$BY00RM4vNgoOw77V48StgphTlDtQvC1', 'wenglish', 'wenglish@closerlook.com', '', '2020-04-29 14:35:06', '', 0, 'wenglish'),
(4, 'rcasanova', '$P$BfUubvAG0ajPko6CGaWgxr80qSYMDW/', 'rcasanova', 'rcasanova@closerlook.com', '', '2020-05-06 19:10:50', '1588792251:$P$B.b4MiJk4xnU.EOzE.XUnwcChpKYG8/', 0, 'Ricky Casanova'),
(5, 'tkneedy', '$P$BTGN/Z4E.wsxNyDN.0VUD3OYDpi2o81', 'tkneedy', 'tkneedy@closerlook.com', '', '2020-05-22 20:30:20', '1590179421:$P$BpGCV6RbEy4ND8I3N6iDzQ2BHg3Pf40', 0, 'Todd Kneedy'),
(6, 'zortiz', '$P$BGMx08chhRgm7RF8MlWiBfHuQ3CUDe/', 'zortiz', 'zortiz@closerlook.com', '', '2020-05-24 16:07:41', '1590336462:$P$BRh8DpTpfhMU42BRPGESvZSaTIpl8V.', 0, 'Zulema');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_indexable`
--

CREATE TABLE `wp_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL,
  `permalink` longtext COLLATE utf8mb4_unicode_520_ci,
  `permalink_hash` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `object_id` int(11) UNSIGNED DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `author_id` int(11) UNSIGNED DEFAULT NULL,
  `post_parent` int(11) UNSIGNED DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_520_ci,
  `post_status` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT '0',
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_520_ci,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT '0',
  `is_robots_noindex` tinyint(1) DEFAULT '0',
  `is_robots_nofollow` tinyint(1) DEFAULT '0',
  `is_robots_noarchive` tinyint(1) DEFAULT '0',
  `is_robots_noimageindex` tinyint(1) DEFAULT '0',
  `is_robots_nosnippet` tinyint(1) DEFAULT '0',
  `twitter_title` text COLLATE utf8mb4_unicode_520_ci,
  `twitter_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_title` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  `language` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_indexable`
--

INSERT INTO `wp_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`) VALUES
(1, 'https://mguniteddev.wpengine.com/author/wpengine/', '49:bf6066f441b2b21efeeb3e76d5ebd8be', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/9315f6ce3baebb09c86901c4497d57b7?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/9315f6ce3baebb09c86901c4497d57b7?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-10 00:31:28', 1, NULL, NULL, NULL, NULL),
(2, NULL, '45:7a034462a4728c0cf6ba01124b15ddf2', 3, 'post', 'page', 1, 0, NULL, NULL, 'Privacy Policy', 'draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(3, 'https://mguniteddev.wpengine.com/author/wenglish/', '49:d1b9d675b3b1ec2f2b6c565d1020ad11', 3, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/2869752f468acaf043f9c5074ef11dfd?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/2869752f468acaf043f9c5074ef11dfd?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-10 00:31:28', 1, NULL, NULL, NULL, NULL),
(4, 'https://mguniteddev.wpengine.com/sign-up/', '41:bb200d0363d1cab0ae030f479de91f12', 9, 'post', 'page', 3, 0, NULL, 'Sign up to join MG United.', 'Sign up', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(5, 'https://mguniteddev.wpengine.com/', '33:32ca4f6cdfc5db8359c295cd82a4ee2a', 10, 'post', 'page', 3, 0, NULL, 'Fierce allies in the daily battle for a better life with myasthenia gravis.', 'United for You', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(6, 'https://mguniteddev.wpengine.com/mission/', '41:27f6226148dab7f2b22f0cf0d816976c', 11, 'post', 'page', 3, 0, NULL, 'We’re here to provide personalized resources to help you confront your MG questions and concerns.', 'Our Mission', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(7, 'https://mguniteddev.wpengine.com/documentary/', '45:fb694dcb8ec8688e26255e6b1c1ae340', 12, 'post', 'page', 3, 0, NULL, 'Sign up to track the production of the first documentary about myasthenia gravis.', 'Documentary', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(8, 'https://mguniteddev.wpengine.com/about-mg/', '42:a41824b07422a1285fd39028ab772186', 14, 'post', 'page', 3, 0, NULL, 'Understand what to expect from your body after a myasthenia gravis diagnosis. MG definitions explained.', 'Living with MG', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(9, 'https://mguniteddev.wpengine.com/real-stories/', '46:a9ab300ba413b0aa965d28feb64d0bb9', 16, 'post', 'page', 3, 0, NULL, 'MG United members share their stories', 'Real Stories', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(10, 'https://mguniteddev.wpengine.com/error/', '39:e7d33ae3adf4c9e85f13d78fdab299e0', 24, 'post', 'page', 3, 0, NULL, NULL, 'Technical Difficulties', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-10 00:35:09', 1, NULL, NULL, NULL, NULL),
(15, 'https://mguniteddev.wpengine.com/author/rcasanova/', '50:f28e59b2d69c81761ed64b5ffa7ec7f1', 4, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/956f8b9432ad94bed6f0bec006a23fa1?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/956f8b9432ad94bed6f0bec006a23fa1?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-24 19:20:35', 1, NULL, NULL, NULL, NULL),
(19, NULL, '58:7d90aa88eef47080684168dc935f4e97', 1, 'term', 'category', NULL, NULL, NULL, NULL, 'Uncategorized', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(20, NULL, NULL, NULL, 'system-page', '404', NULL, NULL, 'Page not found %%sep%% %%sitename%%', NULL, 'Error 404: Page not found', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-24 19:20:35', 1, NULL, NULL, NULL, NULL),
(21, NULL, NULL, NULL, 'system-page', 'search-result', NULL, NULL, 'You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-24 19:20:35', 1, NULL, NULL, NULL, NULL),
(22, NULL, NULL, NULL, 'date-archive', NULL, NULL, NULL, '%%date%% %%page%% %%sep%% %%sitename%%', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:48:48', '2020-07-24 19:20:35', 1, NULL, NULL, NULL, NULL),
(23, 'https://mguniteddev.wpengine.com/author/mgunitedstage/', '54:fc36021e7b9cd09d69803cf2bac2fd33', 2, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/af3e158da08912f01d7684f7c8d0bdf0?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/af3e158da08912f01d7684f7c8d0bdf0?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-05-15 05:49:53', '2020-07-10 00:31:28', 1, NULL, NULL, NULL, NULL),
(24, 'https://mguniteddev.wpengine.com/', '22:19a468129547c40a81f8e84cbe2bad92', NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', 'Your SUPER-powered WP Engine Site', 'Home', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2020-05-15 06:13:57', '2020-06-10 17:26:55', 1, NULL, NULL, NULL, NULL),
(26, 'https://mguniteddev.wpengine.com/disease-and-treatment/treatment-goals/', '71:d535e50a7fcee8a8fef98d3ba96c7387', 27, 'post', 'post', 4, 0, NULL, 'The Importance of Setting Your Personal MG Treatment Goals', 'Treatment Goals', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg', 'MG United provides clear information for those with myasthenia gravis.', '38', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg', '38', 'set-by-user', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Goals-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Goals-scaled.jpg\",\"size\":\"full\",\"id\":\"38\",\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-05-15 18:32:21', '2020-07-15 00:36:52', 1, NULL, NULL, NULL, NULL),
(27, 'https://mguniteddev.wpengine.com/disease-and-treatment/what-is-mg/', '66:863f2e0e3ae1c218c299a13887c19875', 28, 'post', 'post', 4, 0, NULL, 'What is myasthenia gravis, and why does it happen?', 'About MG', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg', 'MG United provides clear information for those with myasthenia gravis.', '37', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg', '37', 'set-by-user', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-scaled.jpg\",\"size\":\"full\",\"id\":\"37\",\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-05-15 18:33:36', '2020-07-10 00:33:17', 1, NULL, NULL, NULL, NULL),
(28, 'https://mguniteddev.wpengine.com/emotional-support/June-2020/emotional-health/', '78:6c70992ccd6440327fde036e3842da4c', 29, 'post', 'post', 4, 0, NULL, 'How to have good emotional health and MG at the same time.', 'Emotional Health', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg', 'MG United provides clear information for those with myasthenia gravis.', '36', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg', '36', 'set-by-user', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Emotional-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunited\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Emotional-scaled.jpg\",\"size\":\"full\",\"id\":\"36\",\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-05-15 18:35:20', '2020-07-10 00:31:03', 1, NULL, NULL, NULL, NULL),
(29, 'https://mguniteddev.wpengine.com/disease-and-treatment/crisis-planning/', '71:6bc9bcce34d3e8233aa2c2fa1d05c6af', 30, 'post', 'post', 4, 0, NULL, 'Crisis 411: What you need to know about myasthenic crisis.', 'Crisis 411', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg', 'MG United provides clear information for those with myasthenia gravis.', '35', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg', '35', 'set-by-user', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Crisis-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Crisis-scaled.jpg\",\"size\":\"full\",\"id\":\"35\",\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-05-15 18:36:20', '2020-07-10 00:34:02', 1, NULL, NULL, NULL, NULL),
(30, 'https://mguniteddev.wpengine.com/community-project/June-2020/my-mg-sole/', '72:004a034dc563900d1bfc01acb3f57f71', 31, 'post', 'post', 4, 0, NULL, 'My MG Sole fights coronavirus blues with kicky art.', 'My MG Sole', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg', 'MG United provides clear information for those with myasthenia gravis.', '34', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg', '34', 'set-by-user', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-MG-Sole-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunited\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-MG-Sole-scaled.jpg\",\"size\":\"full\",\"id\":\"34\",\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-05-15 18:37:31', '2020-07-10 00:34:07', 1, NULL, NULL, NULL, NULL),
(31, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg', '91:a40b15094359ba7f06895659ac5e7e71', 34, 'post', 'attachment', 4, 31, NULL, NULL, 'My MG Sole', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg', NULL, '34', 'attachment-image', NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-MG-Sole-scaled.jpg', '34', 'attachment-image', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-MG-Sole-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunitedstage\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-MG-Sole-scaled.jpg\",\"size\":\"full\",\"id\":34,\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', NULL, NULL, NULL, '2020-05-18 18:36:53', '2020-07-10 00:34:08', 1, NULL, NULL, NULL, NULL),
(32, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg', '90:29e3866d4dd2abcb0ce4be8f28424a36', 35, 'post', 'attachment', 4, 30, NULL, NULL, 'Crisis Planning', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg', NULL, '35', 'attachment-image', NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Crisis-scaled.jpg', '35', 'attachment-image', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Crisis-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunitedstage\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Crisis-scaled.jpg\",\"size\":\"full\",\"id\":35,\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', NULL, NULL, NULL, '2020-05-18 18:40:24', '2020-07-10 00:33:45', 1, NULL, NULL, NULL, NULL),
(33, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg', '93:13d36855ecc9d3caaeac69d6402b35ac', 36, 'post', 'attachment', 4, 29, NULL, NULL, 'Emotional Health', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg', NULL, '36', 'attachment-image', NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Emotional-scaled.jpg', '36', 'attachment-image', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Emotional-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunitedstage\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Emotional-scaled.jpg\",\"size\":\"full\",\"id\":36,\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', NULL, NULL, NULL, '2020-05-18 18:45:23', '2020-07-10 00:31:27', 1, NULL, NULL, NULL, NULL),
(34, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg', '83:900bb6600d788654704d0c6fe388a324', 37, 'post', 'attachment', 4, 28, NULL, NULL, 'About MG', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg', NULL, '37', 'attachment-image', NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-scaled.jpg', '37', 'attachment-image', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunitedstage\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-scaled.jpg\",\"size\":\"full\",\"id\":37,\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', NULL, NULL, NULL, '2020-05-18 18:48:00', '2020-07-10 00:32:40', 1, NULL, NULL, NULL, NULL),
(35, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg', '89:238a4f26ad2c17c0eb027a4fc44baa22', 38, 'post', 'attachment', 4, 27, NULL, NULL, 'Treatment Goals', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg', NULL, '38', 'attachment-image', NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/Square-Image-Goals-scaled.jpg', '38', 'attachment-image', '{\"width\":2560,\"height\":2560,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Goals-scaled.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mgunitedstage\\/wp-content\\/uploads\\/2020\\/05\\/Square-Image-Goals-scaled.jpg\",\"size\":\"full\",\"id\":38,\"alt\":\"\",\"pixels\":6553600,\"type\":\"image\\/jpeg\"}', NULL, NULL, NULL, '2020-05-18 18:52:44', '2020-07-10 00:31:59', 1, NULL, NULL, NULL, NULL),
(36, NULL, '66:f97c51e2a36e350e895e372728dd0f7a', 3, 'term', 'category', NULL, NULL, NULL, NULL, 'Disease &amp; Treatment', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-21 15:38:36', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(37, NULL, '62:5f3a871b63f61e86d39c3ae12bf16968', 4, 'term', 'category', NULL, NULL, NULL, NULL, 'Emotional Support', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-21 15:39:14', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(38, NULL, '62:e8969828999e01258b1ff3ffa215131b', 5, 'term', 'category', NULL, NULL, NULL, NULL, 'Community Project', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-21 15:40:14', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(41, NULL, '80:0c7200ad86d5aa3d643544090338e951', 45, 'post', 'attachment', 4, 0, NULL, NULL, 'mg-united-icon', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/mg-united-icon.png', NULL, '45', 'attachment-image', NULL, NULL, NULL, '45', 'attachment-image', NULL, NULL, NULL, NULL, '2020-05-21 16:30:35', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(42, NULL, '88:759c3d594049ea808e73723751370e2e', 46, 'post', 'attachment', 4, 0, NULL, NULL, 'cropped-mg-united-icon.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/05/cropped-mg-united-icon.png', NULL, '46', 'attachment-image', NULL, NULL, NULL, '46', 'attachment-image', NULL, NULL, NULL, NULL, '2020-05-21 16:30:40', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(45, 'https://mguniteddev.wpengine.com/author/tkneedy/', '48:804eda6e4bc7450c5fb2bd0745e6e883', 5, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/0ab52db9c71e5ac381d81f950aa65513?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/0ab52db9c71e5ac381d81f950aa65513?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-05-22 20:30:20', '2020-07-10 00:31:28', 1, NULL, NULL, NULL, NULL),
(49, 'https://mguniteddev.wpengine.com/author/zortiz/', '47:b8dce7915e7ae013619742c5451fe0f9', 6, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/262fe4d693d60eb7dddcefb278317bcc?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/262fe4d693d60eb7dddcefb278317bcc?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-05-24 16:07:41', '2020-07-10 00:31:28', 1, NULL, NULL, NULL, NULL),
(54, 'https://mguniteddev.wpengine.com/life-with-mg/mg-illuminate-recap/', '66:c4abd9542d5d380fcb2b6a77e9b9127e', 58, 'post', 'post', 4, 0, NULL, NULL, 'MG Illuminate Recap', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg', 'MG United provides clear information for those with myasthenia gravis.', '77', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg', '77', 'set-by-user', '{\"width\":865,\"height\":486,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/06\\/illuminate-recap-featured-image.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/06\\/illuminate-recap-featured-image.jpg\",\"size\":\"full\",\"id\":\"77\",\"alt\":\"\",\"pixels\":420390,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-06-23 14:15:44', '2020-07-14 21:25:23', 1, NULL, NULL, NULL, NULL),
(55, 'https://mguniteddev.wpengine.com/disease-and-treatment/tools-neuros-use/', '72:24eda31c4b53bdeee1832a517e64cc68', 59, 'post', 'post', 4, 0, NULL, NULL, 'Doctor Tools You Can Use', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png', 'MG United provides clear information for those with myasthenia gravis.', '78', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png', '78', 'set-by-user', '{\"width\":466,\"height\":327,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/06\\/doctor-tools-about-mg.png\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/06\\/doctor-tools-about-mg.png\",\"size\":\"full\",\"id\":\"78\",\"alt\":\"\",\"pixels\":152382,\"type\":\"image\\/png\"}', 0, 0, NULL, '2020-06-26 14:11:37', '2020-07-14 21:28:03', 1, NULL, NULL, NULL, NULL),
(56, 'https://mguniteddev.wpengine.com/eating-and-mg/5-mg-friendly-smoothies/', '71:d5d9190751f8c7a697a8b844ca26baf7', 60, 'post', 'post', 4, 0, NULL, NULL, '5 MG-Friendly Smoothies', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg', 'MG United provides clear information for those with myasthenia gravis.', '82', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg', '82', 'set-by-user', '{\"width\":638,\"height\":330,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/06\\/friendly-smoothies.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/06\\/friendly-smoothies.jpg\",\"size\":\"full\",\"id\":\"82\",\"alt\":\"\",\"pixels\":210540,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-06-26 14:12:47', '2020-07-14 21:36:21', 1, NULL, NULL, NULL, NULL),
(58, 'https://mguniteddev.wpengine.com/real-stories/kathy-and-diane/', '62:e0e7719e498597d75d0fb30f73eb344a', 63, 'post', 'post', 4, 0, NULL, NULL, 'Patient Profile &#8211; Kathy &#038; Diane', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg', 'MG United provides clear information for those with myasthenia gravis.', '85', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg', '85', 'set-by-user', '{\"width\":375,\"height\":416,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/06\\/kathy-profile.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/06\\/kathy-profile.jpg\",\"size\":\"full\",\"id\":\"85\",\"alt\":\"\",\"pixels\":156000,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-06-30 15:14:20', '2020-07-14 21:54:16', 1, NULL, NULL, NULL, NULL),
(59, 'https://mguniteddev.wpengine.com/real-stories/leah-gaitan-diaz/', '63:a6432a8e96157b642da9ad8f3fda0451', 64, 'post', 'post', 4, 0, NULL, NULL, 'Patient Profile &#8211; Leah', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg', 'MG United provides clear information for those with myasthenia gravis.', '84', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg', '84', 'set-by-user', '{\"width\":464,\"height\":360,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/06\\/leah-profile-real-story.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/06\\/leah-profile-real-story.jpg\",\"size\":\"full\",\"id\":\"84\",\"alt\":\"\",\"pixels\":167040,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-06-30 15:14:37', '2020-07-14 21:51:02', 1, NULL, NULL, NULL, NULL),
(61, 'https://mguniteddev.wpengine.com/life-with-mg/', '46:54b96b607a666e8b080fa31376c317af', 66, 'post', 'page', 4, 0, NULL, NULL, 'Life with MG', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2020-07-07 14:23:05', '2020-07-10 00:35:08', 1, NULL, NULL, NULL, NULL),
(62, 'https://mguniteddev.wpengine.com/disease-and-treatment/newly-diagnosed/', '71:cfa308c2cfc9bb50814186e72ab8a571', 67, 'post', 'post', 4, 0, NULL, NULL, 'Newly Diagnosed', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg', 'MG United provides clear information for those with myasthenia gravis.', '80', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg', '80', 'set-by-user', '{\"width\":416,\"height\":367,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/07\\/newly-diagnosed.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/07\\/newly-diagnosed.jpg\",\"size\":\"full\",\"id\":\"80\",\"alt\":\"\",\"pixels\":152672,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-07-07 14:23:54', '2020-07-14 21:30:44', 1, NULL, NULL, NULL, NULL),
(63, 'https://mguniteddev.wpengine.com/disease-and-treatment/been-living-with-mg-for-some-time/', '89:c6450a357597aa058de946ec54c4e711', 68, 'post', 'post', 4, 0, NULL, NULL, 'Living with MG for Some Time', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg', 'MG United provides clear information for those with myasthenia gravis.', '81', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg', '81', 'set-by-user', '{\"width\":640,\"height\":330,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/07\\/sometimes-mg.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/07\\/sometimes-mg.jpg\",\"size\":\"full\",\"id\":\"81\",\"alt\":\"\",\"pixels\":211200,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-07-07 14:24:17', '2020-07-14 21:34:18', 1, NULL, NULL, NULL, NULL),
(64, 'https://mguniteddev.wpengine.com/life-with-mg/summer-heat-tips/', '63:615c6a93b9d850b2fd81ad408bad6e9d', 69, 'post', 'post', 4, 0, NULL, NULL, 'Beating the Summer Heat', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg', 'MG United provides clear information for those with myasthenia gravis.', '79', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg', '79', 'set-by-user', '{\"width\":375,\"height\":375,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/07\\/outsmart-summer.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/07\\/outsmart-summer.jpg\",\"size\":\"full\",\"id\":\"79\",\"alt\":\"\",\"pixels\":140625,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-07-07 14:24:44', '2020-07-14 21:29:23', 1, NULL, NULL, NULL, NULL),
(65, 'https://mguniteddev.wpengine.com/real-stories/victor-and-iris-yipp/', '67:817160b6c210194b020035a0a1c2ee6c', 70, 'post', 'post', 4, 0, NULL, NULL, 'Patient Profile &#8211; Victor &#038; Iris', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, 'Check out this article from MG United!', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg', 'MG United provides clear information for those with myasthenia gravis.', '83', 'set-by-user', 'Check out this article from MG United!', 'MG United provides clear information for those with myasthenia gravis.', 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg', '83', 'set-by-user', '{\"width\":375,\"height\":416,\"url\":\"https:\\/\\/mguniteddev.wpengine.com\\/wp-content\\/uploads\\/2020\\/07\\/victor-profile.jpg\",\"path\":\"\\/nas\\/content\\/live\\/mguniteddev\\/wp-content\\/uploads\\/2020\\/07\\/victor-profile.jpg\",\"size\":\"full\",\"id\":\"83\",\"alt\":\"\",\"pixels\":156000,\"type\":\"image\\/jpeg\"}', 0, 0, NULL, '2020-07-07 14:26:03', '2020-07-14 21:43:44', 1, NULL, NULL, NULL, NULL),
(67, NULL, '56:e7189426b498f87a0bea2b6bf7e8595d', 8, 'term', 'category', NULL, NULL, NULL, NULL, 'Eating &amp; MG', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-10 00:07:42', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(68, NULL, '55:eaf13e9af3b169bcfc81fb5465722895', 9, 'term', 'category', NULL, NULL, NULL, NULL, 'Real Stories', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-10 00:08:04', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(69, NULL, '55:9939d311d0c2ed54eca22964e1bb4719', 10, 'term', 'category', NULL, NULL, NULL, NULL, 'Life with MG', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-10 00:08:31', '2020-07-10 00:30:50', 1, NULL, NULL, NULL, NULL),
(70, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg', '95:4738da60a6e629570fac925c793590b9', 77, 'post', 'attachment', 4, 58, NULL, NULL, 'illuminate-recap-featured-image', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/illuminate-recap-featured-image.jpg', NULL, '77', 'attachment-image', NULL, NULL, NULL, '77', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:24:53', '2020-07-14 21:24:53', 1, NULL, NULL, NULL, NULL),
(71, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png', '85:6e4e683175c68b123a36e8aea361ff6f', 78, 'post', 'attachment', 4, 59, NULL, NULL, 'doctor-tools-about-mg', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/doctor-tools-about-mg.png', NULL, '78', 'attachment-image', NULL, NULL, NULL, '78', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:27:40', '2020-07-14 21:27:40', 1, NULL, NULL, NULL, NULL),
(72, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg', '79:6049b17d0b8801a37a02622d91147571', 79, 'post', 'attachment', 4, 69, NULL, NULL, 'outsmart-summer', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/outsmart-summer.jpg', NULL, '79', 'attachment-image', NULL, NULL, NULL, '79', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:29:08', '2020-07-14 21:29:08', 1, NULL, NULL, NULL, NULL),
(73, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg', '79:93349d8b6ad752484a3f2177cf601ad4', 80, 'post', 'attachment', 4, 67, NULL, NULL, 'newly-diagnosed', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/newly-diagnosed.jpg', NULL, '80', 'attachment-image', NULL, NULL, NULL, '80', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:30:17', '2020-07-14 21:30:17', 1, NULL, NULL, NULL, NULL),
(74, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg', '76:cdba4506d8b462c5c992abe46bf6f1c8', 81, 'post', 'attachment', 4, 68, NULL, NULL, 'sometimes-mg', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/sometimes-mg.jpg', NULL, '81', 'attachment-image', NULL, NULL, NULL, '81', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:33:18', '2020-07-14 21:33:18', 1, NULL, NULL, NULL, NULL),
(75, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg', '82:5b505944e3e7ba4f3d1ea3296f261a28', 82, 'post', 'attachment', 4, 60, NULL, NULL, 'friendly-smoothies', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/friendly-smoothies.jpg', NULL, '82', 'attachment-image', NULL, NULL, NULL, '82', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:35:07', '2020-07-14 21:35:07', 1, NULL, NULL, NULL, NULL),
(76, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg', '78:4266f55dd32dfa16e3805772636f8ff6', 83, 'post', 'attachment', 4, 70, NULL, NULL, 'victor-profile', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/07/victor-profile.jpg', NULL, '83', 'attachment-image', NULL, NULL, NULL, '83', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:38:33', '2020-07-14 21:38:33', 1, NULL, NULL, NULL, NULL),
(77, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg', '87:7c8eb9a176064d7a30f95acebff2ac64', 84, 'post', 'attachment', 4, 64, NULL, NULL, 'leah-profile-real-story', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/leah-profile-real-story.jpg', NULL, '84', 'attachment-image', NULL, NULL, NULL, '84', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:50:47', '2020-07-14 21:50:47', 1, NULL, NULL, NULL, NULL),
(78, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg', '77:45de60ae1a78d40f83b36c05e463c9dd', 85, 'post', 'attachment', 4, 63, NULL, NULL, 'kathy-profile', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://mguniteddev.wpengine.com/wp-content/uploads/2020/06/kathy-profile.jpg', NULL, '85', 'attachment-image', NULL, NULL, NULL, '85', 'attachment-image', NULL, NULL, NULL, NULL, '2020-07-14 21:53:58', '2020-07-14 21:53:58', 1, NULL, NULL, NULL, NULL),
(79, 'https://mguniteddev.wpengine.com/?p=86', '38:7fc77248ab503c19f83afc6fcfeb815a', 86, 'post', 'post', 4, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-24 19:20:35', '2020-07-24 19:20:35', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_indexable_hierarchy`
--

CREATE TABLE `wp_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `ancestor_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_indexable_hierarchy`
--

INSERT INTO `wp_yoast_indexable_hierarchy` (`indexable_id`, `ancestor_id`, `depth`, `blog_id`) VALUES
(31, 30, 1, 1),
(32, 29, 1, 1),
(33, 28, 1, 1),
(34, 27, 1, 1),
(35, 26, 1, 1),
(39, 38, 1, 1),
(40, 37, 1, 1),
(70, 54, 1, 1),
(71, 55, 1, 1),
(72, 64, 1, 1),
(73, 62, 1, 1),
(74, 63, 1, 1),
(75, 56, 1, 1),
(76, 65, 1, 1),
(77, 59, 1, 1),
(78, 58, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_migrations`
--

CREATE TABLE `wp_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_migrations`
--

INSERT INTO `wp_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_primary_term`
--

CREATE TABLE `wp_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `term_id` int(11) UNSIGNED NOT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_primary_term`
--

INSERT INTO `wp_yoast_primary_term` (`id`, `post_id`, `term_id`, `taxonomy`, `created_at`, `updated_at`, `blog_id`) VALUES
(1, 27, 3, 'category', '2020-05-15 18:32:44', '2020-07-15 00:36:52', 1),
(2, 28, 3, 'category', '2020-05-15 18:33:47', '2020-07-10 00:33:17', 1),
(3, 29, 4, 'category', '2020-05-15 18:35:24', '2020-05-24 22:03:14', 1),
(4, 30, 3, 'category', '2020-05-15 18:36:26', '2020-07-10 00:34:02', 1),
(5, 31, 5, 'category', '2020-05-15 18:37:43', '2020-05-24 22:01:43', 1),
(6, 58, 10, 'category', '2020-07-10 00:24:17', '2020-07-14 21:25:23', 1),
(7, 59, 3, 'category', '2020-07-10 00:25:07', '2020-07-14 21:28:03', 1),
(8, 60, 8, 'category', '2020-07-10 00:25:30', '2020-07-14 21:36:21', 1),
(9, 63, 9, 'category', '2020-07-10 00:25:53', '2020-07-14 21:54:16', 1),
(10, 64, 9, 'category', '2020-07-10 00:27:45', '2020-07-14 21:51:02', 1),
(11, 70, 9, 'category', '2020-07-10 00:27:58', '2020-07-14 21:43:44', 1),
(12, 67, 3, 'category', '2020-07-10 00:28:22', '2020-07-14 21:30:44', 1),
(13, 68, 3, 'category', '2020-07-10 00:28:43', '2020-07-14 21:34:18', 1),
(14, 69, 10, 'category', '2020-07-10 00:29:08', '2020-07-14 21:29:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_links`
--

CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_meta`
--

CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(1, 0, 0),
(2, 0, 0),
(6, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(10, 0, 0),
(11, 0, 0),
(12, 0, 0),
(14, 0, 0),
(16, 0, 0),
(23, 0, 0),
(25, 0, 0),
(26, 0, 0),
(27, 0, 0),
(28, 0, 0),
(29, 0, 0),
(30, 0, 0),
(31, 0, 0),
(32, 0, 0),
(33, 0, 0),
(39, 0, 0),
(40, 0, 0),
(41, 0, 0),
(42, 0, 0),
(43, 0, 0),
(44, 0, 0),
(47, 0, 0),
(48, 0, 0),
(49, 0, 0),
(50, 0, 0),
(51, 0, 0),
(52, 0, 0),
(53, 0, 0),
(54, 0, 0),
(55, 0, 0),
(56, 0, 0),
(57, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(66, 0, 0),
(67, 0, 0),
(68, 0, 0),
(69, 0, 0),
(70, 0, 0),
(73, 0, 0),
(74, 0, 0),
(75, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_redirection_404`
--
ALTER TABLE `wp_redirection_404`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `url` (`url`(191)),
  ADD KEY `referrer` (`referrer`(191)),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `wp_redirection_groups`
--
ALTER TABLE `wp_redirection_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `wp_redirection_items`
--
ALTER TABLE `wp_redirection_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url` (`url`(191)),
  ADD KEY `status` (`status`),
  ADD KEY `regex` (`regex`),
  ADD KEY `group_idpos` (`group_id`,`position`),
  ADD KEY `group` (`group_id`),
  ADD KEY `match_url` (`match_url`(191));

--
-- Indexes for table `wp_redirection_logs`
--
ALTER TABLE `wp_redirection_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `redirection_id` (`redirection_id`),
  ADD KEY `ip` (`ip`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  ADD KEY `permalink_hash` (`permalink_hash`),
  ADD KEY `object_id_and_type` (`object_id`,`object_type`);

--
-- Indexes for table `wp_yoast_indexable_hierarchy`
--
ALTER TABLE `wp_yoast_indexable_hierarchy`
  ADD PRIMARY KEY (`indexable_id`,`ancestor_id`),
  ADD KEY `indexable_id` (`indexable_id`),
  ADD KEY `ancestor_id` (`ancestor_id`),
  ADD KEY `depth` (`depth`);

--
-- Indexes for table `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_wp_yoast_migrations_version` (`version`);

--
-- Indexes for table `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_taxonomy` (`post_id`,`taxonomy`),
  ADD KEY `post_term` (`post_id`,`term_id`);

--
-- Indexes for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `wp_yoast_seo_meta`
--
ALTER TABLE `wp_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `wp_redirection_404`
--
ALTER TABLE `wp_redirection_404`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_redirection_groups`
--
ALTER TABLE `wp_redirection_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_redirection_items`
--
ALTER TABLE `wp_redirection_items`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `wp_redirection_logs`
--
ALTER TABLE `wp_redirection_logs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
