
<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package mgunited
 */
get_header();
global $jobNumber;
$jobNumber = 'US-NON-20-00171 V1 10/2020';
?>
	<main id="primary" class="site-main">
		<section id="welcome">
			<div class="col col-sm-12 col-md-12 col-8 offset-2">
				<h1>Our apologies</h1>
				<h2>The system is currently experiencing technical difficulties. This error has been recorded and will be addressed promptly.</h2>
				<a role="button" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="capsule primary">RETURN TO HOME</a>
			</div>
		</section>
		<?php get_template_part( 'template-parts/callout', 'social' ); ?>
	</main><!-- #main -->
<?php
get_footer();
