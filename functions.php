<?php
/**
 * mgunited functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mgunited
 */

include_once "environment-detection.php";

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.3.1' );
}

if ( ! function_exists( 'mgunited_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mgunited_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on mgunited, use a find and replace
		 * to change 'mgunited' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'mgunited', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'mgunited' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'mgunited_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'mgunited_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mgunited_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'mgunited_content_width', 640 );
}
add_action( 'after_setup_theme', 'mgunited_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mgunited_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'mgunited' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'mgunited' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'mgunited_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mgunited_scripts() {
	//wp_enqueue_style( 'mgunited-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'mgunited-style', get_stylesheet_directory_uri() . '/style.min.css', array(), _S_VERSION );
	wp_style_add_data( 'mgunited-style', 'rtl', 'replace' );

	wp_enqueue_script( 'mgunited-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	wp_enqueue_script( 'mgunited-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// null version is needed when enqueueing multiple google fonts
	wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap&family=Libre+Baskerville:ital,wght@0,400;0,700;1,400', array(), null);
	wp_enqueue_style('typekit-fonts', 'https://use.typekit.net/dsk6xjs.css', array(), _S_VERSION);

	wp_enqueue_script('gsap-core', '//cdnjs.cloudflare.com/ajax/libs/gsap/3.1.1/gsap.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('gsap-cssrule-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/3.1.1/CSSRulePlugin.js', array('gsap-core'), _S_VERSION, true);
//	wp_enqueue_script();
//	if ( is_page(['living-with-mg','styleguide']) ) {
		wp_enqueue_script('tiny-slider', get_template_directory_uri() . '/js-es6/components/tiny-slider.js', array(), _S_VERSION, true);
//	}

	wp_localize_script('gsap-core', 'myAjax', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'signup_nonce' => wp_create_nonce("signup_nonce")
	));

	// If we're running locally, there'll be a <script type="module"> block
	// at the end of the page that loads the uncompiled ES6 javascript
	if (!mgunited_use_raw_javascript()) {
		wp_enqueue_script('mgunited-main', get_template_directory_uri() . '/js-es6/main.min.js', array(/*'jquery', 'gsap-core', 'gsap-cssrule-plugin','tiny-slider'*/), _S_VERSION, true);
	}
}
add_action( 'wp_enqueue_scripts', 'mgunited_scripts' );



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

