<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package mgunited
 */

include_once get_template_directory() . "/api-tools.php";

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function mgunited_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'mgunited_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function mgunited_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'mgunited_pingback_header' );

const QUESTIONS = array(
	'signUpType' => array(
		1 => 'Person living with MG',
		2 => 'MG caregiver/supporter'
	),
	'gender' => array(
		1 => 'Male',
		2 => 'Female'
	),
	'surveyTopicOfInterest' => array(
		1 => 'Health and wellness',
		2 => 'Life Hacks',
		4 => 'Career/financial planning'
	),
	'surveyStayInvolvedOption' => array(
		1 => 'Help organize a screening when released',
		2 => 'Get behind-the-scenes updates',
		3 => 'Share my MG story'
	)
);

const BASIC_ERROR_CODES = array(
	'ok' => 1,
	'notAuthenticated' => 2,
	'notAuthorized' => 3,
	'validationError' => 4,
	'systemError' => 5,
);

const SIGN_UP_ERROR_CODES = array(
	'signUpRequired' => 1,
	'firstNameRequired' => 2,
	'firstNameFormat' => 3,
	'lastNameRequired' => 4,
	'lastNameFormat' => 5,
	'emailRequired' => 6,
	'emailFormat' => 7,
	'duplicateEmail' => 8,
	'signUpOptInRequired' => 9,
	'goalRequired' => 10,
	'goalOptInRequired' => 11,
	'storyRequired' => 12,
	'storyOptInRequired' => 13,
	'invalidSignUpTypeId' => 14,
	'invalidGenderId' => 15,
	'genderRequired' => 16,
	'invalidTopicOfInterestId' => 17,
	'topicsOfInterestRequired' => 18,
	'invalidStayInvolvedOptionId' => 19,
	'stayInvolvedOptionRequired' => 20,
);

//const REGEX_EMAIL = "^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,24}))$";
const REGEX_PHONE = "^(\d{10})$|^((\d{1}-)?\d{3}-\d{3}-\d{4})$|^(\(\d{3}\) \d{3}-\d{4})$";
const REGEX_ZIP = "^\d{5}$|^\d{5}-\d{4}$";
const REGEX_NAME = "/[a-zA-Z\-\.\,\'\'\` ]+$/";

const CONNECTIVITY_TEST_PASS_MESSAGE = 'pass';
const CONNECTIVITY_TEST_FAIL_MESSAGE = 'fail';

const CONNECTIVITY_TEST_STATUS = array(
	'Disabled' => 1,
	'Pass' => 2,
	'Fail' => 3
);

add_action("wp_ajax_signup_Validate","signup_validate");
add_action("wp_ajax_nopriv_signup_Validate","signup_validate");
function signup_validate() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

	switch($_REQUEST['id']) {
		case 'share-story':
			if ($_REQUEST['value'] == '') $response['errors'][] = SIGN_UP_ERROR_CODES['storyRequired'];
			// Invalid data(?)
			break;
		case 'share-story-optin':
			if (!($_REQUEST['value'] == 'true' || $_REQUEST['value'] == 'false' )) {
				$response['errors'][] = SIGN_UP_ERROR_CODES['storyOptInRequired'];
			}
			break;
		case 'sign-up-first-name':
			if ( $_REQUEST['value'] === '' ) {
				$response['errors'][] = SIGN_UP_ERROR_CODES['firstNameRequired'];
			} else {
				$response['regex'] = preg_match(REGEX_NAME, $_REQUEST['value']);
				if (!preg_match(REGEX_NAME, $_REQUEST['value'])) {
					$response['errors'][] = SIGN_UP_ERROR_CODES['firstNameFormat'];
				}
			}
			break;
		case 'sign-up-last-name':
			if ( $_REQUEST['value'] === '' ) {
				$response['errors'][] = SIGN_UP_ERROR_CODES['lastNameRequired'];
			} else {
				$response['regex'] = preg_match(REGEX_NAME, $_REQUEST['value']);
				if (!preg_match(REGEX_NAME, $_REQUEST['value'])) {
					$response['errors'][] = SIGN_UP_ERROR_CODES['lastNameFormat'];
				}
			}
			break;
		case 'sign-up-email':
			if ( $_REQUEST['value'] === '' ) {
				$response['errors'][] = SIGN_UP_ERROR_CODES['emailRequired'];
			} else {
				if (!is_email($_REQUEST['value'])) {
					$response['errors'][] = SIGN_UP_ERROR_CODES['emailFormat'];
				}
			}
			break;
		case 'sign-up-type':
//			$response['signUpType'] = QUESTIONS['signUpType'][intval($_REQUEST['value'])];
//			$response['signUpTypeZero'] = QUESTIONS['signUpType'][0];
//			$response['signUpTypeNull'] = QUESTIONS['signUpType'][intval(null)];
//			$response['signUpTypeEmpty'] = QUESTIONS['signUpType'][intval('')];
			if (!QUESTIONS['signUpType'][intval($_REQUEST['value'])]) {
				$response['errors'][] = SIGN_UP_ERROR_CODES['invalidSignUpTypeId'];
			}
			break;


	}

	if ($response['errors'] === []) $response['errors'] = null;
	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	echo "this works?";
	die();
}

add_action("wp_ajax_signup_CreateStory","signup_create_story");
add_action("wp_ajax_nopriv_signup_CreateStory","signup_create_story");
function signup_create_story() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

	if (!($_REQUEST['sharingOptIn'] == 'true' || $_REQUEST['sharingOptIn'] == 'false' )) {
		$response['errors'][] = SIGN_UP_ERROR_CODES['storyOptInRequired'];
	}

	if ($_REQUEST['story'] == '') $response['errors'][] = SIGN_UP_ERROR_CODES['storyRequired'];

	if ($response['errors'] === []) {
		$response['errors'] = null;
		$response['signUpStoryId'] = 0;
		// Note: this is where we would store in the database through the API
	} else {
		$response['code'] = BASIC_ERROR_CODES['validationError'];
	}

	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}


add_action("wp_ajax_signup_SignUp","signup_process");
add_action("wp_ajax_nopriv_signup_SignUp","signup_process");
function signup_process() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

	if ( $_REQUEST['firstName'] === '' ) {
		$response['errors'][] = SIGN_UP_ERROR_CODES['firstNameRequired'];
	} else {
		if (!(preg_match(REGEX_NAME, $_REQUEST['firstName']))) {
			$response['errors'][] = SIGN_UP_ERROR_CODES['firstNameFormat'];
		}
	}

	if ( $_REQUEST['lastName'] === '' ) {
		$response['errors'][] = SIGN_UP_ERROR_CODES['lastNameRequired'];
	} else {
		if (!(preg_match(REGEX_NAME, $_REQUEST['lastName']))) {
			$response['errors'][] = SIGN_UP_ERROR_CODES['lastNameFormat'];
		}
	}

	if ( $_REQUEST['email'] === '' ) {
		$response['errors'][] = SIGN_UP_ERROR_CODES['emailRequired'];
	} else {
		if (!is_email($_REQUEST['email'])) {
			$response['errors'][] = SIGN_UP_ERROR_CODES['emailFormat'];
		}
	}

	// @TODO: have to handle duplicate email problem

	if ( $_REQUEST['optin'] !== 'true') $response['errors'][] = SIGN_UP_ERROR_CODES['signUpOptInRequired'];

	if ($response['errors'] === []) {
		$response['errors'] = null;
		$response['firstName'] = $_REQUEST['firstName'];
		$response['guid'] = uniqid();
		$response['lastName'] = $_REQUEST['lastName'];
		$response['signUpId'] = 10;
		$response['surveyComplete'] = false;
		// Note: this is where we would store in the database through the API
	} else {
		$response['code'] = BASIC_ERROR_CODES['validationError'];
	}

	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

add_action("wp_ajax_signup_ValidateGender","signup_validate_gender");
add_action("wp_ajax_nopriv_signup_ValidateGender","signup_validate_gender");
function signup_validate_gender() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

//	if ($_REQUEST['gender'] !== null && $_REQUEST['gender'] !== '') {
//		if (!QUESTIONS['gender'][intval($_REQUEST['gender'])]) {
//			$response['errors'][] = SIGN_UP_ERROR_CODES['invalidGenderId'];
//		}
//	}

	//if ($_REQUEST['otherGender'] !== '') {
		//do a check? but this is optional
	//}

	if ($response['errors'] === []) $response['errors'] = null;
	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

add_action("wp_ajax_signup_ValidateTopicsOfInterest","signup_validate_topics");
add_action("wp_ajax_nopriv_signup_ValidateTopicsOfInterest","signup_validate_topics");
function signup_validate_topics() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

	$topics = explode(',',  $_REQUEST['topicsOfInterest']);
	foreach($topics as $topicId) {
		if (!QUESTIONS['surveyTopicOfInterest'][intval($topicId)]) {
			$response['errors'][] = SIGN_UP_ERROR_CODES['topicsOfInterestRequired'];
			break;
		}
	}

	//if ($_REQUEST['otherTopicOfInterest'] !== '') {
	//do a check? but this is optional
	//}

	if ($response['errors'] === []) $response['errors'] = null;
	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

add_action("wp_ajax_signup_CompleteSurvey","signup_complete_survey");
add_action("wp_ajax_nopriv_signup_CompleteSurvey","signup_complete_survey");
function signup_complete_survey() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

//	if ($_REQUEST['gender'] !== null && $_REQUEST['gender'] !== '') {
//		if (!QUESTIONS['gender'][intval($_REQUEST['gender'])]) {
//			$response['errors'][] = SIGN_UP_ERROR_CODES['invalidGenderId'];
//		}
//	}

//	if (!QUESTIONS['signUpType'][intval($_REQUEST['signUpType'])]) {
//		$response['errors'][] = SIGN_UP_ERROR_CODES['invalidSignUpTypeId'];
//	}

//	$topics = explode(',',  $_REQUEST['topicsOfInterest']);
//	foreach($topics as $topicId) {
//		if (!QUESTIONS['surveyTopicOfInterest'][intval($topicId)]) {
//			$response['errors'][] = SIGN_UP_ERROR_CODES['topicsOfInterestRequired'];
//			break;
//		}
//	}

	if ($response['errors'] === []) {
		$response['errors'] = null;
		$response['signUpId'] = 10;
		// Data Model
//		{
//			gender: "1",
//			otherGender: "",
//			otherTopicsOfInterest: "test",
//			profileType: "1",
//			topicsOfInterest: "1,2",
//		}
		// Note: this is where we would store in the database through the API
	} else {
		$response['code'] = BASIC_ERROR_CODES['validationError'];
	}

	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

add_action("wp_ajax_signup_ValidateStayInvolvedOptions","signup_validate_stay");
add_action("wp_ajax_nopriv_signup_ValidateStayInvolvedOptions","signup_validate_stay");
function signup_validate_stay() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}



	$topics = explode(',',  $_REQUEST['stayInvolvedOptions']);
	foreach($topics as $topicId) {
//		$response["topicval$topicId"] = $topicId;
//		$response['topic'.$topicId] = QUESTIONS['surveyStayInvolvedOption'][intval($topicId)];
		if (!QUESTIONS['surveyStayInvolvedOption'][intval($topicId)]) {
			$response['errors'][] = SIGN_UP_ERROR_CODES['stayInvolvedOptionRequired'];
			break;
		}
	}

	if ($response['errors'] === []) $response['errors'] = null;
	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

add_action("wp_ajax_signup_CreateStayInvolvedOptions","signup_create_stay");
add_action("wp_ajax_nopriv_signup_CreateStayInvolvedOptions","signup_create_stay");
function signup_create_stay() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

	$topics = explode(',',  $_REQUEST['stayInvolvedOptionIds']);
	foreach($topics as $topicId) {
//		$response["topicval$topicId"] = $topicId;
//		$response['topic'.$topicId] = QUESTIONS['surveyStayInvolvedOption'][intval($topicId)];
		if (!QUESTIONS['surveyStayInvolvedOption'][intval($topicId)]) {
			$response['errors'][] = SIGN_UP_ERROR_CODES['stayInvolvedOptionRequired'];
			break;
		}

		if (intval($topicId) === 3 && $_REQUEST['story'] === '') {
//			$response['condition'] = 'true';
			$response['errors'][] = SIGN_UP_ERROR_CODES['stayInvolvedOptionRequired'];
			break;
		}
	}

	if ($response['errors'] === []) {
		$response['errors'] = null;
//		$response['signUpStoryId'] = 10;
		// Data Model

		// Note: this is where we would store in the database through the API
	} else {
		$response['code'] = BASIC_ERROR_CODES['validationError'];
	}

	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

add_action("wp_ajax_signup_CreateGoal","signup_create_goal");
add_action("wp_ajax_nopriv_signup_CreateGoal","signup_create_goal");
function signup_create_goal() {
	$response = array(
		'code' => BASIC_ERROR_CODES['ok'],
		'errors' => array(),
	);

	if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
		$response['code'] = BASIC_ERROR_CODES['systemError'];
		$response['errors'] = null;
		wp_send_json_error($response);
	}

	if (!($_REQUEST['sharingOptIn'] == 'true' || $_REQUEST['sharingOptIn'] == 'false' )) {
		$response['errors'][] = SIGN_UP_ERROR_CODES['goalOptInRequired'];
	}

	if ($_REQUEST['goal'] == '') $response['errors'][] = SIGN_UP_ERROR_CODES['goalRequired'];

	if ($response['errors'] === []) {
		$response['errors'] = null;
		$response['SignUpGoalId'] = 0;
		// Note: this is where we would store in the database through the API
	} else {
		$response['code'] = BASIC_ERROR_CODES['validationError'];
	}

	//$response['request'] = $_REQUEST;

	wp_send_json_success($response);
	die();
}

/*
 * General purpose method for forwarding requests to the MG United API -
 * Returns the raw result if the call was successful
 */

//ini_set('display_errors', 1);
//error_reporting(E_ALL ^ E_NOTICE);

function call_api_for_callback($callback) {
	$response = null;

	// Verify nonce, unless a parameter named 'skip-nonce-check'
	// was included in the request
	if (!array_key_exists('skip-nonce-check', $_REQUEST))
	{
		if (!wp_verify_nonce($_REQUEST['nonce'], "signup_nonce")) {
			header(':', true, 500);
			header('X-PHP-Response-Code: 500', true, 500);
			die();
		}
	}

	// Determine the method we'll call on the API, as specified by a 'method'
	// parameter in the POST or query string
	$requestApiMethod;
	if (array_key_exists('method', $_REQUEST)) {
		$requestApiMethod = $_REQUEST['method'];
	} else {
		header(':', true, 500);
		header('X-PHP-Response-Code: 500', true, 500);
		die();
	}

	// Check for a supplied 'data' parameter
	$requestData;
	if (array_key_exists('data', $_REQUEST))
		$requestData = stripslashes($_REQUEST['data']);
	else
		$requestData = '';

	// Perform API call using the supplied API method & request data
	$apiResponse = call_user_func($callback, $requestApiMethod, $requestData);
	if ($apiResponse != null) {
		// Forward along any cookies set by the API
		foreach ($apiResponse['headers'] as $header) {
			if (strpos(strtolower($header), 'set-cookie:') === 0) {
				header($header);
				break;
			}
		}

		$response = json_decode($apiResponse['result']);
	} else {
		header(':', true, 500);
		header('X-PHP-Response-Code: 500', true, 500);
		die();
	}

	// Output the response
	wp_send_json($response);
	die();
}

add_action("wp_ajax_signupApi","signup_api");
add_action("wp_ajax_nopriv_signupApi","signup_api");
function signup_api() {
	call_api_for_callback("call_mg_united_api");
}

/*
 * Queries API status
 */

add_action("wp_ajax_connectivityTest","connectivity_test");
add_action("wp_ajax_nopriv_connectivityTest","connectivity_test");
function connectivity_test() {
	// Call the "ConnectivityTest" API method
	$apiResponse = call_mg_united_api('ConnectivityTest', null);
	if ($apiResponse == null) {
		echo FAIL_MESSAGE;
		return;
	}

	// Parse the result JSON
	$result = json_decode($apiResponse['result']);

	// "status" is a short indicating the state of the API's connectivitytest page
	switch ($result->status) {
		case CONNECTIVITY_TEST_STATUS['Pass']:
			echo CONNECTIVITY_TEST_PASS_MESSAGE;
			break;
		
		default:
			echo CONNECTIVITY_TEST_FAIL_MESSAGE;
			break;
	}

	// And that's it - all we do is print one of those two messages
	die();
}
