<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mgunited
 */

$body_id = '';

if ( is_front_page() ) {
	$body_id = 'id="home"';
} else {
	$body_id = is_page() || is_single() ? 'id="' . get_post_field( 'post_name' ) . '"' : '';
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/lln0bwz.css">

	<?php wp_head(); ?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K7S9LL3');</script>
	<!-- End Google Tag Manager -->
</head>
<body <?php echo $body_id; ?> <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K7S9LL3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php wp_body_open(); ?>
<div aria-label="MG United Site Page" id="page" class="site">
	<header aria-label="MG United Site Header" id="masthead" class="site-header <?php if ( !is_page('a-mystery-to-me') ) { echo 'white-header'; } ?>">
		<div aria-label="MG United Site Navigation Menu" id="navigation-content">
			<a class="skip-link" aria-label="Skip to content" href='#primary'>Skip to content</a>
			<a aria-label="Home Page" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/mgunited-logo-combined.png" role="img" aria-label="MG United" alt="MG United" />
			</a>

			<nav aria-label="Navigation" id="navigation">
				<?php /*
				This <div> is here primarilly to make it easier to
					measure the height of the expandable menu content
				*/ ?>
					<div>
						<ul aria-label="Navigation Links">
							<?php /*
							A home link - this isn't visible on desktop, and
							on mobile we just show a home icon
							*/ ?>
							<li>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/mgunited-logo-combined.png" aria-label="MG United" alt="MG United" /></a>
							</li>
							<li<?php if ( is_page('about-mg') ) { echo ' class="selected"'; } ?>><a href="<?php echo esc_url( home_url( '/about-mg' ) ); ?>">ABOUT MG</a></li>
							<li<?php if ( is_page('life-with-mg') ) { echo ' class="selected"'; } ?>><a href="<?php echo esc_url( home_url( '/life-with-mg' ) ); ?>">LIFE WITH MG</a></li>
							<li<?php if ( is_page('real-stories') ) { echo ' class="selected"'; } ?>><a href="<?php echo esc_url( home_url( '/real-stories' ) ); ?>">REAL STORIES</a></li>
							<li<?php if ( is_page('things-to-do') ) { echo ' class="selected"'; } ?>><a href="<?php echo esc_url( home_url( '/things-to-do' ) ); ?>">THINGS TO DO</a></li>
							<li<?php if ( is_page('a-mystery-to-me') ) { echo ' class="selected"'; } ?>><a href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>">DOCUMENTARY</a></li>

						</ul>

						<a role="button" href="<?php echo esc_url( home_url( '/sign-up' ) ); ?>">SIGN UP</a>
					</div>
			</nav>

			<div id="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div><!-- #site-navigation -->
	</header><!-- #masthead -->
