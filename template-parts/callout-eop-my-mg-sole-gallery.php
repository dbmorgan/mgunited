<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-mymgsole-gallery-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-mymgsole-gallery">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-mymgsole-gallery-label" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-art-project' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-art-project' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">THINGS TO DO</p>
			<h2 id="eop-callout-mymgsole-gallery-label" class="secondary">Join this kicky community art project</h2>
			<span class="read-duration">9 MIN READ</span>
		</a>
	</div>
</div>
