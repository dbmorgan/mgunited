<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-crisis-info-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eoc-crisis">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-crisis-info-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">DISEASE & TREATMENT</p>
			<h2 id="eop-callout-crisis-info-label" class="secondary">Crisis 411: What You Need to Know about Myasthenic Crisis</h2>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
