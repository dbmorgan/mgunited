<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */
?>

<article id="four-words-article" <?php post_class(); ?>>
	<section id="hero">
		<div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>AUGUST 2020 | <b>6 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>
	<div id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>The Four Words That<br>Changed My Life</h1>
			</div>
		</div>
	</div>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img src="/wp-content/themes/mgunited/images/four-words/morgan-profile-bio.jpg" alt="Author callout image">
				</div>
				<div>
					<p class="byline">By Morgan Greene</p>
					<p><b><em>
					This Maryland native writes a <a href="https://www.iswaswillbe.com/about/" target="_blank" rel="noopener">blog</a> about her MG journey. MG&nbsp;United commissioned Morgan to share one of her blog posts, about a moment of truth every person living with MG can relate to.
					</em></b></p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p>I can remember every detail of my diagnosis moment, but I’ll give you a
				quick summary. I was in the neurologist’s office waiting for him to come
				in. It had been three months since all the testing had begun. I knew
				something was off, but I couldn’t put my finger on it—and neither could the
				doctors. This was my fourth doctor and seventh test.</p>

				<p>He walked in and sat down across from me. Without even making eye contact,
				he opened my file and said, “Yeah, you have myasthenia gravis.” His
				delivery was monotone and matter of fact, as if I had a cold. He tried to
				explain what it was in medical jargon, but it made no sense to me.</p>

				<p>I left with a prescription and a sticky note with the spelling of my
				diagnosis—because who can say myasthenia gravis, let alone spell it? I
				remember feeling relieved. At least the testing was over.</p>

				<p>At the time, I didn’t know that there is no cure for MG. Or that you may have times when the severity can get so bad that it can leave you hospitalized for weeks on end. All he said was, “You have myasthenia gravis.”  I didn’t know the impact this diagnosis would have on my life. I had to learn that for myself on my chronic illness journey.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>As I’ve matured in my journey, I’ve learned that I can still live a good life despite my chronic illness.</p>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Hiding My Chronic Illness</h3>

			<p>That first year after diagnosis was the most brutal. I was in and out of
			the hospital every month. I’m partly to blame because I was determined not
			to let my diagnosis control me. I tried my best to continue my life just as
			it was before, and the effort was destroying me. Once I realized that my
			situation was real, things got bleak.</p>

			<p>I began feeling sorry for myself. The symptoms made me feel horrible. And I was mourning my pre-MG life.</p>

			<p>After getting over the shock of having a chronic illness, I next went into a phase where I wanted to hide it. On the rare occasions that I stepped out I wore sunglasses to hide my droopy eyelids.</p>

			<p>I thought I was doing the world a kindness by shielding it from the monster
			I was becoming. But in all honesty, I was afraid of being judged for having
			MG and having to answer the many, many questions. So, I camouflaged myself
			in public, and secluded myself at home.</p>

			<p>Even there, however, ordinary tasks like cleaning or walking up the stairs
			to my bedroom for rest became increasingly difficult. I can recall randomly
			collapsing on the short walk to the mailbox and feeling leg weakness while
			trying to press the brake in the car.</p>

			<p>MG was threatening the one thing I’d worked so hard to achieve: my
			independence.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">

			<h3>The Turning Point in My Journey</h3>

			<p>A side effect of chronic illness that people fail to mention is the
			loneliness. Friends and family don’t understand because it’s not happening
			to them. It’s one of those things where you don’t get it unless you’ve got
			it.</p>

			<p>It’s difficult to explain that you’re still ill, even though you have good
			days and look ok. It’s hard to justify that just because I could do
			something yesterday doesn’t mean I can handle it today.</p>

			<p>As I’ve matured in my journey, I’ve learned that I can still live a good
			life despite my chronic illness. The turning point was figuring out how to
			adapt and make things work for me and my new normal.</p>

			<p>For example, since heat is a trigger for me, I rarely do anything in the summer. If I do, it’ll be when the sun goes down.</p>

			<p>I always keep my pill pack with me everywhere I go. I space out events on my social calendar to avoid feeling overwhelmed or tired. Little things, but they all count. They all make a difference.</p>

			<p>Thankfully, I’ve also found my safe place. My home is my haven. I can relax
			and recharge my body there. It’s a place where I can just be ill and not
			feel bad about it.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/four-words/morgan-article-photo.jpg" alt="">
					<figcaption><em>“Writing about my life with MG not only helped me, but it opened my eyes to
				a whole new community.”</em></figcaption>
				</figure>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
			<h3>Finding My Community</h3>

			<p>I have been living with a chronic illness for almost four years, and I’m
			finally able to talk about it without bursting into tears or feeling
			melancholy.</p>

			<p>Early on, I found it very difficult to express how I felt. A year after my
			diagnosis, I decided to start a blog and write a letter to my myasthenia
			gravis. If I was feeling this way, others had to be too, right? Writing
			about my life with MG not only helped me, but it opened my eyes to a whole
			new community that welcomed me with open arms and a lack of judgment.</p>

			<p>Even though our bodies react to MG a little differently, we are all
			fighting the same battle. They understand what it’s like to mourn their old
			lives. We learn from one another and stand strong together. That’s why
			having a community has been so important.</p>

			<p>I found my people. People who can relate to having their entire lives
			change in a moment. Like when a doctor says, “You have myasthenia gravis.”</p>

			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div class="content">
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-leah-gaitan-diaz' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-newly-diagnosed' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
