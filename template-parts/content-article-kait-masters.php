<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */
?>

<article id="kait-masters-article" <?php post_class(); ?>>
	<section id="hero">
		<div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>OCTOBER 2020 | <b>9 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>
	<div id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>Setting and Readjusting Life Goals with Myasthenia Gravis</h1>
			</div>
		</div>
	</div>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img class="author-callout sm" src="/wp-content/themes/mgunited/images/kait-masters/author-callout-mobile.jpg" alt="Author callout image">
					<img class="author-callout lg" src="/wp-content/themes/mgunited/images/kait-masters/author-callout-desktop.jpg" alt="Author callout image">
				</div>
				<div>
					<p><b><em>
						Kait Masters was an active, self-described type-A preteen when she began experiencing myasthenia gravis symptoms. It marked the beginning of a journey defined by a relentless pursuit to stay active and engaged in business, family and the MG community. The Gig Harbor, Washington, native shares the importance of setting and readjusting goals throughout your myasthenia gravis journey.
					</em></b></p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>What was it like to be diagnosed with myasthenia gravis at a young age?</h3>
				
				<p>I&rsquo;m a working mom, small business owner, artist&mdash;and a person living with myasthenia gravis. I was 11 years old when I was diagnosed. Before that, I was the typical type-A child. I was involved in everything, and I wanted to be the best at all of it. I played soccer, was in dance and took music lessons. When I was 10, I started having trouble seeing, so my parents got me glasses. Then I told my parents I was tired all the time, and they assumed it was a growth spurt. Those were my first symptoms. I didn&rsquo;t understand what was happening until I started sleeping 18 hours a day and missing a lot of school. Back then, I didn&rsquo;t have the emotional intelligence to communicate what I thought about being diagnosed with myasthenia gravis. But now I can tell you I was scared and confused.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Have your symptoms varied over the years?</h3>

				<p>When I was diagnosed, I had difficulty with vision, chewing and swallowing. With the help of my doctor, I was able to manage my symptoms except for fatigue, which was persistent. However, my symptoms returned when I was in college. I had a lot more difficulty breathing. Since then, my most persistent symptoms have been shortness of breath and fatigue.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>My approach to managing MG symptoms hasn&rsquo;t really changed, but my approach to life has.</p>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Has your approach to living with MG and managing symptoms evolved over time?</h3>

				<p>My perspective of living with myasthenia gravis has definitely changed. Being re-diagnosed after remission was traumatizing. I thought MG was behind me. I had to make adjustments in my life. For example, I used to paint very large pieces, but with the weakness in my arms, I had to scale down the size of the pieces I worked on. Also, I&rsquo;ve learned to prioritize what matters and let go of the things that don&rsquo;t matter as much. That realization has helped me focus on my family, my career and my disease at different times.</p>

				<p>I&rsquo;ve had pretty much the same approach toward symptom management since they returned when I was 20. I try to rest often, get plenty of sleep and do things to reduce stress. I think MG taught me a lot about myself because my approach to managing MG symptoms hasn&rsquo;t really changed, but my approach to life has.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<figure>
					<img class="figure-img lg" src="<?php echo get_template_directory_uri(); ?>/images/kait-masters/inline-desktop.jpg" alt="">
					<img class="figure-img sm" src="<?php echo get_template_directory_uri(); ?>/images/kait-masters/inline-mobile.jpg" alt="">
				</figure>
				<figcaption>
					<p><em>Kait says that painting connects her to nature and helps her cope with MG.</em></p>
				</figcaption>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>In online MG communities &hellip; I saw other people who had purpose and direction despite, or even because of, having MG.</p>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
			<h3>Did you ever feel overwhelmed by your MG? If so, were you able to overcome that?</h3>

			<p>I don&rsquo;t think there was one single moment; it was a series of moments. The first one was being diagnosed with myasthenia gravis again. I had been in remission for a few years, in college and studying to be an artist. I was finally allowing myself to dream of what life could be like. And then my doctor told me I was no longer in remission. As my symptoms continued, I had to drop my classes and take a leave of absence from school. Each setback took me further and further away from my dreams. I wondered if I&rsquo;d ever be able to get my degree or find a career. Would I ever get married or start a family? It felt like my world was crashing down around me.</p>

			<p>I communicated what I was feeling to my friends and family, who were all supportive, but they didn&rsquo;t know what it felt like to have MG. That&rsquo;s when I got involved in online MG communities for the first time. I saw other people who had purpose and direction despite, or even because of, having MG. Seeing others helped me feel not so alone. I realized that I could go back to school and be an artist. I could get married and start a family. I could still dream.</p>
			</div>
		</div>
	</section>


	<section class="row article-body">
		<div>
			<div class="content">
				<h3>You realized you could still dream. What role did that play in your MG journey? </h3>

				<p>It inspired me to finish school. Once I graduated, I worked at a variety of small businesses, although most were not in the art industry. I achieved other dreams, like marrying a wonderful and supportive man who has stayed by me through all the ups and downs. Several years later, when I did find a job in the art industry, I hid how sick I was. My career goals weren&rsquo;t working out the way I had imagined. Eventually, I realized I couldn&rsquo;t pretend to be someone I wasn&rsquo;t. I had to find new dreams. I discovered a new purpose in life by starting my own business, which eventually led to my current fulltime job. I also started to advocate for my health, as well as the health of others who deal with chronic illnesses. Perhaps the most important thing I&rsquo;ve learned from MG is find value in myself, just as I am, and find purpose in that.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>You once lost a job because of your myasthenia gravis symptoms. What did you learn from that?</h3>

				<p>The biggest thing I learned was that hiding my MG diagnosis from coworkers and my boss wasn&rsquo;t helping me. It was difficult to request the accommodations I needed, particularly when they had no idea how sick I was. It was one of the hardest parts of navigating the perception of illness in the workplace. If I had been honest from the start, I might have received the support I needed to continue working there. I realize that not everyone is comfortable sharing their personal information. But it was very empowering to me. I learned from that experience and shared my MG diagnosis with colleagues at my next job.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Describe your leadership roles in both business and the MG community.</h3>

				<p>After coming out of remission, I became active in online myasthenia gravis communities and got involved in giving back. I participated in a program to help those newly diagnosed with MG connect to others with MG. I currently host fundraising events online, including an annual art auction for the Myasthenia Gravis Foundation of America (MGFA). When I became a mom, I stepped back a little from volunteering to focus on work and parenting.</p>

				<p>After starting my business, I volunteered for an organization dedicated to empowering and educating creative entrepreneurs. I led a group of small business owners who also manage chronic illnesses or disabilities. Today, I serve as a full-time community development manager for that organization.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>How can someone get involved in the MG community?</h3>

				<p>There are several organizations that people can reach out to, such as the MGFA, the Myasthenia Gravis Association and the Muscular Dystrophy Association. They each offer resources and support initiatives. Social media also can be a great place to find connections. I have found many friends and made genuine connections through hashtags on certain sites. It&rsquo;s important to find a place where you can connect with others, regardless of platform or venue.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>I think it&rsquo;s more important to be present than to be perfect.</p>
				</blockquote>
			</div>
		</div>
	</section>


	<section class="row article-body">
		<div>
			<div class="content">
				<h3>You&rsquo;ve achieved so many milestones. How do you balance it all?</h3>

				<p>The truth is, I don&rsquo;t do it all. The secret is letting go of certain expectations. I know I can&rsquo;t do everything, so I make choices about what is most important to me. I think it&rsquo;s more important to be present than to be perfect. I prioritize my goals and make conscious decisions to meet my priorities. It doesn&rsquo;t hurt that I&rsquo;ve gotten better at asking for help.</p>

				<p>I think it&rsquo;s important that I don&rsquo;t try to hold myself up to other people&rsquo;s standards. That was a hard lesson, because I was so competitive as a child. I had to define success for myself. And success for me is more about purpose and fulfillment.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Do you have any advice for other people living with myasthenia gravis? </h3>

				<p>Be kind to yourself. It takes time to adjust. Don&rsquo;t compare yourself to others. We&rsquo;re all separate people. Prioritize what matters most and work towards that. You just have to do what&rsquo;s right for you. And ask for help when you need it. I think that was the biggest thing that I had to learn when I became a mom. I&rsquo;ve come to realize that it&rsquo;s okay that I can&rsquo;t do everything.</p>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div class="content">
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-myrealworld' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-roasted-sweet-potato-ginger-soup' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
