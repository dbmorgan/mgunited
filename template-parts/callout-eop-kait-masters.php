<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-kait-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-kait-masters">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-kait-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kait-masters' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kait-masters' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">REAL STORIES</p>
			<h2 id="eop-callout-kait-label" class="secondary">Setting and Readjusting Life Goals with Myasthenia Gravis</h2>
			<span class="read-duration">9 MIN READ</span>
		</a>
	</div>
</div>
