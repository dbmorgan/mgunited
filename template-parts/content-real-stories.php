<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="real-stories-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-6 col-10 offset-1">
				<h1>
					Real stories of people living with myasthenia gravis
				</h1>
				<p>
					Read about people just like you, people with experiences you can relate to and wisdom to help guide your MG journey. You might even be inspired to share <em>your</em> story.
				</p>
			</div>
		</div>
	</section>

	<section id="real-stories-callouts" class="content page-callouts">
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/profile-kait-masters-reverse' ); ?>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/profile-chris-givens' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-morgan' ); ?>
			</div>

			<div class="col col-sm-6 col-6 horizontal-tablet-callout-row">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-victor' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-kathy' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-leah' ); ?>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<section id="real-stories-signup-promo" class="violator">
		<div class="row">
			<div class="col col-sm-6 col-6 offset-3">
				<h2>SIGN UP FOR MG&nbsp;UNITED!</h2>
				<p class="real-stories-signup-promo-copy">Get personalized content and support for wherever you are in your journey with MG. Just enter your email and you’re in.</p>
				<div class="row">
					<div class="col col-sm-6 col-12">
						<div id="real-stories-signup-button">
							<span class="flex-spacer"></span>
							<div>
								<a role="button" href="<?php echo esc_url( home_url( '/sign-up/' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
							</div>
							<span class="flex-spacer"></span>
						</div>
					</div>
				</div>
	</section>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-savory-soul-food-casserole' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-mg-other-conditions' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

	<section id="disclaimer" class="container">
	<div class="row">
		<div class="col">
			<p>*Paid contributor to MG&nbsp;United</p>
		</div>
	</div>
	</section>

</article><!-- #post-<?php the_ID(); ?> -->
