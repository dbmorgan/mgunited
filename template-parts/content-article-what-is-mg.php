<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="about-mg-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>What Is Myasthenia Gravis, and Why Does It Happen?</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JUNE 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
				  The essentials of MG, explained simply
				</h2>
				<p>Myasthenia gravis (my-us-THEE-nee-uh GRAY-vis) may also be called MG. The name means serious muscle weakness, which is the main symptom.<sup>1-3</sup> This muscle weakness often shows up in the face, with drooping eyelids being an early sign. Double or blurred vision, caused by weakness in the eye muscles, is another common symptom. Muscle weakness can extend to arms and legs.<sup>3,4</sup> Smiling, chewing, swallowing and breathing can also be affected.<sup>4,5</sup> MG is complicated. Every person who has it can have a slightly different experience.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					MG Is an Autoimmune Disease<sup>3,6</sup>
				</h3>

				<p>That means MG is caused by your immune system not working the way it&rsquo;s supposed to&mdash;it&rsquo;s overactive.<sup>3</sup> MG is not genetic (inherited), and it&rsquo;s not contagious.<sup>7,8</sup> It sometimes runs in families, but the reasons some people get it aren&rsquo;t fully understood.<sup>9</sup> It affects people of all ages and races, but it is slightly more common in women 20 to 30 years old and men 50 to 60 years old.<sup>4</sup>&nbsp;</p>
				<p>MG is a rare disease. Only about 31,000 to 67,000 Americans have MG.<sup>10,11</sup> Since it&rsquo;s rare, some medical professionals are not fully aware of it or its symptoms. That means symptoms may not be identified as MG immediately, which can be frustrating. Once it&rsquo;s diagnosed, you&rsquo;ll want to see a doctor who is familiar with MG&ndash;probably a neurologist or a neuromuscular specialist who has treated a number of MG patients.</p>
				<p>MG affects the neuromuscular junction, where nerves and muscles meet and communicate.<sup>3</sup> The immune system attacks muscles, which prevents them from receiving messages properly from nerves.<sup>3,12-14</sup> As a result, the muscles don&rsquo;t contract as much as they are supposed to.<sup>15</sup></p>
				<p>The early symptoms of MG are sometimes misunderstood. It&rsquo;s easy to think that a person is feeling weak because they didn&rsquo;t sleep well, worked out too hard, ate the wrong thing or are feeling depressed.<sup>3</sup>&nbsp;</p>
				<p>In addition, MG muscle weakness can vary throughout the day and throughout the body.<sup>3,16</sup> And symptoms are different for different people.<sup>16,17</sup>&nbsp;</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/about-mg/about-mg-nerve.jpg" alt="Neuromuscular Junction" /></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					There Are Different Types of MG
				</h3>
				<p>Antibodies play a big role in MG. In fact, MG can also be categorized by the type of antibodies that can be found in the blood. After diagnosis, a person with MG may learn the exact type of MG-related antibodies that they have.</p>
				<p>About 80% of people with MG have the AChR antibodies, about 15% have the MuSK antibodies, and approximately 3% have LRP4 antibodies. The remainder of patients are truly seronegative, where no antibodies can be identified.<sup>3,13,14,18</sup>&nbsp;</p>
				<p>There are two main categories for symptoms of MG: ocular and generalized. Ocular MG affects the muscles controlling the eyes and eyelids.<sup>3</sup> Generalized MG can affect muscles throughout the body.<sup>17</sup>&nbsp;</p>
				<p>MG is a serious disease. While there is no cure, the symptoms are treatable and can become minimal or even go into remission.<sup>19</sup> Those with MG can lead happy and productive lives. There is a lot to learn, and there are many challenges. Several treatment options are available, as well as good support from the community of people with MG, their doctors, and loving supporters who want to make living with MG better. We are part of that community and want you to be, too.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/about-mg/about-mg-bvessel.jpg" alt="Antibodies in blood" /></p>
			</div>
		</div>
	</section>

	<section id="one-up-bullet-list" class="row">
		<div>
			<div>
				<h3>MG Glossary:</h3>
				<p>When it comes to having MG, knowledge really is power.  Here are a few medical terms that every person with or supporting someone with MG should know.<sup>14,19,20</sup></p>
				<ul>
					<li><strong>Acetylcholine (ACh):</strong> a compound that occurs naturally in the body and helps nerves communicate with muscles</li>
					<li><strong>Acetylcholine receptors (AChR):</strong> help muscles receive acetylcholine signals from nerves</li>
					<li><strong>AChR antibody:</strong> antibody that binds to acetylcholine receptors</li>
					<li><strong>Autoimmune disease:</strong> the immune system mistakenly attacks the body&rsquo;s own cells and tissues</li>
					<li><strong>Diplopia:</strong> double vision</li>
					<li><strong>Dysphonia:</strong> voice disorder</li>
					<li><strong>Dysarthria:</strong> slurred speech</li>
					<li><strong>Dysphagia:</strong> difficulty swallowing</li>
					<li><strong>Exacerbation:</strong> also known as flare-up, symptoms increase in frequency and/or become more severe</li>
					<li><strong>LRP4:</strong> stands for low-density lipoprotein receptor-related protein 4. Like ACh, it&rsquo;s a protein that helps nerves communicate with muscles</li>
					<li><strong>MuSK:</strong> stands for muscle-specific kinase</li>
					<li><strong>MuSK receptor:</strong> Like ACh, it&rsquo;s a protein that helps nerves communicate properly with muscles</li>
					<li><strong>MuSK antibody:</strong> antibodies that bind to the MuSK protein</li>
					<li><strong>Myasthenic crisis:</strong> Worsening of myasthenic weakness requiring intubation or noninvasive ventilation to avoid intubation</li>
					<li><strong>Neuromuscular junction:</strong> site of communication between motor nerves and muscle fibers that makes muscles work properly</li>
					<li><strong>Ptosis:</strong> drooping eyelids</li>
					<li><strong>Seronegative myasthenia gravis:</strong> a form of MG in which autoantibodies are not detectable in the blood</li>
					<li><strong>Thymus, thymectomy:</strong> The thymus is an organ that&#39;s part of the immune system. Thymectomy is a surgery to remove this organ. </li>
				</ul>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Douglas Harper. Myasthenia. Online Etymology Dictionary. https://www.etymonline.com/word/myasthenia. Accessed May 2020.</li>
					<li>Douglas Harper. Gravid. Online Etymology Dictionary. https://www.etymonline.com/search?q=gravis. Accessed May 2020.</li>
					<li>Gilhus NE. <cite>N Engl J Med.</cite> 2016;375(26):2570-2581.</li>
					<li>Mantegazza R, et al. <cite>Ann N Y Acad Sci</cite>. 2003;998:413-23.</li>
					<li>Stetefeld H, et al. <cite>Neurol Res Pract</cite>. 2019;1(19):1-6. </li>
					<li>Melzer N, et al. <cite>J Neurol.</cite> 2016;263(8):1473-1494.</li>
					<li>Ramanujam R, et al. <cite>Twin Res Hum Genet.</cite> 2011;14:129-136.</li>
					<li>Szobor A. <cite>Acta Med Hung.</cite> 1989;46(1):13-21.</li>
					<li>Salvado M, et al.  <cite>J Neurol Sci</cite>. 2016;360:110-114.</li>
					<li>Phillips LH. <cite>Ann N Y Acad Sci</cite>. 2003;998:407-412.</li>
					<li>United States Census Bureau. United States 2019 Population Estimates. https://www.census.gov/search-results.html?searchType=web&amp;cssp=SERP&amp;q=us%20population. Accessed May 2020. </li>
					<li>Lindstrom JM, et al. <cite>Neurology.</cite> 1976;26(11):1054-1059.</li>
					<li>Hoch W, et al. <cite>Nat Med.</cite> 2001;7:365-368.</li>
					<li>Pevzner A, et al. <cite>J Neurol.</cite> 2012;259(3):427-435. </li>
					<li>Ruff RL, et al. <cite>J Neuroimmunol.</cite> 2008; 201-202:13-20. </li>
					<li>Gilhus NE, et al. <cite>Lancet Neurol</cite>. 2015;14(10):1023-1036. </li>
					<li>Hehir MK, et al. <cite>Neurol Clin.</cite> 2018;36(2):253-260. </li>
					<li>Vincent A, et al. <cite>J Neurol Neurosurg Psychiatry</cite>. 1985;48(12):1246-1252.</li>
					<li>Sanders DB, et al. <cite>Neurology.</cite> 2016;87(4):419-425.</li>
					<li>Myasthenia Gravis Foundation of America. Common Terms. https://myasthenia.org/What-is-MG/-Common-Terms. Accessed May 2020. </li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-crisis' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-my-mgsole' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
