<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-smartphone-mg-management-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-smartphone-mg-management">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-smartphone-mg-management-label" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/smartphone-MG-management' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/smartphone-MG-management' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">LIFE WITH MG</p>
			<h2 id="eop-callout-smartphone-mg-management-label" class="secondary">Managing MG: There’s an App for That</h2>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
