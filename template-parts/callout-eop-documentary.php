<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-docu-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eoc-docu">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-docu-label" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>" style="text-decoration:none">
			<span class="bttn-play"></span>
		</a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">MG DOCUMENTARY</p>
			<h2 id="eop-callout-docu-label" class="secondary">One Relentless Illness. Three Unstoppable People.</h2>
			<span class="read-duration">2 MIN VIEW</span>
		</a>
	</div>
</div>
