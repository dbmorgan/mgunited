<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-ai-diet-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-ai-diet">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-ai-diet-label" class="content-tile" href="<?php echo esc_url( home_url( 'eating-and-mg/anti-inflammatory-diet-and-myasthenia-gravis/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( 'eating-and-mg/anti-inflammatory-diet-and-myasthenia-gravis/' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">EATING & MG</p>
			<h2 id="eop-callout-ai-diet-label" class="secondary">Nutrition Buzz: Let’s Look at the Anti-inflammatory Diet</h2>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
