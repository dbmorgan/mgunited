<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="home-callout-smoothies-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-friendly-smoothies">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-smoothies-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-smoothies-label" class="secondary">5 MG-Friendly Smoothies</h2>
			<p>You don’t have to have MG to love these delicious, nutritious recipes. </p>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
