<div class="row eoc-callout" style="background-color: #003b63!important;" tabindex="0" aria-labelledby="home-callout-crisis-info-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-crisis-info">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-crisis-info-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom cta-copy-dark">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-crisis-info-label" class="secondary">Crisis 411: What You Need to Know About Myasthenic Crisis</h2>
			<p>Know the difference between MG crisis and impending crisis? You should.</p>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
