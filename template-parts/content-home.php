<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<section id="hero">
		<div class="row">
			<div class="round-img">
				<img src="<?php echo get_template_directory_uri(); ?>/images/home/mobile/01-HeroChrisGivens.png" alt="Mobile Hero">
			</div>
		</div>
		<div class="row">
			<div id="home-masthead" class="col col-sm-4 col-7">
				<h1 class="masthead">Nobody gets MG.<br>Until they get MG.</h1>
			</div>
		</div>
		<div class="row">
			<div id="home-masthead-sub" class="col col-sm-4 col-7">
				<p class="sub-masthead">Chris Givens gets real about how MG has affected his life and his plans for the future in this interview.*</p>
			</div>
		</div>
		<div class="row">
			<div id="home-masthead-button" class="col col-sm-4 col-7">
				<span class="flex-spacer"></span>
				<div>
					<a role="button" href="<?php echo esc_url( home_url( '/real-stories/chris-givens/' ) ); ?>" class="capsule primary">READ MORE</a>
				</div>
				<span class="flex-spacer"></span>
			</div>
		</div>
	</section>

	<section id="home-jump-links">
		<div class="row">
			<div class="col col-sm-6 col-8 offset-2">
				<ul>
					<li><a href="#home-featured">FEATURED</a></li>
					<li><a href="#home-latest">LATEST</a></li>
					<li><a href="#home-about-mg">ABOUT MG</a></li>
					<li><a href="#home-life-with-mg">LIFE WITH MG</a></li>
					<li><a href="#home-real-stories">REAL STORIES</a></li>
					<li><a href="#home-things-to-do">THINGS TO DO</a></li>
				</ul>
			</div>
		</div>
	</section>

	<section id="home-documentary-promo">
		<video id="hero-video" autoplay muted disablePictureInPicture loop playsinline preload="auto">
			<!-- <video muted disablePictureInPicture preload="auto"> -->
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_FULLCAST_30cutdownWOverlay_00e.mp4" type="video/mp4">
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_FULLCAST_30cutdownWOverlay_00e.webm" type="video/webm">
		</video>
		<div class="container">
			<div class="row">
				<div class="col col-sm-10 col-md-10 col-8">
					<p class="eyebrow">One relentless illness. Three&nbsp;unstoppable&nbsp;people.</p>
					<h1>A <span>Mystery</span> to&nbsp;Me</h1>
					<p class="eyebrow">A Myasthenia Gravis Docuseries</p>
					<a class="capsule" role="button" id="trailer-button" href="<?php echo esc_url( home_url( '/a-mystery-to-me/' ) ); ?>">WATCH THE TRAILER</a>
				</div>
			</div>
		</div>
	</section>

	<section id="home-featured" class="content page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-12">
				<h2 class="home-section-label">FEATURED</h2>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-8">
				<?php get_template_part( 'template-parts/callouts/primary/mg-other-conditions' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-fajitas' ); ?>
			</div>
		</div>
		<div class="row page-callout-row tertiary-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/discussion-guide-children' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/living-with-mg-for-some-time' ); ?>
			</div>
		</div>
	</section>

	<hr>

	<section id="home-latest" class="content page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-12">
				<h2 class="home-section-label">LATEST</h2>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-8">
				<?php get_template_part( 'template-parts/callouts/primary/myrealworld-mg' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-sweet-potato-soup' ); ?>
			</div>
		</div>
		<div class="row page-callout-row tertiary-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-kait' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/anti-inflammatory-diet' ); ?>
			</div>
		</div>
	</section>

	<section id="home-mission-promo">
		<div class="row">
			<div class="col col-sm-6 col-8 offset-2">
				<p class="home-mission-promo-eyebrow">OUR MISSION</p>
				<h2>MG RAISES QUESTIONS.<br>LOTS OF THEM.</h2>
				<p class="home-mission-promo-copy">MG United began as a conversation with people in the MG community. This is what we heard. And it’s why we’re here.</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-sm-6 col-12">
				<div id="home-mission-promo-button">
					<span class="flex-spacer"></span>
					<div>
						<a role="button" href="<?php echo esc_url( home_url( '/mission/' ) ); ?>" class="capsule primary">WATCH VIDEO</a>
					</div>
					<span class="flex-spacer"></span>
				</div>
			</div>
		</div>
	</section>

	<section id="home-about-mg" class="content page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-12">
				<h2 class="home-section-label">ABOUT MG</h2>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/what-is-mg' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/newly-diagnosed' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/living-with-mg-for-some-time' ); ?>
			</div>
		</div>
		<div class="row page-callout-row tertiary-callout-row">
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/doctor-tools' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/mg-other-conditions' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/treatment-goals' ); ?>
			</div>
		</div>
	</section>

	<section id="home-my-real-world-mg-app-promo" class="page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-8 offset-4">
				<h2>SEE HOW MG RESEARCH IS PUSHING FORWARD. (WE HAVE AN APP FOR THAT.)</h2>
				<p class="home-app-promo-copy">MyRealWorld™ MG research study participants may contribute to research firsthand by recording information related to their MG diagnoses and management over a two-year period.</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-sm-6 col-4 offset-4">
				<div id="home-apple-app-promo-button">
					<span class="flex-spacer"></span>
					<div>
						<a role="button" href="https://apps.apple.com/us/app/myrealworld/id1482374347" target="_blank" class="img-button"><img src="<?php echo get_template_directory_uri(); ?>/images/home/desktop/14-apple.svg"></a>
					</div>
					<span class="flex-spacer"></span>
				</div>
			</div>
			<div class="col col-sm-6 col-4">
				<div id="home-android-app-promo-button">
					<span class="flex-spacer"></span>
					<div>
						<a role="button" href="https://play.google.com/store/apps/details?id=com.mg.vitaccess" target="_blank" class="img-button"><img src="<?php echo get_template_directory_uri(); ?>/images/home/desktop/14-google-play-badge.svg"></a>
					</div>
					<span class="flex-spacer"></span>
				</div>
			</div>
		</div>
	</section>

	<section id="home-life-with-mg" class="content page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-12">
				<h2 class="home-section-label">LIFE WITH MG</h2>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-8">
				<?php get_template_part( 'template-parts/callouts/primary/discussion-guide-children' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-casserole' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/myrealworld-mg' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-sweet-potato-soup' ); ?>
			</div>
		</div>
		<div class="row page-callout-row tertiary-callout-row">
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/anti-inflammatory-diet' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/discussion-guide' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/invisible-burden' ); ?>
			</div>
		</div>
	</section>

	<section id="home-real-stories" class="content page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-12">
				<h2 class="home-section-label">REAL STORIES</h2>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-8">
				<?php get_template_part( 'template-parts/callouts/primary/profile-kait-masters' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-morgan' ); ?>
			</div>
		</div>
		<div class="row page-callout-row tertiary-callout-row">
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-victor' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-chris-givens' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/profile-kathy' ); ?>
			</div>
		</div>
	</section>

	<section id="home-share-story-promo">
		<div class="row">
			<div class="col col-sm-6 col-8 offset-4">
				<h2>If you have MG, you have a story to tell.</h2>
				<p class="home-share-story-promo-copy">Every person living with MG has a story that may help others in similar situations. We’d like to hear yours.</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-sm-6 col-8 offset-4">
				<div id="home-share-story-promo-button">
					<span class="flex-spacer"></span>
					<div>
						<a role="button" href="<?php echo esc_url( home_url( '/real-stories/#form-container' ) ); ?>" class="capsule primary">SHARE YOUR MG STORY</a>
					</div>
					<span class="flex-spacer"></span>
				</div>
			</div>
		</div>
		<div class="row promo-img"></div>
	</section>

	<section id="home-things-to-do" class="content page-callouts">
		<div class="row">
			<div class="col col-sm-6 col-12">
				<h2 class="home-section-label">THINGS TO DO</h2>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/mymgsole-gallery' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/my-mg-sole' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/mg-illuminate' ); ?>
			</div>
		</div>
	</section>

	<section id="action" class="row form-module violator">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<section id="disclaimer" class="container">
		<div class="row">
			<div class="col">
				<p>*Paid contributor to MG United</p>
				<p>MG&nbsp;United provides news and information for people with myasthenia gravis and the people who love them. But it does not provide medical advice. Always go to your physician, neurologist or other appropriate health professional for individual guidance, diagnosis and treatment.</p>
			</div>
		</div>
	</section>

</article><!-- #post-<?php the_ID(); ?> -->
