<div class="row eoc-callout vertical-cta-row" style="background-color: #003b63!important;" tabindex="0" aria-labelledby="home-callout-leah-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-leah-profile">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-leah-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom cta-copy-dark">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-leah-label" class="secondary">Leah Gaitan-Diaz and the Empowerment of Positive Thinking</h2>
			<p>This California native started taking charge of her MG by insisting on positivity in her life.</p>
			<span class="read-duration">6 MIN READ</span>
		</a>
	</div>
</div>
