<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="kathy-profile-article" <?php post_class(); ?>>

	<section id="hero">
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>6 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>Kathy Lemenu and Her Wife Diane on MG, Marriage and Communication</h1>
			</div>
		</div>
	</section>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/images/kathy-profile/kathy-bio-image.jpg" alt="" />
				</div>
				<div>
					<p><b><em>
						Kathy and her wife, Diane, were caring for their baby daughter when Kathy was first diagnosed with MG. Surrounded by loving family and friends in Detroit, MI, this couple adjusted to their new reality with open and honest talks. MG&nbsp;United spoke with Kathy and Diane on how they’ve changed the way they communicate in the face of MG.
					</em></b></p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Kathy, your MG worsened gradually. When did it become real for you?
				</h3>
				<p>
					<b>Kathy:</b> As a nurse, I worked mostly in obstetrics. It's a pretty physical department to work in. You're active and have to react quickly to changing situations. But I was having more and more difficulty doing things that weren't a problem before. I hadn’t been diagnosed yet, so I figured it was just age. I didn't even really question it. I thought, “Well, sucks to be me.”
				</p>
				<p>
					But after I was diagnosed, when my sister and I were shopping, I started getting very short of breath and I could barely move. I felt very strange. So we went to the hospital right away. I think being in the hospital was kind of a turning point for Diane too, that this mysterious illness became a real thing with consequences. It was kind of a blessing because it wasn’t a severe hospitalization and I was released pretty quickly. But we got the point about how serious MG can be.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Did you find that being a nurse helped you be a strong self-advocate in the doctor’s office?
				</h3>
				<p>
					<b>Kathy:</b> Actually, no. I'm pretty good at advocating for other people, but not for myself. I guess I’d rather not think about MG too much and just go out and live my life. But Diane is totally different. She wants the details. She wants all the answers that I never bother to get.
				</p>
				<p>
					For example, my doctor told me recently, “You are at stasis, you are at your baseline. My meds, my tricks, aren't going to get you any better than this." So I figured, well, okay. And I went home and told that to Diane and she said, “No, that's not good enough.” She wants us to get a second opinion. So we're going to.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					What challenges has MG placed on your relationship?
				</h3>
				<p>
					<b>Kathy:</b> From the time I got diagnosed until now, I’ve had this terrible fatigue. Diane would say, “The doctor said you shouldn't be tired.” I know she was probably thinking, “Get out there and do the dishes.” She’d ask if this was something I could power through. That doesn't feel good. The misconceptions about fatigue in MG really sets up problems for couples.
				</p>
				<p>
					<b>Diane:</b> Before she was diagnosed, I couldn't understand why she was so tired. I was confused and sometimes I would get frustrated by it. I think back on that time and I think, well, she probably had MG symptoms long before she was diagnosed and I had just assumed that she just didn't want to get out of bed, didn't want to feed the baby. MG is one of those conditions where you can't really see clearly what the effects are. So we started getting smarter about how to communicate. I had to develop a lot more empathy.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					How has the way you communicate changed since Kathy’s MG diagnosis?
				</h3>
				<p>
					<b>Kathy:</b> It's a good thing that she and I like to talk things over. We're like a dog with a bone and we just get in there and talk, talk, talk, talk until we can figure things out for ourselves. It's been difficult for me because I'm older than Diane and fighting stereotypes of age. So sometimes I work harder at hiding it. But you can’t hide it at home every day.
				</p>
				<p>
					<b>Diane:</b> Kathy doesn't want to put the burden of not feeling well on the family, but it's super important for her to tell me. For her to say, “I don't feel well,” when she doesn’t want to talk about it is really brave. And when I need help, I know I can ask, “How can you help me in a way that won't create more fatigue for you?” We really have to be smart about the division of labor. For example, Kathy is really good at talking and reading with our 12-year-old. She can be present for our daughter without exerting a lot of energy. That's a valuable thing that Kathy brings that doesn't knock her out.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/kathy-profile/kathy-diane-profile-midpage.png" alt="" /></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					What advice do you have for other couples trying to talk about MG?
				</h3>
				<p>
					<b>Kathy:</b> It takes such a burden off of me when people understand the fatigue and make it easier for me to live life the way I need to, when they don’t try to convince me to do things I can’t or make me feel like I am losing out on something.
				</p>
				<p>
					<b>Diane:</b> We’ve developed this thing we call ‘kind confession.’ It’s where we give each other permission to voice difficult feelings to each other, as long as we do it in a compassionate way. With kind confession, I don’t have to just silently have these feelings. I can say things out loud like, “I'm feeling sad that you can't go to this event with me.” And Kathy can admit when she’s not feeling well and really wants me to go and have a good time without her.
				</p>
				<p>
					With kindness, you can discuss sensitive subjects, like household chores, without fighting or silent resentment. Caregivers deserve to be able to ask for things from their partners who have MG, even though they might worry it’s going to be too much. You can have faith that your partner is a grown person and can say no. You don’t have to assume that they can’t do things. It's okay to ask.
				</p>
				<p>
					If you trust yourself and your partner, you can get to those vulnerable feelings behind the frustration. You can take the blame out of it. MG can be the bad guy instead of the person who has it or the person who loves them.
				</p>
				<p>
					We still have our struggles. I don’t always remember these things when I'm really tapped out. Sometimes I react like a wife, not like a psychologist. But we can talk about everything. That's one of the best things about our relationship.
				</p>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-leah-gaitan-diaz' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-been-living-with-mg-for-some-time' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
