<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="savory-soul-food-casserole-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>EATING & MG</b></p>
				<h1>Savory Soul Food Casserole</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>SEPTEMBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					Get real cozy with this fall flavored, layered dish—with collard greens, parsnips and sweet potatoes.
				</h2>
				<p>Soul food doesn’t have to be unhealthy. This much-loved cuisine is back on the menu for the health-conscious with some easy recipes you can enjoy at home. Chef Kim Mills appreciates soul food and accepted the challenge to find a way to make it more MG-friendly.</p>
				<p>“While the traditional ways of preparing soul food aren’t ideal for people living with MG, this casserole combines favorites like collard greens, sweet potatoes and parsnips into one really tasty—and healthy—baked dish,” Chef Kim said.</p>
				<p>Made primarily with seasonal veggies, this Savory Soul Food Casserole is an easy way to get healthy ingredients and low-calorie foods into your diet. Check out the recipe below.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/savory-soul-food-casserole/parsnip.png" alt="Casserole" class="lg" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/savory-soul-food-casserole/parsnip-mobile.png" alt="Casserole" class="sm" />
				</p>
				<div class="recipe-bar">
					<div class="outer-col">
						<div>COOK TIME: <b>45 MIN</b></div>
					</div>
					<div class="inner-col">
						<div>PREP TIME: <b>25 MIN</b></div>
					</div>
					<div class="outer-col">
						<div class="print">
							<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/AGX_SoulFoodCasserole_Recipe.pdf' ) ); ?>">PRINT RECIPE</a>
						</div>
					</div>
				</div>
				<div class="recipe">
					<div class="row">
						<div class="ingredients">
							<div>
								<p><strong>Ingredients</strong></p>
								<ul>
									<li>2 pounds parsnips</li>
									<li>1 pound sweet potatoes or yams</li>
									<li>1 &#x00BD; pounds collard greens</li>
									<li>1 cup nonfat Greek yogurt</li>
									<li>2 cups low-sodium chicken broth</li>
									<li>2 tbsp butter</li>
									<li>Pinch of nutmeg</li>
									<li>Salt, to taste</li>
									<li>Pepper, to taste</li>
									<li>Preferred oil or butter, as needed</li>
									<li>1 cup chopped onion</li>
									<li>2 chopped garlic cloves</li>
									<li>2 tbsp tomato paste</li>
									<li>1 &#x00BD; pounds ground turkey (ground beef, chicken or lamb will also work)</li>
									<li>Oregano, to taste</li>
									<li>Garlic powder, to taste</li>
									<li>Thyme, to taste</li>
									<li>Red pepper flakes</li>
								</ul>
							</div>
						</div>
						<div class="instructions">
							<h2>Savory Soul Food Casserole</h2>

							<div class="directions">
								<p><strong>Directions</strong></p>
								<ol>
									<li>Peel and cut parsnips and sweet potatoes into &#x00BD;-inch to 1-inch slices. Place in a large pot and cover with cold water. Add a pinch of salt. Bring to a boil and simmer until the veggies pierce easily with a fork. Drain. While still warm, add Greek yogurt, 1 cup low-sodium chicken broth and 1 tablespoon butter. Mash until combined and the mixture is still chunky. Season with a pinch of nutmeg and salt and pepper to taste.</li>
									<li>While the parsnips and sweet potatoes are cooking, drizzle oil in a skillet over medium heat. Add chopped onions and 1 chopped garlic clove. When onions are translucent, add 2 tablespoons of tomato paste. Cook briefly. Then, add ground meat and season with oregano, garlic powder and thyme to taste. Mix well. Add 1 cup low-sodium chicken broth and cook until simmering.</li>
									<li>Heat oil in a large sauté pan over high heat. Add chopped collard greens and 1 chopped garlic clove. Season with red pepper flakes, salt and pepper to taste. Reduce heat to medium. Add enough water to cover and steam the greens until tender, about 10 minutes.</li>
									<li>Preheat oven to 400&#x00B0;F.</li>
									<li>In a greased 13-by-9-inch baking dish, place a thin layer of mashed potatoes. Spoon the meat evenly over the potato mixture, and top with sautéed collard greens. Add the remaining mashed potatoes, beginning with edges of the pan to create a seal to prevent the casserole from bubbling up. Cover tightly and bake for about 20 minutes. Increase the oven’s temperature to 450&#x00B0;F, and bake for 5 additional minutes or until brown.</li>
								</ol>

							</div>
							<p><strong>Yield:</strong> 4-6 Servings</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'hungry' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-victor-and-iris-yipp' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-zesty-chicken-fajitas' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-been-living-with-mg-for-some-time' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
