<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>
<article id="myasthenia-gravis-friendly-roasted-sweet-potato-and-ginger-soup-article" <?php post_class(); ?>>
	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>EATING & MG</b></p>
				<h1>Roasted Sweet Potato and Ginger Soup</h1>
			</div>
		</div>
	</section>
	<section id="article-hero-strip">
		<div>
			<div>
				<p>OCTOBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h2>Flavorful seasonal veggies and fall spices combine in a snap to create an MG-friendly soup that’ll warm up the whole family. </h2>
				<p>The transition to autumn is so gradual that you may barely notice it happening. One day you look up and the leaves have changed colors, people are wearing cozy sweaters and everyone is looking for the latest pumpkin spice-flavored recipe.</p>
				<p>Speaking of spices, the seasonal shift may also change the way we eat. Instead of the <a href="https://www.mg-united.com/eating-and-mg/5-mg-friendly-smoothies/" target="_self">chilled</a>, light foods of summer, many people start to lean toward a warmer, heartier menu of comfort foods that will keep them toasty and full for a while. That’s what inspired Chef Kim Mills to create this delicious recipe for roasted sweet potato and ginger soup.</p>
				<p>“You’ve got tasty seasonal veggies and plenty of spices in the pureed mix,” Chef Kim said. “There’s not only a lot of flavor here, but this soup is also relatively low-calorie and low-sugar.* Best of all, it’s really easy to prepare.”</p>
				<p>That’s the kind of <a href="https://www.mg-united.com/eating-and-mg/savory-soul-food-casserole/" target="_self">recipe</a> that could earn repeat appearances in any kitchen. Dig in below.</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/roasted-sweet-potato-and-ginger-soup/02_Soup_Plated_with_Garnish.jpg" alt="Sweet Potato Soup" class="lg" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/roasted-sweet-potato-and-ginger-soup/02_Soup_Plated_with_Garnish.jpg" alt="Sweet Potato Soup" class="md" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/roasted-sweet-potato-and-ginger-soup/02_Soup_Plated_with_Garnish_mobile.jpg" alt="Sweet Potato Soup" class="sm" />
				</p>
				<p class="image-description"><i>Roasted sweet potato and ginger soup garnished with cilantro and flaked coconut.</i></p>
				<div class="recipe-bar">
					<div class="outer-col">
						<div>COOK TIME: <b>60 MIN</b></div>
					</div>
					<div class="inner-col">
						<div>PREP TIME: <b>15 MIN</b></div>
					</div>
					<div class="outer-col">
						<div class="print">
							<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MG-United_RoastedSweetPotatoGingerSoup.pdf' ) ); ?>">PRINT RECIPE</a>
						</div>
					</div>
				</div>
				<div class="recipe">
					<div class="row">
						<div class="ingredients">
							<div>
								<p><strong>Ingredients</strong></p>
								<ul>
									<li>2 large sweet potatoes or yams</li>
									<li>8 oz pumpkin pie filling<sup>†</sup></li>
									<li>1 medium-sized shallot or ½ sweet onion</li>
									<li>1 cup coconut milk</li>
									<li>3 cups low-sodium vegetable stock</li>
									<li>½ tsp turmeric</li>
									<li>1 tbsp fresh ginger</li>
									<li>¼ tsp nutmeg</li>
									<li>¼ tsp cumin</li>
									<li>¼ tsp paprika</li>
									<li>Salt and pepper, to taste</li>
									<li>Olive oil, as needed</li>
									<li>Garnish, as desired</li>
								</ul>
							</div>
						</div>
						<div class="instructions">
							<h2>Roasted Sweet Potato and Ginger Soup</h2>
							<img class="social-line" src="<?php echo get_template_directory_uri(); ?>/images/social-line.svg">
							<div class="directions">
								<p style="margin-bottom:18px;"><strong>Directions</strong></p>
								<p style="margin-bottom:17px;">Preheat oven to 425˚F.</p>
								<ol>
									<li>Chop sweet potatoes into cubes or buy them already cubed. Slice shallot or sweet onion in half. </li>
									<li>Toss cut vegetables in olive oil, salt, pepper, cumin, nutmeg, turmeric and paprika until evenly coated. Spread across a parchment paper-lined baking sheet for easy clean up.</li>
									<li>Roast for about 35 to 45 minutes, tossing occasionally until browned. Sweet potato should be firm yet tender.</li>
									<li>Add roasted sweet potato, shallot, pumpkin, ginger, 1 ½ cups vegetable stock and 1 cup coconut milk to blender. Blend until smooth.</li>
									<li>Transfer mixture to medium-sized saucepan and warm over medium heat until soup comes to a light simmer.</li>
									<li>Add in remaining vegetable stock. Continue to simmer, stirring occasionally to combine ingredients. Adjust seasonings to taste with salt and pepper as needed.</li>
								</ol>
							</div><br>
							<p>Serve hot, chilled or at room temperature. </p>
							<p><strong>Yield:</strong> 4 servings</p>
						</div>
					</div>
					<div><br>

						<p><i>*Calories per serving: 253 with coconut milk (180 with light coconut milk)</i></p>
						<p><i><sup>†</sup>May also be substituted with a small- to medium-sized fresh pumpkin. Cut open, remove seeds, dice and roast with sweet potatoes.</i></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/callout', 'hungry' ); ?>
	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-anti-inflammatory-diet' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-kathy-and-diane' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-what-is-mg' ); ?>
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
