<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-burden-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-invisible-burden">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-burden-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/the-invisible-burden' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/the-invisible-burden' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">EMOTIONAL SUPPORT</p>
			<h2 id="eop-callout-burden-label" class="secondary">16 Ways Caregivers Can Care for Themselves</h2>
			<span class="read-duration">10 MIN READ</span>
		</a>
	</div>
</div>
