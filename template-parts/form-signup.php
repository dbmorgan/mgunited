<?php

	// We'll use the API to retrieve static form data
	include_once get_template_directory() . "/api-tools.php";

	$staticFormData = get_static_form_data();
	if ($staticFormData == null)
		return;

	const ID_FIRST_NAME = 'sign-up-first-name';
	const ID_LAST_NAME = 'sign-up-last-name';
	const ID_EMAIL = 'sign-up-email';
	const ID_SIGN_UP_OPTIN = 'sign-up-optin';

	$slug = $wp_query->queried_object->post_name

?>
<form id="form-step-sign-up">
	<!--stayinvolved-->
	<?php if ($slug == 'a-mystery-to-me' || $slug == 'vanetta-drummer-fenton' || $slug == 'glenn-phillips' || $slug == 'teresa-hill-putnam') { ?>
		<div class="form-header">
			<h2>Sign Up for MG United&nbsp;to Watch.</h2>
			<p>MG United members get exclusive access to this unique documentary series and receive an invitation to the virtual US premiere on November 17<sup>th</sup>.</p>
		</div>
	<!-- mymgsole gallery article -->
	<?php } elseif ($slug == 'my-mg-sole-myasthenia-gravis-art-project') { ?>
		<div class="form-header">
			<h2>Join the Soles</h2>
			<p>Just download the kit, and you’ll be inspired to start your art in no time. By signing up for MG United, you will also get regularly updated news and information that is meant for you.</p>
		</div>
	<!--sharegoal-article-->
	<?php } elseif ($slug == 'treatment-goals') { ?>
		<div class="form-header">
			<h2>What Is Your Goal for Life with MG?</h2>
			<p>It's easier to manage your MG when you know what you want to be able to do and experience in your daily life. Share one of your life goals here on MG&nbsp;United—you may inspire others as well.</p>
		</div>

	<!--sharestory-->
	<?php } elseif (
		$slug == 'been-living-with-mg-for-some-time' ||
		$slug == 'victor-and-iris-yipp' ||
		$slug == 'leah-gaitan-diaz' ||
		$slug == 'kathy-and-diane' ||
		$slug == 'morgan-greene' ||
		$slug == 'the-invisible-burden' ||
		$slug == 'chris-givens' ||
		$slug == 'real-stories'
	) { ?>
		<div class="form-header">
			<h2>SHARE YOUR MG STORY WITH US</h2>
			<p>Every person living with MG has a story that may help others in similar situations. We’d like to hear yours.</p>
		</div>
	<?php } ?>

	<?php if ( is_page('sign-up') || is_front_page()) { ?>
		<h2>SIGN UP FOR MG&nbsp;UNITED</h2>
		<h2 class="secondary">Tell us about yourself.</h2>
		<p class="secondary">*indicates required field</p>
	<?php } else { ?>
		<?php if ( $slug == 'a-mystery-to-me' && $slug == 'vanetta-drummer-fenton' && $slug == 'glenn-phillips' && $slug == 'teresa-hill-putnam' ) { ?>
			<h2>SIGN UP FOR MG&nbsp;UNITED</h2>
		<p class="secondary">Tell us about yourself.</p>

		<?php } ?>
		<p class="secondary">*indicates required field</p>
	<?php } ?>

	<fieldset aria-label="First name">
		<label>
			First name*<br />
			<input aria-required="true" type="text" id="<?php echo ID_FIRST_NAME; ?>" maxlength="<?php echo $staticFormData->maxLengths->firstName; ?>" />
		</label>

		<div class="error-message"></div>
	</fieldset>
	<fieldset aria-label="Last name">
		<label>
			Last name*<br />
			<input aria-required="true" type="text" id="<?php echo ID_LAST_NAME; ?>" maxlength="<?php echo $staticFormData->maxLengths->lastName; ?>" />
		</label>

		<div class="error-message"></div>
	</fieldset>

	<fieldset aria-label="Email address">
		<label>
			Email address*<br />
			<input aria-required="true" type="text" id="<?php echo ID_EMAIL; ?>" maxlength="<?php echo $staticFormData->maxLengths->email; ?>" />
		</label>

		<div class="error-message"></div>
	</fieldset>

	<fieldset aria-label="Sign up Optin" class="optin">
		<label class="checkbox">
			<input aria-required="true" type="checkbox" id="<?php echo ID_SIGN_UP_OPTIN; ?>" checked/>
			By clicking &#x201c;Sign Up Now,&#x201d; you agree to sign up for MG&nbsp;United and receive information from argenx, MG&nbsp;United, its affiliates, service providers, and co-promotion partners. Intended for U.S. audiences only.
		</label>

		<div class="error-message"></div>
	</fieldset>

	<input id="sign-up-step1-next" type="submit" class="primary" value="SIGN UP NOW" />
</form>
