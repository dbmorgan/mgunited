<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="home-callout-ai-diet-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-anti-inflammatory">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-ai-diet-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/mg-anti-inflammatory-diet' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/mg-anti-inflammatory-diet' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-ai-diet-label" class="secondary">The Anti-Inflammatory Diet: Hype or Hope?</h2>
			<p>Get the 411 on what the scientific community thinks about this much-discussed approach to eating.</p>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
