<?php
/*
*
* load URL of current page from wordpress
*/
$obj_id = get_queried_object_id();
$current_url = get_permalink( $obj_id );
?>

<!-- get featured image -->
<?php if (has_post_thumbnail( $post->ID ) ): ?>
  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php endif; ?>

<p><b class="social-share" data-image="<?php echo $image[0]; ?>">SHARE</b></p>

<!-- Load Facebook SDK for JavaScript -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '285241805668738',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v7.0'
    });
  };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

<!-- Load Twitter SDK -->
<script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>

<!-- Load Pinterest SDK
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>-->
