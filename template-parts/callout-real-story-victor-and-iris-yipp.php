<div class="row eoc-callout" tabindex="0" aria-labelledby="real-story-callout-victor-label">
	<div class="col col-sm-12 col-7 cta-left-reverse cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/victor-and-iris-yipp' ) ); ?>" style="text-decoration:none">
			<h2 id="real-story-callout-victor-label" class="secondary">Victor and Iris Yipp Find Hope in MG</h2>
			<p>This couple faced Victor’s MG diagnosis together and found hope in his remission.</p>
			<span class="read-duration">6 MIN READ</span>
		</a>
	</div>
	<div role="presentation" class="col col-sm-12 col-5 cta-right-reverse cta-top" id="real-story-victor">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="real-story-callout-victor-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/victor-and-iris-yipp' ) ); ?>" style="text-decoration:none"></a>
	</div>
</div>
