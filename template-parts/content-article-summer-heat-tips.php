<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="summer-heat-tips-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>LIFE WITH MG</b></p>
				<h1>14 Ways to<br/>Outsmart Summer</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					When the weather gets warm, people with MG have to get clever.
				</h2>
				<p>
					Countless songs, movies and books celebrate the magic of summer. Unfortunately for people with MG, summertime can sometimes be no picnic, since hot weather can make MG symptoms worse.<sup>1-3</sup> However, with a little planning and a few pro tips, it’s possible to safely enjoy the magic of summer.
				</p>
				<div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img style="width: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/clock.svg"></div>
				<div class="col-8 content">
					<h3>Timing Is Everything<sup>4</sup></h3>
					<p>If possible, avoid going outside during the hottest parts of the day. Try to use the cooler mornings to knock out chores like walking your dog, running errands and gardening. When you must venture out midday, stay in the shade as much as possible. And take extra breaks to avoid overheating.</p>
				</div>
			</div>
		</div>
	</section>


	<section class="row article-body columns right">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/hat.svg"></div>
				<div class="col-8 content">
					<h3>Dress Cool, Stay Cool<sup>4</sup></h3>
					<p>For people with MG, the smart fashion statement for summer is always cool and comfortable. Stick with loose, light-colored cotton or moisture-wicking clothing. Hats are great to keep the sun out.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Hydrate Like a Boss</h3>
				<p>
					In the warm summer months, staying hydrated is a little more challenging. Make sure to drink plenty of fluids throughout the day. There are many good options, from water to lemonade to iced tea. But keep in mind that not all of them are created equal.<sup>4</sup> There are some you may want to stay away from. Caffeine drinks can have a mild diuretic effect (meaning they make you pee), but studies show that there’s no significant dehydrating effect from moderate daily coffee intake.<sup>5</sup> So don’t let summer hydration worries get between you and your morning joe. 
				</p>
				<p>
					But booze? That’s another story. Alcohol has been shown to have a dehydrating effect on your body.<sup>4,6</sup> So when the mercury is up, your alcohol intake should go down. The smarter bet is icy nonalcoholic drinks and frozen desserts, which can help cool you and hydrate you at the same time. Be sure to check out our <a href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies' ) ); ?>">smoothie recipes</a> created just for people with MG.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/barbell.svg"></div>
				<div class="col-8 content">
					<h3>A Workout Plan for the Dog Days</h3>
					<p>
						If you want to exercise this summer and have your doctor’s okay, try working out in the cooler morning or indoors. And take baby steps to build up your stamina—never overexert yourself. Talk with your doctor about exercises that might work. Also, keep in mind that heat can intensify MG symptoms.<sup>1-3</sup> It’s best to avoid all strenuous activity when your MG symptoms become aggravated.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img alt="Man in pool." src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/man-in-pool.jpg" alt="">
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/pills.svg"></div>
				<div class="col-8 content">
					<h3>The Side Effects of Summer</h3>
					<p>Ask your doctor if any of the medications you take might increase the risks of overheating. Allergy medications, decongestants and blood pressure medications are just a few such examples.<sup>4,7</sup> But your doctor can discuss these and others with you, so just ask!</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body columns right">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/life-ring.svg"></div>
				<div class="col-8 content">
					<h3>Time for a Cool Dip</h3>
					<p>Avoid hot showers and saunas. They may feel good in the moment, but they really heat up your body: the exact thing you’re trying to avoid in the summer. Instead, have a cool shower or wade into the water to cool down.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Tech Up for the Season</h3>
				<p>You may not have heard, but we seem to be living in a golden age of products designed to help keep you cool in the summer. There are, of course, tried and true classics like personal fans, spray bottles, cold washcloths and ice packs. And they’re classics for a reason—they’re simple, inexpensive and work great. So use ’em!</p>
				<p>But you may be less aware of more recent cooling innovations. The list includes cooling vests, towels, bandanas, hats and bracelets. There are also cooling mattresses, mattress pads, pillows, car seats and more. You can find them simply by googling any of the items listed above with the word <em>cooling</em> in front. Prepare to be amazed at all the cool new tools at your disposal to beat the summer heat.</p>
			</div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/14-ways-to-outsmart-summer/sun.svg"></div>
				<div class="col-8 content">
					<h3>Make the Most of Summer</h3>
					<p>Hot weather can certainly make your life with MG a little more challenging. However, you still get to enjoy summer. Just do a little extra planning to avoid the hottest parts of the day. Be aware of medications that could help cause you to overheat. And make it your business to stay hydrated and keep cool. Keep these things in mind, and you too can have a truly magical summer.</p>
				</div>
			</div>
		</div>
	</section>


	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Rutkove SB, et al. <em>Muscle Nerve</em>. 1998;21:1414-1418.</li>
					<li>Rutkove SB. <em>Muscle Nerve</em>. 2001;24:867-882.</li>
					<li>Borenstein S, et al. <em>Lancet</em>. 1974;2(7872):63-66.</li>
					<li>Peiris, AN. <em>JAMA</em>. 2017;318(24):2503.</li>
					<li>Killer S, et al. <em>PLoS One</em>. 2014;9(1):e84154.</li>
					<li>Polhuis, et al. <em>Nutrients</em>. 2017;9(7):660.</li>
					<li>Glazer, JL. <em>Am Fam Physician</em>. 2005;71(11):2133-2140.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-5-mg-friendly-smoothies' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-newly-diagnosed' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
