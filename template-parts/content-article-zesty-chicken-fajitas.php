<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="zesty-chicken-fajitas-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>EATING & MG</b></p>
				<h1>Zesty Chicken Fajitas</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>SEPTEMBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					Chicken, rice and veggies simmer together in a flavor-packed, MG-friendly nod to Mexican food. Dig in.
				</h2>
				<p>You’d be hard-pressed to find someone who doesn’t enjoy Mexican food. It’s jam-packed with flavor, and it’s easy to work into your diet.</p>
				<p>MG&nbsp;United tapped Chef Kim Mills, a big fan of Mexican food, to create this deliciously simple fajita recipe. Because it’s freezer-friendly, you can make this whenever you have the energy to cook, and then reheat it on nights when MG symptoms make dinner prep feel like one task too many.</p>
				<p>“When it’s hot out, you’re not really craving heavy, rich meals,” Chef Kim said. “This recipe combines seasonal veggies, lean meat and whole grains into a dish that’ll have everybody coming back for seconds.”</p>
				<p>Get into the recipe below.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/zesty-chicken-fajitas/02-chicken_fajita.png" alt="Chicken Fajijta" class="lg" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/zesty-chicken-fajitas/02-chicken_fajita-mobile.png" alt="Chicken Fajita" class="sm" />
				</p>
				<div class="recipe-bar">
					<div class="outer-col">
						<div>COOK TIME: <b>20 MIN</b></div>
					</div>
					<div class="inner-col">
						<div>PREP TIME: <b>10 MIN</b></div>
					</div>
					<div class="outer-col">
						<div class="print">
							<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/AGX_ZestyChickenFajitas_Recipe.pdf' ) ); ?>">PRINT RECIPE</a>
						</div>
					</div>
				</div>
				<div class="recipe">
					<div class="row">
						<div class="ingredients">
							<div>
								<p><strong>Ingredients</strong></p>
								<ul>
									<li>3 boneless, skinless chicken breasts</li>
									<li>2 tbsp chopped garlic</li>
									<li>1 cup diced red bell pepper</li>
									<li>&#x00BD; cup diced onion</li>
									<li>&#x00BD; cup riced cauliflower</li>
									<li>2 cups cooked brown rice</li>
									<li>2 bay leaves</li>
									<li>&#x00BC; cup chicken stock</li>
									<li>Preferred oil or butter (enough to coat skillet)</li>
									<li>1 lime cut into quarters</li>
									<li>Fajita seasoning*</li>
								</ul>
							</div>
<!--							<div class="print lg">-->
<!--								<a class="no-interstitial" target="_blank" download href="--><?php //echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-ABJ.pdf' ) ); ?><!--">PRINT RECIPE</a>-->
<!--							</div>-->
						</div>
						<div class="instructions">
							<h2>Zesty Chicken Fajitas</h2>

							<div class="directions">
								<p><strong>Directions</strong></p>
								<ol>
									<li>Remove the chicken from the refrigerator and allow it to reach room temperature before preparing (about 20 minutes). This helps it stay moist and cook evenly.</li>
									<li>Rinse the chicken and pat dry. Liberally season on both sides with fajita seasoning, reserving &#x00BD; tsp for the stir fry.</li>
									<li>In a stockpot, combine the seasoned chicken breast, chicken stock, about 3 cups of water (enough to cover the chicken) and bay leaves. Bring to a boil, then lower the heat to a simmer. The chicken is done when it reaches an internal temperature of 165&#x00B0;F.</li>
									<li>Remove the chicken from the stockpot and allow it to rest for two minutes. Shred the chicken by pulling it apart with two forks. Set aside.</li>
									<li>Coat a skillet with oil over medium heat. Add diced onion, diced red bell pepper, chopped garlic and riced cauliflower. Mix the vegetables and cook until onions are translucent, and the vegetables are soft—about 4 to 5 minutes.</li>
									<li>Add the shredded chicken, rice and remaining fajita seasoning to the pan. Cook until heated through. Sprinkle with the juice of &#x00BC; lime.</li>
								</ol>
							</div>
							<p>Remove bay leaves before eating. This recipe can be frozen in batches and is good for up to 2 months.</p>
							<p><strong>Yield:</strong> 4-6 Servings</p>

							<div class="seasoning">
								<div>
									<p><strong>*Fajita Seasoning</strong></p>
									<p>Make your own at home with less salt and additives. Stir together all ingredients and store in an airtight container.</p>
									<ul>
										<li>2 tsp chili powder</li>
										<li>1 tsp salt</li>
										<li>1 tsp paprika</li>
										<li>&#x00BD; tsp onion powder</li>
										<li>&#x00BD; tsp cumin</li>
										<li>&#x00BD; tsp garlic powder</li>
										<li>&#x00BD; tsp oregano</li>
										<li>&#x00BC; tsp cayenne pepper</li>
									</ul>
								</div>
							</div>

						</div>

					</div>
<!--					<div class="row sm">-->
<!--						<div class="print">-->
<!--							<a class="no-interstitial" target="_blank" download href="--><?php //echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-ABJ.pdf' ) ); ?><!--">PRINT RECIPE</a>-->
<!--						</div>-->
<!--						<div class="directions">-->
<!--							<p><strong>Directions:</strong></p>-->
<!---->
<!--							<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>-->
<!--						</div>-->
<!--					</div>-->
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'hungry' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-savory-soul-food-casserole' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-morgan-greene' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
