<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="about-mg-callout-for-some-time-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="about-mg-sometime">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="about-mg-callout-for-some-time-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/been-living-with-mg-for-some-time' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/been-living-with-mg-for-some-time' ) ); ?>" style="text-decoration:none">
			<h2 id="about-mg-callout-for-some-time-label" class="secondary">Been Living with MG for Some Time?</h2>
			<p>Being prepared for changes in your MG can make a big difference.</p>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
