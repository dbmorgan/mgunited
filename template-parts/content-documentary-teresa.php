<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="documentary-bio" <?php post_class(); ?>>
	<section id="hero">
		<!-- create space  -->
		<video id="hero-video" autoplay muted disablePictureInPicture loop playsinline preload="auto">
			<!-- <video muted disablePictureInPicture preload="auto"> -->
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_TERESA_15cutdown_00c.mp4" type="video/mp4">
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_TERESA_15cutdown_00c.webm" type="video/webm">
		</video>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>OCTOBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="intro">
		<div>
			<div class="container">
				<div class="row">
					<div class="col col-8 offset-2">
						<h3>Teresa Hill-Putnam</h3>
						<p>
							When Teresa’s life was first turned upside down by a myasthenia gravis diagnosis, she had a lot of life to disrupt. She was a dance instructor, the owner of a thriving business and the mother of three young children. Now preparing to help her youngest daughter launch her own performing arts career, Teresa reflects on her MG journey and how it has affected her family and challenged essential ideas she holds of herself as a mother and artist.
						</p>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col col-8 offset-2">
						<blockquote>
							<p>"You feel kind of like you’re a hypochondriac, but it’s all real."</p>
						</blockquote>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row inline-video-row">
					<div class="col col-sm-6 col-8 offset-2">
						<!-- <div class="article-inline-video"><iframe src="https://player.vimeo.com/video/458977564" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div> -->
						<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/468705981" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col col-8 offset-2 callout-rsvp">
						<h4>Join us for the virtual premiere on November&nbsp;17<sup>th</sup></h4>
						<br>
						<p><a href="#" class="capsule tertiary" id="rsvp-modal">RSVP Today!</a> <br>
						</p>
						<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="docu-callouts" class="content">
		<div class="row">
			<div class="col col-sm-6 col-10 offset-2">
				<h2 class="docu-callout-label">More MG Stories</h2>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row">
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/docu-profile-vanetta' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/docu-profile-glenn'); ?>
			</div>
		</div>
	</section>

	<section id="action" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

</article><!-- #post-<?php the_ID(); ?> -->
