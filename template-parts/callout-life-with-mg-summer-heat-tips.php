<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="life-with-mg-callout-summer-heat-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="life-with-mg-beat-summer">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="life-with-mg-callout-summer-heat-label" class="content-tile" href="<?php echo esc_url( home_url( '/life-with-mg/summer-heat-tips' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/life-with-mg/summer-heat-tips' ) ); ?>" style="text-decoration:none">
			<h2 id="life-with-mg-callout-summer-heat-label" class="secondary">14 Ways to Outsmart Summer</h2>
			<p>When the weather gets warm, people with MG have to get clever.</p>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
