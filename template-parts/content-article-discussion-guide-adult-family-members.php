<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="discussion-guide-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>EXPLAINING MG</b></p>
				<h1>Discussion Guide: Talking to Family and Close Friends About MG</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>AUGUST 2020 | <b>7 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>Myasthenia gravis doesn’t make it easy. But helping family and friends “get it” is worth the effort.</h2>
				<p>
					It can be difficult to explain MG to the people who care about you. They might have difficulty understanding how you can seem fine one day and struggle the next. They might be afraid you’re going to die. MG is not an easy illness to explain and you might find yourself having to explain it again. And again. Even people in your life who love you and really want to understand may not grasp it right away. But it’s worth getting the conversation right. The better they understand what you’re going through, the better they’ll be able to support you. Here are some tips on how to help them.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>Some of my family members don’t understand MG. My brothers think there’s nothing wrong with me.</p>
					<cite><strong>Chris G.</strong><br>
						<span class="sub-cite">Hudson, Florida</span></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 1. Explaining MG for the First Time</h3>
				<h4>MG can be difficult to put into words.</h4>
				<p>
					When talking to a friend or loved one, explain what’s going on medically, all the different symptoms, how much they can fluctuate ... It’s a lot for anyone to learn. Plus, their minds may be racing with questions. Give them a minute. Remember that these are the people who care about you. You want to take them with you on your MG journey. It took you a while to understand what you were dealing with; it may take them time to process as well.
				</p>
				<p>
					Start with the basics and talk simply. Don’t get too detailed or clinical. You could say, “MG is a muscle weakness. Muscles are controlled by nerves. If the nerves can’t send signals to the muscles, they can’t work properly. MG symptoms vary from person to person, from day to day, and even from hour to hour.”
				</p>
				<p>
					You could also try an analogy. Here’s one to consider:
				</p>
				<p>
					When you have MG, using your muscles is like ringing a doorbell with faulty wiring. Sometimes the bell rings just fine. Sometimes it doesn’t ring correctly. And sometimes it doesn’t ring at all. MG creates the same kind of unpredictable communication between the nerves and muscles. It’s why a person with MG can look fine but not feel fine. Or feel okay one minute and not the next. People with MG are a lot like that normal-looking doorbell that sometimes can’t ring.
				</p>
				<p>
					That’s just one analogy. You can probably come up with a better one yourself. After all, when it comes to your experience with MG, you are the expert.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<p><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide/discussion-guide-illo-1.svg" alt="">
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 2. Getting the Conversation Started </h3>
				<h4>Think back to when you first got diagnosed. </h4>
				<p>
					You may have wondered, “What’s going to happen to me?” It’s really no different for family or friends. When they first hear the news, they also wonder what’s going to happen to you. Will you have to move in with them? Will you still be “you?” Are you going to die? Your loved ones may have a hard time absorbing any other details about your MG until they’ve gotten those initial questions answered. So, start the conversation there. And keep in mind that “I don’t know” is a perfectly appropriate answer to any of these questions. You can consider it their first lesson in the unpredictable nature of MG.
				</p>
			</div>
		</div>
	</section>


	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 3. Keeping the Conversation Going</h3>
				<h4>As your energy allows, provide honest, ongoing updates.</h4>
				<p>
					MG symptoms, and how they make you feel, can ebb and flow constantly. Your current condition is not always apparent. No matter how you are feeling, try to let others know. It can help them understand what’s going on in the moment, if they should consider changing plans or if they need to help you in any way.
				</p>
				<p>
					Be as descriptive and literal as possible. For example, don’t just say you’re having a bad day; say that you are unable to push an empty shopping cart or chew food because your muscles are tired. Or you are having difficulty taking a shower. These specifics make the realities of MG much more real for the people in your life.
				</p>
				<p>
					You can also use metaphors. Tell them that doing errands today felt like running a marathon and you’re completely wiped out. Tell them that hiking on a hot summer day feels like jogging in quicksand. Tell them that sitting on the couch all afternoon feels like captivity and you’d rather be out and about.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<p><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide/discussion-guide-illo-2.svg" alt="">
				</p>
			</div>
		</div>
	</section>


	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 4. Be Realistic—with Yourself and Others</h3>
				<h4>There may be times when you make plans and those plans have to change. </h4>
				<p>
					Go ahead and make them anyway. Just be honest and manage expectations. Let your friends and family know that you would love to spend time with them, but there is a chance that you may have to bow out at the last moment. You truly want to be part of their lives, but it will take some adjustments. Unpredictability is the nature of MG.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>Tell your story. Because through telling your story, you just never know who's going to jump onto the bandwagon on your team with you to help you.</p>
					<cite><strong>Glenda Thomas</strong><br>
						<span class="sub-cite">MGFA New England Support Group Leader</span></cite>
				</blockquote>
			</div>
		</div>
	</section>



	<section class="row article-body">
		<div>
			<div class="content">
				<h3>It’s Time to Start the Conversation</h3>
				<p>
					Explaining MG is not easy. But learning to discuss it clearly, simply and openly with others can help make your day-to-day life with MG a little better for you and them. Family, friends and supporters who understand your situation better may be more patient, understanding and helpful. Most of all, it will help you feel less alone in your MG journey. It may take some energy to have these conversations, but it’s worth it—for everyone.
				</p>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-invisible-burden' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-victor-and-iris-yipp' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
