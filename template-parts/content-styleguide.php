<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="documentary" <?php post_class(); ?>>
	<section id="hero">
		<div>
			<div class="col col-8 offset-2">
				<h1>Headline One - <span>$font-family-primary</span> &bull; 44/64</h1>
			</div>
		</div>
	</section>

	<br /><br />
	<hr />
	<br /><br />

	<section id="text" class="container">
		<div class="row">
			<div class="col col-sm-12 col-4">
				<h2>Font Families</h2>
				<br />
				<p>$font-family-main &bull; Sparose</p>
				<p>$font-family-primary &bull; Libre Baskerville</p>
				<p>$font-family-secondary &bull; Greycliff CF Bold</p>
				<p>$font-family-tertiary &bull; Greycliff CF Regular</p>
				<p>$font-family-body &bull; Calibri</p>
				<p>$font-family-nav &bull; Greycliff CF Heavy</p>
				<p>$font-family-quote &bull; Black Dolphin Signature Script</p>

				<p>
					Interstitial: <br />
					<code>&lt;a href="" <span>target="_blank"</span>&gt;link&lt;/a&gt; <br /></code><a href="https://www.argenx.com/en-GB/content/privacy-policy/45/" target="_blank">Argenx</a>
				</p>

				<p>
					No intersttial: <br />
					<code>&lt;a href="" <span>class="no-interstitial" target="_blank"</span>&gt;link&lt;/a&gt; <br /></code><a class="no-interstitial" href="https://www.argenx.com/en-GB/content/privacy-policy/45/" target="_blank">Argenx</a>
				</p>
			</div>
			<div class="col col-sm-12 col-8">
				<h2>Text</h2>
				<br />
				<h2>Headline Two - <span>$font-family-primary</span> &bull; 36/48</h2>
				<h2 class="secondary">Headline Two secondary <span>$font-family-primary</span> &bull; 24/32</h2>
				<h3>Headline Three - <span>$font-family-secondary</span> &bull; 72/118</h3>
				<h4>Headline Four - <span>$font-family-secondary</span> 36/44</h4>
				<h5>Headline Five - <span>$font-family-main</span> &bull; 82/118</h5>
				<h6>Headline Six - <span>$font-family-tertiary</span> 36/48</h6>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut <a href="#">enim ad minim</a> veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br />
					- <span>$font-family-body</span>
				</p>
				<p><a href="#" class="nav">NAVIGATION</a> - $font-family-nav</p>
				<br /><br />
				<blockquote><p>Blockquote - <span>$font-family-quote</span></p></blockquote>
			</div>
		</div>
	</section>

	<section id="squares">
		<h2>Colors</h2>
		<br />
		@* row one *@
		<div class="container">
			<div class="row">
				<div class="col-2 black light">

					<p>#000<br><br>$color-black</p>
				</div>

				<div class="col-2 white">

					<p>#fff<br><br>$color-white</p>
				</div>

				<div class="col-2 brand-light light">

					<p>#1CA0B2<br><br>$color-brand-light</p>
				</div>

				<div class="col-2 brand-dark light">

					<p>#195985<br><br>$color-brand-dark</p>
				</div>

				<div class="col-2 text light">

					<p>#393E41<br><br>$color-text</p>
				</div>

				<div class="col-2 cta-primary light">

					<p>#E4572E<br><br>$color-cta-primary</p>
				</div>
				<div class="col-2 cta-secondary">

					<p>#ECA400<br><br>$color-cta-secondary</p>
				</div>

				<div class="col-2 cta-tertiary">

					<p>#F6F6F6<br><br>$color-cta-tertiary</p>
				</div>

				<div class="col-2 modules-bg-primary light">

					<p>#479936<br><br>$color-modules-bg-primary</p>
				</div>

				<div class="col-2 modules-bg-secondary">

					<p>#F3F0E9<br><br>$color-modules-bg-secondary</p>
				</div>

				<div class="col-2 modules-bg-tertiary">

					<p>#cadbef<br><br>$color-modules-bg-tertiary</p>
				</div>

				<div class="col-2 modules-bg-quaternary light">

					<p>#40C2D5<br><br>$color-modules-bg-quaternary</p>
				</div>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col col-12">
				<h2>Blockquotes</h2>
				<p>Will automatically pick up <span>$font-family-quote</span> on the <code>&lt;p&gt;</code> tag inside a <code>&lt;blockquote&gt;</code>.</p>
				<blockquote>
					<p>&#x201c;Patient quote about MG here. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod&#x201d;</p>
					<cite class="celia">
						<strong>Lindsey F.</strong><br />
						Senec, SC
					</cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col col-sm-12 col-6">
				<h2>Button Primary</h2>
				<p>Pimary CTA button for <code>&lt;section&gt;</code>s with white backgrounds and in the main navigation.</p>
				<p><code>&lt;a href="/" class="<span>capsule primary</span>"&gt;JOIN NOW&lt;/a&gt;</code></p>
				<a href="<?php echo esc_url( home_url( '/styleguide' ) ); ?>" class="capsule primary">JOIN NOW</a>
			</div>

			<div class="col col-sm-12 col-6 video">
				<a href="/" class="bttn-play"></a>
				<h5>Can my kids get it?</h5>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col col-4">
				<h2>Button Secondary</h2>
				<p>Secondary CTA button for <code>&lt;section&gt;</code>s with colored background.</p>
				<p><code>&lt;a href="/" class="<span>capsule secondary</span>"&gt;JOIN NOW&lt;/a&gt;</code></p>
				<a href="<?php echo esc_url( home_url( '/styleguide' ) ); ?>" class="capsule secondary">JOIN NOW</a>
			</div>

			<div class="col col-6 mr-auto">
				<h2>Button Tertiary</h2>
				<p>Used in the interstitial for the <strong>Cancel</strong> button</p>
				<p><code>&lt;a href="/" class="<span>capsule tertiary</span>"&gt;JOIN NOW&lt;/a&gt;</code></p>
				<a href="<?php echo esc_url( home_url( '/styleguide' ) ); ?>" class="capsule tertiary">CANCEL</a>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col col-sm-12 col-10">
				<h2>Accordion</h2>
				<p>Use a wrapper div with <code>class="<span>accordion</span>"</code> around a <code>&lt;dl&gt;</code> element with <code>&lt;dt&gt;</code> for the terms and <code>&lt;dd&gt;</code> for the content. Use <code>&lt;br&gt;</code>'s to break up text as needed.</p>

				<div class="accordion">
					<dl class="row">
						<dt> 1. AChE INHIBITORS<span></span></dt>
						<dd>Mestinon&#x00ae; (pyridostigmine) is the most commonly prescribed AChE (acetylcholinesterase) inhibitor, the standard first treatment for most people with MG. Like all AChE inhibitors, Mestinon works to block enzymes that digest AChE, thereby increasing the number of messages available for muscles to respond to. It’s effective 30 minutes to an hour after taking but begins to taper after 4 hours. This means you have to take pills every 4-6 hours. While Mestinon can temporarily relieve symptoms, it does not limit immune system attacks. Mestinon comes with many common side effects, including abdominal cramps, diarrhea, increased flatus, nausea, increased salivation, urinary urgency, sweating, and increased bronchial secretions which can worsen breathing difficulty. People with MuSK-attacking antibodies typically don’t benefit from Mestinon.</dd>

						<dt>2. THYMECTOMY<span></span></dt>
						<dd>This is a surgery to remove the thymus, the gland lying below the sternum that helps to control immune function.  Thymectomy can work to significantly decrease the production of muscle-attacking antibodies. People who experience improvements may have to wait a long time for symptom relief after surgery—potentially several years. And when it’s all said and done, many people have to continue taking MG medications. Thymectomy is not recommended for people over 60 or for people whose immune system is attacking MuSK or LRP4 proteins.</dd>

						<dt>3. STEROIDS <span></span></dt>
						<dd>These drugs, notably Prednisone and Prednisolone, work in multiple ways to decrease the effects of the immune system, thus decreasing the damage to the muscles’ receptors. The time it takes to feel improvement varies widely based on the type of drug. To find the minimal effective dose, steroids are often prescribed initially in a low dose, gradually increased until symptom relief is achieved, and then slowly lowered to minimize side effects. Steroids come with a long list of significant side effects, including weight gain, insomnia, psychosis, high blood pressure, diabetes, and risk of infection. These side effects are unfortunately a very common reality for people with MG. Steroids are affordable and can be effective, but they are no fun.</dd>

						<dt>4. NON-STEROIDAL IMMUNOSUPPRESSIVE THERAPY (IST) <span></span></dt>
						<dd>Immunosuppressive Therapy (IST) is a treatment used as an alternative to steroids, for people who either can’t take them or struggle with significant side effects of steroids. Azathioprine, or AZA, is the most commonly used IST for myasthenia gravis. Typically administered as a daily pill or injection, ISTs can provide people with MG symptom relief and help them minimize steroids and their side effects. But ISTs can take quite a long time to provide relief, from 4-12 months for initial improvement and potentially multiple years for maximum effects. ISTs also come with side effects that vary greatly from treatment to treatment. For example, AZA side effects include flu-like symptoms, GI disturbances (what some doctors have dubbed ‘the colon express’), and increased risk of infection due to the suppression of the immune system. Because of the delayed effect of ISTs, doctors often continue to prescribe steroids until they see that the IST is working. People in that treatment situation may have to contend with the side effects of both steroids and the IST. Ultimately, balancing the benefit of an IST for myasthenia gravis with the risk of suppressing your entire body’s surveillance for infections is something you need to work with your doctor to assess.</dd>

						<dt>5. Rituximab (Rituxan&#x00ae;)<span></span></dt>
						<dd>Rituximab is a biologic oncology drug used primarily for cancer treatments but can also be used as a treatment option for people with MG who are MuSK+. It is sometimes used in combination with an IST. Rituxan calls for a month of weekly infusions, and then on a varying schedule. It can help certain people minimize symptoms and/or steroid use. The most common infusion-related reactions include headaches, chills, fever, high blood pressure, nausea, asthenia, and pain.</dd>

						<dt>6. Eculizumab<span></span></dt>
						<dd>Also known by brand name Soliris&#x00ae;, eculizumab is a biologic that was first introduced as a treatment for a rare blood disease but gained approval for treatment of gMG in 2017. Dosing is delivered intravenously. People can experience increased muscle strength and improved quality of life. But there are drawbacks, including risk of infection. A vaccination is required before people can begin treatment. Eculizumab side effects include headache, nausea, diarrhea and upper respiratory tract infection. Eculizumab is for AChE+ patients only.</dd>

						<dt>7. FcRn Inhibitors (In development)<span></span></dt>
						<dd>This is a novel class of biologic therapy that works by targeting the specific mechanism of action in myasthenia gravis. It specifically decreases antibody production and should theoretically decrease antibody-mediated attacks on the muscle receptors. In other words, it’s a new kind of MG treatment that’s designed to work against the antibodies themselves, rather than simply mask the symptoms. For more information, visit the <a href="https://www.argenx.com/en-GB/content/efgartigimod/22/" target="_blank">argenx website</a>.</dd>

						<dt>8. EMERGENCY CRISIS THERAPY<span></span></dt>
						<dd>
							<strong>IVIg (aka: Intravenous immunoglobulin)</strong><br />
							<br />
							These treatments are primarily prescribed as short term/rescue therapy, or as maintenance therapy for people who can’t tolerate or don’t respond to other treatments. The way IVIg works is simple: mass amounts of normal antibodies are added into the body. More than half of people on IVIg see improvement in symptoms within a week. The downsides include the administration, which can involve several hours in an infusion chair. IVIg may not work the first time it is administered. Multiple infusions per week may be required for some. A common infusion-related “headache” effect can leave people on IVIg feeling weak and nauseous in the day or days immediately following treatment. There is currently a severe shortage of IVIg, causing many patients to postpone treatments indefinitely.<br />
							<br />
							<strong>PLEX (aka: Plasmapheresis or plasma exchange)</strong><br />
							<br />
							PLEX stands for plasma exchange. This process involves removing bad antibodies from the blood. The “cleaned” blood is then infused back to the body. This is usually used as a short-term rescue intervention that offers symptom improvement in less than a week and is commonly used during MG-induced breathing difficulty.
						</dd>
					</dl>
				</div>

			</div>
		</div>
	</section>

	<section class="modules-bg-secondary container">
		<div class="row">
			<div class="col col-12">
				<h2>Carousel</h2>
				<p>Blockquotes inside of a <code>.slider-wrapper</code> div element.</p>

				<div class="slider-wrapper">
					<div class="slider">
						<blockquote class="slide"><p>&#x201c;I&#x2019;m trying to get back to working a full-time job again.&#x201d;</p></blockquote>
						<blockquote class="slide"><p>&#x201c;I want to participate in things I love to do and have the stamina.&#x201d;</p></blockquote>
						<blockquote class="slide"><p>&#x201c;I can&#x2019;t wait until the double vision is gone. I can get more done in a day.&#x201d;</p></blockquote>
					</div>

					<div class="slider-dots">
						<a></a>
						<a></a>
						<a></a>
					</div>
				</div>

			</div>
		</div>
	</section>

	<p>
		<code><span><?php get_template_part( 'template-parts/callout', 'understanding-mg' ); ?></span></code><br />
		<code><span><?php get_template_part( 'template-parts/callout', 'documentary' ); ?></span></code>
	</p>
	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-6">
				<?php get_template_part( 'template-parts/callout', 'understanding-mg' ); ?>
			</div>
			<div class="col col-6">
				<?php get_template_part( 'template-parts/callout', 'documentary' ); ?>
			</div>
		</div>
	</section>

	<p><code><span><?php get_template_part( 'template-parts/callout', 'slab' ); ?></span></code></p>
	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
