<div class="row eoc-callout" tabindex="0" aria-labelledby="real-story-callout-morgan-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top" id="real-story-four-words">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="real-story-callout-morgan-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene' ) ); ?>" style="text-decoration:none">
			<h2 id="real-story-callout-morgan-label" class="secondary">The Four Words That Changed My Life</h2>
			<p>Morgan Greene shares her MG journey, starting with the memorable moment her diagnosis was confirmed.*</p>
			<span class="read-duration">6 MIN READ</span>
		</a>
	</div>
</div>
