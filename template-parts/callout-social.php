<section id="callout-social">
	<div>
		<h2>Want more great content? Follow MG&nbsp;United on social media.</h2>
		<div class="flex-container">
			<img id="social-lines" alt="" src="<?php echo get_template_directory_uri(); ?>/images/social-line.svg" />
		</div>
		<div class="flex-container">
			<span class="flex-spacer"></span>
			<a href="https://www.facebook.com/MGUnitedOfficial/" target="_blank"><img alt="MG United Facebook" src="<?php echo get_template_directory_uri(); ?>/images/facebook.svg" /></a>
			<a href="https://www.instagram.com/mgunited_official/" target="_blank"><img alt="MG United Instagram" src="<?php echo get_template_directory_uri(); ?>/images/instagram.svg" /></a>
			<span class="flex-spacer"></span>
		</div>
	</div>
</section>
