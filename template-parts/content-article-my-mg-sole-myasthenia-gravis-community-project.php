<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="my-mg-sole-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>COMMUNITY PROJECT</b></p>
				<h1>My MG Sole Fights Coronavirus Blues with Kicky Art</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JUNE 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
				  This just-launched community art project lets us join together while staying apart.
				</h2>
				<p>Social distancing has given the whole world a dose of cabin fever. And it's even tougher for people with MG because they have an increased risk of infection.<sup>1,2</sup> That’s why argenx recently launched <a href="/things-to-do/my-mg-sole-myasthenia-gravis-art-project">My MG Sole</a>—a fun community art project for people living with MG. It’s designed to inspire people with MG to connect from home by sharing stories of their journeys with MG, and the stories are told through do-it-yourself art that uses shoes as the canvas.</p>
			<div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/my-mg-sole/my-mg-sole-video.jpg" alt="" /></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Why Shoes?</h3>
				<p>Art therapist Annalise Hammerlund explained the unusual choice of canvas: “Shoes are powerful symbols of shared experience. People who see this art can ‘walk a mile’ in the shoes of people with MG.”</p>
				<p>“Plus,” she added, “painting shoes is just fun.”</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/my-mg-sole/my-mg-sole-participant.jpg" alt="photo of Mike and son displaying converse shoe with dinosaur art" />
					<figcaption><em>“Life with my son Jackson has been one of the most rewarding aspects of my MG journey,” said Mike U., “and Jackson loves dinosaurs.”</em></figcaption>
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>A Paint-Smudged Thumbs Up from the MG Community</h3>
				<p>People in the MG community who have already created art for My MG Sole confirm that the project is not just fun, but surprisingly meaningful.</p>
				<p>“Sitting here, just having the ability to draw on this canvas with my son,” said Mike U. of Morgantown, West Virginia, “is one of the greatest joys I have right now in this new MG life.”</p>
				<p>Alesheia H. of Grand Rapids, Michigan praised the art-making process, saying, “It’s been an amazing creative outlet, bringing calm, less stress and more healing.”</p>
				<p>And Glenda T. of Framingham, Massachusetts highlighted the importance of the final step in the project: sharing the art on social media. “Other people with rare diseases can connect with the story you’re telling and feel like, ‘I have something to offer as well.’”</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
				<p>Let the shoes be your canvas to tell your story of MG.</p>
				<cite><strong>Annalise Hammerlund</strong><br>
				Expressive Arts Therapist</cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Picassos are Welcome, Not Required </h3>
				<p>According to certified patient experience professional Susan Woolner, who helped launch the program, “You don’t have to be a master artist to participate. This isn’t about results—it’s about the physical act of creating the art and expressing the feelings you have about your MG experience.” </p>
				<p>For the inspirationally challenged, the project has creative thought starters that make it easy to get going. And there’s even a way to participate if you don’t want to paint on your own shoes. To learn more, and see more of the art, visit <a href="/things-to-do/my-mg-sole-myasthenia-gravis-art-project">My MG Sole</a>. Or check out the My MG Sole galleries on <a href="https://www.facebook.com/MyMGSole/" target="_blank">Facebook</a> and <a href="https://www.instagram.com/mymgsole/" target="_blank">Instagram</a>.</p>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Sanders DB, et al. <em>Neurology</em>. 2016;87(4):419-425.</li>
					<li>Kassardjian CD, et al. <em>Eur J Neurol</em>. 2020;27(4):702-708.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-leah-gaitan-diaz' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
