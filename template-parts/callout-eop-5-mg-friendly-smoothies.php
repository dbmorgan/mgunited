<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-smoothies-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-smoothies">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-smoothies-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">LIFE WITH MG</p>
				<h2 id="eop-callout-smoothies-label" class="secondary">5 MG-Friendly Smoothies</h2>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
