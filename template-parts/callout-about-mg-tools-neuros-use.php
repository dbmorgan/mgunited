<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="about-mg-callout-doctor-tools-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="about-mg-doctor-tools">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="about-mg-callout-doctor-tools-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use' ) ); ?>" style="text-decoration:none">
			<h2 id="about-mg-callout-doctor-tools-label" class="secondary">Learn More About Common Tools Neurologists Use to Chart Your Symptoms</h2>
			<p>Explore the worksheets that help doctors track your MG.</p>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
