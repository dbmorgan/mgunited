<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="treatment-goals-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>The Importance of Setting Your Personal MG Treatment Goals</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JUNE 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					Having a goal helps you take small, realistic steps and track progress. Here’s how to set yours.
				</h2>

				<p>When beginning treatment for MG, it’s good to be hopeful but also realistic. The disease is complicated, and everyone who has it is different. The important thing is to track progress and speak with your doctor about how things are going. If you think things could be better, speak up. Be sure to tell your doctor immediately about any problems.<sup>1</sup></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Goal 1: Get to Minimal!
				</h3>

				<p>An ambitious goal for any person with MG is to get to a place called “minimal manifestation status.” This means you will not be limited in any way from doing the ordinary activities of everyday living. There may be some muscle weakness and maybe still some drooping eyelids. But your ordinary physical activity won’t be restricted. MG is not preventing you from walking, eating, driving, or doing a job involving light physical activity&mdash;in fact, light exercise can help improve your condition. This implies you’re able to tolerate the medication you’re taking.<sup>1</sup></p>
				<p>When people are initially diagnosed with MG, they usually begin with one simple wish: no longer having MG. That’s both understandable and appropriate. However, as you learn more about the disease and your particular case, you may find that minimal manifestations, with treatment that’s tolerable, is a more useful goal. It could be both more realistic and more useful in helping you to improve your daily life.</p>
				<p>About remission: that’s when there is no muscle weakness or any other symptoms of MG at all. The only exception is that you may still have trouble closing your eyelids. No medications are necessary. Remission is very hard to achieve. But it does happen, and it is the best possible outcome.<sup>1</sup></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Goal 2: Your Personal Best (Lowering Your MG-ADL Score)</h3>

				<p>There is a simple, 8-question survey used to measure the intensity of MG symptoms. It is called the myasthenia gravis activities of daily living profile (MG-ADL). It takes into account the extent of weakness in your: eyes, facial muscles, neck/throat, chest, arms, hands, and legs. It can also be used to determine how you respond to different treatments. Scores range from 0 to 24&mdash;the lower the number, the better!<sup>2</sup></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Goal 3: Avoiding a Crisis
				</h3>

				<p>It’s estimated that 15-20% of all people with MG will have at least one crisis at some point.<sup>3</sup> So it’s important to know what it is and what to do. Myasthenic crisis is a severe MG flare that leads to such difficulty breathing that a ventilator is necessary. It can happen suddenly or gradually.<sup>1</sup> If you are experiencing a crisis, you may experience some or all of these signs:</p>

			</div>

		</div>
	</section>

	<section id="one-up-bullet-list" class="row">
		<div>
			<div>
				<ul>
					<li>It feels harder to breathe.<sup>4</sup></li>
					<li>You can’t complete sentences or count to 20 without getting out of breath.<sup>4,5</sup></li>
					<li>The muscles between your ribs and above the collarbone cave in when you breathe.<sup>5</sup></li>
					<li>Your cough can’t clear saliva or phlegm.<sup>5</sup></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3><strong>If you are struggling to breathe, it is very important to call 911 or get medical help immediately.<sup>4</sup></strong></h3>
				<p>Certain drugs, prescription or otherwise, may cause worsening of MG symptoms.<sup>5</sup> Be prepared with a list of all medications and nontraditional medicine you're taking when you go see your doctor. This will help prevent issues with drug interactions that you might not be aware of. You should always carry a card listing all of your medications with you in the event of an emergency. In addition, you should always check with a doctor before starting any new medication, including over-the-counter medications or supplements.</p>
				<p>You should also get informed about the other diseases and conditions that people diagnosed with MG are often diagnosed with. (Doctors refer to these as comorbidities.) Complications due to these comorbidities could lead to an MG crisis.</p>
				<p>Anyone with MG should have the goal of avoiding a crisis. But it’s also good to be ambitious about managing the condition and living the best life possible. Talking to a doctor and setting realistic and achievable goals can help those with MG feel hopeful and optimistic about the future. There is a lot of help available to get to those goals, and MG&nbsp;United wants provide information to help people with MG reduce their symptoms, get to minimal, and be their best.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>I'm holding onto my scuba gear.  I'm praying I'll be able to dive again.</p>
					<cite><strong>Chris G.</strong><br>
						Hudson, Florida</cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section id="form-body" class="row form-module">
		<div>
			<div class="content">
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharegoal-article' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Sanders DB, et al. <cite>Neurology</cite>. 2016;87(4):419-425.</li>
					<li>Wolfe GI, et al. <cite>Neurology</cite>. 1999;52(7).</li>
					<li>Stetefeld H, et al. <cite>Neurol Res Pract</cite>. 2019;1(19):1-6.</li>
					<li>Wahls SA. <cite>Am Fam Physician</cite>. 2012:86(2):173-182.</li>
					<li>Wendell LC, et al. <cite>Neurohospitalist</cite>. 2011;1(1):16-22.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-what-is-mg' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-crisis' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
