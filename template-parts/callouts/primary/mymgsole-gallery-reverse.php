<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-mymgsole-gallery-label">
	<div class="col col-sm-12 col-7 cta-left-reverse cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-art-project/' ) ); ?>" style="text-decoration:none">
			<!-- <p class="callout-category primary-callout-category eyebrow"><span>DISEASE & TREATMENT</span></p> -->
			<h2 id="primary-callout-mymgsole-gallery-label" class="secondary callout-headline">Join This Kicky Community Art&nbsp;Project</h2>
			<p class="callout-body primary-callout-body">Art brings us together. Join us by sharing your story, expressed with shoe art.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
	<div role="presentation" class="col col-sm-12 col-5 cta-right-reverse cta-top primary-callout-image" id="primary-callout-mymgsole-gallery">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-mymgsole-gallery-label" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-art-project/' ) ); ?>" style="text-decoration:none"></a>
	</div>
</div>
