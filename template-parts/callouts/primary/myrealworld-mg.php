<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-myrealworld-mg-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-myrealworld-mg">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-myrealworld-mg-label" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/myrealworld-mg-myasthenia-gravis-study/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/myrealworld-mg-myasthenia-gravis-study/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>SYMPTOM TRACKING</span></p>
			<h2 id="primary-callout-myrealworld-mg-label" class="secondary callout-headline">See If You Can Help Researchers by Sharing Your MG Journey</h2>
			<p class="callout-body primary-callout-body">MyRealWorld&trade; MG is an app-based research study, based on real-world evidence, designed to help advance the scientific understanding of myasthenia&nbsp;gravis.</p>
			<span class="read-duration callout-read-time">3 MIN READ</span>
		</a>
	</div>
</div>
