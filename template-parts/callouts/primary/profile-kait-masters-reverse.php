<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-profile-kait-masters-label">
	<div class="col col-sm-12 col-7 cta-left-reverse cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kait-masters-myasthenia-gravis-story/' ) ); ?>" style="text-decoration:none">
			 <p class="callout-category primary-callout-category eyebrow"><span>REAL STORIES</span></p>
			<h2 id="primary-callout-profile-kait-masters-label" class="secondary callout-headline">Kait Masters: Setting and Readjusting Life Goals with MG*</h2>
			<p class="callout-body primary-callout-body">Early-onset myasthenia gravis hasn’t stopped Kait Masters, who’s now an artist, businesswoman and mom.</p>
			<span class="read-duration callout-read-time">9 MIN READ</span>
		</a>
	</div>
	<div role="presentation" class="col col-sm-12 col-5 cta-right-reverse cta-top primary-callout-image" id="primary-callout-profile-kait-masters">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-profile-kait-masters-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kait-masters-myasthenia-gravis-story/' ) ); ?>" style="text-decoration:none"></a>
	</div>
</div>
