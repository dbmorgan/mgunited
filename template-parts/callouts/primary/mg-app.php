<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-mg-app-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-mg-app">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-mg-app-label" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/smartphone-MG-management/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/smartphone-MG-management/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>LIFE WITH MG</span></p>
			<h2 id="primary-callout-mg-app-label" class="secondary callout-headline">Managing MG: There’s an App for That</h2>
			<p class="callout-body primary-callout-body">Trying to manage MG is already a task in itself. One way that may help you stay organized could be used right from the palm of your hand.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
