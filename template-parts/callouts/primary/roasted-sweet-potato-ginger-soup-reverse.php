<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-roasted-sweet-potato-ginger-soup-label">
	<div class="col col-sm-12 col-7 cta-left-reverse cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/myasthenia-gravis-friendly-roasted-sweet-potato-and-ginger-soup/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>EATING & MG</span></p>
			<h2 id="primary-callout-roasted-sweet-potato-ginger-soup-label" class="secondary callout-headline">Roasted Sweet Potato and Ginger Soup</h2>
			<p class="callout-body primary-callout-body">Flavorful seasonal veggies and fall spices combine in a snap to create an MG-friendly soup that’ll warm up the whole family.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
	<div role="presentation" class="col col-sm-12 col-5 cta-right-reverse cta-top primary-callout-image" id="primary-callout-roasted-sweet-potato-ginger-soup">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-roasted-sweet-potato-ginger-soup-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/myasthenia-gravis-friendly-roasted-sweet-potato-and-ginger-soup/' ) ); ?>" style="text-decoration:none"></a>
	</div>
</div>
