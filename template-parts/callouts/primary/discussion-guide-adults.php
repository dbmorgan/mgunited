<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-discussion-adults-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-discussion-adults">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-discussion-adults-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-adult-family-members' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-adult-family-members/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>EXPLAINING MG</span></p>
			<h2 id="primary-callout-discussion-adults-label" class="secondary callout-headline">Discussion Guide: Talking to Family and Close Friends About MG</h2>
			<p class="callout-body primary-callout-body">Myasthenia gravis doesn’t make it easy. But helping family and friends “get it” is worth the effort.</p>
			<span class="read-duration callout-read-time">7 MIN READ</span>
		</a>
	</div>
</div>
