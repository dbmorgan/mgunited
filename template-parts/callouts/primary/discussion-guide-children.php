<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-discussion-children-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-discussion-children">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-discussion-children-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-children/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-children/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>EXPLAINING MG</span></p>
			<h2 id="primary-callout-discussion-children-label" class="secondary callout-headline">Discussion Guide: Talking to Children About MG</h2>
			<p class="callout-body primary-callout-body">Talking to children about MG as you cope with your MG diagnosis.</p>
			<span class="read-duration callout-read-time">7 MIN READ</span>
		</a>
	</div>
</div>

