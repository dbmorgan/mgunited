<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-doctor-tools-label">
	<div class="col col-sm-12 col-7 cta-left-reverse cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>DISEASE & TREATMENT</span></p>
			<h2 id="primary-callout-doctor-tools-label" class="secondary callout-headline">Learn More About Common Tools Neurologists Use to Chart Your Symptoms</h2>
			<p class="callout-body primary-callout-body">Explore the worksheets that help doctors track your MG.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
	<div role="presentation" class="col col-sm-12 col-5 cta-right-reverse cta-top primary-callout-image" id="primary-callout-doctor-tools">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-doctor-tools-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use/' ) ); ?>" style="text-decoration:none"></a>
	</div>
</div>
