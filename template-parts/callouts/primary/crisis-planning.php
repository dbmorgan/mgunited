<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-crisis-planning-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-crisis-planning">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-crisis-planning-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>DISEASE & TREATMENT</span></p>
			<h2 id="primary-callout-crisis-planning-label" class="secondary callout-headline">Crisis 411: What You Need to Know About Myasthenic Crisis</h2>
			<p class="callout-body primary-callout-body">Do you know the difference between MG crisis and impending crisis? You should.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
