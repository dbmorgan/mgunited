<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-profile-chris-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-profile-chris">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-profile-chris-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/chris-givens/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/chris-givens/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>REAL STORIES</span></p>
			<h2 id="primary-callout-profile-chris-label" class="secondary callout-headline">Chris Givens Dives into His Life with MG*</h2>
			<p class="callout-body primary-callout-body">Chris gets real about how MG has affected his life and his plans for the future in this interview.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>

