<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-what-is-mg-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-what-is-mg">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-what-is-mg-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/what-is-mg/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/what-is-mg/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>DISEASE & TREATMENT</span></p>
			<h2 id="primary-callout-what-is-mg-label" class="secondary callout-headline">What is Myasthenia Gravis, and Why Does It Happen?</h2>
			<p class="callout-body primary-callout-body">Need a better grasp of how MG works? We cover the essentials.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>

