<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-profile-morgan-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-profile-morgan">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-profile-morgan-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>REAL STORIES</span></p>
			<h2 id="primary-callout-profile-morgan-label" class="secondary callout-headline">The Four Words That Changed My Life*</h2>
			<p class="callout-body primary-callout-body">Morgan Greene shares her MG journey, starting with the memorable moment her diagnosis was confirmed.</p>
			<span class="read-duration callout-read-time">6 MIN READ</span>
		</a>
	</div>
</div>

