<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-mg-other-conditions-label">
	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top primary-callout-image" id="primary-callout-mg-other-conditions">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="primary-callout-mg-other-conditions-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/mg-and-other-conditions/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-7 cta-right cta-bottom cta-copy-dark primary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/mg-and-other-conditions/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category primary-callout-category eyebrow"><span>DISEASE & TREATMENT</span></p>
			<h2 id="primary-callout-mg-other-conditions-label" class="secondary callout-headline">Living with MG and Other Conditions</h2>
			<p class="callout-body primary-callout-body">People living with MG often have to deal with other conditions too. It can get tricky.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>

