<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-docu-profile-glenn-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-docu-profile-glenn">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-docu-profile-glenn-label" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me/glenn-phillips/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me/glenn-phillips/' ) ); ?>" style="text-decoration:none">
			<h2 class="secondary callout-headline">Glenn</h2>
			<p id="secondary-callout-docu-profile-glenn-label" class="callout-body secondary-callout-body">When a diagnosis sidelined Glenn’s firefighting career in 2014, he found ways to adapt.</p>
			<span role="button" class="capsule secondary docu-bio-tile-button">MEET GLENN</span>
		</a>
	</div>
</div>
