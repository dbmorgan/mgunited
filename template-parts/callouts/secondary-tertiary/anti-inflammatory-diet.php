<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-anti-inflammatory-diet-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-anti-inflammatory-diet">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-anti-inflammatory-diet-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/anti-inflammatory-diet-and-myasthenia-gravis/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/anti-inflammatory-diet-and-myasthenia-gravis/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EATING & MG</p>
			<h2 id="secondary-callout-anti-inflammatory-diet-label" class="secondary callout-headline">Nutrition Buzz: Let’s Look at the Anti-inflammatory Diet</h2>
			<p class="callout-body secondary-callout-body">Get the 411 on what the scientific community thinks about this much-discussed approach to eating.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
