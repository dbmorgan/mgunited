<!--<div class="row eoc-callout" tabindex="0" aria-labelledby="primary-callout-profile-morgan-label">-->
<!--	<div role="presentation" class="col col-sm-12 col-5 cta-left cta-top secondary-callout-image" id="secondary-callout-profile-morgan">-->
<!--		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-profile-morgan-label" class="content-tile" href="--><?php //echo esc_url( home_url( '/real-stories/morgan-greene/' ) ); ?><!--" style="text-decoration:none"></a>-->
<!--	</div>-->
<!--	<div class="col col-sm-12 col-12 cta-bottom primary-callout-copy">-->
<!--		<a tabindex="-1" class="content-tile" href="--><?php //echo esc_url( home_url( '/real-stories/morgan-greene/' ) ); ?><!--" style="text-decoration:none">-->
<!--			<p class="callout-category secondary-callout-category eyebrow">REAL STORIES</p>-->
<!--			<h2 id="secondary-callout-profile-morgan-label" class="secondary callout-headline">The Four Words That Changed My Life*</h2>-->
<!--			<p class="callout-body secondary-callout-body">Morgan Greene shares her MG journey, starting with the memorable moment her diagnosis was confirmed.</p>-->
<!--			<span class="read-duration callout-read-time">6 MIN READ</span>-->
<!--		</a>-->
<!--	</div>-->
<!--</div>-->

<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-profile-morgan-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-profile-morgan">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-profile-kathy-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">REAL STORIES</p>
			<h2 id="secondary-callout-profile-morgan-label" class="secondary callout-headline">The Four Words That Changed My Life*</h2>
			<p class="callout-body secondary-callout-body">Morgan Greene shares her MG journey, starting with the memorable moment her diagnosis was confirmed.</p>
			<span class="read-duration callout-read-time">6 MIN READ</span>
		</a>
	</div>
</div>
