<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-discussion-children-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-discussion-children">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-discussion-children-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-children/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-children/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EXPLAINING MG</p>
			<h2 id="secondary-callout-discussion-children-label" class="secondary callout-headline">Discussion Guide: Talking to Children About MG</h2>
			<p class="callout-body secondary-callout-body">Talking to children about MG as you cope with your MG diagnosis.</p>
			<span class="read-duration callout-read-time">7 MIN READ</span>
		</a>
	</div>
</div>
