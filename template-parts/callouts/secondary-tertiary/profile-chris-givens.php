<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-profile-chris-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-profile-chris">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-profile-chris-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/chris-givens/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/chris-givens/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">REAL STORIES</p>
			<h2 id="secondary-callout-profile-chris-label" class="secondary callout-headline">Chris Givens Dives into His Life with MG*</h2>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
