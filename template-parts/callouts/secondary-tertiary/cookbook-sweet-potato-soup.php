<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-cookbook-sweet-potato-soup-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-cookbook-sweet-potato-soup">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-cookbook-sweet-potato-soup-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/myasthenia-gravis-friendly-roasted-sweet-potato-and-ginger-soup/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/myasthenia-gravis-friendly-roasted-sweet-potato-and-ginger-soup/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EATING & MG</p>
			<h2 id="secondary-callout-cookbook-sweet-potato-soup-label" class="secondary callout-headline">Roasted Sweet Potato and Ginger Soup</h2>
			<p class="callout-body secondary-callout-body"></p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
