<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-treatment-goals-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-treatment-goals">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-treatment-goals-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/treatment-goals/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/treatment-goals/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">DISEASE & TREATMENT</p>
			<h2 id="secondary-callout-treatment-goals-label" class="secondary callout-headline">The Importance of Setting Your Personal MG Treatment Goals</h2>
			<p class="callout-body secondary-callout-body">Having a goal helps you take small, realistic steps and track progress. Here’s how to set yours.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
