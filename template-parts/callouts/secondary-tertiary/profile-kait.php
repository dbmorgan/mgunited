<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-profile-kait-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-profile-kait">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-profile-kait-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kait-masters-myasthenia-gravis-story/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kait-masters-myasthenia-gravis-story/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">REAL STORIES</p>
			<h2 id="secondary-callout-profile-kait-label" class="secondary callout-headline">Kait Masters: Setting and Readjusting Life Goals with MG*</h2>
			<p class="callout-body secondary-callout-body"></p>
			<span class="read-duration callout-read-time">9 MIN READ</span>
		</a>
	</div>
</div>
