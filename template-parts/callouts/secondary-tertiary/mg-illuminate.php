<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-mg-illuminate-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-mg-illuminate">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-mg-illuminate-label" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/mg-illuminate-myasthenia-gravis-virtual-event-recap' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/mg-illuminate-myasthenia-gravis-virtual-event-recap' ) ); ?>" style="text-decoration:none">
			<!-- <p class="callout-category secondary-callout-category eyebrow">LIFE WITH MG</p> -->
			<h2 id="secondary-callout-mg-illuminate-label" class="secondary callout-headline">MG Illuminate Shines a National Spotlight on MG Awareness</h2>
			<p class="callout-body secondary-callout-body">This virtual event kicked off MG Awareness Month with an inspiring night of sessions, surprises and coast-to-coast landmark lightings.</p>
			<span class="read-duration callout-read-time">3 MIN READ</span>
		</a>
	</div>
</div>
