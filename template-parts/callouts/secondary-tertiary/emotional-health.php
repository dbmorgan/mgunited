<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-emotional-health-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-emotional-health">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-emotional-health-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/emotional-health/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/emotional-health/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EMOTIONAL SUPPORT</p>
			<h2 id="secondary-callout-emotional-health-label" class="secondary callout-headline">How to Have Good Emotional Health and MG at the Same Time</h2>
			<p class="callout-body secondary-callout-body">From family to support groups to online counseling, you have many options.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
