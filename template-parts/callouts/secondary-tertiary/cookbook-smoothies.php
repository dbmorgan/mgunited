<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-cookbook-smoothies-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-cookbook-smoothies">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-cookbook-smoothies-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/5-mg-friendly-smoothies/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EATING & MG</p>
			<h2 id="secondary-callout-cookbook-smoothies-label" class="secondary callout-headline">5 MG-Friendly Smoothies</h2>
			<p class="callout-body secondary-callout-body">You don’t have to have MG to love these delicious, nutritious recipes.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
