<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-summer-heat-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-summer-heat">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-summer-heat-label" class="content-tile" href="<?php echo esc_url( home_url( '/life-with-mg/summer-heat-tips/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/life-with-mg/summer-heat-tips/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">LIFE WITH MG</p>
			<h2 id="secondary-callout-summer-heat-label" class="secondary callout-headline">14 Ways to Outsmart Summer</h2>
			<p class="callout-body secondary-callout-body">When the weather gets warm, people with MG have to get clever.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
