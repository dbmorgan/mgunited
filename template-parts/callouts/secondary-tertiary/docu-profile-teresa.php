<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-docu-profile-teresa-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-docu-profile-teresa">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-docu-profile-teresa-label" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me/teresa-hill-putnam/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me/teresa-hill-putnam/' ) ); ?>" style="text-decoration:none">
			<h2 class="secondary callout-headline">Teresa</h2>
			<p id="secondary-callout-docu-profile-teresa-label" class="callout-body secondary-callout-body">A single mother of three and a lifelong dance instructor, Teresa leaned on her family to help her manage her diagnosis.</p>
			<span role="button" class="capsule secondary docu-bio-tile-button">MEET TERESA</span>
		</a>
	</div>
</div>
