<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-docu-profile-vanetta-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-docu-profile-vanetta">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-docu-profile-vanetta-label" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me/vanetta-drummer-fenton/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me/vanetta-drummer-fenton/' ) ); ?>" style="text-decoration:none">
			<h2 class="secondary callout-headline">Vanetta</h2>
			<p id="secondary-callout-docu-profile-vanetta-label" class="callout-body secondary-callout-body">Learn how a young mother and personal trainer drew from a well of inner strength to navigate her life with MG.</p>
			<span role="button" class="capsule secondary docu-bio-tile-button">MEET VANETTA</span>
		</a>
	</div>
</div>
