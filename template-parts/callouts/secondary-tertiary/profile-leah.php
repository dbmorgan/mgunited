<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-profile-leah-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-profile-leah">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-profile-leah-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">REAL STORIES</p>
			<h2 id="secondary-callout-profile-leah-label" class="secondary callout-headline">Leah Gaitan-Diaz and the Empowerment of Positive Thinking*</h2>
			<p class="callout-body secondary-callout-body">This California native started taking charge of her MG by insisting on positivity in her life.</p>
			<span class="read-duration callout-read-time">6 MIN READ</span>
		</a>
	</div>
</div>
