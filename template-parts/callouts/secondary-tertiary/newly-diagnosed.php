<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-newly-diagnosed-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-newly-diagnosed">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-newly-diagnosed-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/newly-diagnosed/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/newly-diagnosed/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">DISEASE & TREATMENT</p>
			<h2 id="secondary-callout-newly-diagnosed-label" class="secondary callout-headline">Tips to Help You Cope When You’re Newly Diagnosed</h2>
			<p class="callout-body secondary-callout-body">Getting diagnosed is a first step. Learn what you can do to help prepare for the future.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
