<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-mg-sole-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-mg-sole">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-mg-sole-label" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-community-project' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-community-project' ) ); ?>" style="text-decoration:none">
			<!-- <p class="callout-category secondary-callout-category eyebrow">COMMUNITY PROJECT</p> -->
			<h2 id="secondary-callout-mg-sole-label" class="secondary callout-headline">My MG Sole Fights Coronavirus Blues with Kicky Art</h2>
			<p class="callout-body secondary-callout-body">This just-launched community art project lets us join together while staying apart.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
