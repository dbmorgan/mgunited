<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-profile-kathy-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-profile-kathy">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-profile-kathy-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kathy-and-diane/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kathy-and-diane/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">REAL STORIES</p>
			<h2 id="secondary-callout-profile-kathy-label" class="secondary callout-headline">Kathy Lemenu and Her Wife Diane on MG, Marriage and Communication*</h2>
			<p class="callout-body secondary-callout-body">They’ve learned to avoid the traps that MG sets for couples with something they call ‘kind confession.’</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
