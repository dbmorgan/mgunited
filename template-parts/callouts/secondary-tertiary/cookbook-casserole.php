<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-cookbook-casserole-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-cookbook-casserole">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-cookbook-casserole-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/savory-soul-food-casserole/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/savory-soul-food-casserole/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EATING & MG</p>
			<h2 id="secondary-callout-cookbook-casserole-label" class="secondary callout-headline">Savory Soul Food Casserole</h2>
			<p class="callout-body secondary-callout-body">Get real cozy with this fall flavored, layered dish—with collard greens, parsnips and sweet potatoes.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
