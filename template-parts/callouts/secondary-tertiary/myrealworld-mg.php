<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-myrealworld-mg-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-myrealworld-mg">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-myrealworld-mg-label" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/myrealworld-mg-myasthenia-gravis-study/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/myrealworld-mg-myasthenia-gravis-study/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">SYMPTOM TRACKING</p>
			<h2 id="secondary-callout-myrealworld-mg-label" class="secondary callout-headline">See If You Can Help Researchers by Sharing Your MG Journey</h2>
			<p class="callout-body secondary-callout-body"></p>
			<span class="read-duration callout-read-time">3 MIN READ</span>
		</a>
	</div>
</div>
