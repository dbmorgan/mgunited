<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-other-conditions-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-other-conditions">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-other-conditions-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/mg-and-other-conditions/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/mg-and-other-conditions/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">DISEASE & TREATMENT</p>
			<h2 id="secondary-callout-other-conditions-label" class="secondary callout-headline">Living with MG and Other Conditions</h2>
			<p class="callout-body secondary-callout-body">People living with MG often have to deal with other conditions too. It can get tricky.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
