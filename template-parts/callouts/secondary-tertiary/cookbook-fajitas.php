<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-cookbook-fajitas-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-cookbook-fajitas">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-cookbook-fajitas-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/zesty-chicken-fajitas/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/zesty-chicken-fajitas/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">EATING & MG</p>
			<h2 id="secondary-callout-cookbook-fajitas-label" class="secondary callout-headline">Zesty Chicken Fajitas</h2>
			<p class="callout-body secondary-callout-body">Chicken, rice and veggies simmer together in a flavor-packed, MG-friendly nod to Mexican food. Dig in.</p>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
