<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-for-some-time-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-for-some-time">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-for-some-time-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/been-living-with-mg-for-some-time/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/been-living-with-mg-for-some-time/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">DISEASE & TREATMENT</p>
			<h2 id="secondary-callout-for-some-time-label" class="secondary callout-headline">Been Living with MG for Some Time?</h2>
			<p class="callout-body secondary-callout-body">Being prepared for changes in your MG can make a big difference.</p>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>

