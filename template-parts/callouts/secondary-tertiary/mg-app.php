<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-mg-app-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-mg-app">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-mg-app-label" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/smartphone-MG-management/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/smartphone-MG-management/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">LIFE WITH MG</p>
			<h2 id="secondary-callout-mg-app-label" class="secondary callout-headline">Managing MG: There’s an App for That</h2>
			<span class="read-duration callout-read-time">4 MIN READ</span>
		</a>
	</div>
</div>
