<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="secondary-callout-doctor-tools-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top secondary-callout-image" id="secondary-callout-doctor-tools">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="secondary-callout-doctor-tools-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom secondary-callout-copy">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use/' ) ); ?>" style="text-decoration:none">
			<p class="callout-category secondary-callout-category eyebrow">DISEASE & TREATMENT</p>
			<h2 id="secondary-callout-doctor-tools-label" class="secondary callout-headline">Learn More About Common Tools Neurologists Use to Chart Your Symptoms</h2>
			<span class="read-duration callout-read-time">5 MIN READ</span>
		</a>
	</div>
</div>
