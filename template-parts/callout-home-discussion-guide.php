<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="home-callout-discussion-guide-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-discussion-guide">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-discussion-guide-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-adult-family-members' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/discussion-guide-adult-family-members' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-discussion-guide-label" class="secondary">Discussion Guide: Talking to Family and Close Friends About MG</h2>
			<p>Myasthenia gravis doesn’t make it easy. But helping family and friends “get it” is worth the effort.</p>
			<span class="read-duration">7 MIN READ</span>
		</a>
	</div>
</div>
