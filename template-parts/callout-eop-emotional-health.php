<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-emotional-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eoc-emotion">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-emotional-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/emotional-health' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/emotional-health' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">EMOTIONAL SUPPORT</p>
			<h2 id="eop-callout-emotional-label" class="secondary">How to Have Good Emotional Health and MG at the Same Time </h2>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
