<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="friendly-smoothies-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>LIFE WITH MG</b></p>
				<h1>5 MG-Friendly Smoothies</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					You don’t have to have MG to love these delicious, nutritious recipes.
				</h2>
				<p>Smoothies are great in the summer for beating the heat—or anytime for quick, easy nourishment. In summer, keeping cool is especially important, as heat can increase feelings of weakness in people living with MG.<sup>1-3</sup> A nice, cold, easy-to-swallow drink could help. And taking a break from cooking never hurts, in the summertime or anytime.</p>

				<p>We asked nutritionist Megan Stanley, MS, RD, to develop smoothie recipes for people living with MG. These smoothies aren’t just easy to make, but easy to drink as well. They provide quick nutrition for tired bodies while bringing anti-inflammatory foods into your diet. They all take very little prep work, can be prepared in a dishwasher-safe blender or smoothie maker and are easy to swallow if thoroughly blended. Some ingredients can be purchased pre-chopped and frozen for long-term storage.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-almondbutterjelly.jpg" alt="Almond Butter &amp; Jelly (AB&amp;J)" class="lg" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-almondbutterjelly-sm.jpg" alt="Almond Butter &amp; Jelly (AB&amp;J)" class="sm" />
				</p>
				<div class="recipe">
					<div class="row">
						<div class="ingredients">
							<div>
								<p><strong>Ingredients:</strong></p>
								<ul>
									<li>&#x00BD; cup frozen strawberries</li>
									<li>2 tablespoons almond butter</li>
									<li>1 medium banana </li>
									<li>&#x00BD; cup milk of choice</li>
									<li>1 cup ice (less, if using frozen bananas)</li>
								</ul>
							</div>
							<div class="print lg">
								<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-ABJ.pdf' ) ); ?>">PRINT RECIPE</a>
							</div>
						</div>
						<div class="instructions">
							<h2>Almond Butter &amp; Jelly (AB&amp;J)</h2>
							<p><strong>Yields:</strong> 1 large serving or 2 small servings</p>
							<p><strong>Total Time:</strong> 5 MINS
								<strong class="prep">Prep Time:</strong> 2 MINS</p>
								<p>The AB&J froths over with antioxidant-rich berries and healthy fats, both of which help reduce inflammation, while serving up a big taste of childhood nostalgia.<sup>4,5</sup></p>
								<p><strong>Pro tip:</strong> Freezing bananas for smoothies helps make the consistency creamier.</p>

								<div class="directions lg">
									<p><strong>Directions:</strong></p>
									<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
								</div>
							</div>
						</div>
						<div class="row sm">
							<div class="print">
								<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-ABJ.pdf' ) ); ?>">PRINT RECIPE</a>
							</div>
							<div class="directions">
								<p><strong>Directions:</strong></p>

								<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="row article-body">
			<div>
				<div class="content">
					<p><img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-green-smoothie.jpg" alt="Green Smoothie" class="lg" />
						<img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-green-smoothie-sm.jpg" alt="Green Smoothie" class="sm" /></p>
						<div class="recipe">
							<div class="row">
								<div class="ingredients">
									<div>
										<p><strong>Ingredients:</strong></p>
										<ul>
											<li>&#x00BD; cup spinach (fresh or frozen)</li>
											<li>&#x00BD; medium avocado (can do fresh or frozen avocado halves when ripe)</li>
											<li>1 medium banana</li>
											<li>&#x00BD; cup milk of choice</li>
											<li>1 to 2 teaspoons  honey for sweetness, if needed</li>
											<li>Ice, if needed</li>
										</ul>
									</div>
									<div class="print lg">
										<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Green.pdf' ) ); ?>">PRINT RECIPE</a>
									</div>
								</div>
								<div class="instructions">
									<h2>Green Smoothie</h2>
									<p><strong>Yields:</strong> 1 large serving or 2 small servings</p>
									<p><strong>Total Time:</strong> 5 MINS
										<strong class="prep">Prep Time:</strong> 2 MINS</p>
										<p>The Green Smoothie is packed with inflammation-fighting ingredients while adding some sweetness and greens to your snack.<sup>6</sup></p>
										<p><strong>Pro tip:</strong> Freezing bananas for smoothies helps make the consistency creamier.</p>

										<div class="directions lg">
											<p><strong>Directions:</strong></p>
											<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
										</div>
									</div>
								</div>
								<div class="row sm">
									<div class="print">
										<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Green.pdf' ) ); ?>">PRINT RECIPE</a>
									</div>
									<div class="directions">
										<p><strong>Directions:</strong></p>

										<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>


				<section class="row article-body">
					<div>
						<div class="content">
							<p><img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-tropical-turmeric-smoothie.jpg" alt="Tropical Turmeric Smoothie" class="lg" />
								<img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-tropical-turmeric-smoothie-sm.jpg" alt="Tropical Turmeric Smoothie" class="sm" /></p>
								<div class="recipe">
									<div class="row">
										<div class="ingredients">
											<div>
												<p><strong>Ingredients:</strong></p>
												<ul>
													<li>&#x00BD; cup frozen mango </li>
													<li>&#x00BD; cup frozen pineapple </li>
													<li>1 medium banana</li>
													<li>&#x00BC; teaspoon turmeric</li>
													<li>&#x00BD; cup milk of choice</li>
												</ul>
											</div>
											<div class="print lg">
												<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Tropical.pdf' ) ); ?>">PRINT RECIPE</a>
											</div>
										</div>
										<div class="instructions">
											<h2>Tropical Turmeric Smoothie</h2>
											<p><strong>Yields:</strong> 1 large serving or 2 small servings</p>

											<p><strong>Total Time:</strong> 5 MINS
												<strong class="prep">Prep Time:</strong> 2 MINS</p>

												<p>Turmeric is known to help fight inflammation.<sup>7</sup> Throw in some tropical ingredients and you have a tasty treat. </p>
												<p><strong>Pro tip:</strong> Freezing bananas for smoothies helps make the consistency creamier.</p>

												<div class="directions lg">
												<p><strong>Directions:</strong></p>

												<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
												</div>
											</div>
										</div>
										<div class="row sm">
											<div class="print">
												<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Tropical.pdf' ) ); ?>">PRINT RECIPE</a>
											</div>
											<div class="directions">
												<p><strong>Directions:</strong></p>

												<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>

						<section class="row article-body">
							<div>
								<div class="content">
									<p><img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-chocolate-smoothie.jpg" alt="Chocolate Avocado Popsicle Smoothie" class="lg" />
										<img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-chocolate-smoothie-sm.png" alt="Chocolate Avocado Popsicle Smoothie" class="sm" /></p>
										<div class="recipe">
											<div class="row">
												<div class="ingredients">
													<div>
														<p><strong>Ingredients:</strong></p>
														<ul>
															<li>1 tablespoon dark cocoa powder</li>
															<li>&#x00BD; avocado (can do fresh or frozen avocado halves when ripe)</li>
															<li>1 cup milk of choice</li>
															<li>1 teaspoon honey </li>
															<li>1 cup ice </li>
														</ul>
													</div>
													<div class="print lg">
														<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Chocolate.pdf' ) ); ?>">PRINT RECIPE</a>
													</div>
												</div>
												<div class="instructions">
													<h2>Chocolate Avocado Popsicle Smoothie</h2>
													<p><strong>Yields:</strong> 1 large serving or 2 small servings</p>

													<p><strong>Total Time:</strong> 5 MINS
														<strong class="prep">Prep Time:</strong> 2 MINS</p>

														<p>This smoothie drinks like an indulgent treat because it’s made with antioxidant-rich cocoa and tastes like fudge.<sup>8</sup></p>

													<div class="directions lg">
														<p><strong>Directions:</strong></p>

														<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
													</div>

													</div>
												</div>
												<div class="row sm">
													<div class="print">
														<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Chocolate.pdf' ) ); ?>">PRINT RECIPE</a>
													</div>
													<div class="directions">
														<p><strong>Directions:</strong></p>

														<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</section>


								<section class="row article-body">
									<div>
										<div class="content">
											<p><img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-cherry-smoothie.jpg" alt="Cherry Sweet-Tart Smoothie" class="lg" />
												<img src="<?php echo get_template_directory_uri(); ?>/images/friendly-smoothies/recipe-cherry-smoothie-sm.jpg" alt="Cherry Sweet-Tart Smoothie" class="sm" /></p>
												<div class="recipe">
													<div class="row">
														<div class="ingredients">
															<div>
																<p><strong>Ingredients:</strong></p>
																<ul>
																	<li>8 ounces tart cherry juice</li>
																	<li>1 cup frozen mixed berries </li>
																	<li>6 ounces yogurt of choice</li>
																	<li>Ice, if needed</li>
																</ul>
															</div>

															<div class="print lg">
																<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Cherry.pdf' ) ); ?>">PRINT RECIPE</a>
															</div>
														</div>
														<div class="instructions">
															<h2>Cherry Sweet-Tart Smoothie</h2>
															<p><strong>Yields:</strong> 1 large serving or 2 small servings</p>

															<p><strong>Total Time:</strong> 5 MINS
																<strong class="prep">Prep Time:</strong> 2 MINS</p>

																<p>Tart cherries are often used to help decrease inflammation, due to their high-antioxidant capacity.<sup>9</sup> They can also taste, well…tart. But when you mix them with sweet berries, they become a refreshing sweet treat.</p>

																<div class="directions lg">
																	<p><strong>Directions:</strong></p>

																	<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
																</div>
															</div>
														</div>
														<div class="row sm">
															<div class="print">
																<a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/5-MG-Friendly-Smoothies-Cherry.pdf' ) ); ?>">PRINT RECIPE</a>
															</div>
															<div class="directions">
																<p><strong>Directions:</strong></p>

																<p>Place all ingredients into a blender and blend until smooth. If smoothies are too thick to blend, add more liquid until desired texture is reached.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
										<section id="references">
											<div>
												<div>
													<p><a href="#references"><span></span>References</a></p>
													<ol>
														<li>Rutkove SB, et al. <cite>Muscle Nerve</cite>. 1998;21:1414-1418.</li>
														<li>Rutkove SB. <cite>Muscle Nerve</cite>. 2001;24:867-882.</li>
														<li>Borenstein S, et al. <cite>Lancet</cite>. 1974;2(7872):63-66. </li>
														<li>Joseph SV, et al. <cite>J Agric Food Chem</cite>. 2014;62(18):3886-3903.</li>
														<li>Barreca D, et al. <cite>Nutrients</cite>. 2020;12(3):672.</li>
														<li>Lu Q, et al. <cite>J Agric Food Chem</cite>. 2009;57(21):10408-10413.</li>
														<li>Gupta SC, et al. <cite>Clin Exp Pharmacol Physiol</cite>. 2012;39(3):283-299.</li>
														<li>Becker K, et al. <cite>Front Pharmacol</cite>. 2013;4:154.</li>
														<li>Ferretti G, et al. <cite>Molecules</cite>. 2010;15(10):6993-7005.</li>
													</ol>
												</div>
											</div>
										</section>

										<?php get_template_part( 'template-parts/callout', 'hungry' ); ?>

										<section id="eoc" class="container">
											<div class="row">
												<div class="col col-3">
													<?php get_template_part( 'template-parts/callout', 'eop-summer-heat-tips' ); ?>
												</div>
												<div class="col col-3">
													<?php get_template_part( 'template-parts/callout', 'eop-kathy-and-diane' ); ?>
												</div>
												<div class="col col-3">
													<?php get_template_part( 'template-parts/callout', 'eop-been-living-with-mg-for-some-time' ); ?>
												</div>
											</div>
										</section>

										<?php get_template_part( 'template-parts/callout', 'social' ); ?>

									</article><!-- #post-<?php the_ID(); ?> -->
