<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="home-callout-docu-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-documentary">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-docu-label" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>" style="text-decoration:none">
			<span class="bttn-play"></span>
		</a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-docu-label" class="secondary">One Relentless Illness. Three Unstoppable People.</h2>
			<p>MG&nbsp;United members get a first look at a new documentary featuring real people battling this mysterious disease.</p>
			<span class="read-duration">2 MIN VIEW</span>
		</a>
	</div>
</div>
