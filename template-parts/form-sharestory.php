<?php

	// We'll use the API to retrieve static form data
	include_once get_template_directory() . "/api-tools.php";

	$staticFormData = get_static_form_data();
	if ($staticFormData == null)
		return;

	const ID_STORY = 'share-story';
	const ID_STORY_OPTIN = 'share-story-optin';

?>
<form id="form-step-share-story">
	<div class="form-header">
		<h2>SHARE YOUR MG STORY WITH US</h2>
		<p>Every person living with MG has a story that may help others in similar situations. We’d like to hear yours.</p>
	</div>
	<fieldset aria-labelledby="share-story-textarea">
		<p aria-labelledby="share-story-textarea" id="share-story-textarea"><strong>Tell us about your experience.</strong></p>

		<label aria-labelledby="share-story-textarea">
			<textarea aria-labelledby="share-story-textarea" aria-required="true" id="<?php echo ID_STORY; ?>" placeholder="" maxlength="<?php echo $staticFormData->maxLengths->story; ?>"></textarea>
		</label>
		<div class="error-message"></div>
	</fieldset>

	<fieldset aria-labelledby="share-story-radio">
		<p aria-labelledby="share-story-radio" id="share-story-radio"><strong>Would you be willing to be featured in an MG&nbsp;United member story?</strong></p>

		<div aria-labelledby="share-story-optin-yes">
			<label aria-labelledby="share-story-optin-yes" class="radio">
				<input aria-labelledby="share-story-optin-yes" aria-required="true" type="radio" id="<?php echo ID_STORY_OPTIN; ?>-yes" name="share-story-optin" value="true" />Yes
			</label>
		</div>

		<div aria-labelledby="share-story-optin-yes">
			<label aria-labelledby="share-story-optin-no" class="radio">
				<input aria-labelledby="share-story-optin-no" aria-required="true" type="radio" id="<?php echo ID_STORY_OPTIN; ?>-no" name="share-story-optin" value="false" />No
			</label>
		</div>

		<div class="error-message"></div>
	</fieldset>

	<input type="submit" class="primary" value="NEXT" />
</form>
