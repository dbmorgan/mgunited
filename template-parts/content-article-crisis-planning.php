<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="crisis-planning-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>Crisis 411: What You Need to Know About Myasthenic Crisis</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JUNE 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/images/crisis-planning/author-image.jpg" alt="" />
				</div>
				<div>
					<p>This article is coauthored by Ratna Sanka, MD, neurologist, University of Texas Health, San Antonio, and Phil Cogan, MA, MGFA Board of Directors and Crisis Management Expert.</h3>
				</div>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h2 >Know the signs of an MG crisis? You should.</h2>
				<p >Life eventually teaches us the importance of preparing for emergencies before they happen. But life with MG makes this lesson even more important. A myasthenic crisis needs to be handled properly and quickly.<sup>1-3</sup> There are many reasons that you may delay creating an MG crisis plan. But wouldn&rsquo;t you rather prepare for a crisis that never comes than deal with one without the right information? Here&rsquo;s what you need to know.</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3 >What Is an MG Crisis?</h3>
				<p >A myasthenic crisis happens when worsening myasthenic weakness requires intubation or noninvasive ventilation to avoid intubation.<sup>1</sup> In other words, you can&rsquo;t breathe without a ventilator and need immediate medical help.<sup>1,3</sup></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3 > Red Alert: Signs You&rsquo;re in Crisis </h3>
				<p >There are many reasons for shortness of breath that have nothing to do with MG but may be equally troubling, including lung disease, heart problems, anxiety and more.<sup>4</sup> However, any <em>one</em> of the following could signal you&rsquo;re in MG crisis:</p>
				<p > </p>
				<ul >
					<li>In general, it feels harder to breathe.<sup>4</sup></li>
					<li>You can&rsquo;t complete sentences or count to 20 without getting out of breath.<sup>3,4</sup></li>
					<li>The muscles between your ribs and above the collarbone cave in when you breathe.<sup>3</sup></li>
					<li>Your cough can’t clear saliva or phlegm.<sup>3</sup></li>
				</ul>
				<p ><strong>If you are struggling to breathe, it is very important to call 911 immediately.</strong></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3 >What Is an <em>Impending</em> MG Crisis?</h3>
				<p >Before a full-blown crisis, you may experience an impending crisis. This is a flare or exacerbation that could lead to a crisis in days or weeks. The signs vary by case. Take <em>any</em> of them as an early warning&mdash;you&rsquo;ll need closer monitoring at a hospital with aggressive treatment to prevent a full crisis.</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3 >Red Flags: Trouble with Chewing, Swallowing or Coughing</h3>
				<p >You might have a weak cough that is unable to clear your throat. You may cough frequently while eating and drinking. You could have trouble swallowing and need softer foods. Or you might get the sensation of liquids coming through your nose while swallowing.<sup>2</sup></p>
				<p >Other red flags are a drooping head, speech that's nasal or low volume, or needing to take a breath after every few words.<sup>3</sup></p>
				<p ><strong>If you experience any of these symptoms, you should go to a hospital immediately. </strong></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3 >Yellow Flags: The Signs to Watch<sup>2,5</sup></h3>
				<p >Although breathing, chewing, swallowing and coughing are the most troublesome signs of an impending crisis, there are many others that might be easier to miss, at least at first. These include any worsening of your MG symptoms, from double vision, eye drooping, slurred speech and arm weakness, to falling and trouble walking. </p>
				<p >Regular appointments with your doctor and symptom tracking may help you notice these signs early. </p>
			</div>
		</div>
	</section>

	<section id="one-up-bullet-list" class="row">
		<div>
			<div>
				<h3>Know the Triggers</h3>
				<p>To manage the risk of MG crisis, you need to know what can cause one. The list of known MG crisis triggers includes:<sup>3</sup></p>
				<ul>
					<li>Medications, such as high doses of steroids</li>
					<li>Certain antibiotics </li>
					<li>Some heart and blood pressure medications</li>
					<li>Agents used for anesthesia</li>
					<li>Magnesium</li>
					<li>Illness causing fever, pneumonia or other lung infections</li>
					<li>Stress from trauma or surgery</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row secondary-article-body">
		<div>
			<div class="content">
				<h3>When to Call Your Neurologist. When to Dial 911.</h3>
				<p >What to do in a myasthenic crisis can get complicated. Be sure to talk with your neurologist regularly about your symptoms to help them know when things are getting out of hand.</p>
			</div>
	</section>

	<section id="three-up-bullet-list" class="container">
		<div>
			<div>
				<div class="bullet-list-header">
					<h3>Call Your Neurologist When You Experience:<sup>2</sup></h3>
				</div>
				<div class="bullet-list-body">
					<ul >
						<li> Worsening of any MG symptoms </li>
					</ul>
				</div>
				<div class="bullet-list-header">
					<h3 >Go to the Hospital When You Experience:<sup>2</sup></h3>
				</div>
				<div class="bullet-list-body">
					<ul>
						<li>Weak cough that is unable to clear your throat</li>
						<li>Coughing frequently while eating and drinking</li>
						<li>Trouble swallowing</li>
						<li>Sensation of liquids coming through your nose while swallowing</li>
						<li>Drooping head</li>
						<li>Speech that&#39;s nasal or low volume</li>
						<li>Needing to take a breath after every few words</li>
					</ul>
				</div>
				<div class="bullet-list-header">
					<h3 >Call 911 When You Experience:</h3>
				</div>
				<div class="bullet-list-body">
					<ul>
						<li>Difficulty breathing at rest<sup>4</sup></li>
						<li>Inability to count to 20 in a single breath<sup>3,4</sup></li>
						<li>The muscles between your ribs and above the collarbone caving in when you breathe<sup>3</sup></li>
						<li>Feeling like you are drowning in your own saliva or phlegm<sup>3</sup></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="row secondary-article-body">
		<div>
			<div class="content">
				<h3>Prep Others to Be Prepared </h3>
				<p>It’s important to prepare for a myasthenic crisis ahead of time. Be sure to discuss everything in detail with your neurologist, as every case of MG is different. You should also teach family, friends, caregivers and coworkers about MG and the steps they may need to take if a crisis happens. Even if that day never comes, it feels good to know you have a plan in place to get through an MG crisis safely.</p>
				<p>A complete guide to creating an MG crisis action plan is coming soon to MG&nbsp;United. In the meantime, please print the MGFA’s <a href="https://myasthenia.org/Portals/0/Emergency%20Alert%20Card.pdf" target="_blank" class="no-interstitial">MG Wallet Card</a>, fill it out, and carry with you at all times.</p>
			</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Sanders DB, et al. <cite>Neurology</cite>. 2016;87(4):419-425.</li>
					<li>Stetefeld H, et al. <cite>Neurol Res Pract</cite>. 2019;1(19):1-6.</li>
					<li>Wendell LC, et al. <cite>Neurohospitalist. </cite>2011;1(1):16-22.</li>
					<li>Wahls SA. <cite>Am Fam Physician</cite>. 2012:86(2):173-180.</li>
					<li>Gilhus NE, et al. <cite>Lancet Neurol</cite>. 2015;14(10):1023-1036.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-what-is-mg' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
