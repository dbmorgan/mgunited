<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="documentary-bio" <?php post_class(); ?>>
	<section id="hero">
		<!-- create space  -->
		<video id="hero-video" autoplay muted disablePictureInPicture loop playsinline preload="auto">
			<!-- <video muted disablePictureInPicture preload="auto"> -->
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_VANETTA_15cutdown_00c.mp4" type="video/mp4">
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_VANETTA_15cutdown_00c.webm" type="video/webm">
		</video>

		<div id="ciff-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/documentary/CIFF_OfficialSelection2020_Logo_WhiteRGB.svg" alt="The Chicago International Film Festival 2020 Official Selection"></div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>OCTOBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="intro">
		<div>
			<div class="container">
				<div class="row">
					<div class="col col-8 offset-2">
						<h3>Vanetta Drummer-Fenton</h3>
						<p>“I may be weak, but I’m strong.” Vanetta Drummer-Fenton started using the phrase as a code among her close-knit family to let them know how her symptoms were each day. But now, it’s become a personal mantra to help her deal with MG. </p>
						<p>A popular honor student and step team captain, Vanetta was a classic high school overachiever with energy to spare. Then myasthenia gravis dramatically changed the script on her. Since her diagnosis, her close-knit extended family has given Vanetta the support she’s needed to pursue her unique career as a personal trainer to people with chronic illnesses. And as the mother of a young daughter, Vanetta gained new perspective on a devastating experience she witnessed her mother go through years earlier: the moment Vanetta’s doctors admitted, “We don’t know what’s wrong with her.”</p>
						
						<p>Vanetta’s film from <em>A Mystery to Me</em> has been accepted into the 56<sup>th</sup> Chicago International Film Festival. It will be screened in the Documentary Short Film Competition.</p>
						
						<p>Her film will be competing against some of the best nonfiction work from around the globe for the Gold Hugo for Best Documentary Short at the festival.</p>

					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col col-8 offset-3">
					<blockquote>
						<p>"I may be weak, but I’m strong."</p>
					</blockquote>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row inline-video-row">
					<div class="col col-sm-6 col-8 offset-2">
						<!-- <div class="article-inline-video"><iframe src="https://player.vimeo.com/video/458976614" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div> -->
						<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/468706987" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col col-8 offset-2 callout-rsvp">
						<h4>Join us for the virtual premiere on November&nbsp;17<sup>th</sup></h4>
						<br>
						<p><a href="#" class="capsule tertiary" id="rsvp-modal">RSVP Today!</a> <br>
						</p>
						<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="docu-callouts" class="content">
		<div class="row">
			<div class="col col-sm-6 col-10 offset-2">
				<h2 class="docu-callout-label">More MG Stories</h2>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row">
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/docu-profile-teresa' ); ?>
			</div>
			<div class="col col-sm-6 col-4">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/docu-profile-glenn'); ?>
			</div>
		</div>
	</section>

	<section id="action" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

</article><!-- #post-<?php the_ID(); ?> -->
