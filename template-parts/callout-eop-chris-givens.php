<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-chris-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-chris-givens">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-chris-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/chris-givens' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/chris-givens' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">REAL STORIES</p>
			<h2 id="eop-callout-chris-label" class="secondary">Chris Givens Dives into His Life with MG</h2>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
