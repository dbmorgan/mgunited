<div class="row eoc-callout" tabindex="0" aria-labelledby="real-story-callout-leah-label">
	<div class="col col-sm-12 col-7 cta-left-reverse cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz' ) ); ?>" style="text-decoration:none">
			<h2 id="real-story-callout-leah-label" class="secondary">Leah Gaitan-Diaz and the Empowerment of Positive Thinking</h2>
			<p>This California native started taking charge of her MG by insisting on positivity in her life.</p>
			<span class="read-duration">6 MIN READ</span>
		</a>
	</div>
	<div role="presentation" class="col col-sm-12 col-5 cta-right-reverse cta-top" id="real-story-leah">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="real-story-callout-leah-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz' ) ); ?>" style="text-decoration:none"></a>
	</div>
</div>
