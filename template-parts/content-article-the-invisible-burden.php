<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="the-invsible-burden-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div>
				<p><b>EMOTIONAL SUPPORT</b></p>
				<h1>16 Ways Caregivers Can Care for Themselves</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>AUGUST 2020 | <b>10 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					We are sharing some simple things that may help you care for your mind, body and soul.
				</h2>
				<p>
					Dealing with myasthenia gravis in the family can seem like it takes all of your focus, leaving little time or energy for your own needs. Self-care is absolutely worth the effort and it can allow you to better care for your loved one. Caregivers—whether spouses, partners, friends or family members—can support loved ones without becoming overwhelmed or unhappy. Balance and self-care are important. Here are some helpful tips.
				</p>
				<div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img class="illo" style="width: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/invisible-burden/illo-1.svg"></div>
				<div class="col-8 content">
					<h3>1. Acknowledge Your Struggle. It’s Real</h3>
					<p>Life with MG can be unpredictable—up one day and down the next. It can even change hour to hour. And when it comes to your role in the family? Well, that can change quickly, too. In addition to perhaps working full-time, you may find yourself making and planning meals, shuttling the kids to after-school activities, helping with homework, putting the kids to bed and managing finances and health insurance claims. You’re doing all this while caring for a person whose life was upended with an MG diagnosis.</p>
					<p>There might be days when you may feel overwhelmed. Admitting that is the crucial first step in self-care. Take a moment to own this truth: Your struggle, like that of your loved one, is very real.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>2. Get Enough Sleep</h3>
				<p>Generally, seven to nine hours per night is considered adequate.<sup>1</sup> A good night’s sleep can be restorative and may help people function better with less anxiety.<sup>2</sup></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>3. Exercise</h3>
				<p>Exercise can take on different forms, whether it is taking a walk, playing with your kids or grandkids, or weeding your yard. It can be anything that fits your lifestyle and gets you moving. If you have kids, ask them to join you. Exercise has many benefits, including helping to reduce stress and anxiety, boost self-esteem and reduce blood pressure.<sup>3</sup></p>
			</div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img class="illo" alt="" src="<?php echo get_template_directory_uri(); ?>/images/invisible-burden/illo-2.svg"></div>
				<div class="col-8 content">
					<h3>4. Carve Out Time Every Week to do Something You Enjoy</h3>
					<p>Blow the dust off your guitar. Finish a crossword puzzle. Catch up on a TV show you love. Even little breaks to focus on yourself can go a long way in lifting your spirits and re-energizing you.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>5. Consider Speaking to a Counselor or Join a Support Group for Caregivers</h3>
				<p>Therapy and support groups can provide safe spaces for you to vent and explore your emotional health. You can find a licensed professional online, starting with your health insurance provider, to determine if therapy is covered under your plan. Also check out your state’s medical board website. For more information about therapy resources, read <a href="<?php echo esc_url( home_url( '/emotional-support/emotional-health/' ) ); ?>">“How to Have Good Emotional Health and MG at the Same Time</a>.”</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>6. Enlist Your Kids</h3>
				<p>If you have kids old enough to pitch in with household chores, ask them for help with dishwashing, doing laundry or vacuuming. Some parents have no problem with this; others may really struggle with it. If you’re in the latter group, go back to step one: acknowledge that you sometimes have more to do than any one person can get done in a day. That’s when you need to delegate. And you may be surprised at how much they are willing to help out.</p>
			</div>
		</div>
	</section>

	<section class="row article-body columns right">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img class="illo" alt="" src="<?php echo get_template_directory_uri(); ?>/images/invisible-burden/illo-3.svg"></div>
				<div class="col-8 content">
					<h3>7. Take Shortcuts to Ease Your Load</h3>
					<p>Everything from groceries to school supplies can be ordered online. There are sites for meal delivery and laundry services. Many of these services are convenient and will give back valuable time to you and your family.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>8. Ask Friends and Family for Help, even if It’s Uncomfortable for You</h3>
				<p>Don’t be shy about asking; people affected by MG often need help. Perhaps a friend or family member can take the kids to school? Or maybe a neighbor can tend to your garden? Text, call or connect with people through phone apps. There are a number of  sites that let you chat together or organize for friends and neighbors to do things, like bring you meals.</p>
			</div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img class="illo" alt="" src="<?php echo get_template_directory_uri(); ?>/images/invisible-burden/illo-4.svg"></div>
				<div class="col-8 content">
					<h3>9. Educate Yourself About MG</h3>
					<p>Everyone’s MG journey is different, and that’s why it’s sometimes called the snowflake disease. This makes it difficult for others—even healthcare professionals—to fully understand what someone is going through. Adding to this challenge is that sometimes people living with MG may not share everything, making things even more confusing.</p>
					<p>Research MG as much as you can. Learning more will help you better plan your days, juggle chores and set reasonable expectations.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>10. Communicate Openly with Your Loved One</h3>
				<p>Give each other room to express feelings—and be as kind as possible while doing it. In Detroit, MI, psychologist Diane K. and her spouse living with MG refer to it as ‘kind confession.’ “It’s where we give each other permission to voice difficult feelings to each other, as long as we do it in a compassionate way,” Diane says. “With kindness, you can discuss sensitive subjects, like household chores, without fighting or silent resentment.”</p>
				<p>Let your partner opt out of an activity or chore with dignity. Try to accept that they’re simply too tired to do it. It’s not a choice. </p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>With kindness, you can discuss sensitive subjects, like household chores, without fighting or silent resentment.</p>
					<cite><strong>Caregiver Diane K.</strong></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>11. Reset Each Day</h3>
				<p>Every day is a new day. Take a moment every morning to reflect on the positives or meditate. Life with MG is unpredictable, which means that yesterday’s fatigue may be replaced with a shot of energy today, and vice versa.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>12. Use a Writing Surface, such as a Whiteboard, to Facilitate Communication</h3>
				<p>Certified patient experience expert Susan Woolner recommends that caregivers place a whiteboard in a prominent place in the kitchen to use as an MG communication center. “List things you need help with or things that are bothering you. If MG stops you from doing something, write that down, too. It can be an easy way to communicate requests for help. And it helps you share a powerful visual record of how MG affects your life together.”</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>13. Use Rankings to Communicate the Daily Status of Your Partner or Spouse</h3>
				<p>A ten-point scale or a thermometer rating (low to high) can quickly convey how MG is affecting your spouse or partner on a given day. People living with MG may not be able to articulate how much they’re struggling with a symptom such as breathing or vision, but they can always say, “I’m at a seven today.” You may need several ratings for things they struggle with, whether it’s breathing, vision, muscle weakness or brain fog.</p>
			</div>
		</div>
	</section>

	<section class="row article-body columns left">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img class="illo" alt="" src="<?php echo get_template_directory_uri(); ?>/images/invisible-burden/illo-5.svg"></div>
				<div class="col-8 content">
					<h3>14. Rethink the Division of Labor</h3>
					<p>While your partner may not be up to doing everything they used to do, they still want to contribute. Together, reevaluate household chores and rethink who does what. Your spouse can take on tasks requiring less physical energy, such as checking homework, making grocery lists and online shopping. This will help free you up to do the chores your partner can’t handle physically.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body columns right">
		<div>
			<div class="wrapper">
				<div class="col-4 image"><img class="illo" alt="" src="<?php echo get_template_directory_uri(); ?>/images/invisible-burden/illo-6.svg"></div>
				<div class="col-8 content">
					<h3>15. Let Some Things Go</h3>
					<p>Manage stress by unburdening yourself. To start, separate your to-do list into must-dos and can-waits. Also ease up on your standards. Does it really matter that your child didn’t clean the bathroom the way you’d like? Hey, it’s cleaner than before. Job done.</p>
					<p>Leah Gaitan-Diaz, a married Los Angeles native who has been living with MG for five years, used to be a perfectionist. “But with MG, that’s stress you don’t need,” she says. “It’s alright if something’s in the wrong place. You can always fix it later. Give yourself that permission.”</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>16. Direct Negative Feelings at MG, Not the Person Caring for You</h3>
				<p>Managing MG is you and your loved one’s common goal. If either one of you gets frustrated, find an emotional outlet rather than direct those feelings at each other. It’s a mind-shift that can make all the difference and allow MG to bring you closer together.</p>
				<p>This takes partnership, according to Diane K. “If you trust yourself and your partner, you can get to those vulnerable feelings behind the frustration. You can take the blame out of it,” she says. “MG can be the bad guy instead of the person who has it or the person who loves them.”</p>
				<p>For those who care for people living with MG, the struggle is real and can be overwhelming at first for everyone in the household. It’s important to take care of yourself—and that means making adjustments and coming up with a plan.  Strive for open, honest communication. Explore different ways to get things done. Above all, be good to yourself by staying healthy and carving out time to do the things you love. Tending to yourself and your needs will positively impact the person in your life who’s living with MG.</p>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Hirshkowitz M, et al. <em>Sleep Health</em>. 2015;1(4):233-243.</li>
					<li>Simon EB, et al. <em>Nat Hum Behav</em>. 2020;4(1):100-110.</li>
					<li>Mikkelsen K, et al. <em>Maturitas</em>. 2017;106:48-56.</li>
				</ol>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-kathy-and-diane' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-discussion-guide' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
