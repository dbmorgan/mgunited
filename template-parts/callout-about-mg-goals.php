<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="about-mg-callout-goals-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="about-mg-goals">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="about-mg-callout-goals-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/treatment-goals' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/treatment-goals' ) ); ?>" style="text-decoration:none">
			<h2 id="about-mg-callout-goals-label" class="secondary">The Importance of Setting Your Personal MG Treatment Goals</h2>
			<p>Having a goal helps you take small, realistic steps and track progress. Here’s how to set yours.</p>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
