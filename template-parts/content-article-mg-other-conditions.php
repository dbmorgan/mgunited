<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */
?>

<article id="mg-other-conditions-article" <?php post_class(); ?>>
	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>Living with MG and<br>Other Conditions</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>AUGUST 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>
	<!-- <div id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>The Four Words That Changed My Life</h1>
			</div>
		</div>
	</div> -->
	<section class="row article-body">
		<div>
			<div class="content">
				<h2>People living with MG often have to deal with other conditions, too. It can get tricky.</h2>
				<p>According to recent studies, 65%-88% of people living with MG may also deal with one or more additional chronic diseases or conditions.<sup>1-3</sup> Your doctors may call these “comorbidities,” which is just another way of saying “related conditions.” For example, high blood pressure is a common comorbidity, or related condition, of diabetes.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote script-font">
					<p>It took two years before my MG was diagnosed. My symptoms—fatigue, muscle weakness and double vision—were always chalked up to my other conditions.</p>

					<cite><strong>Zach M.</strong><br>
						<span class="sub-cite">Patient and MG Uniter</span></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Layers of Confusion</h3>
				<p>The most common related conditions for people living with MG are metabolic syndrome, heart disease, high blood pressure, and diabetes. People diagnosed with MG are also at risk of for other autoimmune diseases such as rheumatoid arthritis, lupus and thyroid disorders.<sup>1</sup>  In addition, the risk of developing depression doubles following an MG diagnosis.<sup>4</sup> Some of these conditions can cause symptoms similar to MG, such as weakness and fatigue. This can make it difficult to know which illness is responsible for which symptom. That’s why it’s important that your care team knows all the conditions you’ve been diagnosed with.</p>
				<br>
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/mg-other-conditions/article-mg-other-condit.png" alt="">
				<figcaption><em>High blood pressure is one of the most common related conditions for people living with MG.<sup>1</sup></em></figcaption>
				</p>
			</div>
		</div>
	</section>

<!-- 	<section class="row article-body">
		<div>
			<div class="content">

			</div>
		</div>
	</section> -->


	<section class="row article-body">
		<div>
			<div class="content">

			<h3>Communicating with Your Care Team is Key</h3>

			<p>You may be seeing a few different doctors and specialists, some of whom may have less experience in treating MG. Each time you visit a doctor, share your full health picture, including your medications and treatments. Your entire care team should know your health status. This helps ensure that you’re getting the best and most complete care.</p>

			<p>It's important to keep close tabs on all of your medications. Make sure that your care team and pharmacist stay current on your changing list of medications. You may want to share with your care team if you are feeling better or worse after taking a medication. This information will help in determining the right care plan for you on your MG journey. It's even common to get a second opinion to help you feel comfortable with your diagnosis and treatment.</p>

			<p>The bottom line: listen to your body. Don’t assume that a new symptom is directly related to MG. It may be related to another condition or a medication you are taking. Talk to your care team if you are not feeling well and schedule an appointment if you have new symptoms. Having as much information as possible about your symptoms, including when they arose and how long they last, can help you and your care team make management decisions.</p>

			</div>
		</div>
	</section>


	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>“Your primary care provider serves as quarterback of your interdisciplinary team. The PCP is the generalist who coordinates communication among the specialists.</p>

					<cite><strong>Niraja S. Suresh, MD</strong></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">

			<h3>Reach Out to Others</h3>

			<p>Having MG and other conditions is a lot to handle. If you have a significant other, family member or friend who supports you in your MG journey, they can help you manage all the information on your care. Ask them to join you on your visits to your doctor. They can take notes and ask clarifying questions. They can also provide a helpful outside perspective. For example, they can watch for any changes in your physical or mental health and share them with you. You can record their feedback and add it to your list of things to talk about with your care team.</p>

			<p>One of the best resources available to you is other people in the MG community. They also live with MG and have experiences you can learn from. So reach out.  While their symptoms may be different, they are likely to understand what you are going through and are willing to help. Another way to connect with others is to join a <a href="<?php echo esc_url( home_url( '/emotional-support/emotional-health' ) ); ?>">local MG support group</a> or advocacy groups like the <a href="https://myasthenia.org/" target="_blank">Myasthenia Gravis Foundation of America</a>, the <a href="http://www.mgakc.org/" target="_blank">Myasthenia Gravis Association</a> and <a href="https://www.myastheniagravis.org/" target="_blank">Conquer Myasthenia Gravis</a>.</p>

			<p>Taking an active role in your care may be empowering. Good communication can give you some sense of control when you’re dealing with several doctors and symptoms. It also can help deepen your connection to your care team, which can lead to better care and outcomes.</p>

			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Misra UK, et al. <cite>Acta Neurol Belg</cite>. 2020;120(1):59-64.</li>
					<li>Diaz CV, et al. <cite>J Neurol Neurophysiol</cite>. 2015;6(5).</li>
					<li>Tanovska N, et al.  <cite>Maced J Med Sci</cite>. 2018;6(3):472-478.</li>
					<li>Chu H, et al. <cite>Front Psychiatry</cite>. 2019;10(481):1.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-crisis' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-my-mgsole' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
