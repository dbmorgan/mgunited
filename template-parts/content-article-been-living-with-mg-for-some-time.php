<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="been-living-with-mg-for-some-time-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>Been Living with MG for Some Time?</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2 class="short-title">Being prepared for changes in your MG can make a big difference.</h2>
				<p>By now, your myasthenia gravis (MG) diagnosis may feel like a distant memory. Maybe your treatment plan is working for you and you’re feeling okay, or maybe you could be feeling better.</p>
				<p>While it can be hard to think about the future when dealing with the daily symptoms of MG, it’s important to stay empowered. A good way to prepare for potential changes is by staying informed. You’ll be surprised at how much new information on MG is out there. It may help you determine a path to improving your quality of life. </p>
			<div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>I set up Google Alerts about MG, read newsletters, listen to podcasts, watch seminars … As a mature myasthenic, staying current is how I stay empowered.</p>
					<cite><strong>Rachel Higgins</strong><br>
						Austin, TX<br>
						Diagnosed in 2008</cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Know Your MG-ADL Score</h3>
				<p>One tool that your care team may use to monitor how you are doing is the Myasthenia Gravis Activities of Daily Living (MG-ADL). The profile is a simple, eight-question survey that measures the intensity of MG symptoms across your entire body and the impact of symptoms on daily activities. Total scores can range from 0 to 24, and the lower the number, the better.<sup>1</sup></p>
				<p>Knowing your MG-ADL score may help you understand where you are with your disease. Regularly discussing your MG-ADL scores with your care team can also help you better communicate your
					symptoms, identify which symptoms you experience most consistently over time and help you set goals
					for the future.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<div class="download-slab">
					<div class="download">Download icon</div>

					<p>Download and print the MG-ADL survey to discuss with your doctor.</p>
					<div class="bttn-wrap">
						<a role="button" target="_blank"  download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MG_Activities_of_Daily_Living_MG-ADL_Profile.pdf' ) ); ?>" class="capsule primary no-interstitial">DOWNLOAD NOW</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Know Your MG Serotype</h3>
				<p>There are a few different types of MG (also called serotypes), and your treatment plan will be impacted by your type of MG.<sup>2</sup> Your serotype is determined by the type of antibodies found in your blood that are attacking your muscles.<sup>2</sup></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content bg-color_tan">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/living-with-mg-for-some-time/mg-sometime-chart.png" alt=""></p>
			</div>
		</div>
	</section>

	<section class="row article-body short-content">
		<div>
			<div class="content">
				<p>If you don’t know your serotype, ask your care team so you can be informed about what types of treatments are available to you.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Know How MG Works and What’s Being Researched by Scientists</h3>
				<p>Just like your MG may change over time, scientists’ understanding of how the disease works has
					changed a lot, especially within the past few years. There is a lot of research being done on MG, so
					it’s helpful to be familiar with the basics of how MG works to help you interpret new
					information.</p>
				<p>As you may already know, MG is caused by a breakdown in communication between nerves and muscles.
					When your immune system mistakenly attacks your muscles, your muscles have trouble receiving signals
					properly.<sup>2,6</sup></p>
				<p>To find out more about how MG works, read <a href="<?php echo esc_url( home_url( '/disease-and-treatment/what-is-mg' ) ); ?>">“What Is Myasthenia Gravis, and Why Does It Happen?”</a></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Know Your Crisis Plan</h3>
				<p>
					Tracking your MG-ADL score, knowing your serotype and understanding how MG works will help you and your care team make informed decisions about your treatment plan. One way to be more prepared for dealing with your MG is to have a plan for how to avoid a crisis if you have a flare.
				</p>
				<p>A flare is a sudden worsening of symptoms, which may include difficulty swallowing or breathing.
					Flares can occur for a number of reasons, including stress, infection and not taking high enough
					doses of MG medications.<sup>7</sup> If a flare is not controlled, it can progress to a crisis, which would
					require a trip to the hospital.<sup>7</sup> That’s why it’s best to communicate and check in regularly with
					your care team about how you are doing.</p>
				<p>Everyone with MG should do their best to avoid a crisis, and there are things to avoid (such as
					conflicting medicines). But you should also have a crisis plan, just in case. This crisis plan
					should include your family, caregivers and friends. For more information about creating a crisis
					plan, check out <a href="<?php echo esc_url( home_url( '/disease-and-treatment/crisis-planning' ) ); ?>">“Crisis 411: What You Need to Know About Myasthenic Crisis.”</a></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Get Started</h3>
				<p>
					Maybe you only knew a couple of these things, or maybe you didn’t know any of them. That’s okay! With MG, knowledge is power, and it could help you and your care team develop your treatment plan.
				</p>
				<p>Ready? Here are some tips to consider:</p>
				<p></p>
				<ul>
					<li>Download the <a class="no-interstitial" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MG_Activities_of_Daily_Living_MG-ADL_Profile.pdf' ) ); ?>">MG-Activities of Daily Life</a> profile and discuss how to use it with your care team</li>
					<li>Find out what your MG serotype is</li>
					<li>Get to know how MG works in the body, and do some research about what scientists have recently learned about MG</li>
					<li>Work with your care team to create a crisis plan, and make sure your family, caregivers and friends know the plan as well</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Share What You Know with the MG&nbsp;United Community</h3>
				<p>While living with MG for many years can be exhausting, it is also undeniably educational. You know a lot more than most about living with MG. And the strategies you’ve developed over time to manage your life better are extremely valuable. We hope you’ll take a moment to share your hard-earned wisdom to help others make progress in their MG journey.</p>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Wolfe GI, et al. <em>Neurology</em>. 1999;52(7).</li>
					<li>Gilhus NE. <em>N Engl J Med</em>. 2016;375(26):2570-2581.</li>
					<li>Gilhus NE, et al. <em>Nat Rev Neurol.</em> 2016;12(5):259-268.</li>
					<li>Behin A, Le Panse R. <em>J Neuromuscul Dis.</em> 2018;5(3):265-277.</li>
					<li>Zisimopoulou P, et al. <em>J Autoimmun.</em> 2014;52:139-145.</li>
					<li>Ruff RL, et al. <em>J Neuroimmunol.</em> 2008;201-202:13-20.</li>
					<li>Gummi RR, et al. <em>Muscle Nerve.</em> 2019;60(6):693-699.</li>
				</ol>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-victor-and-iris-yipp' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-tools-neuros-use' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
