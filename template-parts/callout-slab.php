
<?php //$categoryid = get_the_category()[0]->term_id; ?>

<section id="callout-slab" class="row">
	<div>
		<div>
			<div>
				<h2>Sign up for MG&nbsp;United!</h2>
			</div>
			<div>
				<!--Eating & MG-->
<!--				--><?php //if ($categoryid == 8)  { ?>
<!--					<p>-->
<!--						Hungry for more? Sign up today and you'll never miss another recipe.-->
<!--					</p>-->
<!--				--><?php //} else {?>
					<p>
						Get personalized content and support for wherever you are in your journey with MG. Just enter your email and you&#x2019;re in.
					</p>
<!--				--><?php //} ?>
			</div>
			<div>
				<a role="button" href="<?php echo esc_url( home_url( '/sign-up' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
			</div>
		</div>
	</div>
</section>
