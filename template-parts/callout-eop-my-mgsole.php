<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-my-mgsole-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eoc-mgsole">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-my-mgsole-label" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-community-project' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/things-to-do/my-mg-sole-myasthenia-gravis-community-project' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">COMMUNITY PROJECT</p>
			<h2 id="eop-callout-my-mgsole-label" class="secondary">My MG Sole Fights Coronavirus Blues with Kicky Art</h2>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
