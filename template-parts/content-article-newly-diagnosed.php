<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="how-to-cope-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>Tips to Help You Cope When You’re Newly Diagnosed</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					Understanding how life works with MG as a part of it
				</h2>
				<p>Being diagnosed with myasthenia gravis (MG) can bring up a lot of emotions. You may feel confused, disappointed and anxious. This is normal and very common. However, it can also be validating and a relief. And now that you have a diagnosis, you can begin to figure out how to live your best life with MG as a part of it.</p>
				<p>One important thing to remember is that you are not alone. There are many others who have been through this, and you can look to their stories for strength. It’s a good idea to learn as much as you can about your illness, so you can think about what lifestyle changes you may need to make moving forward.</p>
				<p>Everyone experiences MG a little differently, and your journey is going to be unique. There is a lot of new information to take in, and knowledge is power. The sooner you understand how to continue living your best life with this new diagnosis, the more confident you will feel.</p>
			<div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>Initially, I had so many questions. About my career, my future, my life. And I found answers in the MG community.</p>
					<cite><strong>Alicia Angel</strong><br>
						Diagnosed June 2019</cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Everyone’s Experience with MG Is Different<sup>1,2</sup></h3>
				<p>Many people who are diagnosed with MG have never even heard of the illness before. It’s a rare disease that affects people of all ages and races. MG is slightly more common in women 20 to 30 years old and men 50 to 60 years old.<sup>3</sup> Only about 31,000 to 67,000 Americans currently have MG.<sup>4,5</sup> That’s not a lot, but there is a small but mighty community of people living with MG who are eager to provide support to others.</p>
				<p>From the start, it’s helpful to note how MG impacts you as an individual. One way to do this is by looking at the Myasthenia Gravis Activities of Daily Living (MG-ADL) scale below. This scoring system is mainly used by neurologists who treat MG, but it can also give you more insights as you speak with your care team about tracking symptoms and setting achievable goals.<sup>6</sup><p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<div class="download-slab">
					<div class="download">Download icon</div>

					<p>Download and print the MG-ADL survey to discuss with your care team.</p>
					<div class="bttn-wrap">
						<a role="button" target="_blank"  download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MG_Activities_of_Daily_Living_MG-ADL_Profile.pdf' ) ); ?>" class="capsule primary no-interstitial">DOWNLOAD NOW</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p>Take a moment and consider what you are currently experiencing. During treatment course, your score may change, stay stable or even improve. Talk to your care team so they can help you determine the best goals for you and your disease. MG is always going to be a part of your life, but the goal is to make sure that you have it under control, instead of the other way around.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Learning About the Different Types of MG</h3>
				<p>For those who don’t know, antibodies are proteins the immune system uses to fight bacteria and viruses.<sup>7</sup> The type of MG you have depends on the type of MG-related antibodies found in your blood, which is also known as your serotype.<sup>7,8</sup> These antibodies are generally part of the IgG family. About 80 percent of people with MG have AChR antibodies, about 15 percent have MuSK antibodies, and approximately 3 percent have LRP4 antibodies. The last 2 percent of people with MG are truly seronegative, which means the antibodies cannot be measured.<sup>9-11</sup>  Knowing your serotype can be important, especially when discussing management options with your care team.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>“Why Is This Happening?”</h3>
				<p>Learning more about your illness can help pull back the curtain on some of the mysteries of MG, but you still may be wondering <em>why</em> you are experiencing these symptoms. MG is caused by a breakdown in communication between the nerves and the muscles.<sup>8</sup> MG is an autoimmune disease that attacks a place called the neuromuscular junction. Your immune system mistakenly attacks your muscles in that area, stopping them from properly receiving messages from the nerves.<sup>8,12</sup> For more details, take a look at <a href="<?php echo esc_url( home_url( '/disease-and-treatment/what-is-mg' ) ); ?>">“What Is Myasthenia Gravis, and Why Does It Happen?”</a></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>I have MG but MG doesn't have me.</p>
					<cite><strong>Niki G.</strong><br>
						Greenleaf, WI</cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Moving Forward with Your Life</h3>
				<p>This is just the beginning of your MG journey, and there’s going to be a learning curve. Don’t forget that you are not alone! You can read stories of others like you <a href="<?php echo esc_url( home_url( '/real-stories' ) ); ?>">here</a>. There are communities available with people who have been living with MG for a long time who are eager to help others. MG&nbsp;United has also partnered with <a href="<?php echo esc_url( home_url( '/mission/#advocacy-groups' ) ); ?>">several advocacy groups</a> working together to find ways to help improve the quality of life for people like you living with MG every day. Consider finding a support group near you, and give a meeting a try. It might be the most empowering thing you can do to get on the right path. We are here to help.</p>
				<p>MG is unpredictable. Stay informed and connect with others who have gone through this before. Arming yourself with knowledge, support and guidance from your care team will give you the understanding to help manage your illness.</p>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Gilhus NE, et al. <em>Lancet Neurol</em>. 2015;14(10):1023-1036.</li>
					<li>Hehir MK, et al. <em>Neurol Clin</em>. 2018;36(2):253-260.</li>
					<li>Mantegazza R, et al. <em>Ann N Y Acad Sci</em>. 2003;998:413-23.</li>
					<li>United States Census Bureau. <em>United States 2019 Population Estimates.</em>
						https://www.census.gov/search-results.html?searchType=web&cssp=SERP&q=us%20population. Accessed
						May 2020.</li>
					<li>Phillips LH. <em>Ann N Y Acad Sci</em>. 2003;998:407-412.</li>
					<li>Wolfe GI, et al. <em>Neurology</em>. 1999;52(7):1487-1489.</li>
					<li>Lu LL, et al. <em>Nat Rev Immunol</em>. 2018;18(1):46-61.</li>
					<li>Gilhus NE. <em>N Engl J Med</em>. 2016;375(26):2570-2581.</li>
					<li>Vincent A, et al. <em>J Neurol Neurosurg Psychiatry</em>. 1985;48(12):1246-1252.</li>
					<li>Hoch W, et al. <em>Nat Med</em>. 2001;7:365-368.</li>
					<li>Pevzner A, et al. <em>J Neurol</em>. 2012;259(3):427-435.</li>
					<li>Ruff RL, et al. <em>J Neuroimmunol</em>. 2008;201-202:13-20.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-what-is-mg' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-leah-gaitan-diaz' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
