<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="discussion-guide-children-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>EXPLAINING MG</b></p>
				<h1>Discussion Guide: Talking to Children About MG</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>SEPTEMBER 2020 | <b>7 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img class="author-callout sm" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/Author_Callout_Image_mobile.jpg" alt="Author callout image">
					<img class="author-callout lg" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/Author_Callout_Image@1x.jpg" alt="Author callout image">
				</div>
				<div>
					<p class="byline">By Emily Edlynn, PhD</p>
					<p><b>
							<em>Emily Edlynn, PhD, is a clinical health psychologist at Oak Park Behavioral Medicine in Oak Park, IL. Dr. Edlynn served as Clinical Program Director at children’s hospitals in California and Colorado, received a faculty appointment at the University of Colorado School of Medicine, and is the author of numerous parenting articles for </em>Parents Magazine, Huffington Post <em>and</em> Scary Mommy.</em>
						</b></p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>Talking to children about MG as you cope with your MG diagnosis.</h2>
				<p>
					You may be unsure how to start the conversation, what to say and how to support a child when you are still figuring it all out yourself. This guide, which is based on my own professional experiences with my patients, has some tools that may help you communicate with children of all ages with honesty and support. Your care team may have additional insights on how to approach conversations with children.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote author">
					<p>In being honest about the hard parts of living with MG, you are giving your child the gift of knowing they can trust you.</p>
					<cite><strong>Emily Edlynn, PhD</strong><br>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 1: Starting the Big Talk</h3>
				<h4>Honest and Simple</h4>
				<p>
					Starting the conversation can be difficult. Putting a name to MG and explaining how it affects the body may help children start to process that information. The level of detail depends on the child’s age, but it’s important for them to know the following:
				</p>
				<ol>
					<li>How MG affects the body, such as describing problems with how nerves and muscles work together. Using pictures may help.</li>
					<li>How you and your doctors work together to manage your MG.</li>
					<li>It’s not contagious.</li>
					<li>You may look fine on the surface, but MG does not go away and there are days when you may not feel well.</li>
					<li>Living with myasthenia gravis has taught you to pace yourself and plan more, so you can try to live a full, fun life.</li>
				</ol>
				<p>Consider describing how you might look or act different because of myasthenia gravis so they know what to expect. Explain what it feels like for you. This may mean saying something like it hurts your body in ways that they sometimes cannot see with their eyes. And it can change quickly, so that feeling good and bad can feel like a roller coaster going up and down suddenly.</p>
				<p>In being truthful about the difficult parts of living with myasthenia gravis, you are giving your child the gift of knowing they can trust you. When adults make well-meaning promises such as “everything will be fine” and then it isn’t, children can realize that you are not giving them reliable information. As painful as it may feel to be unable to protect a child from difficult situations, it shows them you believe they can deal with it and can enhance their trust in you.</p>
				<p>While this is the approach I generally recommend to parents, it never hurts to get a second opinion. If you have concerns that your child may not be ready or able to discuss your condition, you may want to reach out to your care team for support and to discuss an approach that feels right for your family.</p>
				<h4>Developmental Differences</h4>
				<p>Here are some communication tips based on ages and stages. These are broad categories with general suggestions based on child development. You know your child best, so do what you think fits. However, it’s always a good idea to consult with your child’s pediatrician for guidance on how to have this difficult conversation with your child.</p>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<div class="row age-row">
					<div class="age-col one">
						<div class="img-container-bg">
							<img class="col-img" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/DiscussionChild_Illustrations-01.png"/>
						</div>
						<div class="img-container">
							<img class="col-img" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/DiscussionChild_Illustrations01.svg"/>
						</div>
						<div class="pre-head">AGES 3-5</div>
						<div class="head">Preschool</div>
						<div class="list-content">
							<ul>
								<li>
									Use concrete details about the body
								</li>
								<li>
									Preschoolers don’t understand permanence. They may think the illness is temporary no matter what you say
								</li>
								<li>
									You may need to repeat information again and again
								</li>
							</ul>
						</div>
					</div>
					<div class="age-col two">
						<div class="img-container-bg">
							<img class="col-img" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/DiscussionChild_Illustrations-02bg.png"/>
						</div>
						<div class="img-container">
							<img class="col-img" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/DiscussionChild_Illustrations-02.svg"/>
						</div>
						<div class="pre-head">AGES 6-12</div>
						<div class="head">School</div>
						<div class="list-content">
							<ul>
								<li>
									Your explanation can be more detailed about how the illness hurts the body
								</li>
								<li>
									They tend to have more of a grasp of mortality and may ask about the future
								</li>
							</ul>
						</div>
					</div>
					<div class="age-col three">
						<div class="img-container-bg">
							<img class="col-img" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/DiscussionChild_Illustrations-03bg.png"/>
						</div>
						<div class="img-container">
							<img class="col-img" src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/DiscussionChild_Illustrations-03.svg"/>
						</div>
						<div class="pre-head">AGES 13-18</div>
						<div class="head">Adolescents</div>
						<div class="list-content">
							<ul>
								<li>
									Most likely to ask big picture questions that don’t have answers, like why someone they love would get a rare disease
								</li>
								<li>
									Likely to get their information without you (e.g., via internet searches), so be prepared to help them figure out what is a trustworthy source of information versus what is not
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 2: After the Big Talk</h3>
				<h4>What to Expect</h4>
				<p>
					Children will have a range of responses, and they may vary considerably. Some younger children process difficult information through play, so if you have the big talk and they run off to play as if nothing happened, it’s okay. Others may cry or ask lots of questions. Teenagers may do their own version of separating and withdrawing, or wanting to be with friends, and that is also a form of processing that is totally normal. Your child’s pediatrician is a great resource, so don’t hesitate to reach out for additional guidance on expectations.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<h3>Step 3: Keeping the Big Talk Going</h3>
				<h4>What’s Next</h4>
				<p>
					The “Big Talk” is only the beginning. As life with myasthenia gravis goes on, the conversation continues. Children and teens can be fickle about when and how to discuss deeper issues, so I recommend being patient and letting them take the lead. Ask them how much they want to know. Some kids prefer a lot of detail, whereas more information makes others feel more anxious and they will cope better with the big picture only. Both responses are just fine. Helping children feel more comfortable may provide them a sense of control in a situation characterized by lack of control.
				</p>
				<p>
					Adults love to anticipate children’s questions and often beat them to it by explaining even before the children ask. In this case, let the child know you are available to answer any of their questions when they are ready to ask. Sometimes a child asks a question when they are emotionally ready for the answer. If you worry that they appear distressed and upset and may be avoiding asking questions, you may gently remind them you will be there for them when they are ready.
				</p>
				<p>
					A theme of having this conversation with children is that neither of you has full control over myasthenia gravis. It’s important to find strength and hope in situations in which you may feel overwhelmed and helpless. I find that my patients, regardless of age, may cope better with stress when they feel like they know what is going on.
				</p>
				<p>
					You can work with your child or teen to identify ways they can be helpful. I recommend to caregivers I work with, however, that they not demand their children’s help and put them in a position of even more powerlessness. It can be empowering to work with your child or teen to find ways they would want to be part of the plan to help you cope with the hard days.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body dark">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>Whether she knows it or not, my daughter is my motivation for how I get through the hard days.</p>
					<cite><strong>Rachel H.</strong><br>
						<span class="sub-cite">Austin, Texas</span></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps">
		<div>
			<div class="content">
				<p>
					I also recommend to caregivers another way to help build a child’s sense of security: develop and share with them a plan for when your MG symptoms are acting up. Sometimes knowing there is a plan in place on who will take care of them when you're not feeling well may help them feel more assured.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body dicussion-steps last">
		<div>
			<div class="content">
				<h3>Special Considerations</h3>
				<h4>When to Worry</h4>
				<p>
					The range of “normal” coping in children and teens can be quite broad and diverse. Look out for changes in your child’s behavior and speak to your care team, including mental health professionals or your pediatrician, if you notice any differences in behavior. These are trained professionals who are willing to help guide you and your child.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/discussion-guide-children/inline.jpg" alt="Discussion Guide Children">
					<figcaption><em>Though it’s a difficult conversation to have, having it might bring you closer.</em></figcaption>
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>The Role of a Grandparent with MG</h3>
				<p>
					If you are a grandparent who is planning to explain your disease to a grandchild, it is helpful to communicate your intentions with their parents. They may have suggestions for what will work best for the different personalities of their children. If possible, parents should be present for the first Big Talk, both to provide emotional support to the child and to know what was said so they can reinforce it in follow-up conversations where you may not be present. Moreover, there may be instances in which a parent may not want their child to know of their grandparent’s condition. Be sure to obtain their permission in any situation.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Just the Beginning</h3>
				<p>
					Parents and grandparents want what’s best for their children, and these conversations can be helpful for children to feel assured and hopeful. You may find that these communication tips and strategies may help these important conversations go more smoothly. I find that this openness and honesty can be a way for everyone in the family to feel closer and more connected in their shared journey.
				</p>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-discussion-guide' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-zesty-chicken-fajitas' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
