<?php

	// We'll use the API to retrieve static form data
	include_once get_template_directory() . "/api-tools.php";

	$staticFormData = get_static_form_data();
	if ($staticFormData == null)
		return;

	const ID_GOAL = 'share-goal';
	const ID_GOAL_OPTIN = 'share-goal-optin';

?>
<form id="form-step-share-goal">
	<div class="form-header">
		<h2>What Is Your Goal for Life with MG?</h2>

		<p>It's easier to manage your MG when you know what you want to be able to do and experience in your daily life. Share one of your life goals here on MG&nbsp;United—you may inspire others as well.</p>
	</div>

	<fieldset aria-label="Enter your goal for life with MG">
		<p><label for="<?php echo ID_GOAL; ?>"><strong>Enter below:</strong></label></p>
		<textarea aria-required="true" id="<?php echo ID_GOAL; ?>" placeholder="" maxlength="<?php echo $staticFormData->maxLengths->goal; ?>"></textarea>
		<div class="error-message"></div>
	</fieldset>

	<fieldset aria-labelledby="ok-to-shary-story-field">
		<p id="ok-to-shary-story-field"><strong>We may reach out to request more about your story in order to develop additional content on MG&nbsp;United. Would that be okay?</strong></p>

		<div>
			<label aria-labelledby="share-goal-optin-yes" class="radio">
				<input aria-required="true" type="radio" id="<?php echo ID_GOAL_OPTIN; ?>-yes" name="<?php echo ID_GOAL_OPTIN; ?>" value="true" />
				Yes
			</label>
		</div>

		<div>
			<label aria-labelledby="share-goal-optin-no" class="radio">
				<input aria-required="true" type="radio" id="<?php echo ID_GOAL_OPTIN; ?>-no" name="<?php echo ID_GOAL_OPTIN; ?>" value="false" />
				No
			</label>
		</div>

		<div class="error-message"></div>
	</fieldset>

	<input type="submit" class="primary" value="NEXT" />
</form>
