<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="victor-profile-article" <?php post_class(); ?>>

	<section id="hero">
		<div>

		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>6 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>Victor and Iris Yipp Find Hope in MG</h1>
			</div>
		</div>
	</section>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/images/real-stories-victor/victor-profile-bio.png" alt="Victor at home" />
				</div>
				<div>
					<p><b><em>
					Victor Yipp and his wife, Iris, have been an unstoppable team through the highs and lows of Victor’s MG diagnosis. With Victor now in remission, the couple is spending their golden years seeing family, going on skiing trips and leading a local MG support group in their hometown of Oak Park, IL. Victor is also busy writing a novel. They sat down with MG&nbsp;United to share their story and their desire to give hope to the MG community.
					</em></b></p>
				</div>
			</div>
		</div>
	</section>


	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Can you describe the journey to Victor’s MG diagnosis?</h3>
				<p><strong>Iris:</strong> The entire year of 2017 led up to that MG diagnosis. Victor started getting symptoms, one by one. The first one was an issue with swallowing.</p>
				<p><strong>Victor:</strong> The doctors thought it was a GI problem.</p>
				<p><strong>Iris:</strong> Yeah, but the GI treatments didn’t help. Then you had double vision, so we went to the ophthalmologist.</p>
				<p><strong>Victor:</strong> They couldn't find anything wrong. But everything was adding up: the swallowing issue, the double vision, the inability to hold up my head…</p>
				<p><strong>Iris:</strong> We went to a bunch of doctors. But it was our own doctor who eventually figured it out. He had started researching MG after we came in because he’d never seen it before. But things went downhill quickly after that&mdash;the diagnosis and the hospitalization were only days apart. Victor was diagnosed on Wednesday, November 1, 2017. By Sunday he couldn’t drink water and needed to go to the hospital.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>I was in the hospital for 32 days.</p>
					<cite><strong>Victor Yipp</strong></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>How long were you in the hospital? What was the experience like?</h3>
				<p><strong>Victor:</strong> I was in the hospital for 32 days; I counted every day. They tried various therapies. I was taking the standard medications for MG, but none of them worked. Turns out I have a rare type of MG. It's called anti-MuSK.</p>
				<p><strong>Iris:</strong> Remember that day when the first treatment didn't work? You couldn't keep your eyes open, and you had lost a third of your body weight. That time was the most depressing. I don't know how you felt, but I was scared.</p>
				<p><strong>Victor:</strong> Yeah, it was a struggle. We eventually found one drug that helped. I was able to leave the hospital after that.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>What happened after you got out of the hospital? How did life change?</h3>
				<p><strong>Victor:</strong> I was just laying around. I had very little strength, very little stamina.</p>
				<p><strong>Iris:</strong> It took months for him to recover. With MG you can look fine, but you may be unable to get through the day. You have to rest. For instance, when he first got out, we got a temporary handicap parking permit. I always felt like people were going to judge us.</p>
				<p><strong>Victor:</strong> Because I didn’t look like I needed handicap parking. I think that's the one thing many people misunderstand. You look normal, but there's a lot going on inside that people can’t see.</p>
				<p><strong>Iris:</strong> You did need it though. In the beginning you could walk, but you couldn’t walk the whole distance of the parking lot.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>How did you think MG would affect your future?</h3>
				<p><strong>Iris:</strong> I didn’t really think ahead, beyond dealing with the symptoms right then. We did try some MG support groups. And in one of them, there was someone in remission for about 15 years. I thought, ‘Wow that would be amazing.’ It gave us hope. I thought, ‘You can do it Victor.’</p>
				<p><strong>Victor:</strong> I generally have a pretty positive outlook on life. Knowing that someone was able to get to remission, I thought, I'll get over this eventually. I'll get back to life. Maybe not the same, but close to what it was before.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/real-stories-victor/real-stories-victor-cubs-game.png" alt="Victor smiling at a Chicago Cubs game">
					<figcaption><em>Being in remission, Victor has been able to enjoy his favorite sports</em></figcaption>
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Being in remission with MG is very rare. But Victor, you were able to achieve remission. What is that like?</h3>
				<p><strong>Victor:</strong> After three sessions of the drug, I didn't have any serious symptoms. It has been a year and a half since my hospitalization. I still need to take one to two naps every day because of fatigue, but that's my only remaining symptom. Being 75 years of age and able to do nearly everything that I used to be able to do before I got MG is quite an accomplishment.</p>
				<p><strong>Iris:</strong> Yeah, by the time the permanent handicap permit arrived, he didn’t need it anymore!</p>
				<p><strong>Victor:</strong> It’s gathering dust in our car right now. I am very lucky; you don’t often hear about people with MG going into remission. And I know some people with MG who still have to go to the hospital multiple times in a year or have side effects from their medications. We have a lot of admiration for those with MG who still have to go to work.</p>
				<p><strong>Iris:</strong> So many people continue to have horrible symptoms and need to be hospitalized. It's really tough because all people want to do is carry on with their lives. Hopefully people see Victor and have hope. Remission is rare, but it is still possible.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>The muscles and the nerves, they're not talking to one another.</p>
					<cite><strong>Victor Yipp</strong></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>You’ve started an MG support group in Oak Park, outside of Chicago. What motivated you to start it?</h3>
				<p><strong>Victor:</strong> One of the reasons I volunteer and lead an MG support group is because I feel we're very fortunate. I want to give back to the MG community and give a helping hand to other people. When we were first going to support groups, we met a guy in remission. He gave us hope. Now that I’m in remission, I could be that guy for someone else.</p>
				<p>You don’t have to feel alone with this disease. I wanted to be able to reach out to a community that is underserved. Support groups let you share your experience with other people and have resources to turn to. Our group keeps up with the latest therapy and clinical trial developments.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Any parting advice for those struggling with MG?</h3>
				<p><strong>Victor:</strong> I feel very lucky and even a bit embarrassed because I can do a lot of things in remission. I can still go skiing; I just have some fatigue. MG is a mysterious, rare disease. It’s known as the snowflake disease because it affects every person with MG differently. The complications are hard to understand. MG takes a lot of patience. Transitioning back into a more regular lifestyle doesn’t happen overnight by any means. And through it all, it’s important to reach out. Not only to your doctors, but also to the MG community to share your experiences.</p>
				<p><strong>Iris:</strong> You have to realize it's still there. It's not really going away. We are lucky that the tiredness is the only remaining symptom Victor has. Hopefully, people see Victor in remission and think it’s possible. It's not easy, but it’s possible.</p>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-kathy-and-diane' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-tools-neuros-use' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
