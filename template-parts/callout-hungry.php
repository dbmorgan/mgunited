<section id="callout-slab" class="row">
	<div>
		<div>
			<div>
				<h2>Sign up for MG&nbsp;United!</h2>
			</div>
			<div>
					<p>
						Hungry for more? Sign up today and you’ll never miss another recipe.
					</p>
			</div>
			<div>
				<a role="button" href="<?php echo esc_url( home_url( '/sign-up' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
			</div>
		</div>
	</div>
</section>
