<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="home-callout-what-is-mg-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="mop-under">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-what-is-mg-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/what-is-mg' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/what-is-mg' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-what-is-mg-label" class="secondary">What Is Myasthenia Gravis, and Why Does It Happen?</h2>
			<p>Need a better grasp of how MG works? We cover the essentials.</p>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
