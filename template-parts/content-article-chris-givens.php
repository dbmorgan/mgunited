<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */
?>

<article id="chris-givens-article" <?php post_class(); ?>>
	<section id="hero">
		<div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>SEPTEMBER 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>
	<div id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>Chris Givens Dives into His Life with MG</h1>
			</div>
		</div>
	</div>

	<section class="row article-body">
		<div>
			<div class="content lead-in">
				<h2>Chris gets real about how MG has affected his life and his plans for the future in this interview.</h2>
			</div>
		</div>
	</section>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img class="author-callout sm" src="/wp-content/themes/mgunited/images/chris-givens/author-callout-mobile.jpg" alt="Author callout image">
					<img class="author-callout lg" src="/wp-content/themes/mgunited/images/chris-givens/author-callout-desktop.jpg" alt="Author callout image">
				</div>
				<div>
					<p><b><em>
								Before being diagnosed with myasthenia gravis four years ago, Chris Givens was always on the move. He served in the U.S. Air Force for nearly 13 years. The Florida native would go spearfishing and lobstering. He was an avid scuba diver, dirt biker and motorcyclist. He lived abroad for several years. Now he’s doing his best trying to adjust to a life with MG.
					</em></b></p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>What were your early symptoms of myasthenia gravis?</h3>
				<p>At first, it was just really weird. It's not like it hits you all at once. I started choking a lot on food. After about a month, the symptoms really kicked in hard, like, weekly. My whole body felt like jelly. Walking made my legs achy, like running a marathon. I went from 190 pounds down to 140 pounds because I couldn’t keep any food down. I thought, <em>What the heck is going on?</em> I never go to doctors, but finally I went to Veterans Affairs. They thought it was a gastrointestinal (GI) issue. I spent maybe eight months doing a GI workup, including esophagus testing. A throat doctor told me, ‘You’re fine. It’s all in your head. Go see a shrink.’ My GI doctor thought I had ALS* and sent me to a neurologist, who was right out of school. But it turned out she knew exactly what it was—myasthenia gravis.</p>
				<p>It was a blessing to know what my condition was. Because the hardest part is the unknown.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>The worst part is when you’re told, ‘It’s all in your head.’</p>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>And now? How are you doing?</h3>
				<p>While managing my condition, I gained weight, got rashes and lost bone density. I’ve aspirated or breathed in water. I can handle all that. The worst part is when you’re told, ‘It’s all in your head.’ You begin to think that it <em>is</em> in your head. But I’m not crazy. It’s not in my head. People with myasthenia gravis can look just fine, but it’s frustrating when people don’t believe you have the condition. Some of my family members think my MG is a joke. They don’t think I’m sick. A friend thought I was drunk because I had double vision and couldn’t walk straight.</p>
				<p>I can choose to listen to that—or not. I’ve even worked to change how I talk to myself. I’ve always been proud. If I’d get cut, I’d deal with it for a couple of minutes and then go back to work. I never went to a doctor. I was raised to think that men who go to doctors are weak. Now I look for support from my care team and grown daughters.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>How do you relieve stress or try to relax?</h3>
				<p>Gardening is my new hobby. It’s a step in the right direction. I just sit there, watch flowers, water them or do whatever. Gardening isn’t fast and exciting, but it lifts my spirits. It’s peaceful and not too strenuous.</p>
			</div>
		</div>
	</section>


	<section class="row article-body">
		<div>
			<div class="content">
			<h3>Have you looked for support from the myasthenia gravis community?</h3>

			<p>I joined a support group in St. Petersburg, Florida, which is about 40 miles south of my town, Hudson. It was actually kind of nice. I went on an MG walk two years ago. It was my first one. I talked with a couple of people on the walk. I met another veteran, and we talked very frankly about living with myasthenia gravis. That walk was really good for me, because my daughter went with me and we got to spend some quality time together. A whole care team from the University of South Florida and their patients were there. It helps to find support and talk to people.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>I’m going to be a grandpa for the first time. I want to hold my granddaughter.</p>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Are there MG life hacks or workarounds that you can share?</h3>
				<p>I have trouble eating and swallowing, so I’ll be careful not to talk 30 minutes before eating. I’ve found it can make chewing and swallowing easier. Otherwise, my jaw can get too tired. When I drive, I notice that my leg gets really tired holding down the pedal. I love cruise control for driving longer stretches. Another tip is to use an electric toothbrush. My arm gets tired holding up a regular toothbrush.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<figure>
					<img class="figure-img lg" src="<?php echo get_template_directory_uri(); ?>/images/chris-givens/inline-desktop.jpg" alt="">
					<img class="figure-img sm" src="<?php echo get_template_directory_uri(); ?>/images/chris-givens/inline-mobile.jpg" alt="">
				</figure>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>What are your personal goals?</h3>
				<p>I’m going to be a grandpa for the first time. I want to hold my granddaughter. My daughter tells me, ‘You’ve got to be around for her.’ I’m the only grandparent on her side. I also hope to continue taking care of my parents, who I’ve been living with in Florida. They’re in their 80s.</p>
				<p>I want to live as normal a life as possible. I used to be really busy, really active. I had to sell my motorcycle, my dirt bike and all my gear because of MG. I’d like to dirt bike with my brother again. Last year he competed in a 1,100-mile Ironman race in Baja, California. I was in the chase truck. It was pretty great because the weather was cold. It was perfect. I just looked at my fishing license and lobster permit. I’m thinking, <em>Do I renew?</em> I’m still holding on to my scuba equipment. I’m praying one day I’ll be able to scuba dive again. When you dive, like 80 to 90 feet below, there’s a lot of pressure on your lungs. I’m hoping to do this stuff again.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p class="disclaimer">
					<em>* amyotrophic lateral sclerosis</em>
				</p>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div class="content">
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-discussion-guide' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-zesty-chicken-fajitas' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
