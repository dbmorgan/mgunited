<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-roasted-sweet-potato-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-roasted-sweet-potato">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-roasted-sweet-potato-label" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/roasted-sweet-potato-and-ginger-soup/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/eating-and-mg/roasted-sweet-potato-and-ginger-soup/' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">EATING & MG</p>
			<h2 id="eop-callout-roasted-sweet-potato-label" class="secondary">Roasted Sweet Potato and Ginger Soup</h2>
			<span class="read-duration">4 MIN READ</span>
		</a>
	</div>
</div>
