<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="sign-up-page" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<h1>Never miss a thing!</h1>
				<p>Join MG&nbsp;United to get regularly updated news and advice that matters most to you.</p>
				<a role="button" href="#sign-up-form" class="capsule primary">SIGN UP NOW</a>
			</div>
		</div>
	</section>

	<section id="sign-up-form" class="form-module">
		<div>
			<div>
				<div>
					<div class="form-header"></div>
					<div id="form-container">
						<?php get_template_part( 'template-parts/form', 'signup' ); ?>
						<?php get_template_part( 'template-parts/form', 'survey' ); ?>
						<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
