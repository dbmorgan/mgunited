<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-mg-other-conditions-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-mg-other-conditions">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-mg-other-conditions-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/mg-and-other-conditions/' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/mg-and-other-conditions/' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">DISEASE & TREATMENT</p>
			<h2 id="eop-callout-mg-other-conditions-label" class="secondary">Living with MG and Other Conditions</h2>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
