<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="mymgsole-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<h1>Join this kicky community&nbsp;art project</h1>
				<div id="hero-video">
					<!-- <iframe src="https://player.vimeo.com/video/429698404" title="Sizze/Sunseeker Video" frameborder="0" allow="fullscreen" allowfullscreen></iframe> -->
					<iframe src="https://player.vimeo.com/video/422924155" width="640" height="357" frameborder="0" allow="autoplay, fullscreen" allowfullscreen title="MG United How To Get Started Video"></iframe>
				</div>
				<p>We’re apart now more than ever, and art can bring us together. Join us by sharing your story, expressed with shoe art.  Every &#x201c;Sole&#x201d; helps tell the larger story of the MG community and brings us closer together.</p>
			</div>
		</div>
	</section>

	<section class="row article-body steps-wrap">
		<div>
			<div class="content steps">
				<h3>JOIN THE SOLES</h3>
				<div class="row">
					<div class="col-4">
						<p class="step-icon">1</p>
						<p class="step-headline">Choose your canvas</p>
						<p>Use your own shoe. Print one of our templates or trace or draw your own.</p>
					</div>

					<div class="col-4">
						<p class="step-icon">2</p>
						<p class="step-headline">Craft your story</p>
						<p>The instructions include several inspirational Story Starters.</p>
					</div>

					<div class="col-4">
						<p class="step-icon">3</p>
						<p class="step-headline">Share</p>
						<p>Take a photo of your shoe art and share on your socials with #myMGsole.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-4"><p><a href="#sharestory" data-id="sharestory" class="capsule primary">GET STARTED</a></p></div>
				</div>
				<div>
				</div>
			</section>

			<section class="row article-body">
				<div>
					<div class="content gallery">
						<h2>See how other &#x201c;soles&#x201d;  in the MG community have shared their stories. Get inspired to share yours.</h2>

						<h4>My MG Sole Gallery</h4>

						<div class="gallery-list-items">
							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-1.jpg" alt="Mike and Jackson’s shoe art">
								<blockquote>
									&#x201c;Life with my son Jackson has been one of the most rewarding aspects of my MG journey.&#x201d;
									<cite>Mike U. <br>Morgantown, WV</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-2.jpg" alt="Alesheia’s shoe art">
								<blockquote>
									&#x201c;It’s great to use my creative energy to help try and bring more calm, less stress, more healing.&#x201d;
									<cite>Alesheia H. <br>Grand Rapids, MI</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-3.jpg" alt="Greg’s shoe art">
								<blockquote>
									&#x201c;Through sharing your story, you just never know who’s going to jump on your team with you.&#x201d;
									<cite>Greg H. <br>Boston, MA</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-4.jpg" alt="Ed the Artist’s shoe art">
								<blockquote>
									&#x201c;Art is definitely therapy to me. It’s soothing to sit down and create.&#x201d;
									<cite>@Edtheartist <br>My MG Sole Resident Artist</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-5.jpg" alt="Abby’s shoe art">
								<blockquote>
									&#x201c;I really like art. It’s something that helped soothe me when I like, am
									stressed out and it’s something that I love to do and I’ve always done.&#x201d;
									<cite>Abby N.<br>
									Littleton, MA</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-6.jpg" alt="Alecia’s shoe art">
								<blockquote>
									&#x201c;My life has changed drastically since being diagnosed with myasthenia
									gravis, but I’m learning to adapt.&#x201d;
									<cite>Alicia A.<br>New York, NY</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-7.jpg" alt="Lauren’s and Dave’s shoe art">
								<blockquote>
									&#x201c;We have just completed our MG Sole art project, decorating our shoes, and I
									definitely channeled my inner artistry and my mother would be very proud
									of me.&#x201d;
									<cite>Lauren J. and Dave J.<br>Mt. Pleasant, SC</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-8.jpg" alt="Niki’s shoe art">
								<blockquote>
									&#x201c;MG has been an uphill battle, but a pleasant one because I’ve made it that
									way.&#x201d;
									<cite>
										Niki G.<br>
										Greenleaf, WI
									</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-9.jpg" alt="Rachel’s shoe art">
								<blockquote>
									&#x201c;I was stoked. Dude, who doesn’t love playing with paint? It’s
									awesome.&#x201d;
									<cite>
										Rachel H.<br>
										Houston, TX
									</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-10.jpg" alt="Meredith’s shoe art">
								<blockquote>
									&#x201c;Myasthenia gravis requires you to think creatively and think outside the box. And you know, it’s honestly made me a stronger person for it.&#x201d;
									<cite>Meredith O.<br>St. Louis, MO</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-11.jpg" alt="Jessica’s shoe art">
								<blockquote>
									&#x201c;When I first heard about this project, I was really excited because any type of creative outlet I think is always good.&#x201d;
									<cite>
										Jessica S. <br>Tampa, FL
									</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-12.jpg" alt="DaDonna’s shoe art">
								<blockquote>
									&#x201c;The right shoe, on the other hand, will still be colorful and lovely, but the colors will be subtler pastels.&#x201d;
									<cite>
										DaDonna W.<br>
										St. Petersburg, FL
									</cite>
								</blockquote>
							</div>


							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-13.jpg" alt="Dawn’s shoe art">
								<blockquote>
									&#x201c;Thankfully, I have a very supportive spouse and family and friends.&#x201d;
									<cite>
										Dawn W.<br>
										Stone Mountain, GA
									</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-14.jpg" alt="Alexis’ family’s shoe art">
								<blockquote>
									&#x201c;I choose to find the good in each day.&#x201d;
									<cite>
										Alexis R.<br>
										Buford, GA
									</cite>
								</blockquote>
							</div>

							<div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/gallery-15.jpg" alt="Kelsey’s shoe art">
								<blockquote>
									&#x201c;When I heard about MG Sole, I thought this is perfect.&#x201d;
									<cite>
										Kelsey C.<br>
										Basehor, KS
									</cite>
								</blockquote>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="sharestory" class="row form-module">
				<div>
					<div class="content">
						<div id="form-container">
							<?php get_template_part( 'template-parts/form', 'signup' ); ?>
							<?php get_template_part( 'template-parts/form', 'survey' ); ?>
							<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
						</div>
					</div>
				</div>
			</section>

			<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

			<section id="eoc" class="container">
				<div class="row">
					<div class="col col-3">
						<?php get_template_part( 'template-parts/callout', 'eop-been-living-with-mg-for-some-time' ); ?>
					</div>
					<div class="col col-3">
						<?php get_template_part( 'template-parts/callout', 'eop-kait-masters' ); ?>
					</div>
					<div class="col col-3">
						<?php get_template_part( 'template-parts/callout', 'eop-zesty-chicken-fajitas' ); ?>
					</div>
				</div>
			</section>

			<?php get_template_part( 'template-parts/callout', 'social' ); ?>

		</article><!-- #post-<?php the_ID(); ?> -->
