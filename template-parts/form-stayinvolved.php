<?php

	// We'll use the API to retrieve static form data
	include_once get_template_directory() . "/api-tools.php";

	$staticFormData = get_static_form_data();
	if ($staticFormData == null)
		return;

	const ID_STAY_INVOLVED = 'sign-up-stay-involved';
	const ID_STORY = 'share-story';
	const ID_STORY_OPTIN = 'share-story-optin';

?>
<form id="form-step-stay-involved">
	<div class="form-header">
		<h2>Become Part of the Action</h2>
		<p>Join MG&nbsp;United to get exclusive access to this compelling documentary, including behind-the-scenes updates and exclusive screenings for your local MG support group.</p>
	</div>
	
	<fieldset aria-labelledby="stay-involved-field">
		<p id="stay-involved-field"><strong>How would you like to stay involved?</strong></p>

		<?php foreach( $staticFormData->surveyStayInvolvedOptions as $surveyStayInvolvedOption ) { ?>
			<div>
				<label class="checkbox">
					<input type="checkbox" name="<?php echo ID_STAY_INVOLVED; ?>" value="<?php echo $surveyStayInvolvedOption->id; ?>" />
					<?php echo $surveyStayInvolvedOption->value; ?>
				</label>
			</div>
		<?php } ?>

		<div class="error-message"></div>
	</fieldset>

	<div id="form-step-stay-involved-story">
		<fieldset aria-labelledby="stay-involved-story-field">
			<p id="stay-involved-story-field"><strong>Tell us your MG story and we'll get in touch if we use your story.</strong></p>

			<label>
				<textarea id="<?php echo ID_STORY; ?>" placeholder="" maxlength="<?php echo $staticFormData->maxLengths->story; ?>"></textarea>
			</label>
			<div class="error-message"></div>
		</fieldset>
	</div>

	<input type="submit" class="primary" value="JOIN US" />
</form>
