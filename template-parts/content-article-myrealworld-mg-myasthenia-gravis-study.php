<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>
<article id="myrealworld-mg-myasthenia-gravis-study-article" <?php post_class(); ?>>
	<section id="hero">
		<div>
			<div class="col col-sm-12 col-md-12 col-10 offset-1">
				<p><b>SYMPTOM TRACKING</b></p>
				<h1>See If You Can Help Researchers by Sharing Your Myasthenia Gravis Journey</h1>
			</div>
		</div>
	</section>
	<section id="article-hero-strip">
		<div>
			<div>
				<p>OCTOBER 2020 | <b>3 MIN READ</b></p>
				<?php get_template_part('template-parts/social', 'buttons'); ?>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h2>MyRealWorld&trade; MG is an app-based research study, based on real-world evidence, designed to help
					advance the scientific understanding
					of myasthenia gravis.</h2>
				<p>By entering details on the app on your <a
						href="https://www.mg-united.com/life-with-mg/smartphone-mg-management/" target="_self">smartphone</a>,
					like your general health, symptoms and daily activities, you may be able to contribute to this study. That’s why argenx partnered with MGFA and Vitaccess, a digital
					healthcare research company, to sponsor this new app-based study designed to help advance
					understanding of MG and the impact it has on someone's daily life.</p>
<!--				collecting data on-->
				<p>The study is the first app-based global research project focused on MG symptoms and
					their
					effects on individuals living with this condition. Through the MyRealWorld&#8482; MG app, we
					will gather data from people living with MG in nine different countries. Your personal information will
					not be revealed; it will be de-identified to protect your privacy.* Data from all participants will
					be combined and shared with the scientific community to inform the continuation of MG research. The
					aim of the study is to collect information from real patients living with MG and provide access to
					researchers so they may try to better understand the impact of <a
						href="https://www.mg-united.com/disease-and-treatment/what-is-mg/" target="_self">myasthenia
						gravis</a> on people's lives.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<img class="lg"
					 src="<?php echo get_template_directory_uri(); ?>/images/myrealworld-mg-myasthenia-gravis-study/02_PhoneTracking.png"
					 alt="MyRealWorld&trade; MG">
				<img class="md"
					 src="<?php echo get_template_directory_uri(); ?>/images/myrealworld-mg-myasthenia-gravis-study/02_PhoneTracking.png"
					 alt="MyRealWorld&trade; MG">
				<img class="sm"
					 src="<?php echo get_template_directory_uri(); ?>/images/myrealworld-mg-myasthenia-gravis-study/02_PhoneTracking_mobile.png"
					 alt="MyRealWorld&trade; MG">
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>See How You Might Help Drive MG Research Forward</h3>
				<p>Now available internationally on both Android and iOS devices, the MyRealWorld&trade; MG app-based
					study is open to an adult diagnosed with myasthenia gravis. If you choose to participate, you will be
					able to set up a medical profile in the app where you can record information about your symptoms and
					treatment. The app-based study also includes questionnaires and surveys about additional diagnoses,
					symptoms, and other activities of your daily life.</p>
				<p>Over a two-year period, the app-based study aims to capture more complete data on the effects of
					myasthenia gravis. All information is gathered, analyzed and de-identified to protect your privacy
					by Vitaccess, which specializes in creating real-world evidence platforms for rare disease studies.
					The research findings will be shared with study sponsors argenx and MGFA, as well as other patient
					organizations across nine countries.</p>
				<br><br>
				<blockquote class="article-pullquote author">
					<p>Let’s help researchers understand more about how MG affects us.</p>
					<cite><strong>Nancy Law, patient living with MG</strong><br>
				</blockquote>
				<p>“Our ability to collect large amounts of firsthand data over a relatively short period of time could
					enable researchers and others in the
					medical community to gain a broader understanding of the illness,” said Srikanth Muppidi, MD,
					Clinical Associate Professor, Neurology
					and Neurological Sciences, Stanford Hospital and Clinics, Palo Alto, California. “The hope is that this
					knowledge could possibly lead to better management of MG for all patients.”</p>
				<p>Since you will need to input your symptoms into the MyRealWorld&#8482; MG app, you may find it a
					helpful way to track and monitor your <a
						href="https://www.mg-united.com/disease-and-treatment/tools-neuros-use/" target="_self">symptoms</a>.
					The MyRealWorld&trade; MG app may make it easier for you to support the larger myasthenia gravis
					community
					while doing something that may already be part of your routine. By participating, you may be able to
					help raise awareness of the illness and help bridge the communication gap that may exist between
					people living with MG and health care providers.</p>
				<p>“Every person I know who’s living with MG is having a different experience with it,” said Nancy Law,
					a person living with MG. “Each patient’s symptoms are unique, and I think a study-based app, like
					MyRealWorld&#8482;, is great. I’d be glad to share my story. We may be able to help the researchers
					understand a lot more about how MG affects us.” </p>
				<br>
				<p><i>*Discover more about how your personal identity is protected in this study <a
							href="https://myrealworld.com/uk/en/myasthenia-gravis/privacy/" target="_blank">here</a>.</i>
				</p>
			</div>
		</div>
	</section>
	<section id="share-story-promo">
		<div class="row see-if-eligible">
			<div class="col col-sm-6 col-6 offset-1">
				<h2>SEE IF YOU ARE ELIGIBLE TO TAKE PART IN MG RESEARCH</h2>
				<p class="share-story-promo-copy">If you are interested in participating in the study, download the app
					in the Apple Store or Google Play.</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-sm-6 col-6 offset-1">
				<div id="share-story-promo-button">
					<span class="flex-spacer"></span>
					<div>
						<div class="row">
							<div class="col col-sm-3 col-md-6 col-6">
								<div id="home-apple-app-promo-button">
									<span class="flex-spacer"></span>
									<div>
										<a role="button" href="https://apps.apple.com/us/app/myrealworld/id1482374347"
										   target="_blank" class="img-button"><img
												src="<?php echo get_template_directory_uri(); ?>/images/myrealworld-mg-myasthenia-gravis-study/apple.svg"></a>
									</div>
									<span class="flex-spacer"></span>
								</div>
							</div>
							<div class="col col-sm-3 col-md-6 col-6">
								<div id="home-android-app-promo-button">
									<span class="flex-spacer"></span>
									<div>
										<a role="button"
										   href="https://play.google.com/store/apps/details?id=com.mg.vitaccess"
										   target="_blank" class="img-button"><img
												src="<?php echo get_template_directory_uri(); ?>/images/myrealworld-mg-myasthenia-gravis-study/google-play-badge.svg"></a>
									</div>
									<span class="flex-spacer"></span>
								</div>
							</div>
						</div>
					</div>
					<span class="flex-spacer"></span>
				</div>
			</div>
		</div>
		<div class="row promo-img offset-7 md lg"></div>
	</section>
	<?php get_template_part('template-parts/callout', 'slab'); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3 col-sm-3">
				<?php get_template_part('template-parts/callout', 'eop-morgan-greene'); ?>
			</div>
			<div class="col col-3 col-sm-3">
				<?php get_template_part('template-parts/callout', 'eop-been-living-with-mg-for-some-time'); ?>
			</div>
			<div class="col col-3 col-sm-3">
				<?php get_template_part('template-parts/callout', 'eop-anti-inflammatory-diet'); ?>
			</div>
		</div>
	</section>
	<?php get_template_part('template-parts/callout', 'social'); ?>
</article><!-- #post-<?php the_ID(); ?> -->
