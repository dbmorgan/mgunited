<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="mission-page" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-6 col-10 offset-1">
				<h1>
					MG affects more than just <span class="nowrap">your muscles.</span><br />
					That&#x2019;s why we&#x2019;re here.
				</h1>
				<p>
					Wherever you are in your myasthenia gravis journey, MG United wants to help make today a better day.
				</p>
			</div>
		</div>
	</section>

	<section id="welcome">
		<div>
			<div class="col col-sm-6 col-10 offset-1">
				<div class="manifesto-video">
					<iframe src="https://player.vimeo.com/video/421148855" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>

	<section id="illo-cards">
		<div>
			<div class="row">
				<div class="col col-sm-6 col-8 offset-2">
					<h2>
						What We’re About
					</h2>
					<p>
						MG United is dedicated to providing you with personalized, carefully selected resources that try to address the unique ways MG can affect your life—not just your body. Resources include:
					</p>
				</div>

			</div>

			<div class="illo-cards">
				<div class="row">
					<div class="col col-sm-6 col-4 illo-container">
						<div class="illo">
							<img alt="illustration" src="<?php echo get_template_directory_uri(); ?>/images/about/illo-woman-book.png"/>
						</div>
						<div class="illo-text">
							<p>
								Life hacks and articles designed to help you live your best life, every day.
							</p>
						</div>
					</div>
					<div class="col col-4 illo-container">
						<div class="illo">
							<img alt="illustration" src="<?php echo get_template_directory_uri(); ?>/images/about/illo-woman-beanbag.png"/>
						</div>
						<div class="illo-text">
							<p>
								Real stories of struggle and success that you can learn from and relate to.
							</p>
						</div>
					</div>
					<div class="col col-4 illo-container">
						<div class="illo">
							<img alt="illustration" src="<?php echo get_template_directory_uri(); ?>/images/about/illo-group-selfie.png"/>
						</div>
						<div class="illo-text">
							<p>
								Exclusive access to an upcoming documentary about MG.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="mission-signup-promo" class="violator">
		<div class="row">
			<div class="col col-sm-6 col-6 offset-3">
				<h2>SIGN UP FOR MG&nbsp;UNITED!</h2>
				<p class="mission-signup-promo-copy">Get personalized content and support for wherever you are in your journey with MG. Just enter your email and you’re in.</p>
				<div class="row">
					<div class="col col-sm-6 col-12">
						<div id="mission-signup-button">
							<span class="flex-spacer"></span>
							<div>
								<a role="button" href="<?php echo esc_url( home_url( '/sign-up/' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
							</div>
							<span class="flex-spacer"></span>
						</div>
					</div>
				</div>
	</section>

	<section id="advocacy-groups" class="container">
		<div>
			<div class="row">
				<div class="col col-sm-6 col-10 offset-1">
					<h2>We&#x2019;re in this together</h2>
					<p>MG&nbsp;United is proud to join a community of outstanding MG advocacy groups, all working to improve the quality of life for people living with MG.</p>
				</div>
			</div>
			<div class="row">
				<a href="https://www.myastheniagravis.org" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/about/logo-conquer-mg.png" alt="Conquer MG" width="219" /></a>
				<a href="https://www.mda.org" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/about/logo-mda.png" alt="Muscular Dystrophy Association" width="226" /></a>
				<a href="http://www.mgakc.org" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/about/logo-mga.png" alt="Myasthenia Gravis Association" width="185" /></a>
				<a href="https://myasthenia.org" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/about/logo-mgfa.png" alt="Myasthenia Gravis Foundation of America" width="165" /></a>
				<a href="https://mg-mi.org" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/about/mg-mi-logo.png" alt="MG-MI" width="165" /></a>
			</div>
		</div>
	</section>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-what-is-mg' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-myrealworld-mg' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
