<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-summer-heat-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-beat-summer">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-summer-heat-label" class="content-tile" href="<?php echo esc_url( home_url( '/life-with-mg/summer-heat-tips' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/life-with-mg/summer-heat-tips' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">LIFE WITH MG</p>
			<h2 id="eop-callout-summer-heat-label" class="secondary">14 Ways to Outsmart Summer</h2>
			<span class="read-duration">4 MIN READ </span>
		</a>
	</div>
</div>
