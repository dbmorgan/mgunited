<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="about-mg" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-6 col-10 offset-1">
				<h1>
					Myasthenia gravis can be complicated.<br/>We try to keep it simple.
				</h1>
				<p>
					No matter where you are in your MG journey, we’re here to provide straightforward, reliable information about myasthenia gravis.
				</p>
			</div>
		</div>
	</section>

	<section id="about-mg-callouts" class="content page-callouts">
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/what-is-mg' ); ?>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/doctor-tools-reverse' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/mg-other-conditions' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/living-with-mg-for-some-time' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/newly-diagnosed' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/treatment-goals' ); ?>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/crisis-planning' ); ?>
			</div>
		</div>
	</section>

	<section id="about-mg-signup-promo" class="violator">
		<div class="row">
			<div class="col col-sm-6 col-6 offset-3">
				<h2>SIGN UP FOR MG&nbsp;UNITED!</h2>
				<p class="about-mg-signup-promo-copy">Get personalized content and support for wherever you are in your journey with MG. Just enter your email and you’re in.</p>
		<div class="row">
			<div class="col col-sm-6 col-12">
				<div id="about-mg-signup-button">
					<span class="flex-spacer"></span>
					<div>
						<a role="button" href="<?php echo esc_url( home_url( '/sign-up/' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
					</div>
					<span class="flex-spacer"></span>
				</div>
			</div>
		</div>
	</section>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-discussion-guide' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-invisible-burden' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
