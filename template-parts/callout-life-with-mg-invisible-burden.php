<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="life-with-mg-callout-burden-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="life-with-mg-invisible-burden">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="life-with-mg-callout-burden-label" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/the-invisible-burden' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/emotional-support/the-invisible-burden' ) ); ?>" style="text-decoration:none">
			<h2 id="life-with-mg-callout-burden-label" class="secondary">16 Ways Caregivers Can Care for Themselves</h2>
			<p>
				Self-care is easy to overlook. We are sharing some simple things that may help you care for your mind, body and soul.
			</p>
			<span class="read-duration">10 MIN READ</span>
		</a>
	</div>
</div>
