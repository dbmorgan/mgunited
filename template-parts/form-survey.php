<?php

	// We'll use the API to retrieve static form data
	include_once get_template_directory() . "/api-tools.php";

	$staticFormData = get_static_form_data();
	if ($staticFormData == null)
		return;

	const ID_SIGN_UP_TYPE = 'sign-up-type';
	const ID_TOPICS_OF_INTEREST = 'sign-up-topics-of-interest';
	const ID_OTHER_TOPICS_OF_INTEREST = 'sign-up-other-topics';
	const ID_GENDER = 'sign-up-gender';
	const ID_GENDER_OTHER = 'sign-up-gender-other';

	$slug = $wp_query->queried_object->post_name
?>
<form id="form-step-survey">

	<h2>Thank you, <span class="sign-up-first-name"></span>!</h2>
	<?php if ($slug == 'my-mg-sole-myasthenia-gravis-art-project') { ?>

		<h2 class="secondary">Thanks for signing up. Here’s the kit you asked for. </h2>
		<p><a href="<?php echo get_template_directory_uri(); ?>/images/mymgsole-gallery/MyMGSole_Instructions.pdf" class="no-interstitial" target="_blank">DOWNLOAD NOW</a></p>

	<?php } ?>

	<h2 class="secondary">If you have a minute, tell us more about yourself.</h2>

	<fieldset aria-labelledby="survey-category-field">
		<p id="survey-category-field">
			<strong>Which category best describes you?</strong>
		</p>

		<?php foreach( $staticFormData->profileTypes as $profileType ) { ?>
			<div>
				<label class="radio">
					<input type="radio" name="<?php echo ID_SIGN_UP_TYPE; ?>" value="<?php echo $profileType->id; ?>" />
					<?php echo $profileType->value; ?>
				</label>
			</div>
		<?php } ?>

		<div class="error-message"></div>
	</fieldset>

	<fieldset aria-labelledby="survey-gender-field">
		<p id="survey-gender-field">
			<strong>Gender</strong>
		</p>

		<?php foreach( $staticFormData->genders as $gender ) { ?>
			<div>
				<label class="radio">
					<input type="radio" name="<?php echo ID_GENDER; ?>" value="<?php echo $gender->id; ?>" />
					<?php echo $gender->value; ?>
				</label>
			</div>
		<?php } ?>

		<div>
			<input type="radio" id="<?php echo ID_GENDER_OTHER; ?>-radio" name="<?php echo ID_GENDER; ?>" value="" />
			<input type="text" id="<?php echo ID_GENDER_OTHER; ?>" maxlength="<?php echo $staticFormData->maxLengths->gender; ?>" placeholder="Self-identify" />
		</div>

		<div class="error-message"></div>
	</fieldset>


	<fieldset aria-labelledby="survey-content-field">
		<p id="survey-content-field"><strong>What type of content are you most interested in seeing on MG United?</strong></p>
		<?php foreach( $staticFormData->surveyTopicsOfInterest as $surveyTopicOfInterest ) { ?>
			<div>
				<label class="checkbox">
					<input type="checkbox" name="<?php echo ID_TOPICS_OF_INTEREST; ?>" value="<?php echo $surveyTopicOfInterest->id; ?>" />
					<?php echo $surveyTopicOfInterest->value; ?>
				</label>
			</div>
		<?php } ?>
	</fieldset>

	<?php
		// "Other topics of interest" was removed as part of the October 2020 updates

		/*
		<fieldset aria-labelledby="survey-topic-field">
			<div aria-labelledby="survey-topic-field">
				<p id="survey-topic-field"><strong>Are there other topics you’d be interested in?</strong></p>
				<textarea aria-labelledby="survey-topic-field" id="<?php echo ID_OTHER_TOPICS_OF_INTEREST; ?>" maxlength="<?php echo $staticFormData->maxLengths->otherTopicsOfInterest; ?>"></textarea>

				<div class="error-message"></div>
			</div>
		</fieldset>
		*/
	?>

	<input id="sign-up-step2-next" type="submit" class="primary" value="SUBMIT" />

	<p class="disclaimer">Filling out this information is optional and not required to receive MG United updates. You have already signed up for MG United.</p>
</form>
