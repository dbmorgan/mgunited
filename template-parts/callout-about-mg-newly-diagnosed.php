<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="about-mg-callout-newly-diagnosed-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="about-mg-newly-diagnosed">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="about-mg-callout-newly-diagnosed-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/newly-diagnosed' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/newly-diagnosed' ) ); ?>" style="text-decoration:none">
			<h2 id="about-mg-callout-newly-diagnosed-label" class="secondary">Tips to Help You Cope When You’re Newly Diagnosed</h2>
			<p>Getting diagnosed is a first step. Learn what you can do to help prepare for the future.</p>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
