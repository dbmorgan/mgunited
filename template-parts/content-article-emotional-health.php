<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="emotional-health-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div>
				<p><b>EMOTIONAL SUPPORT</b></p>
				<h1>How to Have Good Emotional Health and MG at the Same Time</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JUNE 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					From family to support groups to online counseling, you have many options.
				</h2>
				<p>Whether you have myasthenia gravis or love someone who does, you know how easy it is to let the physical side of MG take all your time and energy. But, like it or not, your emotional health also needs attention. As anyone in the community can tell you, MG takes a toll on your feelings. Fortunately, there is help available through self-care, support groups and counseling—online and in the real world. Here's what you need to know: </p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Depression and MG </h3>
				<p>Many people who live with MG share a similar story about the struggle to get diagnosed. When the standard tests come back normal, they are incorrectly told they have depression and are referred to psychiatrists. Obviously, this is frustrating for someone dealing with an all-too-real chronic disease. But it’s also a little ironic. Because if you didn’t have depression beforehand, dealing with MG every day can be enough to bring it on. In fact, a recent study showed that people living with MG are almost twice as likely to develop depressive disorders.<sup>1</sup></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Grappling with Guilt</h3>
				<p>There’s no getting around it: for people living with MG, guilt is a thing. They feel guilty about not being able to make plans or do things with family and friends. Many also point to guilt about the burden their MG might place on spouses and significant others. </p>
				<p>And loved ones get a double helping. They can feel guilty for not being the perfect supporters. And they can also feel bad about negative feelings they may harbor about how MG in the family affects them personally.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>The Downsides of Being ‘Strong and Silent’</h3>
				<p>Some people deal with their feelings about MG with silence. They pretend they don’t have any feelings about MG at all. It’s a very human instinct, but not a good long-term strategy. It can lead to bouts of moodiness, irritability, isolation, and depression. Studies have linked hidden emotion to serious health issues like heart disease.<sup>2</sup></p>
				<p>One way or another, your feelings will come out. The key is to allow them to emerge in ways that are healing and helpful to you and your loved ones. </p>
			<div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/emotional-health/emotional-health-illo.jpg" alt="" /></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Accept Your Feelings, Then Accept Help</h3>
				<p>It’s important to embrace a simple truth: All the emotions that this life-changing condition stir up are okay. You are allowed to have them. You certainly aren’t alone in feeling them.</p>
				<p>Just like the physical symptoms of MG, these emotions are not something you should deal with on your own. There are lots of different kinds of help to improve your emotional health. </p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Let the Therapy Come to You</h3>

				<p>If possible, consider meeting with a licensed therapist. Ideally, you want someone with experience treating people with chronic conditions. But if cost or mobility rule out office visits, there are several forms of virtual therapy that are budget friendly and can put you face-to-face with a licensed therapist in your own home. Here are just a few to consider:</p>

				<ul>
					<li><p><a href="https://www.betterhelp.com/" target="_blank">betterhelp</a>: offers affordable, private counseling sessions for $40-70/week</p></li>
					<li><p><a href="https://www.talkspace.com/" target="_blank">talkspace:</a> matches you with a therapist, with pricing starting at $65/week (which can be paid with certain EAP plans)</p></li>
					<li><p><a href="https://business.amwell.com/service-lines/telepsychiatry/" target="_blank">amwell</a>: offers numerous medical services, including telepsychiatry</p></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Let the MG Community Help</h3>

				<p>One of the best ways to deal with your feelings is to share them with people who have walked a mile in your shoes. People living with MG meet in support groups across the country, both in person and online. These groups give you a chance to connect with others, as well as helpful resources and learning. This list of advocacy groups is a place to start; chances are there’s one near you:</p>
				<ul>
					<li><p><a href="https://www.myastheniagravis.org/" target="_blank">Conquer MG</a> (Illinois, SE Wisconsin and NW Indiana)</p></li>
					<li><p><a href="https://interland3.donorperfect.net/weblink/WebLink.aspx?name=E248489&id=24" target="_blank">MG Friends</a>: A peer-to-peer support group which links people living with MG together by phone</p></li>
					<li><p><a href="http://www.mgakc.org/" target="_blank">Myasthenia Gravis Association</a>: An organization that focuses on "supporting patients with myasthenia gravis in the heartland one day at a time."</p></li>
					<li><p><a href="https://myasthenia.org/" target="_blank">Myasthenia Gravis Foundation of America</a>: Provides numerous resources, including this <a href="https://myasthenia.org/Community-Resources/Support-Groups-MG-Friends" target="_blank">support group finder</a> to help you find one in your state</p></li>
					<li><p><a href="https://mg-mi.org/" target="_blank">Mg-Mi</a> (Michigan)</p></li>
					<li><p><a href="https://www.mgminnesota.com/" target="_blank">mgMN</a> (Minnesota)</p></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/emotional-health/emotional-health-couple.jpg" alt="" /></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Lean into Social Media</h3>
				<p>There are lots of MG support groups on all of the major social media platforms. You can find well-known people in the community sharing great advice, information, and encouragement on Facebook, Instagram, Twitter, and more. These platforms are all free—and there for you, day or night. To get started, just pick a platform and search phrases like “myasthenia gravis,” “MG awareness,” or “MG warrior.” You can also find MG&nbsp;United on Facebook and Instagram.</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Take Your Emotional Health Seriously</h3>

				<p>Just like nutrition or sleep, emotional health is important for everyone, but crucial for people who live with MG—whether you have it or support someone who does. Get to know your feelings—both good and bad. Embrace all the help that’s there for you. Tending to your emotional health is something you can do to live your best life with MG. </p>
			</div>

		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Chu H, et al. <em>Front Psychiatry</em>. 2019;10(481):1.</li>
					<li>Chapman BP, et al. <em>J Psychosom Res</em>. 2013;75(4):381-385.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-crisis' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

	</article><!-- #post-<?php the_ID(); ?> -->
