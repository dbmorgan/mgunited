<?php
	$check = is_404();
	if ($check == 1) $slug = '';
	else $slug = $wp_query->queried_object->post_name
	//$slug = $wp_query->queried_object->post_name
?>

<div aria-label="MG United Site Violator" id="violator" class="site-violator open<?php if ($slug=='a-mystery-to-me') {?> documentary<?php }?>">
	<div class="outer">
		<div id="violator-header">

			<?php if ($slug == 'a-mystery-to-me') { ?>
				<h1>Ready To Watch?</h1>
			<?php } else { ?>
				<h1>NEVER MISS A THING!</h1>
			<?php }; ?>
		</div>
		<div id="violator-toggle" class="violator-open"> </div>
		<div id="violator-body" class="open">
			<?php if ($slug == 'a-mystery-to-me') { ?>
				<p>The Nov. 17<sup>th</sup> premiere of <em>A Mystery to Me</em> is available only when you sign up for MG United. Signing up is always free.</p>
			<?php } else { ?>
				<p>By signing up for MG United, you'll have access to the latest tips from our community on living a better life with MG.</p>
			<?php }; ?>
				<a role="button" href="#" class="capsule scroll-to-button">SIGN UP NOW</a>
		</div>
	</div>

</div>

