<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-morgan-greene-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-morgan-greene">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-morgan-greene-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/morgan-greene' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">REAL STORIES</p>
			<h2 id="eop-callout-morgan-greene-label" class="secondary">The Four Words That Changed My Life*</h2>
			<span class="read-duration">6 MIN READ</span>
		</a>
	</div>
</div>
