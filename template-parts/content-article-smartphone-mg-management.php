<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="smartphone-mg-management-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>LIFE WITH MG</b></p>
				<h1>Managing MG: There’s an App for That</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>SEPTEMBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					Trying to manage myasthenia gravis is already a task in itself. One way that may help you stay organized could be used right from the palm of your hand.
				</h2>
				<p>
					When you’re living with myasthenia gravis, sometimes it feels like you need a personal assistant to keep track of everything. From doctor’s visits to treatments to symptom monitoring, things can get complicated—fast. But you’ve got a powerful tool within reach that may be able to help you better manage life with MG: your smartphone.
				</p>
				<p>
					Even if you’re not the most tech-savvy person in the world, there are apps available that are designed to make it easier to record and share information with your care team and family. Try one of these apps to see if it helps you track your myasthenia gravis symptoms more easily.*
				</p>
				</div>
				</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<div class="content-header">
					<b>Apps You May Have Already<sup>&#x2020;</sup></b>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/voicememo.png"></div>
				<div class="content-col">
					<h3>Voice Memo</h3>
					<p>
						Weakness in the hands is common for many people living with MG. If you’re not able to type or write, just call out to your phone’s virtual assistant and have them launch the voice memo app. Apps like this can be used to make a quick, pain-free note to yourself or help record doctor’s visits so that you and your loved ones don’t forget any important details. Just make sure you get your care provider’s permission before recording.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/calendar.png"></div>
				<div class="content-col">
					<h3>Calendar</h3>
					<p>
						Living with MG often means living with a busy schedule, and it’s easy to lose track of everything. Consider using a calendar app to save information about your appointments and treatments. And you can also set alerts for each event. Sometimes, that small “ding” is all you need to be reminded.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/notes.png"></div>
				<div class="content-col">
					<h3>Notes</h3>
					<p>
						The notes app can be used for keeping a diary about living with myasthenia gravis or jotting down quick, digital reminders. You can share your notes over text or email with your family so that they’re always aware of how you’re feeling. You can also loop in your care team if they’re open to it.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<div class="content-header">
					<b>Apps You May Want to Download<sup>&#x2020;</sup></b>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/medisafe.png"></div>
				<div class="content-col">
					<h3>Medisafe</h3>
					<p>
						While technology shouldn’t replace your care team or pharmacist, there are apps out there that can help you keep track of your medicines, which can be tough, especially if you are keeping track of multiple medications. Medisafe is designed to let you record your medications, set the dosing schedule and add reminders. Once you’ve entered your medications, its database can even alert you to potential adverse drug reactions. Pretty neat, but you may want to discuss features of this app with your care team first.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/mymed.png"></div>
				<div class="content-col">
					<h3>My Medical Info</h3>
					<p>
						Forms, forms, forms! Myasthenia gravis has probably brought many forms into your life that have sent you scrambling to find some small detail about your insurance coverage or health history. The My Medical Info app was designed to help with that. This app lets you save all that information in one organized, accessible location.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/fooducate.png"></div>
				<div class="content-col">
					<h3>Fooducate</h3>
					<p>
						Eating balanced, nutritious meals and maintaining a healthy weight is a good goal for everyone, including people with myasthenia gravis. The Fooducate app can help you set the number of calories you’re aiming for each day and input the food you eat. It can also assist with the caloric and macronutrient math. Easy peasy.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/calm.png"></div>
				<div class="content-col">
					<h3>Calm</h3>
					<p>
						Managing stress is important. Calm is one of the most popular apps for meditation and relaxation, with more than 50 million downloads. With a range of guided meditations, master classes and soothing sleep stories, Calm is designed to help bring a little peace when things may be not so calm.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/sleep.png"></div>
				<div class="content-col">
					<h3>Sleep Cycle Smart Alarm Clock</h3>
					<p>
						For people living with MG, symptoms and side effects can get in the way of a good night’s sleep. The Sleep Cycle Smart Alarm Clock tracks your sleeping habits and wakes you up only during light sleep. The goal is to help you learn more about your sleep patterns—and feel more rested.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content wrapper">
				<div class="img-col image"><img style="width: auto; height: auto" alt="" src="<?php echo get_template_directory_uri(); ?>/images/smartphone-mg-management/myrealMG.png"></div>
				<div class="content-col">
					<h3>MyRealWorld&#x2122; MG</h3>
					<p>
						While other apps may help you handle living with MG, the MyRealWorld&#x2122; MG app is a free tool used to help conduct a virtual MG study recording diagnosis, symptoms and quality of life for patients with myasthenia gravis. It's important to know that all the information you input is shared confidentially through an international medical study, which was designed to help researchers learn more about this mysterious and misunderstood condition. This study is sponsored by argenx.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p class="disclaimer">
					*<em>These mobile applications are being provided for informational purposes only. This article does not constitute as an endorsement or approval by argenx of these products. The mobile applications should not be a substitute for professional medical advice. Always seek the advice of your physician.</em>
				</p>
				<p class="disclaimer">
					<sup>&#x2020;</sup><em>Fees may apply.</em>
				</p>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-crisis' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
