<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="life-with-mg" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-6 col-10 offset-1">
				<h1>
					Your daily life with myasthenia gravis
				</h1>
				<p>
					MG United explores the many ways you can try to improve the quality of your daily life with MG.
				</p>
			</div>
		</div>
	</section>

	<section id="life-with-mg-callouts" class="content page-callouts">
		<div class="row page-callout-row horizontal-tablet-callout-row display-eyebrow">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/myrealworld-mg' ); ?>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row display-eyebrow">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/roasted-sweet-potato-ginger-soup-reverse' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/discussion-guide-children' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/discussion-guide' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-fajitas' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-casserole' ); ?>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/mg-app' ); ?>
			</div>
		</div>
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/invisible-burden-reverse' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/anti-inflammatory-diet' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/emotional-health' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/cookbook-smoothies' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/summer-heat' ); ?>
			</div>
		</div>

	</section>

	<section id="life-with-mg-signup-promo" class="violator">
		<div class="row">
			<div class="col col-sm-6 col-6 offset-3">
				<h2>SIGN UP FOR MG&nbsp;UNITED!</h2>
				<p class="life-with-mg-signup-promo-copy">Get personalized content and support for wherever you are in your journey with MG. Just enter your email and you’re in.</p>
				<div class="row">
					<div class="col col-sm-6 col-12">
						<div id="life-with-mg-signup-button">
							<span class="flex-spacer"></span>
							<div>
								<a role="button" href="<?php echo esc_url( home_url( '/sign-up/' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
							</div>
							<span class="flex-spacer"></span>
						</div>
					</div>
				</div>
	</section>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-myrealworld' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-newly-diagnosed' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

<!--	<section id="disclaimer" class="container">-->
<!--	<div class="row">-->
<!--		<div class="col">-->
<!--			<p>*Paid contributor to MG&nbsp;United</p>-->
<!--		</div>-->
<!--	</div>-->
<!--	</section>-->

</article><!-- #post-<?php the_ID(); ?> -->
