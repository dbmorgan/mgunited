<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="documentary" <?php post_class(); ?>>

	<section id="hero">
		<video id="hero-video" autoplay muted disablePictureInPicture loop playsinline preload="auto">
		<!-- <video muted disablePictureInPicture preload="auto"> -->
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_FULLCAST_30cutdownWOverlay_00e.mp4" type="video/mp4">
			<source src="<?php echo get_template_directory_uri(); ?>/videos/ArgMGdoc_WebClips_FULLCAST_30cutdownWOverlay_00e.webm" type="video/webm">
		</video>
		<div class="container">
				<div class="row">
					<div class="col col-sm-10 col-md-10 col-8">
						<p class="eyebrow">One relentless illness. Three&nbsp;unstoppable&nbsp;people.</p>
						<h1>A <span>Mystery</span> to&nbsp;Me</h1>
						<p class="eyebrow">A Myasthenia Gravis Docuseries</p>
						<a class="capsule" role="button" id="trailer-button" href="#">WATCH THE TRAILER</a>

				</div>
			</div>
		</div>

		<div id="ciff-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/documentary/CIFF_OfficialSelection2020_Logo_WhiteRGB.svg" alt="The Chicago International Film Festival 2020 Official Selection"></div>
	</section>

	<section id="intro">
		<div>
			<div class="container">
				<div class="row">
					<div class="col col-10 offset-1">
						<h3>The First-Ever MG Documentary Film Series</h3>

						<p><em>A Mystery to Me</em> is the first documentary series about myasthenia gravis to reveal the hidden toll this unpredictable, unfair illness takes on three people who live with it every day.</p>
						
						<a href="#" class="underline">KEEP ME UPDATED</a>
						<br class="sm">
						<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col col-12">
						<div class="container">
							<div class="row">
								<div class="col col-5">
									<p>Directed by<br>
									<strong>BEN STRANG</strong></p>
									<p>Produced by<br>
									<strong>SAROFSKY PICTURES</strong></p>
									<p>In Association with<br>
									<strong>KARTEMQUIN FILMS</strong></p>
									<p>and<br>
										<strong>MUSEUM + CRANE</strong></p>
								</div>
								<div class="col col-7">
									<h4>Join us for the virtual premiere on November&nbsp;17<sup>th</sup></h4>
									<br>
									<p><a href="#" class="capsule tertiary" id="rsvp-modal">RSVP Today!</a> <br>
									</p>
									<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="docu-cards">
		<div>
			<div class="container">
				<div class="row">
					<div class="col col-12">

						<h2>Three Films, <br>
						Three MG Stories</h2>

						<div class="container patient-video-card vanetta">
							<div class="row">
								<div class="col col-6">
									<h4>Vanetta</h4>

									<p>“I may be weak, but I’m strong.” Vanetta Drummer-Fenton started using the phrase as a code among her close-knit family to let them know how her symptoms were each day. But now, it’s become a personal mantra to help her deal with MG.</p>

									<p class="hilite">Vanetta’s film from <em>A Mystery to Me</em> has been accepted into the 56<sup>th</sup> Chicago International Film Festival.</p>

									<a href="/a-mystery-to-me/vanetta-drummer-fenton" class="capsule secondary">MEET VANETTA</a>

								</div>
							</div>
							<div class="ciff-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/documentary/CIFF_OfficialSelection2020_Logo_WhiteRGB.svg" alt="The Chicago International Film Festival 2020 Official Selection"></div>
						</div>

						<div class="container patient-video-card teresa">
							<div class="row">
								<div class="col col-6 offset-6">
									<h4>Teresa</h4>

									<p>Teresa Hill-Putnam turned her talents for dance into a career as an instructor at her own studio. But MG threatened to take it all away. Learn how Teresa managed to keep her family and business going, all while dealing with the unpredictable challenges of MG. </p>

									<a href="/a-mystery-to-me/teresa-hill-putnam" class="capsule secondary">MEET TERESA</a>
								</div>
							</div>
						</div>
						
						<div class="container patient-video-card glenn">
							<div class="row">
								<div class="col col-6">
									<h4>Glenn</h4>

									<p>A career firefighter, Glenn Phillips has lived a life of dauntless service and courage under fire. Now he’s taking on his biggest challenge yet: life with MG. See how a man used to working at the center of the action copes with a sidelining illness.</p>

									<a href="/a-mystery-to-me/glenn-phillips" class="capsule secondary">MEET GLENN</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

	<section id="action" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

</article><!-- #post-<?php the_ID(); ?> -->
