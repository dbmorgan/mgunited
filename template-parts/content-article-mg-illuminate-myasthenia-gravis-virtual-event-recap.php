<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="june-recap-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>LIFE WITH MG</b></p>
				<h1>MG Illuminate Shines a National Spotlight on MG Awareness</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>3 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>
					This virtual event kicked off MG Awareness Month with an inspiring night of sessions, surprises and coast-to-coast landmark lightings.
				</h2>
				<p>
					June MG Awareness Month started with a bang this year at MG Illuminate, a June 1<sup>st</sup> virtual event for the national MG community. The two-hour online program featured engaging sessions and discussions, several big surprises and a light show in MG teal that stretched from sea to shining sea.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Lights! Landmarks! Teal!
				</h3>
				<p>
					Boston’s Zakim Bridge and the Santa Monica Pier Ferris wheel kicked off a dramatic coast-to-coast lighting of landmarks in teal for MG Awareness Month. Be sure to check out all the landmark lightings on <a href="https://www.facebook.com/MGUnitedOfficial/" target="_blank">Facebook</a> and <a href="https://www.instagram.com/mgunited_official/" target="_blank">Instagram.</a>
				</p>
				<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/430374785" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
				MG&nbsp;United Launched Live to the MG Community
				</h3>
				<p>
					Katrina Gary of argenx provided a website tour, shared plans for future content and welcomed special guest Leah Gaitan-Diaz, the subject of MG&nbsp;United’s first “Real Stories” <a href="<?php echo esc_url( home_url( '/real-stories/leah-gaitan-diaz' ) ); ?>">interview</a>. “All this time I’ve been looking for a resource like MG&nbsp;United,” said Leah, “and I realized that MG&nbsp;United was looking for me, too.”
				</p>
				<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/431451710" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
				  Patient-Doctor Team Makes a Dynamic Duo
				 </h3>
				<p>
					MG support group leader Rachel Higgins and Dr Ericka P. Greene shared the stage for a fascinating Q&A session about the power of patient-doctor communications, pointing to their success in building thriving MG patient support programs throughout Texas as an example. As Dr Greene said, “If you build it, they will come.”
				</p>
				<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/430376025" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					My MG Sole Challenge Drives MG Awareness
				</h3>
				<p>
					Featured <a href="<?php echo esc_url( home_url( '/community-project/my-mg-sole' ) ); ?>">My MG Sole</a> participant Alicia Angel explained the moving MG story behind her soles and joined the My MG Sole Challenge by daring her friend Amy Zehner to participate on social media. “Come on Amy,” said Alicia, “we need to see <em>your</em> MG sole!”
				</p>
				<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/432211582" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
				  Filmmakers Give Inside Look at Upcoming MG Documentary
				</h3>
				<p>
					Director Ben Strang, along with producers Karen Carter and Caryn Capotosto, answered questions about the upcoming <a href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>">MG documentary</a> and debuted its trailer. “What we really hope will come through,” said Ben, “is our immense love, respect and fascination with these stories.”
				</p>
				<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/432218281" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
				  The Biggest MG Awareness Month Ever?
				 </h3>
				<p>
					From landmark lightings to the My MG Sole Challenge on social media, a first-ever MG documentary series and the launch of MG&nbsp;United, there are plenty of reasons to call June 2020 the biggest MG Awareness Month ever. If you’d like to watch MG Illuminate from the opening session to the coast-to-coast finale, please check out the video below.
				</p>
				<div class="article-inline-video"><iframe src="https://player.vimeo.com/video/430380588" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-leah-gaitan-diaz' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-my-mgsole' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
