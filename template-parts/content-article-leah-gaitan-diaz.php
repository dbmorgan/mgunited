<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="leah-profile-article" <?php post_class(); ?>>

	<section id="hero">
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>6 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section id="sub-hero">
		<div>
			<div class="col col-sm-12 col-12">
				<p><b>REAL STORIES</b></p>
				<h1>Leah Gaitan-Diaz and the Empowerment of Positive Thinking</h1>
			</div>
		</div>
	</section>

	<section id="author-blurb" class="row article-body">
		<div>
			<div>
				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/images/leah-profile/leah-profile-bio.jpg" alt="" />
				</div>
				<div>
					<p><b><em>
					This married Los Angeles native has been living with myasthenia gravis for five years, becoming a dedicated advocate for the community along the way. Leah talks with MG&nbsp;United about what doctors don’t seem to grasp about living with MG, the importance of positivity and her former—and perhaps future—life as a beekeeper.
					</em></b></p>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					What’s something about MG you think doctors struggle to understand?
				</h3>
				<p>
					That’s an easy one: the fatigue! For example, my menstrual cycle just wipes me out. Yesterday I was in bed until one o'clock in the afternoon. I could not get out of bed or wake up. I know that fatigue is a normal part of having your period, but with MG it has become three times worse for me. And no doctor has figured that part out. They might look at us and say, “You're a woman, that's normal.” But no, this is not normal. I never experienced that kind of fatigue before I got diagnosed.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Can you describe your MG treatment journey?
				</h3>
				<p>
					It can be challenging for people who are MuSK-positive to find effective treatment. It certainly has been for me. I’ve tried almost everything. And I’ve experienced lots of complications, mistakes in how the treatments were administered and ups and downs in effectiveness. For example, one week the treatment would leave me feeling great, really energized, like, “Hey, let’s go shopping!” But the next week, I wasn’t able to do anything.
				</p>
				<p>
					I’m in a clinical trial now that I heard about at a support group. I always keep an eye out for clinical trials for MuSK-positive patients. Even if I can’t participate, I get the word out to MuSK-positive people in the community who might benefit. I know their struggle all too well.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Lots of women don’t talk about their age, but you do. Why?
				</h3>
				<p>
					Yes, I’m 44. I talk about it because it’s a big part of my MG story. I got diagnosed at 40, right when we were seriously thinking about having a child. But with the ongoing struggle to stabilize my MG, I realized I couldn’t. I’m not stable. I have to focus on taking care of myself.
				</p>
				<p>
					For a lot of women, that goes against their instincts. That’s why I share it. I want to say to women with MG, “It’s okay to focus on your own health.” You have to give yourself permission to do that.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>Get to know your body. Listen to it.</p>
					<cite><strong>Leah Gaitan-Diaz</strong></cite>
				</blockquote>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Any advice for handling daily life with MG?
				</h3>
				<p>
					This is the one thing that I tell everybody in the MG community: Get to know your body. Listen to it. That means if you're tired, take a nap. If you don’t feel up to taking a shower or putting away laundry, that stuff can wait. It's okay.
				</p>
				<p>
					That was a <em>huge</em> change of mindset for me. I was such a perfectionist; everything needed to be done just right. But with MG, that’s stress you don’t need. It's alright if something’s in the wrong place, you know? It's not the end of the world. You can always fix it later. Give yourself that permission.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					What’s the best medical advice you ever got?
				</h3>
				<p>
					A nurse once told me, “We’re giving you all this medicine, but if you’re stressed, it’s like you’re taking water.” That really opened my eyes. I started getting really serious about stress management, making it a priority to the point that I started limiting contact with family members who didn’t understand my MG and weren’t supportive. You have to do whatever it takes to stay happy and healthy. I love my family, but I allowed myself to step away and create a life filled only with people who lift me up, because that stress is literally toxic for me.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					You recently qualified for permanent disability benefits. What was that process like, and is there any advice you’d give people about how to navigate it?
				</h3>
				<p>
					It took five years and ten minutes. Five years to get to the courtroom, and then ten minutes for the judge to say, “Yes, you qualify.” I started crying. After five years, having to stop working, having no income, relying on my husband for everything from financial support to washing my hair—it was hard. I’ve always been very independent. This gave me back a piece of that independence.
				</p>
				<p>
					To folks just getting into that process, I’d advise a couple things. First, know the rules for qualifying. They change, so you have to track them. Also, keep records of your treatments and crises. Ask your doctor for help and tell them what it’s for. Ultimately, if you’re scared and just don’t want the headache, get a lawyer. That’s what I should have done at the start. It’s a legal process, and a lawyer can really help guide you through it.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/leah-profile/leah-profile-photo.jpg" alt="leah cooking" /></p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					So often, treatment goals are stated in cold, clinical terms like “avoid crisis” and “minimize symptoms.” Do you have any personal treatment goals?
				</h3>
				<p>
					As an adult, I decided to learn about beekeeping and the importance of pollination. I used to teach afterschool programs about it for children. Kids are naturals; they don’t have the fear adults bring to it.
				</p>
				<p>
					Through my husband’s contacts, we’ve gotten a lot of calls to teach beekeeping at schools and libraries here in Los Angeles. I wish I could, but I just can't commit. Because I don't know what's going to happen tomorrow. I'm too unpredictable. It’s frustrating, because I have my educational posters, my bee suit, the boxes, all these gadgets for the kids to look at and touch and feel—I’m all set. It breaks my heart because it’s my passion. I love the bees, I love nature, and I love children. And I can’t do it until my disease is more stabilized.
				</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					You’ve had significant challenges with MG, yet you come across as so positive in person. How do you manage that?
				</h3>
				<p>
					It’s a deliberate choice. People living with MG do get sad. But I made a conscious choice to turn that around, to focus on what I have, rather than what I don’t have. I have my own place with my husband. We have food on the table. We’re doing okay. I deal with the stress by being happy with where I'm at now and not worrying about tomorrow. If a bill doesn’t get paid today, it's okay. It’ll get paid tomorrow. If the car breaks down, we can call Uber. Or, do we really have to go out? Most times, no, it’s not really necessary, you know? My husband says, “You're always so positive.” It’s a choice I made for my own health and happiness to just be grateful for the things I have now.
				</p>
			</div>
		</div>
	</section>

	<section id="sharestory" class="row form-module">
		<div>
			<div>
				<div id="form-container">
					<?php get_template_part( 'template-parts/form', 'sharestory' ); ?>
					<?php get_template_part( 'template-parts/form', 'signup' ); ?>
					<?php get_template_part( 'template-parts/form', 'survey' ); ?>
					<?php get_template_part( 'template-parts/form', 'thankyou' ); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-victor-and-iris-yipp' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-kathy-and-diane' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-emotional-health' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
