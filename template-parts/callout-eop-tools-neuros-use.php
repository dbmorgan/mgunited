<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-doctor-tools-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-doctor-tools">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-doctor-tools-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">DISEASE & TREATMENT</p>
			<h2 id="eop-callout-doctor-tools-label" class="secondary">Learn More About Common Tools Neurologists Use to Chart Your Symptoms</h2>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
