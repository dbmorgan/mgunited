<div class="row eoc-callout" tabindex="0" aria-labelledby="home-callout-dr-tools-label">
	<div role="presentation" class="col col-sm-12 col-8 cta-left cta-top" id="home-doctor-tools">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-dr-tools-label" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-4 cta-right cta-bottom cta-copy-dark">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/disease-and-treatment/tools-neuros-use' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-dr-tools-label" class="secondary">Learn More About Common Tools Neurologists Use to Chart Your Symptoms</h2>
			<p>Explore the worksheets that help doctors track your MG.</p>
			<span class="read-duration">5 MIN READ</span>
		</a>
	</div>
</div>
