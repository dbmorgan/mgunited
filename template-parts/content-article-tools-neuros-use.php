<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="the-tools-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>DISEASE & TREATMENT</b></p>
				<h1>Learn More About Common Tools Neurologists Use to Chart Your Symptoms</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>JULY 2020 | <b>5 MIN READ</b></p>
				<?php get_template_part( 'template-parts/social', 'buttons' ); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>Explore the worksheets that help doctors track your MG.</h2>

				<p>Every person with MG is different, but all share the desire to feel better. Tracking your symptoms can help you gain control of your MG journey. MG, of course, doesn’t make that easy. Many symptoms vary from day-to-day or even throughout the same day.<sup>1</sup> Neurologists have a variety of tools to help them understand how you’re doing.</p>

				<p>Some of these tools you may know. Others may be new to you. Becoming familiar with them may allow you and your doctor to more effectively discuss how you’re doing, determine what is or isn’t working with your treatment plan, and work together to make goals for the future.</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					Myasthenia Gravis Activities of Daily Living (MG-ADL)<sup>2</sup>
				</h3>

				<p>The MG-ADL profile provides a quick assessment of the extent of your MG symptoms. Answering these eight questions for your doctor can reveal how MG affects you every day. Researchers even use this tool during clinical trials to determine if MG treatments are working. Here’s one of the questions:</p>

				<div class="symptoms-box">
					<div class="row">
						<div class="header title">Grade</div>
						<div class="value title"><strong>Talking</strong></div>
					</div>
					<div class="row">
						<div class="header">0</div>
						<div class="value">Normal</div>
					</div>
					<div class="row">
						<div class="header">1</div>
						<div class="value">Intermittent slurring or nasal speech</div>
					</div>
					<div class="row">
						<div class="header">2</div>
						<div class="value">Constant slurring or nasal speech, but can be understood</div>
					</div>
					<div class="row">
						<div class="header">3</div>
						<div class="value">Difficult to understand speech </div>
					</div>

				</div>

				<p>Most of the questions are about simple activities, like breathing, brushing your teeth and getting up out of a chair. The last two questions are about your eyes. As you know, eye-related symptoms are common in MG.<sup>1</sup> To get your result, you and your doctor score where you are currently for each question and then add all eight scores together. </p>

				<p>The lower your score, the better! If your score goes down by two points or more, it could be a sign that your treatment is working.<sup>3</sup> If your score starts to increase, you should talk to your doctor to see if your treatment plan should change.<sup>3</sup> </p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<div class="download-slab">
					<div class="download">Download icon</div>

					<p>Download and print the MG-ADL survey to discuss with your doctor.</p>
					<div class="bttn-wrap">
						<a role="button" target="_blank" href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MG_Activities_of_Daily_Living_MG-ADL_Profile.pdf' ) ); ?>" download class="capsule primary no-interstitial">DOWNLOAD NOW</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<blockquote class="article-pullquote">
					<p>MG symptoms are like the stock market. There are highs and lows, but you need to focus on the trend line. The MG-ADL profile helps doctors distinguish a meaningful deviation from a bad day.</p>
					<cite><strong>Niraja S. Suresh, MD</strong></cite>
				</blockquote>

				<h3>Revised 15-Item Myasthenia Gravis Quality of Life Scale (MG-QOL15r)<sup>4</sup></h3>
				<p>Another test, the MG-QOL15r, is a 15-item survey that evaluates how your MG symptoms are affecting your emotional well-being and ability to be independent. </p>

				<p>
					As you know, living with MG can be challenging, so it is important to recognize what effect MG is having on your independence and mental health. Taking this test regularly can help you and your doctor uncover what areas are most important to you. This will help your care team develop an MG management plan targeted at relieving the symptoms that hold you back from things you want to do.
				</p>
			</div>

		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<div class="download-slab">
					<div class="download">Download icon</div>

					<p>Download and print the MG-QOL15r to discuss with your doctor.</p>
					<div class="bttn-wrap">
						<a role="button" target="_blank" download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MG_Quality_of_Life_QOL_Measure.pdf' ) ); ?>" class="capsule primary no-interstitial">DOWNLOAD NOW</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Post-Interventional Status (PIS)<sup>5</sup></h3>
				<p>The PIS system is a tool often used in clinical studies to evaluate how well an MG treatment worked or didn’t work. If you have never taken an MG treatment, the PIS system won’t be relevant to you yet.</p>

				<p>The PIS system is the most clinical (complex and “doctory”) of the tools included in this article. But there are two ways in which it could be helpful to a person with MG. If you are in remission—meaning you are experiencing very few or no symptoms—or close enough to consider it as a goal, the PIS system articulates two different levels of remission. It also articulates three different levels of “minimal manifestations.” In general, that means your daily activities aren’t limited by your MG, but you still experience some muscle weakness. These three levels outlined in the PIS system can help people with MG and their doctors discuss minimal manifestations with much greater precision.</p>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<div class="download-slab">
					<div class="download">Download icon</div>

					<p>Download and print the PIS survey to evaluate how well your treatment is working.</p>
					<div class="bttn-wrap">
						<a role="button" target="_blank"  download href="<?php echo esc_url( home_url( 'wp-content/themes/mgunited/pdfs/MGFA_Post_Intervention_Status_MGFA-PIS.pdf' ) ); ?>" class="capsule primary no-interstitial">DOWNLOAD NOW</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">	
				<h3>Myasthenia Gravis Foundation of America (MGFA) Clinical Classification<sup>5</sup></h3>
				<p>If you have entered any clinical trials for MG treatments, you may have heard of the MGFA Clinical Classification. The MGFA Clinical Classification helps identify patients with similar MG symptoms. This is a useful tool when researchers are trying to set up an experiment correctly.</p>
				<p>While this is a clinical tool, you may still find it helpful to know your MGFA Clinical Classification. </p>
				<p>There are five main MGFA Clinical classes. The MFGA provides a <a target="_blank" download href="https://myasthenia.org/Portals/0/MGFA%20Classification.pdf">printable PDF</a> with full clinical definitions for each class. But they can be summarized like this:</p>

				<div class="classes-box">
					<div class="row">
						<div class="header title">Class</div>
						<div class="description title">Description</div></div>
					<div class="row">
						<div class="header">1</div>
						<div class="description">Eye muscle weakness alone</div></div>
					<div class="row">
						<div class="header">2</div>
						<div class="description">Mild weakness of other muscles &#x00B1; eye muscle weakness</div></div>
					<div class="row">
						<div class="header">3</div>
						<div class="description">Moderate weakness of other muscles &#x00B1; eye muscle weakness</div></div>
					<div class="row">
						<div class="header">4</div>
						<div class="description">Severe weakness of other muscles &#x00B1; eye muscle weakness</div></div>
					<div class="row">
						<div class="header">5</div>
						<div class="description">Cannot breathe without being intubated</div></div>
				</div>

			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Start Tracking So You Can Start Setting Goals</h3>
				<p>
					If you haven’t already, please consider taking a look at the tools included in this overview and decide which ones you’d like to discuss with your doctor. Using one or more regularly may help you keep better track of your condition. And they may allow for a shared language to ease your conversations with your care team through a mutual understanding.
				</p>
				<p>Armed with the knowledge about your MG, you and your doctor can work together more effectively to set goals for the future. For more information on goal setting and to get started, check out <a href="<?php echo esc_url( home_url( '/disease-and-treatment/treatment-goals' ) ); ?>">“The Importance of Setting Your Personal MG Treatment Goals.”</a></p>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>
					<li>Wolfe GI, et al. <cite>Neurology</cite>. 1999;52(7):1487-1489.</li>
					<li>Gilhus NE. <cite>N Engl J Med</cite>. 2016;375(26):2570-2581.</li>
					<li>Barnett C, et al. <cite>Neurol Clin</cite>. 2018;36(2):339-353.</li>
					<li>Burns TM, et al. <cite>Muscle Nerve</cite>. 2016;54:1015-1022.</li>
					<li>Task Force of the Medical Scientific Advisory Board of the Myasthenia Gravis Foundation of America, et al. <cite>Neurology</cite>. 2000;55(1):16-23.</li>
				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'slab' ); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-treatment-goals' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-victor-and-iris-yipp' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
