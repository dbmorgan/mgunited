<div class="row eoc-callout vertical-cta-row" tabindex="0" aria-labelledby="eop-callout-mgrealworld-mg-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="eop-myrealworld-mg">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="eop-callout-mgrealworld-mg-label" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/myrealworld-mg-myasthenia-gravis-study' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/symptom-tracking/myrealworld-mg-myasthenia-gravis-study' ) ); ?>" style="text-decoration:none">
			<p class="eyebrow">SYMPTOM TRACKING</p>
			<h2 id="eop-callout-mgrealworld-mg-label" class="secondary">See If You Can Help Researchers by Sharing Your MG Journey</h2>
			<span class="read-duration">3 MIN READ</span>
		</a>
	</div>
</div>
