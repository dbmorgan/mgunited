<div class="row eoc-callout vertical-cta-row" style="background-color: #003b63!important;" tabindex="0" aria-labelledby="home-callout-kathy-label">
	<div role="presentation" class="col col-sm-12 col-12 cta-top" id="home-kathy-profile">
		<a aria-hidden="true" tabindex="-1" aria-labelledby="home-callout-kathy-label" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kathy-and-diane' ) ); ?>" style="text-decoration:none"></a>
	</div>
	<div class="col col-sm-12 col-12 cta-bottom cta-copy-dark">
		<a tabindex="-1" class="content-tile" href="<?php echo esc_url( home_url( '/real-stories/kathy-and-diane' ) ); ?>" style="text-decoration:none">
			<h2 id="home-callout-kathy-label" class="secondary">Kathy Lemenu and Her Wife Diane on MG, Marriage and Communication</h2>
			<p>They’ve learned to avoid the traps that MG sets for couples with something they call ‘kind confession.’</p>
			<span class="read-duration">6 MIN READ</span>
		</a>
	</div>
</div>
