<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="things-to-do" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-6 col-10 offset-1">
				<h1>
					Take part in MG events!
				</h1>
				<p>
					Here are ways to get involved or see what is happening in the myasthenia gravis community.
				</p>
			</div>
		</div>
	</section>

	<section id="things-to-do-callouts" class="content page-callouts">
		<div class="row page-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-12">
				<?php get_template_part( 'template-parts/callouts/primary/mymgsole-gallery-reverse' ); ?>
			</div>
		</div>
		<div class="row page-callout-row two-up-callout-row horizontal-tablet-callout-row">
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/my-mg-sole' ); ?>
			</div>
			<div class="col col-sm-6 col-6">
				<?php get_template_part( 'template-parts/callouts/secondary-tertiary/mg-illuminate' ); ?>
			</div>
		</div>
	</section>

	<section id="things-to-do-signup-promo" class="violator">
		<div class="row">
			<div class="col col-sm-6 col-6 offset-3">
				<h2>SIGN UP FOR MG&nbsp;UNITED!</h2>
				<p class="things-to-do-signup-promo-copy">Get personalized content and support for wherever you are in your journey with MG. Just enter your email and you’re in.</p>
				<div class="row">
					<div class="col col-sm-6 col-12">
						<div id="things-to-do-signup-button">
							<span class="flex-spacer"></span>
							<div>
								<a role="button" href="<?php echo esc_url( home_url( '/sign-up/' ) ); ?>" class="capsule primary">SIGN UP NOW</a>
							</div>
							<span class="flex-spacer"></span>
						</div>
					</div>
				</div>
	</section>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-documentary' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-myrealworld' ); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part( 'template-parts/callout', 'eop-savory-soul-food-casserole' ); ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/callout', 'social' ); ?>

</article><!-- #post-<?php the_ID(); ?> -->
