<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

?>

<article id="anti-inflammatory-diet-and-myasthenia-gravis-article" <?php post_class(); ?>>

	<section id="hero">
		<div>
			<div class="col col-sm-10 col-10 offset-1">
				<p><b>EATING & MG</b></p>
				<h1>Nutrition Buzz: Let’s Look at the Anti-inflammatory Diet</h1>
			</div>
		</div>
	</section>

	<section id="article-hero-strip">
		<div>
			<div>
				<p>OCTOBER 2020 | <b>4 MIN READ</b></p>
				<?php get_template_part('template-parts/social', 'buttons'); ?>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h2>Get the 411 on what the scientific community thinks about this much-discussed approach to
					eating.</h2>
<br>
				<ul class="tan">
					<li>The anti-inflammatory diet, or any diet for that matter, including any diet involving
						supplements, should always be discussed with your care team.
					</li>
					<li>While diets can be beneficial to general health, they never take the place of treatment for
						medical conditions.
					</li>
					<li>It’s important to remember that you should never stop taking any medication without first
						talking to your doctor.
					</li>
					<li>The Academy of Nutrition and Dietetics does not recommend dropping entire food groups from your
						diet.<sup>1</sup>
					</li>
				</ul>
<br>
				<p>Diet trends come and go. Lately, there’s been curiosity in the myasthenia gravis community about the
					anti-inflammatory diet. MG United set out to explore what it is.</p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>
					What Is the Anti-inflammatory Diet?
				</h3>
				<p>According to the Mayo Clinic, “Scientists are still unraveling how food affects the body's
					inflammatory processes, but they know a few things. Research shows that what you eat can affect the
					levels of C-reactive protein (CRP)—a marker for inflammation—in your blood.”<sup>2</sup></p>

				<p>The anti-inflammatory diet is based on the idea that eating habits can affect inflammation.
					People who follow the diet avoid foods thought to cause inflammation and eat foods rich in
					ingredients thought to suppress it.<sup>1</sup></p>
			</div>
		</div>
	</section>
	<section class="row article-body">
		<div>
			<div class="content">
				<h3>What Anti-inflammatory Dieters <em>Don’t</em> Eat<sup>1</sup></h3>
				<p>People following the anti-inflammatory diet stay away from foods that are high in simple carbs and
					saturated fats. They also avoid foods that are low in fiber and unsaturated fats. Some foods that
					fans of the anti-inflammatory diet are likely to limit include:</p><br>
				<ul>
					<li>Sweets, candies and cakes</li>
					<li>Refined white pastas and bread</li>
					<li>Processed foods</li>
					<li>Fried foods</li>
					<li>Red meat and full-fat dairy foods</li>
					<li>Any foods high in saturated and trans fats</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>What Anti-inflammatory Dieters <em>Do</em> Eat<sup>1</sup></h3>
				<p>In general, foods within the anti-inflammatory diet are those foods you often hear encouraged by
					nutritionists. The diet encourages fruits and vegetables, whole grains and plant-based proteins. The
					anti-inflammatory diet pushes for foods that are rich in polyunsaturated fatty acids, vitamins A and
					D, polyphenols and gingerols.</p>
				<p>Here’s a list of ingredients that anti-inflammatory dieters look for, and some of the foods that are
					rich in
					them: </p><br>
				<ul>
					<li><strong>Polyunsaturated fatty acids (such as omega-3s):</strong> brussels sprouts, cabbage,
						cauliflower, fennel, kale, tuna, mackerel, Baltic herring, walnuts, chia seeds and flax
						seeds<sup>1,3,4</sup></li>
					<li><strong>Vitamin A:</strong> carrots, spinach, tomatoes, peaches, apricots, mangos, nectarines,
						papayas, pumpkins and sweet potatoes<sup>5,6</sup></li>
					<li><strong>Vitamin D:</strong> salmon, trout, mackerel, herring, kipper, mushrooms, as well as
						fortified milks, cereals and juices<sup>7,8</sup></li>
					<li><strong>Polyphenols:</strong> fruits, vegetables, whole grains, extra virgin olive oil and dark
						chocolate<sup>9-11</sup> (See “<a
							href="<?php echo get_template_directory_uri(); ?>/eating-and-mg/5-mg-friendly-smoothies/">5
							MG-Friendly Smoothies</a>” for recipes with the above.)
					</li>
					<li><strong>Gingerols:</strong> turmeric (aka curcumin), ginger, cinnamon, rosemary, thyme and black
						pepper<sup>11</sup></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/images/ai-diet/article-ai-diet-image1.png"
						 alt="">
				</p>
				<figcaption>
					<p><em>Anti-inflammatory dieters look for foods rich in vitamins A and D, polyunsaturated fatty acids, polyphenols and gingerols.</em></p>
				</figcaption>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Portion Size Still Matters<sup>12</sup></h3>
				<p>Sensible portion control is a cornerstone of weight management, and the anti-inflammatory diet is no exception.<sup>13</sup> Discussions around specific portions and types of food should be part of your ongoing dialogue with your care team. One example of an anti-inflammatory food plan that has some general portion-control guidelines is the Mediterranean diet. This diet mimics eating habits historically found in the regions around the Mediterranean Sea and is recognized by the World Health Organization as a healthy diet.<sup>1,12</sup></p>
				<p>Here are the Mediterranean diet’s general guidelines:</p><br>
				<ul>
					<li>Whole grains daily (for example, whole grain bread, pasta or brown rice)</li>
					<li>Four to six servings of fruits daily</li>
					<li>Two to three servings of vegetables daily</li>
					<li>Olive oil (as the main fat)</li>
					<li>One or two nonfat or low-fat dairy products daily</li>
					<li>Four to six servings of fish, poultry or nuts per week</li>
					<li>Four to five servings of red meat per month</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="row article-body">
		<div>
			<div class="content">
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/images/ai-diet/article-ai-diet-image-2.png"
						 alt="">
				</p>
				<figcaption>
					<p><em>The Mediterranean diet is an example of an anti-inflammatory food plan. </em></p>
				</figcaption>
			</div>
		</div>
	</section>


	<section class="row article-body">
		<div>
			<div class="content">
				<h3>Trying to Eat Healthier Is Always a Good Choice</h3>
				<p>Right now, the science is unclear about the potential benefits of the anti-inflammatory diet. But generally, experts agree that this style of eating is healthy.<sup>1</sup> For people with myasthenia gravis, finding the right diet can be possible with some help from your care provider.</p>
			</div>
		</div>
	</section>

	<section id="references">
		<div>
			<div>
				<p><a href="#references"><span></span>References</a></p>
				<ol>

					<li>American Dietetic Association. <cite>J Am Diet Assoc</cite>. 2010;110(11):1780.</li>
					<li>Mayo Clinic. How to use food to help fight inflammation. Mayo Clinic website.
						https://www.mayoclinic.org/healthy-lifestyle/nutrition-and-healthy-eating/in-depth/how-to-use-food-to-help-your-body-fight-inflammation/art-20457586.
						Accessed July 15, 2020.
					</li>
					<li>Calder PC. <cite>Nutrients</cite>. 2010;2(3):355-374.</li>
					<li>Cholewski M, et al. <cite>Nutrients</cite>. 2018;10(11):1662.</li>
					<li>Reifen R. <cite>Proc Nutr Soc</cite>. 2002;61(3):397-400.</li>
					<li>Faber M, et al. <cite>J Sci Food Agric</cite>. 2007;87:366-377.</li>
					<li>Guillot X, et al. <cite>Joint Bone Spine</cite>. 2010;77(6):552-557.</li>
					<li>Moulas AN, et al. <cite>J Biotechnol</cite>. 2018;285:91-101.</li>
					<li>Yahfoufi N, et al. <cite>Nutrients</cite>. 2018;10(11):1618.</li>
					<li>Vemuri M, et al. Health Effects of Foods Rich in Polyphenols. In: De Meester F, et al. <cite>Wild-Type
							Food in Health Promotion and Disease Prevention</cite>. Humana Press; 2008.
						https://link-springer-com.turing.library.northwestern.edu/chapter/10.1007/978-1-59745-330-1_27.
						Accessed July 15, 2020.
					</li>
					<li>Rondanelli M, et al. <cite>Nutr Res Rev</cite>. 2018;31(1):131-151.</li>
					<li>Rolls BJ, et al. <cite>Int J Obes (Lond)</cite>. 2014;38(Suppl 1):S1-S8.</li>
					<li>Chrysohoou C, et al. <cite>J Am Coll Cardiol</cite>. 2004;44(1):152-158.</li>

				</ol>
			</div>
		</div>
	</section>

	<?php get_template_part('template-parts/callout', 'slab'); ?>

	<section id="eoc" class="container">
		<div class="row">
			<div class="col col-3">
				<?php get_template_part('template-parts/callout', 'eop-5-mg-friendly-smoothies'); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part('template-parts/callout', 'eop-been-living-with-mg-for-some-time'); ?>
			</div>
			<div class="col col-3">
				<?php get_template_part('template-parts/callout', 'eop-tools-neuros-use'); ?>
			</div>
		</div>
	</section>

	<?php get_template_part('template-parts/callout', 'social'); ?>

</article><!-- #post-<?php the_ID(); ?> -->
