<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mgunited
 */

get_header();
global $jobNumber;
$jobNumber = 'US-NON-20-00171 V1 10/2020'
?>

	<main aria-label="Real Stories Page Content" id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'real-stories' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();
