<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mgunited
 */

get_header();
global $jobNumber;
$jobNumber = 'US-NON-20-00113 V2 10/2020';
?>

	<main aria-label="Documentary Teresa Biography Page Content" id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'documentary-teresa' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();
