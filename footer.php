<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mgunited
 */
global $jobNumber;
if (empty($jobNumber)) {
  $jobNumber = 'agx20-0130 05/2020';
}
?>
	<footer aria-label="MG United Site Footer">
		<div aria-label="MG United Site Footer Logos and Links">
<div class="row">

	<div class="left-image hidden-desktop show-under-1075">
		<img class="footer-image" src="<?php echo get_template_directory_uri(); ?>/images/mgunited-logo-combined.png" alt="MG United" />
	</div>

<div class="hidden-mobile">
	<ul aria-label="MG United Site Footer Page Links" class="primary-row">

		<li><a href="<?php echo esc_url( home_url( '/about-mg' ) ); ?>">About MG</a></li>
		<li><a href="<?php echo esc_url( home_url( '/life-with-mg' ) ); ?>">Life with MG</a></li>
		<li><a href="<?php echo esc_url( home_url( '/real-stories' ) ); ?>">Real Stories</a></li>
		<li><a href="<?php echo esc_url( home_url( '/things-to-do' ) ); ?>">Things To Do</a></li>
		<li><a href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>">Documentary</a></li>
		<li><a href="<?php echo esc_url( home_url( '/mission' ) ); ?>">Mission</a></li>
		<li><a href="<?php echo esc_url( home_url( '/sign-up' ) ); ?>">Sign Up Now</a></li>
	</ul>
</div>
</div>

			<div class="row">
			<div class="left-image hide-under-1075"><img class="footer-image" src="<?php echo get_template_directory_uri(); ?>/images/mgunited-logo-combined.png" alt="MG United" /></div>

			<ul aria-label="MG United Site Footer Page Links" class="primary">

				<li><a href="<?php echo esc_url( home_url( '/about-mg' ) ); ?>">About MG</a></li>
				<li><a href="<?php echo esc_url( home_url( '/life-with-mg' ) ); ?>">Life with MG</a></li>
				<li><a href="<?php echo esc_url( home_url( '/real-stories' ) ); ?>">Real Stories</a></li>
				<li><a href="<?php echo esc_url( home_url( '/things-to-do' ) ); ?>">Things To Do</a></li>
				<li><a href="<?php echo esc_url( home_url( '/a-mystery-to-me' ) ); ?>">Documentary</a></li>
				<li><a href="<?php echo esc_url( home_url( '/mission' ) ); ?>">Mission</a></li>
				<li><a href="<?php echo esc_url( home_url( '/sign-up' ) ); ?>">Sign Up Now</a></li>
			</ul>

			<ul aria-label="argenx Privacy Policy and Contact Us" class="secondary">
<!--				<li>Intended for U.S. audiences only.</li>-->
				<li><a href="https://www.argenx.com/privacy-policy" target="_blank">Privacy Policy</a></li>
				 <li><a href="https://www.argenx.com/contact-us" target="_blank">Contact Us</a></li>
				<li>
					&#x00a9;2020 argenx <?php echo $jobNumber; ?>
				</li>
			</ul>

			<div class="right-image"><img class="footer-image right-image" src="<?php echo get_template_directory_uri(); ?>/images/argenx-logo-combined.png" alt="argenx" /></div>
			</div>
		</div>
	</footer>

	<!-- violator -->
	<?php
		$check = is_404();
		global $wp;
		if ($check == 1) $slug = '';
		else $slug = add_query_arg( array(), $wp->request );
		//else $slug = $wp_query->queried_object->post_name;
		echo '<script type="text/javascript">console.log("'.$slug.'")</script>'
	?>
	<?php if (is_front_page() || $slug == "a-mystery-to-me") { ?> <!--  || $check !== 1 -->
		<?php get_template_part( 'template-parts/violator', 'article' ); ?>
	<?php } ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<?php
// Switch between the raw ES6 javascript & the compiled javascript based on
// the environment we're running in
if (mgunited_use_raw_javascript()) { ?>

<!--
	We're running locally (triggered by HTTP_HOST "mgunited.test"), which means:
		- The raw ES6 javascript is referenced instead of the bundled main.min.js file
		- The QA API is used instead of the production API
		- A random number is tacked onto the main.js reference to try and avoid caching
-->
<script type="module">
	import { MGUnited } from "<?php echo get_template_directory_uri() . '/js-es6/main.js?r=' . random_int(0, 100000); ?>";

	new MGUnited({"version":<?php echo json_encode(_S_VERSION); ?>, "buildDate":<?php echo json_encode(date('c')) ?>, enableGridOverlay: true});
</script>

<?php } else { ?>

<script type="text/javascript">
	new MGUnited({"version":<?php echo json_encode(_S_VERSION); ?>, "buildDate":<?php echo json_encode(date('c')) ?>});
</script>

<?php } ?>

</body>
</html>
