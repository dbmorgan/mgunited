<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mgunited
 */

get_header();
get_template_part( 'template-parts/header', 'article' );

global $jobNumber;
$jobNumber = 'US-NON-20-00115 V1 10/2020';
?>

	<main aria-label="My MG Sole Myasthenia Gravis Art Project Article Page Content" id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'article-my-mg-sole-mg-art-project' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_template_part( 'template-parts/footer', 'article' );
get_footer();
